|   |   |
|---|---|
|Fonte |[Direção-Geral da Educação](https://www.dge.mec.pt/relatoriosestatisticas-0)|
| Metodologia| Os dados foram recolhidos manualmente, consultando os relatórios anuais publicados no site da [Direção-Geral da Educação](https://www.dge.mec.pt/relatoriosestatisticas-0).
| Artigo(s)|[​Exames nacionais. Notas sobem a Português e Matemática, descem a Filosofia e Físico-Química](https://rr.sapo.pt/2019/07/12/pais/exames-nacionais-notas-sobem-a-portugues-e-matematica-descem-a-filosofia-e-fisico-quimica/noticia/157740/)|