|   |   |
|---|---|
|Fonte |Ministério da Defesa|
| Metodologia| Os dados foram cedidos à Renascença pelo Ministério da Defesa. Em [dados-originais](dados-originais) pode encontrar os dados cedidos pelo ministério da Defesa e, em [dados-limpos](dados-limpos) pode encontrar os dados limpos.
| Artigo(s)|[Quem quer ser militar? "O baixo salário não compensa”](https://rr.sapo.pt/2019/08/01/pais/quem-quer-ser-militar-o-baixo-salario-nao-compensa/noticia/159774/)|