|   |   |
|---|---|
|Fonte | [election-results.eu](https://www.election-results.eu/)|
| Metodologia| Os dados foram retirados do site [election-results.eu](https://www.election-results.eu/) e transformados em .csv.
| Análise | Usado o Google Sheets, calculou-se a [taxa de crescimento](taxa_crescimento_participacao_eleitoral.csv) entre as eleições europeias de 2014 e as de 2019.
| Artigo(s)| [Abalos sísmicos pela Europa, maré socialista em Portugal. As europeias 2019 à lupa](https://rr.sapo.pt/noticia/152811/abalos-sismicos-pela-europa-mare-socialista-em-portugal-as-europeias-2019-a-lup)|