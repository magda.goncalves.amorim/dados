
library(needs)
needs(ggmap,rio)

data <- import("input/tabula-REPA-Geral.csv")

data$morada <- paste0(data$Endereço," ",data$Concelho," ",data$Distrito)

data$coordinates <- geocode(data$morada , output = "latlon" , source = "google")

data$lon <- data$coordinates$lon
data$lat <- data$coordinates$lat

data$coordinates <- NULL

export(data,"output/postos_gerais.csv")


