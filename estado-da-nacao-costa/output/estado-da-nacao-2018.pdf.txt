Senhor Presidente,

Senhoras e senhores deputados,

1. Ao abrir o terceiro debate do Estado da Nação desta legislatura quero reafirmar os quatro compromissos que o Governo assumiu perante os portugueses e este Parlamento no seu Programa de Governo.

Primeiro: virar a página da austeridade devolvendo a confiança aos cidadãos e repondo a normalidade constitucional.

Segundo: relançar a economia através de um programa assente na recuperação de rendimentos das famílias e na criação de condições para o investimento das empresas.

Terceiro: recuperar os níveis de proteção social, promovendo a redução da pobreza e a diminuição das desigualdades.

Quarto: equilibrar de modo sustentado as finanças públicas, reduzindo o défice e o peso da dívida pública.

2. Há três anos muitos questionavam se conseguiríamos conciliar o fim da austeridade com a participação na Zona Euro. Três anos volvidos podemos dizer: sim, conseguimos.

Outros duvidavam se conseguiríamos melhorar a competitividade, rompendo com o    modelo de desenvolvimento assente no empobrecimento e na destruição de direitos. Três anos volvidos, confirma-se que sim, conseguimos.

E três anos volvidos podemos mesmo acrescentar: conseguimos porque mudámos de políticas.

A devolução de rendimentos levou a um aumento da confiança, que deu um impulso decisivo à recuperação da economia. O crescimento de 2,7% do PIB registado em 2017 foi o maior deste século, o investimento cresceu 9,1%, naquela que é a maior variação homóloga dos últimos 19 anos, e as exportações de bens e serviços cresceram 11,2% em 2017.

Também no emprego, os resultados falam por si. Foram criados mais de 300 mil empregos e a taxa de desemprego recuou para o nível mais baixo desde 2002 (7,2%), registando a queda mais acentuada da Zona Euro em 2017. Há hoje menos 250 mil desempregados, dos quais 190 mil de longa duração.

Não se trata só de mais emprego, mas também de melhor emprego. 85% dos novos contratos são contratos sem termo; mais de 40 mil pessoas que estavam empregadas a tempo parcial encontraram um emprego a tempo completo; e o dinamismo do mercado de trabalho permitiu recuperar 45 mil desencorajados para a população ativa.

Quebrado o ciclo de empobrecimento e de retrocesso social em que o país tinha mergulhado, as desigualdades diminuíram, com o coeficiente de gini a atingir o nível mais baixo de sempre, e a pobreza recuou para os níveis pré-crise, com 80 mil pessoas a libertarem-se do risco de pobreza.

Em resumo, temos mais crescimento, melhor emprego, maior igualdade. 

Este percurso foi feito a par com uma execução orçamental que reforça a sustentabilidade das contas públicas e a credibilidade do país. O défice mais baixo da democracia foi reduzido para 0,9% do PIB, levando a Comissão Europeia a retirar Portugal da lista dos países com desequilíbrios macroeconómicos excessivos, ao mesmo tempo que a dívida pública registou a maior descida desde 1991, recuando, em 2017, 4 pontos percentuais.

Na base destes resultados no défice e na dívida pública não está qualquer corte, nem a falta a qualquer compromisso assumido com os portugueses ou nesta Assembleia. Está, por um lado, o crescimento do emprego, no efeito que tem na redução da despesa em subsídios de desemprego e no aumento das receitas, em especial as contribuições para a segurança social; por outro lado, temos a descida dos juros da dívida, resultado da confiança que uma gestão rigorosa assegura; e, por fim, o crescimento da economia, que garante a sustentabilidade desta consolidação.

Os resultados que alcançámos não resultam nem do acaso, nem da conjuntura. Eles só são possíveis porque mudámos as políticas.

Foi porque revertemos os cortes nos salários, aumentámos as pensões e o Salário Mínimo Nacional que o rendimento real das famílias cresceu 4,7%.

Foi porque repusemos os valores dos mínimos sociais, atualizámos o Indexante de Apoios Sociais, aumentámos as pensões mais baixas e reforçámos o abono de família que a pobreza baixou para níveis pré- crise.

Foi porque desbloqueámos e acelerámos a execução do PT2020 que somos hoje o primeiro país com maior execução na Europa, entre os programas com dimensão relevante.

Esta mudança só foi possível porque se formou nesta Assembleia uma maioria que viabilizou o Programa deste Governo e tem viabilizado a sua execução que, pela  parte do Governo, prosseguiremos com determinação e confiança, no horizonte da legislatura.

Senhor Presidente
Senhoras e Senhores Deputados

3. Este é o retrato dos resultados já alcançados. Mas este não é o limite da nossa ambição, do nosso dever e do nosso compromisso com os portugueses. Sabemos bem que é preciso continuar a fazer mais e melhor.

Há ainda um caminho a percorrer e queremos prosseguir este caminho sem recuos ou impasses. A estabilidade das políticas é crucial para a manutenção da confiança. A confiança é determinante para a continuidade do investimento. O investimento é   essencial ao crescimento e à criação de emprego. E só com crescimento e emprego é possível aumentar o rendimento e consolidar de forma sustentada as finanças públicas.

Não podemos pôr em causa tudo aquilo que construímos. O bem-estar dos portugueses e o progresso do país têm de ser as nossas prioridades.

Para não voltar a ter famílias em constante sobressalto sobre o dia de amanhã, temos de ter a coragem de dizer claramente o que é possível e o que não é. Para não voltar a ter jovens obrigados a emigrar, temos de rejeitar o modelo dos salários baixos e dos empregos precários. Para não voltar a ter uma sociedade fragmentada e um território dividido, não podemos voltar a aceitar um Estado minimalista e distante.

Senhor Presidente
Senhoras e Senhores deputados

4. Há três críticas fundamentais que procuram desvalorizar os resultados alcançados. Por um lado, teríamos renunciado ao investimento público esgotando os recursos em despesa corrente. Por outro lado, a persistência de dificuldades nos serviços públicos resultariam de uma austeridade encapotada. Por fim, teríamos adiado sine die as reformas necessárias para ultrapassar os bloqueios do país.

A realidade desmente estas três críticas.

Em primeiro lugar, o investimento público cresceu 22% em 2017, valor que ainda este ano terá um crescimento mais significativo, agora com particular destaque para o investimento da Administração Central.

Ou seja, recuperámos a importância do investimento público no processo de desenvolvimento do país, com base na prioridade ao investimento de proximidade, ao investimento que aumenta o potencial produtivo e a internacionalização da economia, ao investimento que contribui para a coesão territorial e a descarbonização.

Quanto ao investimento público de proximidade, destaca-se a reabertura de 20 tribunais, a criação de 17 novas lojas e de 246 espaços do cidadão, a construção de 113 centros de saúde ou a reabilitação de mais de 200 escolas, algumas que se tinham tornado verdadeiros símbolos do desinvestimento como a Escola Secundária Alexandre Herculano, no Porto, ou o Conservatório Nacional, em Lisboa.

O Programa Nacional de Regadios é um excelente exemplo de um investimento fundamental para o aumento do potencial produtivo da nossa agricultura, que visa criar e modernizar mais 90 mil hectares de área, metade na extensão do Alqueva e outra metade replicando os seus efeitos nas regiões Centro e Norte.

Na Ferrovia estão em execução os investimentos de ligação a Espanha, nas linhas do Minho, da Beira Alta e da Beira Baixa. E foi já lançado o maior investimento ferroviário dos últimos 100 anos, a nova linha de ligação do porto de Sines à fronteira do Caia.

No que respeita à Rodovia, ignorada no PT2020, concentrámo-nos nas ligações às  áreas de localização empresarial e nas obras de requalificação e duplicação do IP3, via essencial para a revitalização do interior e para o combate à sinistralidade rodoviária.

A descarbonização da economia exige forte investimento na mobilidade urbana. Demos prioridade à aquisição de novo material circulante para as frotas dos STCP e da Carris, e avançámos com os investimentos de expansão dos Metros de Lisboa e Porto. Lançámos decididamente o investimento no sempre adiado Sistema de Mobilidade do Mondego, agora aprovado por proposta deste Governo na reprogramação do Portugal 2020.

Em conclusão, não cortámos ou adiámos investimento público, aumentámo-lo e acelerámos a sua execução.

Em segundo lugar, os serviços públicos. Voltámos a investir no Serviço Nacional de Saúde, com um reforço anual de 700 M€, contratando mais 7.900 profissionais, alargando a rede de Unidades de Saúde Familiar e de Cuidados Continuados Integrados e reduzindo as taxas moderadoras. É por isso que a despesa das famílias com saúde baixou, que temos hoje mais 302 mil consultas hospitalares e mais 19 mil cirurgias, e que reduzimos de 15% para 7% os portugueses sem médico de família.

Apostámos igualmente na valorização da escola pública, tendo hoje mais 7.000 professores nos quadros, mais 2.500 assistentes operacionais, menos alunos por turma, manuais gratuitos no 1.º ciclo e uma ação social reforçada, e novos programas de promoção do sucesso escolar e de flexibilização pedagógica. É por isso que o sucesso escolar aumentou 2 p.p. no conjunto do ensino básico e secundário enquanto o abandono precoce baixou para os 12,6%, aproximando-se da meta dos 10% em 2020.

Em suma, não prosseguimos a austeridade, investimos para recuperar os danos da austeridade nos serviços públicos.

Em terceiro lugar, rejeitamos também a acusação de que não estamos a fazer as reformas que permitem que o país enfrente os seus bloqueios estruturais e que nos preparem para os desafios do futuro.

A descentralização de competências para as autarquias, que estamos em condições de aprovar em breve nesta Assembleia, é a pedra angular da reforma do Estado, porque acreditamos que dando mais meios e competências às freguesias, municípios e áreas   metropolitanas podemos ter um Estado mais eficiente, mais próximo das pessoas e mais eficaz na resolução dos seus problemas.

Ao longo deste ano, a habitação foi finalmente assumida como uma prioridade com o lançamento de uma nova geração de políticas, que visa assegurar habitação acessível e digna a todos os portugueses, a promoção da reabilitação, a estabilidade do arrendamento e a resolução dos problemas de carência habitacional.

Também na área das florestas, estamos a concretizar uma reforma há muito adiada. Simplificámos as condições de constituição das ZIF e criámos as entidades de gestão florestal de forma a que as áreas florestais possam ser exploradas numa escala economicamente viável, e lançámos um programa de aproveitamento da biomassa.

Por outro lado, há que levar até ao fim a execução do cadastro rural e florestal, cujo projeto piloto já arrancou e tem mais de 41 mil prédios georreferenciados; amanhã, em Conselho de Ministros extraordinário, aprovaremos os Planos Regionais de Ordenamento Florestal.

Criámos o Programa Capitalizar para combater o excessivo endividamento e dependência de financiamento bancário das empresas portuguesas, um dos principais bloqueios estruturais do país. Este foi essencial para que as empresas voltassem a investir e a contratar, ajudando o crescimento do investimento empresarial e do emprego.

Mas a reforma mais profunda é a que decorre de termos assumido a inovação como motor do desenvolvimento, numa política transversal e articulada desde a universalização do pré-escolar a partir dos 3 anos aos laboratórios colaborativos, investindo na educação, formação, investigação e desenvolvimento, transferência do conhecimento para o tecido empresarial, o apoio ao empreendedorismo e à transição para a
Indústria 4.0.

Ao contrário do que muitas vezes é repetido, não temos governado ao sabor do dia-a-dia nem sem visão estratégica. Pelo contrário: temos, de forma aberta e participada pela sociedade, procurado dotar o país dos necessários instrumentos estratégicos de médio prazo.

É o que temos feito anualmente com o Programa Nacional de Reformas, com o debate para uma década de convergência com o Portugal 2030, ou com o PNPOT que aprovaremos amanhã em Conselho de Ministros. É o que faremos no debate público do Programa Nacional de Investimentos para a próxima década, que queremos discutir nesta Assembleia para que seja aprovado por uma ampla maioria, que desejamos não inferior a 2/3, que garanta a estabilidade ao processo de decisão, sem avanços e recuos penalizadores, como infelizmente aconteceu – como hoje é claro para todos - com o Aeroporto Internacional de Lisboa, com elevados custos que a economia nacional
suporta.

Senhor Presidente
Senhoras e Senhores Deputados

Quando nos reencontrarmos nesta Assembleia, em Setembro, entramos na última sessão legislativa e estaremos a preparar o último Orçamento do Estado desta legislatura.

E quero ser claro: não será pelo facto de estarmos a pouco mais de um ano das eleições que vamos sacrificar o que já conquistámos ou mudar de rumo.

Garantir a sustentabilidade do trajeto seguido até agora, consolidando os bons resultados alcançados e prosseguindo o esforço de recuperação de rendimentos e a prioridade à melhoria dos serviços públicos, a promoção do investimento, a redução das desigualdades e o reforço da coesão territorial, ao mesmo passo que prosseguimos a redução do défice e da dívida - esse é o sentido do Orçamento do Estado que apresentaremos dentro de poucos meses.

O próximo orçamento do estado é, em primeiro lugar, um orçamento de continuidade.

Continuidade na recuperação de rendimentos. No próximo ano, prosseguiremos o descongelamento de carreiras e, pelo segundo ano consecutivo, mais de 95% das pensões serão aumentadas, 68% acima da inflação.

Continuidade na melhoria da proteção social. No próximo ano, será completado o aumento progressivo do abono de família nos primeiros três anos de vida e a reposição do 4.º escalão e entrará em pleno funcionamento a terceira componente da Prestação Única de Deficiência, relativa à compensação por encargos específicos decorrentes da situação de deficiência.

Continuidade na melhoria dos serviços públicos. Educação e Saúde, naturalmente. Mas também nos transportes públicos, em que, com o investimento do próximo ano, iremos quadruplicar nesta legislatura o investimento realizado na legislatura anterior. Para além da Ferrovia e do reforço dos recursos humanos, da segurança, do material circulante e do alargamento das redes de metro, investiremos 50 M€ na aquisição de 10 novos navios para a Transtejo e Soflusa.

Continuidade na coesão territorial, dando tradução financeira ao Programa Nacional de Coesão Territorial que aprovaremos amanhã.

De continuidade nas políticas de recuperação económica e de captação de investimento e de inovação. Por isso, para manter o ritmo de apoio ao investimento mais inovador, aprovámos na reprogramação do PT2020 uma mudança no sistema de incentivos que permitirá aumentar os apoios às empresas em mais 5 mil M€. De continuidade no
equilíbrio das contas públicas.

Mas este é um orçamento em que podemos, graças ao percurso que fizemos, concentrar-nos no futuro.

E concentrar-nos no futuro é, desde logo, assumirmos como grande desígnio nacional que as gerações mais jovens possam ter, em Portugal, todas as oportunidades de se realizarem plenamente do ponto de vista pessoal e profissional. Garantir que esta geração tem futuro e constrói o futuro de Portugal.

O próximo orçamento incluirá um programa de forte estímulo fiscal e de apoio à mobilidade familiar no acesso à habitação e à educação para promover o regresso de emigrantes, especialmente dos jovens que se viram forçados a deixar o país nos anos de forte crise económica.

O Orçamento do Estado para o próximo ano deve também responder à centralidade da cultura e da ciência como as bases da sociedade do conhecimento. No caso da Ciência, o orçamento terá medidas que permitam alcançar o investimento de 1,5% do PIB em I&D em 2019, reforçando a convergência com o objetivo de 3% do PIB em 2030; no caso da Cultura, garantir que em 2019 teremos o maior orçamento de sempre para a cultura, reforçando o apoio à criação e à recuperação do património.

A Administração Pública tem também um papel essencial na sociedade da inovação. Assim, honrando os compromissos do Governo de valorizar, rejuvenescer e capacitar a administração, procederemos, no próximo ano, ao concurso de recrutamento para os quadros do Estado de 1.000 jovens com formação superior, de modo a reforçar os centros de competências e as áreas estratégicas de conceção e de planeamento de políticas públicas e de digitalização da administração.

Senhor Presidente
Senhoras e Senhores Deputados

O caminho que escolhemos é o que nos permitirá cumprir a ambição de mais crescimento, melhor emprego e maior igualdade. E é também um caminho que prepara o país para o futuro através de políticas que melhorem a vida dos portugueses, que promovam a igualdade de oportunidades, a coesão do território e a competitividade da nossa economia.

Muito foi conseguido nestes dois anos e meio, mas há muito ainda por fazer. É focados no muito que temos pela frente que continuaremos motivados e determinados a percorrer o caminho que iniciámos, sem recuos, nem ficar a marcar passo, progredindo, passo a passo, com confiança.