---
title: "O que disse António Costa em todos os debates do Estado da Nação?"
author: "Rui Barros (rui.barros@r r.pt)"
date: "`r format(Sys.Date())`"
output:
   html_document:
     keep_md: TRUE
     code_folding: show
     echo: TRUE
     warning: FALSE
     message: FALSE
     theme: lumen
     df_print: kable
     toc: yes
     toc_depth: 4
     toc_float: 
       collapsed: false
       smooth_scroll: false
---


# Introdução

Neste documento vamos proceder a uma análise de discurso das palavras do primeiro-ministro António Costa nos quatro debates do Estado da Nação.

## Questões orientadoras:

- Qual foi o ano com o discurso mais palavroso?
- Quais foram as palavras mais usadas em cada um dos anos?
- Quais foram as palavras que tornaram o discurso deste ano único?
- Qual foi o discurso mais positivo?
- Qual foi o discurso mais negativo?
- Que sentimento prevalece em cada um dos anos?
- Qual foi o sentimento que se destacou este ano em relação aos outros anos?
- Qual foi a combinação de palavras mais frequente? (bigrams)

## Dependências


```{r}
library("needs")
needs(pdftools,dplyr,purrr,readr,tidyr,tidytext,stringr,rio,knitr,stopwords,ggplot2,wordcloud,tibble,quanteda,tidylo,forcats,ggthemes)
```

# Importação de dados

Os ficheiros com os discursos foram todos disponibilizados pelo governo no seu site institucional descarregados para a pasta input.

## Extração de dados do pdf

Comos os ficheiros estavam em pdf, foi necessário proceder ao seu processamento e transformação para txt para poderem ser analisados.

```{r}
# x <- list.files(path = "input")
# 
# for (i in 1:9) {
#    text <- pdf_text(paste0("input/", x[i])) %>% 
#          strsplit("\n") %>% 
#          lapply(write, paste0("output/", x[i],".txt"), append=TRUE)
# }

```

Depois do processamento dos .pdfs, foi necessário proceder à necessária limpeza dos ficheiros, que incluiram também outros elementos que não importavam para a análise.

Esta limpeza foi feita manualmente. Optou-se por preservar o máximo possível a integridade dos textos, tendo-se apenas eliminado espaços desnecessariamente acrescentados na conversão, bem como o texto referente ao número de página, cabeçalhos e rodapés.

No caso do discurso de 2019, optamos por corrigir a formulação "4 anos" para "quatro anos" porque o primeiro-ministro tinha essa formulação escrita das duas formas ao longo do texto.

Os discursos limpos em .txt estão em [output/discursos-final](output/discursos-final).

## Unnest de todos os discursos


```{r}
tbl <- list.files(path = "input/discursos-finais",pattern = "*.txt",
               full.names = T) 

df <- tbl %>%
       map_chr(~ read_file(.)) %>%
       data_frame(text = .,ano = tbl)

#remove path do ano
df$ano<- gsub("input/discursos-finais/estado-da-nacao-","",df$ano) 
df$ano <- gsub(".txt","",df$ano) 


```

Procedeu-se à consequente tokenização destas palavras:

```{r}
data <- df %>%
         unnest_tokens(word, text)
nrow(data)
```

Foram assim tidas, como ponto de partida, as 8.741 palavras que compõe os quatro discursos inaugurais do debate do Estado da Nação pelo primeiro-ministro.

No entanto, nem todas as palavras interessam na análise textual uma vez que há palavras como os determinantes e afins que, apesar de muito comuns no uso corrente da linguagem, não acrescentam muito - as chamadas Stop Words.

Para esta análise, consideraram-se as Stop Words para Língua Portuguesa elencadas pelo package [Stopwords](http://stopwords.quanteda.io/reference/stopwords.html)

```{r}
stop_words <- as.data.frame(stopwords("pt", source = "stopwords-iso"))
stop_words <- rename(stop_words, 'word' = 'stopwords("pt", source = "stopwords-iso")')
target <- c("um","dois","três","quatro","cinco","seis","sete","oito","nove","dez","ano","anos")
stop_words <- stop_words %>%
  filter(!word %in% target)
export(stop_words, "output/stop_words_pt.csv","csv")
```

As palavras eliminadas estão disponíveis [aqui](output/stop_words_pt.csv).

```{r}
data2 <- data %>% 
        anti_join(stop_words)
nrow(data2)

```



# Análise 

## Qual foi o ano com o discurso mais palavroso?

```{r}
count_por_ano <- data %>% 
   group_by(ano) %>% 
   tally() %>% 
   arrange(-n)
count_por_ano

```

O discurso de 2019 foi o segundo mais palavroso, sendo superado pelo prferido em 2018, quando António Costa proferiu 2.733 palavras no discurso de abertura do discurso do Estado da Nação.

## Qual foi a palavra mais usada por Costa nestes quatro anos?

```{r}
top_palavras <- data2 %>%
                  count(word, sort = TRUE) %>% 
                  mutate(word = reorder(word, n))

export(top_palavras,"output/top_palavras_mais_usadas_sempre.csv","csv")

top_palavras %>% filter(n > 10) %>% 
                  ggplot(aes(word, n)) +
                  geom_col() +
                  xlab(NULL) +
                  coord_flip()
```


Em nuvem de palavras...
```{r}
pal <- brewer.pal(8,"Dark2")

top_palavras %>% 
  with(wordcloud(word, n, random.order = FALSE, max.words = 13, colors=pal))
```

Nos quatro discursos, a palavra mais repetida por Costa foi assim "investimento" (49), "país"(39), "emprego"(29), "confiança"(28), "portugueses"(27) e "crescimento"(27).

## Quais foram as palavras mais usadas em cada um dos anos?

## 2016

```{r}
top_palavras_2016 <- data2 %>%
                  filter(ano == 2016) %>% 
                  count(word, sort = TRUE) %>% 
                  mutate(word = reorder(word, n))
                  
export(top_palavras_2016,"output/top_palavras_mais_usadas_2016.csv","csv")

top_palavras_2016 %>% filter(n > 5) %>% 
                  ggplot(aes(word, n)) +
                  geom_col() +
                  xlab(NULL) +
                  coord_flip()      

```



## 2017

```{r}
top_palavras_2017 <- data2 %>%
                  filter(ano == 2017) %>% 
                  count(word, sort = TRUE) %>% 
                  mutate(word = reorder(word, n))
                  
export(top_palavras_2017,"output/top_palavras_mais_usadas_2017.csv","csv")

top_palavras_2017 %>% filter(n > 5) %>% 
                  ggplot(aes(word, n)) +
                  geom_col() +
                  xlab(NULL) +
                  coord_flip()      

```


## 2018

```{r}
top_palavras_2018 <- data2 %>%
                  filter(ano == 2018) %>% 
                  count(word, sort = TRUE) %>% 
                  mutate(word = reorder(word, n))
                  
export(top_palavras_2018,"output/top_palavras_mais_usadas_2018.csv","csv")

top_palavras_2018 %>% filter(n > 5) %>% 
                  ggplot(aes(word, n)) +
                  geom_col() +
                  xlab(NULL) +
                  coord_flip()      

```


## 2019

```{r}
top_palavras_2019 <- data2 %>%
                  filter(ano == 2019) %>% 
                  count(word, sort = TRUE) %>% 
                  mutate(word = reorder(word, n))
                  
export(top_palavras_2019,"output/top_palavras_mais_usadas_2019.csv","csv")

top_palavras_2019 %>% filter(n > 5) %>% 
                  ggplot(aes(word, n)) +
                  geom_col() +
                  xlab(NULL) +
                  coord_flip()      

```

```{r}

top_palavras_2019 %>% 
  with(wordcloud(word, n, random.order = FALSE, max.words = 20, colors=pal))
```




## Quais foram as palavras que tornaram o discurso deste ano único?

Para proceder a esta análise, usamos o package [tidylo](https://github.com/juliasilge/tidylo), que calcula a razões de probabilidades ponderadas para as palavras por discurso (ou seja, a razão entre a probabilidade de uma palavra ocorrer num grupo e a probabilidade dessa palavra ocorrer noutro grupo).

Usamos como referência a publicação de Julia Silge [aqui](https://juliasilge.com/blog/introducing-tidylo/).

```{r}


top_palavras <- data2 %>%
                  group_by(ano) %>% 
                  count(word, sort = TRUE) %>%
                  ungroup
top_palavras

word_log_odds <- top_palavras %>%
  bind_log_odds(ano, word, n) 

word_log_odds %>%
  arrange(-log_odds)


```

Vendo isto num gráfico

```{r}

a <- word_log_odds %>%
    group_by(ano) %>%
    top_n(5, log_odds) %>%
    ungroup %>%
    mutate(ano = as.factor(ano),
           word = fct_reorder(word, log_odds)) %>%
    ggplot(aes(word, log_odds, fill = ano)) +
    geom_col(show.legend = FALSE) +
    facet_wrap(~ano, scales = "free_y") +
    coord_flip() +
    scale_y_continuous(expand = c(0,0)) +
    labs(y = "Razões de probabilidades ponderadas de palavras por discurso",
         x = NULL,
         title = "Quais as palavras mais específicas em cada um dos discursos?",
         subtitle = "Palavras com maior probabilidade de serem encontradas no discurso no discurso de cada ano.") + 
   theme_hc()

a

ggsave("output/grafico_prob_palavras.svg", plot = a,
  scale = 1, width = NA, height = NA, units = c("in", "cm", "mm"),
  dpi = 300, limitsize = TRUE)


```

Palavras que diferenciam (ou marcam) este discurso de outros




## Positividade/negatividade dos discursos

Uma das análises possíveis é fazer "sentiment analysis" dos dados - ou seja, que emoções estão associadas a um texto. A área da análise textual de sentimento está em franca expansão, com diversos recursos e metodologias a ser aplicadas.

Para este trabalho, e por uma questão de limitação de tempo e recursos, recorreu-se a uma das formas de análise mais rudimentares - a análise por associação de sentimento palavra a palavra.

Para tal, recorreu-se ao [trabalho de Saif M. Mohammad e Peter D. Turney](https://saifmohammad.com/WebPages/NRC-Emotion-Lexicon.html) que, através de crowsoursing, criaram uma base de dados com 14.183 palavras que atribui um valor booleano a um ou mais dos oito sentimentos básicos definidos na teoria Robert Plutchik. A estes oito sentimentos, acrescentaram ainda mais dois: positiva ou negativa.

Segundo este autor, as oito emoções básicas são: Raiva (Anger), Medo (Fear), Trizteza (Sadness), Nojo (Disgust), Surpresa (Surprise), Antecipação (Anticipation), Alegria (Joy) e Confiança (Trust).

Assim, em jeito de exemplo, a palavra "abominar" provoca, segundo a base de dados dos autores, os sentimentos de Raiva, Nojo, e Medo, ao passo que a palavra "elogio" provoca sentimentos de Antecipação, Felicidade e Confiança.

A lista de palavras e a respectiva classificação de Saif M. Mohammad e Peter D. Turney, do National Research Council Canada, pode ser consultada [aqui](input/NRC-Emotion-Lexicon.csv)


```{r}
sentiments <- import("input/NRC-Emotion-Lexicon.csv")
data3 <- data2 %>% left_join(sentiments, by= "word")

data_sentiment <- data3 %>% na.omit()

```


### Costa tem discursos mais positivos ou negativos?
```{r}
sum(data_sentiment$Positive)/nrow(data_sentiment)
sum(data_sentiment$Negative)/nrow(data_sentiment)

```
Quase uma em cada três palavras proferidas por Costa na globalidade dos quatro discursos.

### Qual foi o discurso mais positivo?E o mais negativo?

```{r}
data_sentiment %>% 
   group_by(ano) %>% 
   summarise(Sum_Posit = sum(Positive),
             Sum_Nega = sum(Negative),
             count = n()) -> sentiments_pos_neg

sentiments_pos_neg$per_Pos <- (sentiments_pos_neg$Sum_Posit/sentiments_pos_neg$count)*100
sentiments_pos_neg$per_Neg <- (sentiments_pos_neg$Sum_Nega/sentiments_pos_neg$count)*100

sentiments_pos_neg

```

```{r}
sentimentos_transformed <- import("input/sentimentos_transformed.csv")

#sentimentos_transformed$per <- as.numeric(sentimentos_transformed$per)

b <- sentimentos_transformed %>% ggplot() + geom_bar(aes(y = per, x = ano, fill= Sentimento), stat="identity") + 
  coord_flip() +
    labs(y = NULL,
         x = NULL) + 
  scale_fill_manual(values = c("#ff4c4c","#bfbfbf", "#66b266" )) + 
  scale_y_continuous(labels = function(x) paste0(x, "%")) +
   theme_hc()

b

ggsave("output/grafico_sentimentos.svg", plot = b,
  scale = 1, width = NA, height = NA, units = c("in", "cm", "mm"),
  dpi = 300, limitsize = TRUE)

```




## Que sentimento prevalece em cada um dos anos?



```{r}
sentimentos_ano <- data_sentiment %>% 
                group_by(ano) %>% 
                summarise_at(c("Anger","Anticipation","Disgust","Fear","Joy","Sadness","Surprise","Trust"), sum, na.rm = TRUE)

sentimentos_ano

sentimentos_ano <- as.data.frame(t(sentimentos_ano))
colnames(sentimentos_ano ) <- as.character(unlist(sentimentos_ano [1,]))
sentimentos_ano <- sentimentos_ano[-1, ]

sentimentos_ano <- sentimentos_ano %>%  add_rownames( var = "sentimento" )

export(sentimentos_ano,"output/sentimentos_ano.csv","csv")


```
# Análise por bigramas

```{r}
data_bigrams <- df %>%
  unnest_tokens(bigram, text, token = "ngrams", n = 2)


bigrams_separated <- data_bigrams %>%
  separate(bigram, c("word1", "word2"), sep = " ")

bigrams_filtered <- bigrams_separated %>%
  filter(!word1 %in% stop_words$word) %>%
  filter(!word2 %in% stop_words$word)

# new bigram counts:
bigram_counts <- bigrams_filtered %>% 
  count(word1, word2, sort = TRUE)

bigrams_united <- bigrams_filtered %>%
  unite(bigram, word1, word2, sep = " ")

bigrams_united
```

## Top bigrams

```{r}

top_bigrams <- bigrams_united %>%
                  count(bigram, sort = TRUE) %>% 
                  mutate(bigram = reorder(bigram, n))
top_bigrams 
```

"melhor emprego" foi a expressão mais vezes usada pelo PM nos quatro discursos (11 vezes)	


## Top bigrams 2019

```{r}
top_bigrams_2019 <- bigrams_united %>%
                  filter(ano == 2019) %>% 
                  count(bigram, sort = TRUE) %>% 
                  mutate(bigram = reorder(bigram, n))
top_bigrams_2019 
```





