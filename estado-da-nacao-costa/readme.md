|   |   |
|---|---|
|Fonte | [Site do XXI Governo Constitucional](https://www.portugal.gov.pt)|
| Metodologia| A Renascença recolheu os [ficheiros pdf](estado-da-nacao-costa/input) com os discursos do primeiro-ministro entre 2016 e 2019. Esses ficheiros foram depois convertidos em [documentos texto](input/discursos-finais) usando um script em R.
| Análise | Todo o código da análise pode ser consultado [aqui](analise.Rmd)
| Artigo(s)| ["Quatro anos" por 19 vezes. As palavras e expressões mais usadas por Costa no discurso do Estado da Nação](https://rr.sapo.pt/2019/07/11/politica/quatro-anos-por-19-vezes-as-palavras-e-expressoes-mais-usadas-por-costa-no-discurso-do-estado-da-nacao/especial/157563/)|