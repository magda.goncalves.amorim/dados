Senhor Presidente

Senhoras e Senhores deputados,

No dia imediatamente a seguir à saída de Portugal do procedimento por défice excessivo, os portugueses foram subitamente abalados com a maior catástrofe das últimas décadas.

Em menos de 24 horas, passámos do alívio à comoção, da satisfação à revolta, evidenciando como, perante o primado da vida humana, tudo o mais tem um valor
relativo.

Nunca mais nenhum de nós poderá esquecer aquele dia; e temos o dever de tudo fazer para estarmos à altura da memória dos que faleceram, da dor dos seus familiares e amigos, do terror das populações ameaçadas pelas chamas, da coragem e brio dos que combateram o fogo, dos que acorreram ao socorro, dos técnicos que investigam o ocorrido, da gratidão devida ao auxílio internacional, da corrente de solidariedade que percorreu toda a sociedade, mostrando mais uma vez a extraordinária capacidade de os portugueses se unirem e mobilizarem para vencer a adversidade.

O que nos é exigido é muito claro. Duas tarefas imediatas e enfrentar um desafio estrutural.

Primeiro, reconstruir o que foi destruído e restabelecer a capacidade produtiva dos territórios afetados.

O Governo está a trabalhar intensamente em colaboração com as autarquias. Hoje já estão em reconstrução as 3 primeiras habitações e estradas, linhas de baixa e média tensão e os cabos de fibra das comunicações estão restabelecidos.

Segundo, esclarecer cabalmente o que aconteceu e, em função dos factos, apurar responsabilidades, qualquer que seja a sua natureza. Temos direito a saber, e o dever de total transparência perante os cidadãos.

Aqui quero reafirmar, que sem prejuízo da informação e avaliação que os organismos públicos produziram ou estão a concluir, ou dos estudos científicos que solicitou, o Governo dará toda a colaboração e apoio tanto ao inquérito crime aberto pelo Ministério Público como à Comissão Técnica Independente constituída por esta Assembleia.

Mas…

Senhor Presidente

Senhoras e Senhores Deputados

O que é essencial, o que é estrutural para podermos evitar novas catástrofes é enfrentar o desafio de revitalizar o interior e reordenar a floresta.

Se há respostas que ainda não temos, porque os inquéritos ainda decorrem, há soluções que sabemos que há muito o país espera e não pode continuar a
esperar.

Foi por isso que o Governo criou logo em Março de 2016 a Unidade de Missão para a Valorização do Interior e foi por isso que, em outubro do ano passado, o Governo lançou a reforma da floresta, que está desde abril em debate nesta Assembleia.

Não podemos continuar a lamentar que o grande problema é o abandono das florestas e não aprovar os instrumentos de mobilização das terras ao abandono.

Não podemos continuar a justificar que o micro mini-fundio da floresta não gera rendimento suficiente e não criarmos as estruturas associativas, privadas ou públicas, que permitam a necessária valorização económica da floresta.

Não podemos continuar a dizer que o problema é não se conhecer os proprietários de milhões de hectares e não aprovar mecanismos que agilizem – finalmente! – a conclusão do cadastro.

Não podemos continuar a reclamar o fim da monocultura do eucalipto e não travar a expansão da sua área de plantio.

Todos sabemos que é isto que é necessário fazer. E não podemos continuar a adiar, porque receamos o impacto económico na indústria do papel ou porque temos          medo das revoltas populares contra a demarcação de propriedades.

É difícil, exige tempo e só a prazo produz resultados? Sim. Mas muito mais difícil é deixar andar e muito mais tempo perdemos se mais tarde começarmos.

Apelo por isso, mais uma vez, ao esforço conjunto para consensualizar esta reforma estrutural para o futuro do País.

E a melhor forma de dizermos nunca mais, é lançarmos nestes sete concelhos martirizados pelos incêndios de Pedrógão e Góis um projeto piloto das políticas de revitalização do território e reordenamento florestal. Para esse efeito, iremos deslocalizar para Pedrógão a Unidade de Missão para a Valorização do Interior para que diretamente no terreno o possa executar.

Senhor Presidente

Senhoras e Senhores deputados,

Este debate do Estado da Nação é também o momento de balanço geral e de antevisão do futuro do País.

Há cinco indicadores que marcam o último ano.

Primeiro, o mais importante, a prioridade das prioridades da nossa política económica, o emprego: são mais de 175.000 os novos postos de trabalho criados desde o início de 2016. A taxa de desemprego recuou para 9,5%, num contexto de aumento da
população ativa.

Em segundo lugar: a decisiva criação de confiança nos agentes económicos: A confiança dos consumidores encontra-se no valor mais alto de sempre e o clima económico atingiu máximos dos últimos 15 anos.

Em terceiro, o investimento. O investimento em volume teve no 1.º trimestre de 2017, o maior crescimento homólogo dos últimos 18 anos. E os indicadores avançados mostram a manutenção ou mesmo a aceleração dos atuais níveis de crescimento.

Em quarto lugar: o crescimento da economia sustentado no emprego, no investimento e na confiança. Depois da recuperação ao longo de 2016, o PIB atingiu no 1º trimestre deste ano o maior crescimento desde o início do século retomando finalmente a convergência com a zona euro.

Em quinto: pela primeira vez nos últimos 10 anos, o país cumpriu as metas orçamentais, registando o défice mais baixo da nossa democracia, e assegurando a saída do Procedimento por Défices Excessivos.

Sim, havia mesmo alternativa. E o Governo e a maioria cumpriram a alternativa com que se comprometeram.

Os bons resultados que alcançámos não são fruto de um acaso. Estes bons resultados surgem das boas políticas que adotámos.

Afinal, não precisámos de continuar a empobrecer nem a destruir os direitos laborais para que a exportação de bens tenha tido em Maio o crescimento homólogo de
15,4%.

Afinal não precisámos de aumentar a carga fiscal, nem cortar as pensões e os apoios sociais, para consolidar as nossas finanças públicas.

Pelo contrário, foi a nova política de rendimentos e a previsibilidade da diminuição da carga fiscal que gerou confiança e foi o incentivo ao investimento e à qualificação dos trabalhadores que melhorou a nossa competitividade.

Este é o caminho que temos de prosseguir para termos mais crescimento, melhor emprego, mais igualdade. Investir no conhecimento, na inovação e na dignificação do trabalho.

Senhor Presidente

Senhoras e Senhores Deputados

É certo, também, que ainda estamos longe dos níveis de crescimento, de emprego e de igualdade que ambicionamos para Portugal e para os portugueses. O progresso é uma meta que exige persistência. Que exige políticas sustentadas e sustentáveis.

Um Estado forte, que garanta a defesa nacional, a segurança interna, e uma justiça próxima dos cidadãos. Mas ao mesmo tempo um Estado moderno, ágil, eficaz,           inteligente e estrategicamente empreendedor. Um Estado que garanta o acesso de todos a serviços públicos de qualidade, que combata as desigualdades, que estimule a participação cívica e que contribua para a coesão social e territorial.

Desde logo, reforçando o investimento na saúde e na educação enquanto direitos   fundamentais dos cidadãos.

Na fase do debate, o Ministro Adalberto Campos Fernandes terá a oportunidade de apresentar, o que estamos e continuaremos a fazer para garantir um SNS que responda cada vez melhor às necessidades e expectativas dos cidadãos

Na Educação, aprofundaremos a estratégia de combate ao défice de qualificações, - o verdadeiro défice estrutural do País - promovendo o sucesso escolar das novas gerações e abrindo novas oportunidades às antigas gerações.

No próximo ano letivo, teremos dois novos importantes contributos para a promoção do sucesso escolar.

Por um lado, beneficiando do novo modelo de avaliação, teremos maior autonomia pedagógica na gestão flexível dos currículos; por outro lado a redução do número de alunos por turma nas escolas em territórios educativos de intervenção prioritária,

A habitação tem de ser uma nova área prioritária nas políticas públicas, dirigida agora às classes médias e em especial às novas gerações, não as condenando ao endividamento ou ao abandono do centro das cidades, promovendo a oferta de habitação para arrendamento acessível. No ajustamento governativo que amanhã apresentarei ao Presidente da República, está previsto precisamente a autonomização da habitação como Secretaria de Estado.

Mas a capacidade de intervenção estratégica do Estado apenas poderá ser melhorada se fortalecermos e modernizarmos a Administração Pública, se promovermos a inovação no setor público e valorizarmos o exercício de funções públicas.

Continuaremos, pois, empenhados no programa Simplex+, procurando simplificar cada vez mais o dia a dia dos cidadãos e reduzir os custos de contexto para as empresas, e colocaremos em funcionamento os Centros de Competência que, através da concentração de competências qualificadas em domínios especializados, permitirão diminuir a contratação externa e reforçar o conhecimento e o saber fazer dentro da Administração Pública.

Também a valorização do trabalho em funções públicas continuará a ser uma prioridade, estando previsto já para 2018 o arranque do processo de descongelamento de carreiras, tal como previsto no Programa de Governo.

Para uma verdadeira coesão social e territorial será ainda essencial pôr em marcha o processo de descentralização em discussão nesta Assembleia.

O exemplo dos transportes públicos é muito impressionante. Depois de anos e anos a perder passageiros, os STCP já recuperaram este ano 7% de passageiros e os novos   tarifários  da  CARRIS, permitiram mais +470.000 viagens/mês para utentes com mais de 65 anos e +100.000 viagens/mês para crianças até aos 10 anos. Pela primeira vez em 17 anos, a CARRIS abriu ontem 2 novas carreiras, abrindo mais 3 até ao final do mês e, hoje mesmo, os STCP adjudicaram a aquisição de 173 novos autocarros, aliás à indústria nacional.

Este é o exemplo que podemos replicar, se libertarmos do centralismo as áreas em que, como a experiência comprova, podemos confiar que Municípios e Freguesias podem fazer mais e melhor.

Senhor Presidente

Senhoras e Senhores Deputados

É preciso pensar para lá da atual legislatura e projetarmos para a década uma visão de médio prazo que reforce simultaneamente a competitividade externa e a coesão interna, como as bases de convergência continuada e sustentada com a União
Europeia.

Há mais vida para além desta legislatura e a necessidade de prepararmos desde já o Portugal Pós 2020 impõe a oportunidade deste debate.

Iniciámos no passado dia 19 de junho no Conselho de Concertação Territorial a apreciação com as Regiões Autónomas e as Autarquias das Linhas Gerais das Prioridades do Programa Pòs 2020, debate que iremos alargar ao Conselho Económico e Social e que promoveremos nesta Assembleia na reabertura dos trabalhos parlamentares.

A matriz em que trabalhamos assenta em dois eixos horizontais:
- Inovação e Conhecimento
- Qualificação, Formação e Emprego
        
Em 4 eixos territoriais:
- Energia e Alterações Climáticas
- Economia do Mar
- Inserção nas Redes e Mercados Globais
- Interioridade e Mercado Ibérico

É uma matriz que assenta num modelo claro, sermos mais coesos internamente, sermos mais competitivos na economia global e assim reforçarmos a convergência             com a União  Europeia. Três C’s: competitividade e coesão para a convergência.

Senhor Presidente

Senhoras e Senhores Deputados

Aqui estamos respondendo à emergência, persistindo com equilíbrio na concretização bem sucedida da mudança de política que marca esta legislatura, e com uma          visão estratégica para uma década de convergência que em conjunto estamos a construir.