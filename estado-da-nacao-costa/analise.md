---
title: "O que disse António Costa em todos os debates do Estado da Nação?"
author: "Rui Barros (rui.barros@r r.pt)"
date: "2019-07-11"
output:
   html_document:
     keep_md: TRUE
     code_folding: show
     echo: TRUE
     warning: FALSE
     message: FALSE
     theme: lumen
     df_print: kable
     toc: yes
     toc_depth: 4
     toc_float: 
       collapsed: false
       smooth_scroll: false
---


# Introdução

Neste documento vamos proceder a uma análise de discurso das palavras do primeiro-ministro António Costa nos quatro debates do Estado da Nação.

## Questões orientadoras:

- Qual foi o ano com o discurso mais palavroso?
- Quais foram as palavras mais usadas em cada um dos anos?
- Quais foram as palavras que tornaram o discurso deste ano único?
- Qual foi o discurso mais positivo?
- Qual foi o discurso mais negativo?
- Que sentimento prevalece em cada um dos anos?
- Qual foi o sentimento que se destacou este ano em relação aos outros anos?
- Qual foi a combinação de palavras mais frequente? (bigrams)

## Dependências



```r
library("needs")
needs(pdftools,dplyr,purrr,readr,tidyr,tidytext,stringr,rio,knitr,stopwords,ggplot2,wordcloud,tibble,quanteda,tidylo,forcats,ggthemes)
```

# Importação de dados

Os ficheiros com os discursos foram todos disponibilizados pelo governo no seu site institucional descarregados para a pasta input.

## Extração de dados do pdf

Comos os ficheiros estavam em pdf, foi necessário proceder ao seu processamento e transformação para txt para poderem ser analisados.


```r
# x <- list.files(path = "input")
# 
# for (i in 1:9) {
#    text <- pdf_text(paste0("input/", x[i])) %>% 
#          strsplit("\n") %>% 
#          lapply(write, paste0("output/", x[i],".txt"), append=TRUE)
# }
```

Depois do processamento dos .pdfs, foi necessário proceder à necessária limpeza dos ficheiros, que incluiram também outros elementos que não importavam para a análise.

Esta limpeza foi feita manualmente. Optou-se por preservar o máximo possível a integridade dos textos, tendo-se apenas eliminado espaços desnecessariamente acrescentados na conversão, bem como o texto referente ao número de página, cabeçalhos e rodapés.

No caso do discurso de 2019, optamos por corrigir a formulação "4 anos" para "quatro anos" porque o primeiro-ministro tinha essa formulação escrita das duas formas ao longo do texto.

Os discursos limpos em .txt estão em [output/discursos-final](output/discursos-final).

## Unnest de todos os discursos



```r
tbl <- list.files(path = "input/discursos-finais",pattern = "*.txt",
               full.names = T) 

df <- tbl %>%
       map_chr(~ read_file(.)) %>%
       data_frame(text = .,ano = tbl)
```

```
## Warning: `data_frame()` is deprecated, use `tibble()`.
## This warning is displayed once per session.
```

```r
#remove path do ano
df$ano<- gsub("input/discursos-finais/estado-da-nacao-","",df$ano) 
df$ano <- gsub(".txt","",df$ano) 
```

Procedeu-se à consequente tokenização destas palavras:


```r
data <- df %>%
         unnest_tokens(word, text)
nrow(data)
```

```
## [1] 8742
```

Foram assim tidas, como ponto de partida, as 8.741 palavras que compõe os quatro discursos inaugurais do debate do Estado da Nação pelo primeiro-ministro.

No entanto, nem todas as palavras interessam na análise textual uma vez que há palavras como os determinantes e afins que, apesar de muito comuns no uso corrente da linguagem, não acrescentam muito - as chamadas Stop Words.

Para esta análise, consideraram-se as Stop Words para Língua Portuguesa elencadas pelo package [Stopwords](http://stopwords.quanteda.io/reference/stopwords.html)


```r
stop_words <- as.data.frame(stopwords("pt", source = "stopwords-iso"))
stop_words <- rename(stop_words, 'word' = 'stopwords("pt", source = "stopwords-iso")')
target <- c("um","dois","três","quatro","cinco","seis","sete","oito","nove","dez","ano","anos")
stop_words <- stop_words %>%
  filter(!word %in% target)
export(stop_words, "output/stop_words_pt.csv","csv")
```

As palavras eliminadas estão disponíveis [aqui](output/stop_words_pt.csv).


```r
data2 <- data %>% 
        anti_join(stop_words)
```

```
## Joining, by = "word"
```

```
## Warning: Column `word` joining character vector and factor, coercing into
## character vector
```

```r
nrow(data2)
```

```
## [1] 4174
```



# Análise 

## Qual foi o ano com o discurso mais palavroso?


```r
count_por_ano <- data %>% 
   group_by(ano) %>% 
   tally() %>% 
   arrange(-n)
count_por_ano
```

<div class="kable-table">

ano        n
-----  -----
2018    2733
2019    2388
2017    1833
2016    1788

</div>

O discurso de 2019 foi o segundo mais palavroso, sendo superado pelo prferido em 2018, quando António Costa proferiu 2.733 palavras no discurso de abertura do discurso do Estado da Nação.

## Qual foi a palavra mais usada por Costa nestes quatro anos?


```r
top_palavras <- data2 %>%
                  count(word, sort = TRUE) %>% 
                  mutate(word = reorder(word, n))

export(top_palavras,"output/top_palavras_mais_usadas_sempre.csv","csv")

top_palavras %>% filter(n > 10) %>% 
                  ggplot(aes(word, n)) +
                  geom_col() +
                  xlab(NULL) +
                  coord_flip()
```

![](analise_files/figure-html/unnamed-chunk-8-1.png)<!-- -->


Em nuvem de palavras...

```r
pal <- brewer.pal(8,"Dark2")

top_palavras %>% 
  with(wordcloud(word, n, random.order = FALSE, max.words = 13, colors=pal))
```

![](analise_files/figure-html/unnamed-chunk-9-1.png)<!-- -->

Nos quatro discursos, a palavra mais repetida por Costa foi assim "investimento" (49), "país"(39), "emprego"(29), "confiança"(28), "portugueses"(27) e "crescimento"(27).

## Quais foram as palavras mais usadas em cada um dos anos?

## 2016


```r
top_palavras_2016 <- data2 %>%
                  filter(ano == 2016) %>% 
                  count(word, sort = TRUE) %>% 
                  mutate(word = reorder(word, n))
                  
export(top_palavras_2016,"output/top_palavras_mais_usadas_2016.csv","csv")

top_palavras_2016 %>% filter(n > 5) %>% 
                  ggplot(aes(word, n)) +
                  geom_col() +
                  xlab(NULL) +
                  coord_flip()      
```

![](analise_files/figure-html/unnamed-chunk-10-1.png)<!-- -->



## 2017


```r
top_palavras_2017 <- data2 %>%
                  filter(ano == 2017) %>% 
                  count(word, sort = TRUE) %>% 
                  mutate(word = reorder(word, n))
                  
export(top_palavras_2017,"output/top_palavras_mais_usadas_2017.csv","csv")

top_palavras_2017 %>% filter(n > 5) %>% 
                  ggplot(aes(word, n)) +
                  geom_col() +
                  xlab(NULL) +
                  coord_flip()      
```

![](analise_files/figure-html/unnamed-chunk-11-1.png)<!-- -->


## 2018


```r
top_palavras_2018 <- data2 %>%
                  filter(ano == 2018) %>% 
                  count(word, sort = TRUE) %>% 
                  mutate(word = reorder(word, n))
                  
export(top_palavras_2018,"output/top_palavras_mais_usadas_2018.csv","csv")

top_palavras_2018 %>% filter(n > 5) %>% 
                  ggplot(aes(word, n)) +
                  geom_col() +
                  xlab(NULL) +
                  coord_flip()      
```

![](analise_files/figure-html/unnamed-chunk-12-1.png)<!-- -->


## 2019


```r
top_palavras_2019 <- data2 %>%
                  filter(ano == 2019) %>% 
                  count(word, sort = TRUE) %>% 
                  mutate(word = reorder(word, n))
                  
export(top_palavras_2019,"output/top_palavras_mais_usadas_2019.csv","csv")

top_palavras_2019 %>% filter(n > 5) %>% 
                  ggplot(aes(word, n)) +
                  geom_col() +
                  xlab(NULL) +
                  coord_flip()      
```

![](analise_files/figure-html/unnamed-chunk-13-1.png)<!-- -->


```r
top_palavras_2019 %>% 
  with(wordcloud(word, n, random.order = FALSE, max.words = 20, colors=pal))
```

![](analise_files/figure-html/unnamed-chunk-14-1.png)<!-- -->




## Quais foram as palavras que tornaram o discurso deste ano único?

Para proceder a esta análise, usamos o package [tidylo](https://github.com/juliasilge/tidylo), que calcula a razões de probabilidades ponderadas para as palavras por discurso (ou seja, a razão entre a probabilidade de uma palavra ocorrer num grupo e a probabilidade dessa palavra ocorrer noutro grupo).

Usamos como referência a publicação de Julia Silge [aqui](https://juliasilge.com/blog/introducing-tidylo/).


```r
top_palavras <- data2 %>%
                  group_by(ano) %>% 
                  count(word, sort = TRUE) %>%
                  ungroup
top_palavras
```

<div class="kable-table">

ano    word                    n
-----  --------------------  ---
2018   investimento           24
2019   anos                   23
2018   um                     22
2019   quatro                 21
2016   um                     18
2016   país                   14
2019   confiança              14
2019   um                     14
2016   cumprimos              13
2016   investimento           12
2019   legislatura            12
2019   melhor                 12
2019   portugal               12
2018   país                   11
2019   portugueses            11
2018   anos                   10
2018   emprego                10
2018   programa               10
2019   emprego                10
2016   problemas               9
2017   um                      9
2018   ano                     9
2018   crescimento             9
2018   nacional                9
2018   orçamento               9
2019   continuar               9
2019   crescimento             9
2019   país                    9
2016   governo                 8
2016   programa                8
2017   anos                    8
2018   continuidade            8
2019   futuro                  8
2019   investimento            8
2016   execução                7
2016   medidas                 7
2016   nacional                7
2017   continuar               7
2017   crescimento             7
2017   podemos                 7
2017   presidente              7
2018   confiança               7
2018   economia                7
2018   execução                7
2018   políticas               7
2018   portugueses             7
2018   três                    7
2019   igualdade               7
2016   agir                    6
2016   ano                     6
2016   empresas                6
2016   milhões                 6
2016   portugueses             6
2017   deputados               6
2017   governo                 6
2017   senhor                  6
2017   senhoras                6
2017   senhores                6
2018   futuro                  6
2018   governo                 6
2018   podemos                 6
2018   público                 6
2018   públicos                6
2018   recuperação             6
2018   resultados              6
2018   serviços                6
2019   governo                 6
2019   presidente              6
2019   recuperação             6
2016   compromissos            5
2016   euros                   5
2016   portugal                5
2016   social                  5
2017   ano                     5
2017   cidadãos                5
2017   convergência            5
2017   debate                  5
2017   emprego                 5
2017   investimento            5
2017   país                    5
2017   públicas                5
2018   2017                    5
2018   assembleia              5
2018   austeridade             5
2018   coesão                  5
2018   deputados               5
2018   dívida                  5
2018   legislatura             5
2018   portugal                5
2018   presidente              5
2018   pública                 5
2018   públicas                5
2018   saúde                   5
2018   senhor                  5
2018   senhoras                5
2018   senhores                5
2018   social                  5
2019   contas                  5
2019   democracia              5
2019   deputados               5
2019   problemas               5
2019   senhor                  5
2019   senhores                5
2019   vida                    5
2016   1                       4
2016   2016                    4
2016   assente                 4
2016   desenvolvimento         4
2016   emprego                 4
2016   incentivo               4
2016   investimentos           4
2016   º                       4
2016   orçamental              4
2016   presidente              4
2016   projetos                4
2016   prometemos              4
2016   reabilitação            4
2016   visão                   4
2017   assembleia              4
2017   coesão                  4
2017   confiança               4
2017   défice                  4
2017   estrutural              4
2017   floresta                4
2017   melhor                  4
2017   políticas               4
2017   valorização             4
2018   3                       4
2018   administração           4
2018   amanhã                  4
2018   áreas                   4
2018   aumento                 4
2018   criação                 4
2018   défice                  4
2018   desenvolvimento         4
2018   empresas                4
2018   essencial               4
2018   famílias                4
2018   jovens                  4
2018   melhor                  4
2018   passo                   4
2018   pib                     4
2018   pobreza                 4
2018   prioridade              4
2018   redução                 4
2018   rendimentos             4
2018   sociedade               4
2018   territorial             4
2018   voltar                  4
2019   2018                    4
2019   alternativa             4
2019   ano                     4
2019   aumentámos              4
2019   aumento                 4
2019   balanço                 4
2019   composições             4
2019   medidas                 4
2019   políticas               4
2019   prioridade              4
2019   programa                4
2019   segurança               4
2019   senhoras                4
2019   social                  4
2019   ue                      4
2016   100                     3
2016   aceleração              3
2016   ambição                 3
2016   competitividade         3
2016   condições               3
2016   confiança               3
2016   criar                   3
2016   deputados               3
2016   direitos                3
2016   economia                3
2016   eliminámos              3
2016   europeia                3
2016   face                    3
2016   família                 3
2016   famílias                3
2016   financeiro              3
2016   global                  3
2016   inovação                3
2016   lançámos                3
2016   maio                    3
2016   marca                   3
2016   melhor                  3
2016   modelo                  3
2016   modernização            3
2016   planos                  3
2016   queremos                3
2016   reformas                3
2016   senhor                  3
2016   senhoras                3
2016   senhores                3
2016   urbana                  3
2017   2016                    3
2017   abandono                3
2017   capacidade              3
2017   competitividade         3
2017   conhecimento            3
2017   economia                3
2017   económico               3
2017   exige                   3
2017   gerações                3
2017   habitação               3
2017   inovação                3
2017   interior                3
2017   legislatura             3
2017   política                3
2017   portugal                3
2017   portugueses             3
2017   previsto                3
2017   programa                3
2017   públicos                3
2017   resultados              3
2017   social                  3
2017   territorial             3
2017   últimos                 3
2018   2                       3
2018   4                       3
2018   alcançados              3
2018   aprovaremos             3
2018   baixou                  3
2018   bloqueios               3
2018   competências            3
2018   condições               3
2018   conseguimos             3
2018   cresceu                 3
2018   crise                   3
2018   cultura                 3
2018   debate                  3
2018   desigualdades           3
2018   despesa                 3
2018   educação                3
2018   empresarial             3
2018   escolar                 3
2018   estabilidade            3
2018   família                 3
2018   florestal               3
2018   forte                   3
2018   garantir                3
2018   habitação               3
2018   igualdade               3
2018   inovação                3
2018   investimentos           3
2018   lisboa                  3
2018   m                       3
2018   melhoria                3
2018   mobilidade              3
2018   níveis                  3
2018   pensões                 3
2018   porto                   3
2018   pré                     3
2018   promoção                3
2018   pt2020                  3
2018   recuos                  3
2018   recuperar               3
2018   reforço                 3
2018   reforma                 3
2018   reformas                3
2018   sustentabilidade        3
2018   volvidos                3
2019   3                       3
2019   agradecer               3
2019   austeridade             3
2019   bases                   3
2019   cartão                  3
2019   certas                  3
2019   cidadãos                3
2019   constitucional          3
2019   debate                  3
2019   economia                3
2019   justiça                 3
2019   linha                   3
2019   lisboa                  3
2019   longo                   3
2019   médio                   3
2019   permitiu                3
2019   possibilidade           3
2019   prazo                   3
2019   procuramos              3
2019   públicos                3
2019   reduzimos               3
2019   reduzir                 3
2019   rendimento              3
2019   renovação               3
2019   respeito                3
2019   resultados              3
2019   solução                 3
2019   sustentável             3
2019   transportes             3
2016   adicionais              2
2016   administrações          2
2016   agimos                  2
2016   anos                    2
2016   anterior                2
2016   autonomia               2
2016   baixos                  2
2016   balanço                 2
2016   candidaturas            2
2016   capitalizar             2
2016   clima                   2
2016   comissão                2
2016   compromisso             2
2016   comunitários            2
2016   concretizar             2
2016   concursos               2
2016   construir               2
2016   contratos               2
2016   crescimento             2
2016   défice                  2
2016   destruição              2
2016   dias                    2
2016   dificuldades            2
2016   discordar               2
2016   domínio                 2
2016   dotação                 2
2016   elevado                 2
2016   funcionários            2
2016   fundamentais            2
2016   fundo                   2
2016   fundos                  2
2016   futuro                  2
2016   homólogo                2
2016   ignoramos               2
2016   iniciativa              2
2016   janeiro                 2
2016   médio                   2
2016   meta                    2
2016   municípios              2
2016   palavra                 2
2016   papel                   2
2016   parlamentar             2
2016   pensões                 2
2016   plano                   2
2016   pobreza                 2
2016   poderem                 2
2016   prazo                   2
2016   pré                     2
2016   precisamos              2
2016   prioridade              2
2016   prioridades             2
2016   públicos                2
2016   pusemos                 2
2016   qualificação            2
2016   recuperação             2
2016   redução                 2
2016   rendimento              2
2016   república               2
2016   resolver                2
2016   salários                2
2016   saúde                   2
2016   sido                    2
2016   simplex                 2
2016   taxa                    2
2016   três                    2
2016   trimestre               2
2016   valorização             2
2016   vida                    2
2017   10                      2
2017   2020                    2
2017   3                       2
2017   abrindo                 2
2017   administração           2
2017   afinal                  2
2017   alternativa             2
2017   aprovar                 2
2017   assenta                 2
2017   atingiu                 2
2017   autarquias              2
2017   avaliação               2
2017   carga                   2
2017   carreiras               2
2017   carris                  2
2017   claro                   2
2017   colaboração             2
2017   conjunto                2
2017   contexto                2
2017   continuaremos           2
2017   década                  2
2017   desafio                 2
2017   dever                   2
2017   difícil                 2
2017   direitos                2
2017   dois                    2
2017   económica               2
2017   educação                2
2017   eixos                   2
2017   enfrentar               2
2017   escolar                 2
2017   essencial               2
2017   estratégica             2
2017   europeia                2
2017   externa                 2
2017   finalmente              2
2017   fiscal                  2
2017   funções                 2
2017   futuro                  2
2017   garanta                 2
2017   homólogo                2
2017   igualdade               2
2017   indicadores             2
2017   indústria               2
2017   início                  2
2017   interna                 2
2017   intervenção             2
2017   iremos                  2
2017   linhas                  2
2017   matriz                  2
2017   missão                  2
2017   modelo                  2
2017   nacional                2
2017   níveis                  2
2017   oportunidade            2
2017   passado                 2
2017   passageiros             2
2017   pedrógão                2
2017   prazo                   2
2017   precisámos              2
2017   prioridade              2
2017   prioridades             2
2017   prioritária             2
2017   problema                2
2017   procedimento            2
2017   processo                2
2017   promovendo              2
2017   pública                 2
2017   público                 2
2017   qualificação            2
2017   reforma                 2
2017   sabemos                 2
2017   saída                   2
2017   sermos                  2
2017   stcp                    2
2017   sucesso                 2
2017   territórios             2
2017   trimestre               2
2017   união                   2
2017   unidade                 2
2017   viagens                 2
2017   vida                    2
2017   visão                   2
2018   1                       2
2018   10                      2
2018   19                      2
2018   2019                    2
2018   2020                    2
2018   2030                    2
2018   abono                   2
2018   acelerámos              2
2018   adiado                  2
2018   ambição                 2
2018   apoios                  2
2018   aprovado                2
2018   aquisição               2
2018   assente                 2
2018   assumido                2
2018   aumentámos              2
2018   aumentar                2
2018   base                    2
2018   beira                   2
2018   caso                    2
2018   centros                 2
2018   ciclo                   2
2018   ciência                 2
2018   circulante              2
2018   claro                   2
2018   competitividade         2
2018   compromisso             2
2018   compromissos            2
2018   concentrar              2
2018   conhecimento            2
2018   conseguiríamos          2
2018   contas                  2
2018   contrário               2
2018   contratos               2
2018   convergência            2
2018   criámos                 2
2018   críticas                2
2018   dando                   2
2018   década                  2
2018   deficiência             2
2018   descarbonização         2
2018   descida                 2
2018   desemprego              2
2018   económica               2
2018   empobrecimento          2
2018   empregos                2
2018   escola                  2
2018   estruturais             2
2018   euro                    2
2018   familiar                2
2018   feito                   2
2018   ferrovia                2
2018   finanças                2
2018   formação                2
2018   geração                 2
2018   gestão                  2
2018   investir                2
2018   lançámos                2
2018   ligação                 2
2018   material                2
2018   metade                  2
2018   ministros               2
2018   modelo                  2
2018   modo                    2
2018   mudámos                 2
2018   mudança                 2
2018   º                       2
2018   oportunidades           2
2018   percorrer               2
2018   percurso                2
2018   permitirá               2
2018   possam                  2
2018   potencial               2
2018   problemas               2
2018   processo                2
2018   produtivo               2
2018   programas               2
2018   prosseguimos            2
2018   prosseguiremos          2
2018   proteção                2
2018   proximidade             2
2018   quadros                 2
2018   queremos                2
2018   reabilitação            2
2018   recuou                  2
2018   recursos                2
2018   reduzindo               2
2018   reforçando              2
2018   rendimento              2
2018   reprogramação           2
2018   resolução               2
2018   salários                2
2018   segurança               2
2018   sociais                 2
2018   sucesso                 2
2018   território              2
2018   últimos                 2
2018   vida                    2
2018   visa                    2
2018   zona                    2
2019   10                      2
2019   20                      2
2019   2014                    2
2019   2015                    2
2019   2017                    2
2019   30                      2
2019   4                       2
2019   ala                     2
2019   alternativas            2
2019   aprovámos               2
2019   assembleia              2
2019   assente                 2
2019   aumentou                2
2019   capaz                   2
2019   cidadão                 2
2019   circulação              2
2019   compromissos            2
2019   condições               2
2019   constituição            2
2019   construir               2
2019   contrário               2
2019   cortes                  2
2019   crianças                2
2019   criou                   2
2019   cumprir                 2
2019   década                  2
2019   défice                  2
2019   desafio                 2
2019   desígnio                2
2019   desigualdades           2
2019   diferente               2
2019   dificuldades            2
2019   digital                 2
2019   direitos                2
2019   edifício                2
2019   educação                2
2019   ensino                  2
2019   escolar                 2
2019   escolher                2
2019   esperança               2
2019   estratégicos            2
2019   família                 2
2019   famílias                2
2019   fatalismo               2
2019   forças                  2
2019   fortemente              2
2019   fundamentais            2
2019   garantindo              2
2019   género                  2
2019   gerou                   2
2019   impostos                2
2019   inevitabilidade         2
2019   inovação                2
2019   institucional           2
2019   internacional           2
2019   investimentos           2
2019   investir                2
2019   m                       2
2019   material                2
2019   média                   2
2019   melhorar                2
2019   mudança                 2
2019   nacional                2
2019   navios                  2
2019   negamos                 2
2019   normalidade             2
2019   º                       2
2019   orçamentos              2
2019   países                  2
2019   parlamentar             2
2019   passado                 2
2019   passo                   2
2019   pediátrica              2
2019   pensões                 2
2019   plano                   2
2019   política                2
2019   precisamos              2
2019   previsibilidade         2
2019   problema                2
2019   pública                 2
2019   qualidade               2
2019   recuperaram             2
2019   redução                 2
2019   reduziu                 2
2019   reforça                 2
2019   reforço                 2
2019   rendimentos             2
2019   renovar                 2
2019   responder               2
2019   salários                2
2019   saúde                   2
2019   serviços                2
2019   situação                2
2019   sociedade               2
2019   suma                    2
2019   superior                2
2019   sustentabilidade        2
2019   taxas                   2
2019   termos                  2
2019   transtejo               2
2019   triplo                  2
2019   últimos                 2
2019   volvidos                2
2016   1.000                   1
2016   194                     1
2016   200                     1
2016   2008                    1
2016   2015                    1
2016   2020                    1
2016   3                       1
2016   3,2                     1
2016   4                       1
2016   4.0                     1
2016   400                     1
2016   45                      1
2016   450                     1
2016   454                     1
2016   5,5                     1
2016   600                     1
2016   7                       1
2016   90                      1
2016   abertura                1
2016   abono                   1
2016   abril                   1
2016   abriu                   1
2016   acabámos                1
2016   ação                    1
2016   acelerador              1
2016   acima                   1
2016   acordo                  1
2016   adotamos                1
2016   adotámos                1
2016   adultos                 1
2016   agindo                  1
2016   alargámos               1
2016   alqueva                 1
2016   alteração               1
2016   ambiente                1
2016   âmbito                  1
2016   ancora                  1
2016   antecipação             1
2016   antecipem               1
2016   ap                      1
2016   apresenta               1
2016   apresentámos            1
2016   apresentando            1
2016   apresentar              1
2016   aprofunda               1
2016   aprovámos               1
2016   aprovar                 1
2016   àqueles                 1
2016   arautos                 1
2016   assegure                1
2016   assegurem               1
2016   assembleia              1
2016   associe                 1
2016   assumidos               1
2016   assumimos               1
2016   assumir                 1
2016   assumiu                 1
2016   atingido                1
2016   atingir                 1
2016   ativa                   1
2016   atribuição              1
2016   atualizámos             1
2016   aumentámos              1
2016   austeridade             1
2016   autarquias              1
2016   autonomias              1
2016   avisos                  1
2016   azul                    1
2016   b                       1
2016   bacias                  1
2016   baixámos                1
2016   baixaram                1
2016   basta                   1
2016   bloqueios               1
2016   capitalização           1
2016   casas                   1
2016   celebrámos              1
2016   central                 1
2016   centros                 1
2016   chegar                  1
2016   ciclo                   1
2016   cidadãos                1
2016   científico              1
2016   circular                1
2016   cláusula                1
2016   coerente                1
2016   combate                 1
2016   compensação             1
2016   comprometido            1
2016   comunidades             1
2016   concertação             1
2016   concretiza              1
2016   confiar                 1
2016   confirmados             1
2016   conhecer                1
2016   conjuntamente           1
2016   conjuntas               1
2016   conseguiremos           1
2016   consistente             1
2016   consolidação            1
2016   constatarem             1
2016   constituir              1
2016   construção              1
2016   construindo             1
2016   contexto                1
2016   continuados             1
2016   contrária               1
2016   contratar               1
2016   contribuição            1
2016   cooperam                1
2016   coro                    1
2016   correspondem            1
2016   corresponderam          1
2016   correspondesse          1
2016   corte                   1
2016   cortes                  1
2016   credibilizar            1
2016   crescer                 1
2016   criação                 1
2016   criando                 1
2016   crucial                 1
2016   csi                     1
2016   cubra                   1
2016   cuidados                1
2016   custos                  1
2016   dados                   1
2016   declarou                1
2016   defesa                  1
2016   deficiência             1
2016   deixaram                1
2016   democrático             1
2016   demos                   1
2016   dependência             1
2016   desafios                1
2016   desbloquear             1
2016   descentralização        1
2016   desemprego              1
2016   desenhar                1
2016   desequilíbrios          1
2016   desfavorecidas          1
2016   desgraça                1
2016   desigualdades           1
2016   desmentir               1
2016   determinação            1
2016   diálogo                 1
2016   dimensão                1
2016   diminuir                1
2016   dinamizar               1
2016   direito                 1
2016   disse                   1
2016   diversidade             1
2016   diversificado           1
2016   dívida                  1
2016   economias               1
2016   educação                1
2016   efeitos                 1
2016   eficiência              1
2016   elegemos                1
2016   eleições                1
2016   empreendedores          1
2016   empresarial             1
2016   encenação               1
2016   endividamento           1
2016   energética              1
2016   energia                 1
2016   enfrentados             1
2016   enfrentam               1
2016   enfrentando             1
2016   enormes                 1
2016   erradicação             1
2016   escolar                 1
2016   escolas                 1
2016   escondem                1
2016   escondemos              1
2016   esperança               1
2016   estabelecemos           1
2016   estabilizar             1
2016   estável                 1
2016   estratégia              1
2016   estratégicos            1
2016   estruturais             1
2016   euro                    1
2016   europa                  1
2016   excecional              1
2016   excessivo               1
2016   excessivos              1
2016   executados              1
2016   existem                 1
2016   existiam                1
2016   extraordinária          1
2016   faça                    1
2016   favorável               1
2016   feriados                1
2016   financeira              1
2016   financiamento           1
2016   financiar               1
2016   fomentar                1
2016   fonte                   1
2016   frente                  1
2016   funcionamento           1
2016   funções                 1
2016   fundaram                1
2016   gratuitos               1
2016   grau                    1
2016   greve                   1
2016   hectares                1
2016   herdámos                1
2016   hidrográficas           1
2016   histórico               1
2016   honrando                1
2016   honrar                  1
2016   horizonte               1
2016   ia                      1
2016   identidade              1
2016   igualdade               1
2016   imi                     1
2016   incentivem              1
2016   indústria               1
2016   ine                     1
2016   iniciando               1
2016   início                  1
2016   institucional           1
2016   interesse               1
2016   interesses              1
2016   interior                1
2016   intervenção             1
2016   investe                 1
2016   investidores            1
2016   investir                1
2016   irs                     1
2016   iva                     1
2016   jovens                  1
2016   judiciário              1
2016   justifica               1
2016   legislatura             1
2016   letivo                  1
2016   levar                   1
2016   levará                  1
2016   limitações              1
2016   limpa                   1
2016   linha                   1
2016   lisboa                  1
2016   locais                  1
2016   longo                   1
2016   majoração               1
2016   manifestada             1
2016   manuais                 1
2016   mapa                    1
2016   mar                     1
2016   máxima                  1
2016   mecanismo               1
2016   medida                  1
2016   melhorar                1
2016   melhores                1
2016   mínimo                  1
2016   mínimos                 1
2016   missão                  1
2016   mobilidade              1
2016   moderadoras             1
2016   morada                  1
2016   move                    1
2016   mudança                 1
2016   municipal               1
2016   municipalização         1
2016   nacionais               1
2016   necessárias             1
2016   necessário              1
2016   necessidades            1
2016   ninguém                 1
2016   noção                   1
2016   normalidade             1
2016   obteve                  1
2016   obtido                  1
2016   obtivemos               1
2016   ocultação               1
2016   oposição                1
2016   órgãos                  1
2016   orientados              1
2016   outubro                 1
2016   pagámos                 1
2016   pagar                   1
2016   página                  1
2016   pais                    1
2016   particular              1
2016   partidos                1
2016   passarem                1
2016   património              1
2016   paz                     1
2016   pedu                    1
2016   penhora                 1
2016   pensionistas            1
2016   período                 1
2016   permanente              1
2016   permitir                1
2016   permitiram              1
2016   pib                     1
2016   pilares                 1
2016   política                1
2016   populações              1
2016   porta                   1
2016   porto                   1
2016   posições                1
2016   possa                   1
2016   posteriores             1
2016   precariedade            1
2016   precisa                 1
2016   precisamente            1
2016   precisámos              1
2016   prestação               1
2016   previsões               1
2016   previsto                1
2016   previstos               1
2016   privada                 1
2016   procedimento            1
2016   procura                 1
2016   procuramos              1
2016   procurámos              1
2016   produz                  1
2016   prometêramos            1
2016   propósitos              1
2016   proximidade             1
2016   pt2020                  1
2016   qren                    1
2016   qualidade               1
2016   qualificações           1
2016   qualificado             1
2016   realidade               1
2016   reanimar                1
2016   reconhece               1
2016   reconhecemos            1
2016   reconhecido             1
2016   reconstruir             1
2016   recorde                 1
2016   recuperámos             1
2016   recursos                1
2016   rede                    1
2016   reduzimos               1
2016   reduzir                 1
2016   reduziu                 1
2016   reforçam                1
2016   reforma                 1
2016   regadios                1
2016   regionais               1
2016   reintroduzimos          1
2016   rejeitámos              1
2016   relativos               1
2016   relatório               1
2016   relevante               1
2016   relevantes              1
2016   representam             1
2016   repusemos               1
2016   requalificação          1
2016   respeitam               1
2016   respeitar               1
2016   respeito                1
2016   resposta                1
2016   restabelecendo          1
2016   restauração             1
2016   resultado               1
2016   revela                  1
2016   revisão                 1
2016   revolvidos              1
2016   rigor                   1
2016   rompe                   1
2016   rsi                     1
2016   rumo                    1
2016   sabemos                 1
2016   saída                   1
2016   sair                    1
2016   salario                 1
2016   saldo                   1
2016   salvaguarda             1
2016   seis                    1
2016   serenidade              1
2016   setor                   1
2016   setores                 1
2016   sistemas                1
2016   soberania               1
2016   sobressalto             1
2016   sobretaxa               1
2016   socias                  1
2016   solidariedade           1
2016   startup                 1
2016   sucesso                 1
2016   suficiente              1
2016   surpreende              1
2016   suspensos               1
2016   tarifa                  1
2016   taxas                   1
2016   tecido                  1
2016   temporário              1
2016   termo                   1
2016   termos                  1
2016   território              1
2016   tomar                   1
2016   trabalhadores           1
2016   transportes             1
2016   ultrapassaríamos        1
2016   união                   1
2016   única                   1
2016   unidade                 1
2016   unidades                1
2016   unilateral              1
2016   universalização         1
2016   urbanas                 1
2016   urbano                  1
2016   urbanos                 1
2016   veem                    1
2016   vencimentos             1
2016   verdadeiras             1
2016   virar                   1
2016   viu                     1
2016   vivam                   1
2016   vive                    1
2016   viver                   1
2016   vivia                   1
2016   vontade                 1
2017   1                       1
2017   100.000                 1
2017   15                      1
2017   15,4                    1
2017   17                      1
2017   173                     1
2017   175.000                 1
2017   18                      1
2017   19                      1
2017   1º                      1
2017   2                       1
2017   2017                    1
2017   2018                    1
2017   24                      1
2017   4                       1
2017   470.000                 1
2017   65                      1
2017   7                       1
2017   9,5                     1
2017   abalados                1
2017   aberto                  1
2017   abril                   1
2017   abriu                   1
2017   acaso                   1
2017   aceleração              1
2017   acessível               1
2017   acesso                  1
2017   aconteceu               1
2017   acorreram               1
2017   adalberto               1
2017   adiar                   1
2017   adjudicaram             1
2017   adotámos                1
2017   adversidade             1
2017   afetados                1
2017   agentes                 1
2017   ágil                    1
2017   agilizem                1
2017   ajustamento             1
2017   alargar                 1
2017   alcançámos              1
2017   aliás                   1
2017   alívio                  1
2017   alterações              1
2017   alto                    1
2017   altura                  1
2017   alunos                  1
2017   amanhã                  1
2017   ambicionamos            1
2017   ameaçadas               1
2017   amigos                  1
2017   andar                   1
2017   antevisão               1
2017   antigas                 1
2017   apelo                   1
2017   apoios                  1
2017   apreciação              1
2017   apresentar              1
2017   apresentarei            1
2017   aprofundaremos          1
2017   apurar                  1
2017   aquisição               1
2017   áreas                   1
2017   arranque                1
2017   arrendamento            1
2017   assegurando             1
2017   associativas            1
2017   ativa                   1
2017   atuais                  1
2017   atual                   1
2017   aumentar                1
2017   aumento                 1
2017   autocarros              1
2017   autónomas               1
2017   autonomia               1
2017   autonomização           1
2017   auxílio                 1
2017   avançados               1
2017   baixa                   1
2017   balanço                 1
2017   bases                   1
2017   beneficiando            1
2017   bens                    1
2017   brio                    1
2017   c’s                     1
2017   cabalmente              1
2017   cabos                   1
2017   cadastro                1
2017   campos                  1
2017   catástrofe              1
2017   catástrofes             1
2017   centralismo             1
2017   centro                  1
2017   centros                 1
2017   certo                   1
2017   chamas                  1
2017   cidades                 1
2017   científicos             1
2017   cinco                   1
2017   cívica                  1
2017   classes                 1
2017   clima                   1
2017   climáticas              1
2017   coesos                  1
2017   colocaremos             1
2017   combata                 1
2017   combate                 1
2017   combateram              1
2017   começarmos              1
2017   comissão                1
2017   comoção                 1
2017   competência             1
2017   competências            1
2017   competitivos            1
2017   comprometeram           1
2017   comprova                1
2017   comunicações            1
2017   concelhos               1
2017   concentração            1
2017   concertação             1
2017   concluir                1
2017   conclusão               1
2017   concretização           1
2017   condenando              1
2017   confiar                 1
2017   conhecer                1
2017   consensualizar          1
2017   consolidar              1
2017   constituída             1
2017   construir               1
2017   consumidores            1
2017   continuada              1
2017   continuará              1
2017   contrário               1
2017   contratação             1
2017   contribua               1
2017   contributos             1
2017   coragem                 1
2017   cortar                  1
2017   criação                 1
2017   criados                 1
2017   crianças                1
2017   criarmos                1
2017   crime                   1
2017   criou                   1
2017   cumpriram               1
2017   cumpriu                 1
2017   currículos              1
2017   custos                  1
2017   dará                    1
2017   décadas                 1
2017   decisiva                1
2017   decorrem                1
2017   defesa                  1
2017   défices                 1
2017   deixar                  1
2017   demarcação              1
2017   democracia              1
2017   descentralização        1
2017   descongelamento         1
2017   desemprego              1
2017   desigualdades           1
2017   deslocalizar            1
2017   destruído               1
2017   destruir                1
2017   devida                  1
2017   dignificação            1
2017   diminuição              1
2017   diminuir                1
2017   direito                 1
2017   diretamente             1
2017   dirigida                1
2017   discussão               1
2017   dizermos                1
2017   domínios                1
2017   dor                     1
2017   económicos              1
2017   educativos              1
2017   efeito                  1
2017   eficaz                  1
2017   emergência              1
2017   empenhados              1
2017   empobrecer              1
2017   empreendedor            1
2017   empresas                1
2017   encontra                1
2017   endividamento           1
2017   energia                 1
2017   equilíbrio              1
2017   esclarecer              1
2017   escolas                 1
2017   esforço                 1
2017   especial                1
2017   especializados          1
2017   espera                  1
2017   esperar                 1
2017   esquecer                1
2017   estando                 1
2017   estarmos                1
2017   estimule                1
2017   estradas                1
2017   estratégia              1
2017   estrategicamente        1
2017   estruturas              1
2017   estudos                 1
2017   eucalipto               1
2017   euro                    1
2017   evidenciando            1
2017   evitar                  1
2017   excessivo               1
2017   excessivos              1
2017   executar                1
2017   exercício               1
2017   exigido                 1
2017   expansão                1
2017   expectativas            1
2017   experiência             1
2017   exportação              1
2017   extraordinária          1
2017   factos                  1
2017   faleceram               1
2017   familiares              1
2017   fase                    1
2017   fernandes               1
2017   fibra                   1
2017   finanças                1
2017   flexível                1
2017   florestal               1
2017   florestas               1
2017   fogo                    1
2017   formação                1
2017   fortalecermos           1
2017   forte                   1
2017   freguesias              1
2017   fruto                   1
2017   função                  1
2017   funcionamento           1
2017   fundamentais            1
2017   fundio                  1
2017   garantir                1
2017   gera                    1
2017   gerais                  1
2017   gerou                   1
2017   gestão                  1
2017   globais                 1
2017   global                  1
2017   góis                    1
2017   governativo             1
2017   gratidão                1
2017   habitações              1
2017   hectares                1
2017   horizontais             1
2017   humana                  1
2017   ibérico                 1
2017   imediatamente           1
2017   imediatas               1
2017   impacto                 1
2017   impõe                   1
2017   importante              1
2017   importantes             1
2017   impressionante          1
2017   incêndios               1
2017   incentivo               1
2017   independente            1
2017   informação              1
2017   iniciámos               1
2017   inquérito               1
2017   inquéritos              1
2017   inserção                1
2017   instrumentos            1
2017   inteligente             1
2017   intensamente            1
2017   interioridade           1
2017   internacional           1
2017   internamente            1
2017   investigam              1
2017   investir                1
2017   junho                   1
2017   justiça                 1
2017   justificar              1
2017   laborais                1
2017   lamentar                1
2017   lançarmos               1
2017   lançou                  1
2017   letivo                  1
2017   libertarmos             1
2017   longo                   1
2017   maio                    1
2017   manutenção              1
2017   mar                     1
2017   marca                   1
2017   marcam                  1
2017   marcha                  1
2017   março                   1
2017   martirizados            1
2017   máximos                 1
2017   mecanismos              1
2017   média                   1
2017   médias                  1
2017   médio                   1
2017   medo                    1
2017   melhorada               1
2017   melhorou                1
2017   memória                 1
2017   mercado                 1
2017   mercados                1
2017   meta                    1
2017   metas                   1
2017   micro                   1
2017   milhões                 1
2017   mini                    1
2017   ministério              1
2017   ministro                1
2017   mobilização             1
2017   mobilizarem             1
2017   modernizarmos           1
2017   moderno                 1
2017   monocultura             1
2017   mostram                 1
2017   mostrando               1
2017   mudança                 1
2017   municípios              1
2017   nação                   1
2017   natureza                1
2017   necessária              1
2017   necessário              1
2017   necessidade             1
2017   necessidades            1
2017   nenhum                  1
2017   º                       1
2017   ocorrido                1
2017   oferta                  1
2017   oportunidades           1
2017   orçamentais             1
2017   organismos              1
2017   outubro                 1
2017   papel                   1
2017   parlamentares           1
2017   participação            1
2017   passámos                1
2017   pedagógica              1
2017   pensar                  1
2017   pensões                 1
2017   percorreu               1
2017   perdemos                1
2017   perder                  1
2017   permitam                1
2017   permitiram              1
2017   permitirão              1
2017   persistência            1
2017   persistindo             1
2017   pib                     1
2017   piloto                  1
2017   plantio                 1
2017   podermos                1
2017   população               1
2017   populações              1
2017   populares               1
2017   pôr                     1
2017   pós                     1
2017   pòs                     1
2017   possa                   1
2017   postos                  1
2017   precisamente            1
2017   preciso                 1
2017   prejuízo                1
2017   prepararmos             1
2017   previsibilidade         1
2017   primado                 1
2017   privadas                1
2017   procurando              1
2017   produtiva               1
2017   produz                  1
2017   produziram              1
2017   progresso               1
2017   projetarmos             1
2017   projeto                 1
2017   promoção                1
2017   promoveremos            1
2017   promovermos             1
2017   propriedades            1
2017   proprietários           1
2017   prosseguir              1
2017   qualidade               1
2017   qualificações           1
2017   qualificadas            1
2017   reabertura              1
2017   reafirmar               1
2017   receamos                1
2017   reclamar                1
2017   reconstrução            1
2017   reconstruir             1
2017   recuou                  1
2017   recuperação             1
2017   recuperaram             1
2017   redes                   1
2017   redução                 1
2017   reduzir                 1
2017   reforçando              1
2017   reforçar                1
2017   reforçarmos             1
2017   reforce                 1
2017   regiões                 1
2017   registando              1
2017   relativo                1
2017   rendimento              1
2017   rendimentos             1
2017   reordenamento           1
2017   reordenar               1
2017   replicar                1
2017   república               1
2017   responda                1
2017   respondendo             1
2017   responsabilidades       1
2017   respostas               1
2017   restabelecer            1
2017   restabelecidos          1
2017   retomando               1
2017   revitalização           1
2017   revitalizar             1
2017   revolta                 1
2017   revoltas                1
2017   satisfação              1
2017   saúde                   1
2017   secretaria              1
2017   século                  1
2017   seguir                  1
2017   segurança               1
2017   serviços                1
2017   sete                    1
2017   setor                   1
2017   simplex                 1
2017   simplificar             1
2017   simultaneamente         1
2017   sns                     1
2017   sociais                 1
2017   sociedade               1
2017   socorro                 1
2017   solicitou               1
2017   solidariedade           1
2017   soluções                1
2017   subitamente             1
2017   sucedida                1
2017   suficiente              1
2017   surgem                  1
2017   sustentada              1
2017   sustentadas             1
2017   sustentado              1
2017   sustentáveis            1
2017   tarefas                 1
2017   tarifários              1
2017   taxa                    1
2017   técnica                 1
2017   técnicos                1
2017   tensão                  1
2017   termos                  1
2017   terras                  1
2017   terreno                 1
2017   territoriais            1
2017   território              1
2017   terror                  1
2017   tido                    1
2017   total                   1
2017   trabalhadores           1
2017   trabalhamos             1
2017   trabalhos               1
2017   transparência           1
2017   transportes             1
2017   travar                  1
2017   três                    1
2017   turma                   1
2017   últimas                 1
2017   unirem                  1
2017   utentes                 1
2017   valorizarmos            1
2017   vencer                  1
2017   verdadeira              1
2017   volume                  1
2017   zona                    1
2018   0,9                     1
2018   1,5                     1
2018   1.000                   1
2018   100                     1
2018   11,2                    1
2018   113                     1
2018   12,6                    1
2018   15                      1
2018   17                      1
2018   190                     1
2018   1991                    1
2018   2,7                     1
2018   2.500                   1
2018   20                      1
2018   200                     1
2018   2002                    1
2018   22                      1
2018   246                     1
2018   250                     1
2018   300                     1
2018   302                     1
2018   4,7                     1
2018   4.0                     1
2018   40                      1
2018   41                      1
2018   45                      1
2018   5                       1
2018   50                      1
2018   68                      1
2018   7                       1
2018   7,2                     1
2018   7.000                   1
2018   7.900                   1
2018   700                     1
2018   80                      1
2018   85                      1
2018   9,1                     1
2018   90                      1
2018   95                      1
2018   abandono                1
2018   aberta                  1
2018   abrir                   1
2018   ação                    1
2018   acaso                   1
2018   aceitar                 1
2018   acentuada               1
2018   acessível               1
2018   acesso                  1
2018   acima                   1
2018   aconteceu               1
2018   acreditamos             1
2018   acrescentar             1
2018   acusação                1
2018   adiada                  1
2018   adiámos                 1
2018   aeroporto               1
2018   agricultura             1
2018   ajudando                1
2018   alargamento             1
2018   alargando               1
2018   alcançámos              1
2018   alcançar                1
2018   alexandre               1
2018   alqueva                 1
2018   alta                    1
2018   alunos                  1
2018   ampla                   1
2018   angular                 1
2018   anterior                1
2018   anual                   1
2018   anualmente              1
2018   apostámos               1
2018   apresentaremos          1
2018   aprovámos               1
2018   aprovar                 1
2018   aproveitamento          1
2018   aproximando             1
2018   arrancou                1
2018   arrendamento            1
2018   articulada              1
2018   assegura                1
2018   assegurar               1
2018   assistentes             1
2018   assumida                1
2018   assumirmos              1
2018   assumiu                 1
2018   atingir                 1
2018   ativa                   1
2018   atualizámos             1
2018   aumenta                 1
2018   aumentadas              1
2018   aumentámo               1
2018   aumentou                1
2018   autarquias              1
2018   avançámos               1
2018   avanços                 1
2018   baixa                   1
2018   baixas                  1
2018   baixos                  1
2018   bancário                1
2018   bases                   1
2018   básico                  1
2018   bens                    1
2018   biomassa                1
2018   cadastro                1
2018   caia                    1
2018   capacitar               1
2018   capitalizar             1
2018   captação                1
2018   carência                1
2018   carreiras               1
2018   carris                  1
2018   causa                   1
2018   central                 1
2018   centralidade            1
2018   centro                  1
2018   cidadão                 1
2018   cidadãos                1
2018   cirurgias               1
2018   claramente              1
2018   coeficiente             1
2018   colaborativos           1
2018   combate                 1
2018   combater                1
2018   comissão                1
2018   compensação             1
2018   completado              1
2018   completo                1
2018   componente              1
2018   conceção                1
2018   concentrámo             1
2018   conciliar               1
2018   conclusão               1
2018   concretizar             1
2018   concurso                1
2018   confirma                1
2018   conjunto                1
2018   conjuntura              1
2018   conquistámos            1
2018   consecutivo             1
2018   conseguido              1
2018   conservatório           1
2018   consolidação            1
2018   consolidando            1
2018   consolidar              1
2018   constante               1
2018   constitucional          1
2018   constituição            1
2018   constrói                1
2018   construção              1
2018   construímos             1
2018   consultas               1
2018   continuados             1
2018   continuar               1
2018   continuaremos           1
2018   contratando             1
2018   contratar               1
2018   contribui               1
2018   contribuições           1
2018   coragem                 1
2018   cortámos                1
2018   corte                   1
2018   cortes                  1
2018   credibilidade           1
2018   cresceram               1
2018   criados                 1
2018   criar                   1
2018   crucial                 1
2018   cuidados                1
2018   cumprir                 1
2018   custos                  1
2018   d                       1
2018   danos                   1
2018   decididamente           1
2018   decisão                 1
2018   decisivo                1
2018   decorre                 1
2018   decorrentes             1
2018   deixar                  1
2018   democracia              1
2018   demos                   1
2018   dependência             1
2018   desafios                1
2018   desbloqueámos           1
2018   descentralização        1
2018   descongelamento         1
2018   desejamos               1
2018   desempregados           1
2018   desencorajados          1
2018   desequilíbrios          1
2018   desígnio                1
2018   desinvestimento         1
2018   desmente                1
2018   destaca                 1
2018   destaque                1
2018   destruição              1
2018   desvalorizar            1
2018   determinação            1
2018   determinados            1
2018   determinante            1
2018   deu                     1
2018   dever                   1
2018   devolução               1
2018   devolvendo              1
2018   die                     1
2018   dificuldades            1
2018   digitalização           1
2018   digna                   1
2018   dimensão                1
2018   diminuição              1
2018   diminuíram              1
2018   dinamismo               1
2018   direitos                1
2018   discutir                1
2018   distante                1
2018   dividido                1
2018   dois                    1
2018   dotar                   1
2018   duplicação              1
2018   duração                 1
2018   duvidavam               1
2018   economicamente          1
2018   efeito                  1
2018   efeitos                 1
2018   eficaz                  1
2018   eficiente               1
2018   eleições                1
2018   elevados                1
2018   emigrantes              1
2018   emigrar                 1
2018   empreendedorismo        1
2018   empregadas              1
2018   encapotada              1
2018   encargos                1
2018   encontraram             1
2018   endividamento           1
2018   enfrente                1
2018   ensino                  1
2018   entidades               1
2018   entramos                1
2018   entrará                 1
2018   equilibrar              1
2018   equilíbrio              1
2018   escala                  1
2018   escalão                 1
2018   escolas                 1
2018   escolhemos              1
2018   esforço                 1
2018   esgotando               1
2018   espaços                 1
2018   espanha                 1
2018   especial                1
2018   especialmente           1
2018   específicos             1
2018   estaremos               1
2018   estarmos                1
2018   estímulo                1
2018   estratégica             1
2018   estratégicas            1
2018   estratégicos            1
2018   europa                  1
2018   europeia                1
2018   excelente               1
2018   excessivo               1
2018   excessivos              1
2018   exige                   1
2018   expansão                1
2018   exploradas              1
2018   exportações             1
2018   extensão                1
2018   extraordinário          1
2018   facto                   1
2018   falam                   1
2018   faremos                 1
2018   ferroviário             1
2018   ficar                   1
2018   finalmente              1
2018   financeira              1
2018   financiamento           1
2018   fiscal                  1
2018   fizemos                 1
2018   flexibilização          1
2018   florestais              1
2018   florestas               1
2018   focados                 1
2018   forçados                1
2018   formou                  1
2018   fragmentada             1
2018   freguesias              1
2018   frente                  1
2018   fronteira               1
2018   frotas                  1
2018   funcionamento           1
2018   fundamentais            1
2018   fundamental             1
2018   garanta                 1
2018   garante                 1
2018   georreferenciados       1
2018   gerações                1
2018   gini                    1
2018   governado               1
2018   graças                  1
2018   gratuitos               1
2018   habitacional            1
2018   hectares                1
2018   herculano               1
2018   homóloga                1
2018   honrando                1
2018   horizonte               1
2018   hospitalares            1
2018   humanos                 1
2018   i                       1
2018   ignorada                1
2018   igualmente              1
2018   impasses                1
2018   importância             1
2018   impulso                 1
2018   incentivos              1
2018   incluirá                1
2018   indexante               1
2018   indústria               1
2018   infelizmente            1
2018   inferior                1
2018   inflação                1
2018   iniciámos               1
2018   inovador                1
2018   instrumentos            1
2018   integrados              1
2018   interior                1
2018   internacional           1
2018   internacionalização     1
2018   investigação            1
2018   investimos              1
2018   investindo              1
2018   investiremos            1
2018   ip3                     1
2018   iremos                  1
2018   juros                   1
2018   laboratórios            1
2018   lançado                 1
2018   lançamento              1
2018   legislativa             1
2018   levando                 1
2018   levar                   1
2018   levou                   1
2018   libertarem              1
2018   ligações                1
2018   limite                  1
2018   linha                   1
2018   linhas                  1
2018   lista                   1
2018   lo                      1
2018   localização             1
2018   lojas                   1
2018   longa                   1
2018   longo                   1
2018   macroeconómicos         1
2018   manter                  1
2018   manuais                 1
2018   manutenção              1
2018   marcar                  1
2018   médico                  1
2018   medidas                 1
2018   médio                   1
2018   meios                   1
2018   melhorar                1
2018   melhorem                1
2018   mercado                 1
2018   mergulhado              1
2018   meta                    1
2018   metro                   1
2018   metropolitanas          1
2018   metros                  1
2018   minho                   1
2018   minimalista             1
2018   mínimo                  1
2018   mínimos                 1
2018   moderadoras             1
2018   modernizar              1
2018   mondego                 1
2018   motivados               1
2018   motor                   1
2018   mudar                   1
2018   muitas                  1
2018   municípios              1
2018   nação                   1
2018   naturalmente            1
2018   navios                  1
2018   necessárias             1
2018   necessários             1
2018   normalidade             1
2018   norte                   1
2018   objetivo                1
2018   obras                   1
2018   obrigados               1
2018   operacionais            1
2018   orçamental              1
2018   ordenamento             1
2018   p.p                     1
2018   página                  1
2018   países                  1
2018   papel                   1
2018   par                     1
2018   parcial                 1
2018   parlamento              1
2018   participação            1
2018   participada             1
2018   particular              1
2018   património              1
2018   pedagógica              1
2018   pedra                   1
2018   penalizadores           1
2018   percentuais             1
2018   permitam                1
2018   permitem                1
2018   permitiu                1
2018   persistência            1
2018   peso                    1
2018   pessoal                 1
2018   piloto                  1
2018   planeamento             1
2018   planos                  1
2018   plenamente              1
2018   pleno                   1
2018   pnpot                   1
2018   política                1
2018   população               1
2018   pôr                     1
2018   portuguesas             1
2018   possíveis               1
2018   prazo                   1
2018   precários               1
2018   preciso                 1
2018   precoce                 1
2018   prédios                 1
2018   prepara                 1
2018   preparar                1
2018   preparem                1
2018   prestação               1
2018   principais              1
2018   prioridades             1
2018   procederemos            1
2018   procurado               1
2018   procuram                1
2018   professores             1
2018   profissionais           1
2018   profissional            1
2018   profunda                1
2018   progredindo             1
2018   progressivo             1
2018   progresso               1
2018   projeto                 1
2018   promovam                1
2018   promovendo              1
2018   promover                1
2018   proposta                1
2018   prosseguindo            1
2018   prosseguir              1
2018   quadruplicar            1
2018   quatro                  1
2018   quebrado                1
2018   queda                   1
2018   questionavam            1
2018   reabertura              1
2018   reafirmar               1
2018   real                    1
2018   realidade               1
2018   realizado               1
2018   realizarem              1
2018   receitas                1
2018   recrutamento            1
2018   recuando                1
2018   recuperámos             1
2018   rede                    1
2018   redes                   1
2018   reduzido                1
2018   reduzimos               1
2018   reencontrarmos          1
2018   reforça                 1
2018   reforçada               1
2018   reforçámos              1
2018   reforçar                1
2018   regadios                1
2018   regiões                 1
2018   regionais               1
2018   registado               1
2018   registando              1
2018   registou                1
2018   regresso                1
2018   rejeitamos              1
2018   rejeitar                1
2018   rejuvenescer            1
2018   relançar                1
2018   relativa                1
2018   relevante               1
2018   renunciado              1
2018   repetido                1
2018   replicando              1
2018   repondo                 1
2018   reposição               1
2018   repusemos               1
2018   requalificação          1
2018   respeita                1
2018   responder               1
2018   resultado               1
2018   resultam                1
2018   resultariam             1
2018   resumo                  1
2018   retirar                 1
2018   retrato                 1
2018   retrocesso              1
2018   revertemos              1
2018   revitalização           1
2018   rigorosa                1
2018   risco                   1
2018   ritmo                   1
2018   rodovia                 1
2018   rodoviária              1
2018   rompendo                1
2018   rumo                    1
2018   rural                   1
2018   sabemos                 1
2018   sabor                   1
2018   sacrificar              1
2018   salário                 1
2018   século                  1
2018   secundária              1
2018   secundário              1
2018   seguido                 1
2018   sentido                 1
2018   serviço                 1
2018   sessão                  1
2018   setembro                1
2018   si                      1
2018   significativo           1
2018   símbolos                1
2018   simplificámos           1
2018   sine                    1
2018   sines                   1
2018   sinistralidade          1
2018   situação                1
2018   sobressalto             1
2018   soflusa                 1
2018   stcp                    1
2018   subsídios               1
2018   suma                    1
2018   superior                1
2018   suporta                 1
2018   sustentada              1
2018   sustentado              1
2018   taxa                    1
2018   taxas                   1
2018   tecido                  1
2018   tendo                   1
2018   termo                   1
2018   termos                  1
2018   tornado                 1
2018   tradução                1
2018   trajeto                 1
2018   transferência           1
2018   transição               1
2018   transportes             1
2018   transtejo               1
2018   transversal             1
2018   trata                   1
2018   tribunais               1
2018   turma                   1
2018   última                  1
2018   ultrapassar             1
2018   única                   1
2018   unidades                1
2018   universalização         1
2018   urbana                  1
2018   valores                 1
2018   valorização             1
2018   valorizar               1
2018   vamos                   1
2018   variação                1
2018   verdadeiros             1
2018   via                     1
2018   viabilizado             1
2018   viabilizou              1
2018   viável                  1
2018   viram                   1
2018   virar                   1
2018   visão                   1
2018   vista                   1
2018   voltámos                1
2018   voltassem               1
2018   zif                     1
2019   0,5                     1
2019   1.000                   1
2019   11                      1
2019   119                     1
2019   121,5                   1
2019   15                      1
2019   18                      1
2019   180                     1
2019   1º                      1
2019   2.000                   1
2019   2019                    1
2019   2050                    1
2019   22                      1
2019   250                     1
2019   28                      1
2019   2º                      1
2019   300.000                 1
2019   305                     1
2019   31                      1
2019   35                      1
2019   350.000                 1
2019   382                     1
2019   3º                      1
2019   45                      1
2019   64                      1
2019   8,2                     1
2019   89                      1
2019   9                       1
2019   93                      1
2019   abandono                1
2019   abono                   1
2019   abrange                 1
2019   abrir                   1
2019   abriu                   1
2019   ação                    1
2019   acesso                  1
2019   adjudicar               1
2019   admitimos               1
2019   adotadas                1
2019   adotar                  1
2019   adultos                 1
2019   afirmação               1
2019   afirmar                 1
2019   agenda                  1
2019   agendamento             1
2019   ágil                    1
2019   agimos                  1
2019   alargado                1
2019   alargámos               1
2019   alargando               1
2019   alcançado               1
2019   alcançámos              1
2019   alcançando              1
2019   aliviámos               1
2019   alterações              1
2019   alunos                  1
2019   anacrónico              1
2019   anterior                1
2019   antídoto                1
2019   anunciar                1
2019   apareceu                1
2019   apoiado                 1
2019   apoiar                  1
2019   apresentámos            1
2019   aprofundando            1
2019   aprovações              1
2019   aquisição               1
2019   armadas                 1
2019   arrastaram              1
2019   assegurámos             1
2019   assumi                  1
2019   assumido                1
2019   atraiu                  1
2019   atrevo                  1
2019   autoestima              1
2019   autonomia               1
2019   autonomias              1
2019   azul                    1
2019   baixa                   1
2019   básico                  1
2019   be                      1
2019   bloqueia                1
2019   calendário              1
2019   caminhos                1
2019   canavezes               1
2019   capacidade              1
2019   capitalização           1
2019   carbónica               1
2019   cartões                 1
2019   castelopermitirá        1
2019   cede                    1
2019   celeridade              1
2019   centrais                1
2019   chave                   1
2019   ciência                 1
2019   circulam                1
2019   círculo                 1
2019   claros                  1
2019   climáticas              1
2019   co2                     1
2019   coesão                  1
2019   colegas                 1
2019   colocando               1
2019   começámos               1
2019   comissão                1
2019   compara                 1
2019   compete                 1
2019   competitividade         1
2019   concessão               1
2019   conciliação             1
2019   concludentes            1
2019   conclusão               1
2019   configuram              1
2019   confirma                1
2019   conflito                1
2019   conhecemos              1
2019   conhecimento            1
2019   conjuntas               1
2019   conjunto                1
2019   conquistou              1
2019   consciência             1
2019   conseguimos             1
2019   consenso                1
2019   considerou              1
2019   consistência            1
2019   constituímos            1
2019   construíram             1
2019   conta                   1
2019   contactar               1
2019   contínua                1
2019   contratos               1
2019   contributo              1
2019   convergência            1
2019   cooperação              1
2019   cor                     1
2019   corte                   1
2019   cp                      1
2019   credibilidade           1
2019   cresce                  1
2019   crescemos               1
2019   crescer                 1
2019   criados                 1
2019   criámos                 1
2019   cuidar                  1
2019   cultura                 1
2019   cumprimos               1
2019   cumprindo               1
2019   curto                   1
2019   custo                   1
2019   dados                   1
2019   décimas                 1
2019   declarada               1
2019   definição               1
2019   deixaram                1
2019   democrática             1
2019   demográfico             1
2019   demonstram              1
2019   demos                   1
2019   deram                   1
2019   derrubar                1
2019   desafios                1
2019   desassossego            1
2019   descentralização        1
2019   descrença               1
2019   desejavam               1
2019   desenvolvidos           1
2019   desenvolvimento         1
2019   designadamente          1
2019   desinvestimento         1
2019   desistimos              1
2019   deslocar                1
2019   destruição              1
2019   determinação            1
2019   determinante            1
2019   dezenas                 1
2019   diabo                   1
2019   diáspora                1
2019   difícil                 1
2019   dignidade               1
2019   dimensão                1
2019   direto                  1
2019   discriminações          1
2019   discussão               1
2019   disfarçou               1
2019   dispensa                1
2019   dispõem                 1
2019   disponibilizámos        1
2019   distanciamento          1
2019   dívida                  1
2019   dois                    1
2019   douro                   1
2019   drasticamente           1
2019   duplicou                1
2019   duradoura               1
2019   efetiva                 1
2019   elaborámos              1
2019   elementos               1
2019   eletrificação           1
2019   elevada                 1
2019   eliminámos              1
2019   emigração               1
2019   emissões                1
2019   emitidos                1
2019   empreendedorismo        1
2019   empregos                1
2019   empresarial             1
2019   empresas                1
2019   encara                  1
2019   encerradas              1
2019   encerramento            1
2019   enorme                  1
2019   entendido               1
2019   equipamento             1
2019   escola                  1
2019   escolares               1
2019   escrupuloso             1
2019   espaço                  1
2019   espera                  1
2019   esquecer                1
2019   essencial               1
2019   estabilidade            1
2019   estancou                1
2019   estável                 1
2019   estimadas               1
2019   estónia                 1
2019   estrangeiro             1
2019   estratégia              1
2019   estratégica             1
2019   estudantes              1
2019   eurobarómetro           1
2019   europeia                1
2019   evidência               1
2019   excessivo               1
2019   execução                1
2019   executar                1
2019   exemplos                1
2019   exigência               1
2019   exportações             1
2019   facto                   1
2019   familiar                1
2019   fator                   1
2019   fatores                 1
2019   feito                   1
2019   férias                  1
2019   ferroviário             1
2019   financeira              1
2019   financiado              1
2019   financiamento           1
2019   fiscalização            1
2019   fizemos                 1
2019   floresta                1
2019   focados                 1
2019   formação                1
2019   formações               1
2019   forte                   1
2019   fugimos                 1
2019   função                  1
2019   funcionamento           1
2019   ganho                   1
2019   ganhos                  1
2019   garantia                1
2019   garantimos              1
2019   garantiu                1
2019   geração                 1
2019   gerar                   1
2019   gestão                  1
2019   governámos              1
2019   gratuitos               1
2019   grupos                  1
2019   habitação               1
2019   harmonia                1
2019   história                1
2019   históricos              1
2019   honrar                  1
2019   horizonte               1
2019   hospital                1
2019   ideia                   1
2019   identidade              1
2019   imobilizadas            1
2019   imobilizado             1
2019   impasse                 1
2019   implementámos           1
2019   impressivo              1
2019   incentivo               1
2019   incerteza               1
2019   inclusão                1
2019   incógnita               1
2019   inconstitucional        1
2019   independência           1
2019   induz                   1
2019   infraestruturas         1
2019   início                  1
2019   inovador                1
2019   inovadoras              1
2019   inspirada               1
2019   instabilidade           1
2019   instituições            1
2019   integralmente           1
2019   interior                1
2019   internacionais          1
2019   interrompida            1
2019   investimos              1
2019   irs                     1
2019   islândia                1
2019   joão                    1
2019   junho                   1
2019   juro                    1
2019   juros                   1
2019   lançou                  1
2019   legítima                1
2019   lei                     1
2019   leis                    1
2019   liberdade               1
2019   libertaram              1
2019   líder                   1
2019   linhas                  1
2019   líquido                 1
2019   lixo                    1
2019   locais                  1
2019   maio                    1
2019   maiores                 1
2019   manifestas              1
2019   manuais                 1
2019   manutenção              1
2019   marca                   1
2019   marco                   1
2019   massiva                 1
2019   maternidades            1
2019   melhorando              1
2019   melhores                1
2019   melhorou                1
2019   mensal                  1
2019   mercado                 1
2019   metas                   1
2019   metro                   1
2019   metros                  1
2019   migratórios             1
2019   milhões                 1
2019   militar                 1
2019   militares               1
2019   mina                    1
2019   minho                   1
2019   mínimo                  1
2019   ministro                1
2019   modelo                  1
2019   moderação               1
2019   moderadoras             1
2019   morosa                  1
2019   motivação               1
2019   motivou                 1
2019   motor                   1
2019   mundo                   1
2019   muro                    1
2019   nação                   1
2019   nacionalidad            1
2019   necessária              1
2019   necessariamente         1
2019   necessário              1
2019   necessários             1
2019   necessidades            1
2019   nelas                   1
2019   neutralidade            1
2019   norma                   1
2019   noruega                 1
2019   notações                1
2019   notável                 1
2019   oásis                   1
2019   obras                   1
2019   ocorrendo               1
2019   oe                      1
2019   oeste                   1
2019   online                  1
2019   orçamento               1
2019   orgulhoso               1
2019   orientação              1
2019   otimismo                1
2019   ousado                  1
2019   outrem                  1
2019   pagar                   1
2019   pagávamos               1
2019   página                  1
2019   painel                  1
2019   par                     1
2019   parlamentares           1
2019   participação            1
2019   partilho                1
2019   passada                 1
2019   pcp                     1
2019   pedido                  1
2019   pendências              1
2019   percorrer               1
2019   percorrido              1
2019   permanente              1
2019   permitam                1
2019   permite                 1
2019   permitindo              1
2019   permitirá               1
2019   pessimismo              1
2019   pessoal                 1
2019   pev                     1
2019   pib                     1
2019   pme’s                   1
2019   pobres                  1
2019   pobreza                 1
2019   podemos                 1
2019   podíamos                1
2019   político                1
2019   pontuais                1
2019   populações              1
2019   populismo               1
2019   porto                   1
2019   português               1
2019   portuguesas             1
2019   posições                1
2019   positivo                1
2019   positivos               1
2019   postos                  1
2019   precariedade            1
2019   precisar                1
2019   precisava               1
2019   precoce                 1
2019   prescindem              1
2019   presente                1
2019   prestação               1
2019   prestar                 1
2019   prestígio               1
2019   preventiva              1
2019   preventivamente         1
2019   previram                1
2019   previstas               1
2019   principal               1
2019   principalmente          1
2019   privação                1
2019   procedimento            1
2019   processo                1
2019   processuais             1
2019   procurado               1
2019   produtividade           1
2019   profissionais           1
2019   profissional            1
2019   programação             1
2019   programado              1
2019   progressos              1
2019   projeta                 1
2019   projeto                 1
2019   prometemos              1
2019   promoveu                1
2019   propondo                1
2019   prosseguido             1
2019   prosseguir              1
2019   provam                  1
2019   provámos                1
2019   provou                  1
2019   ps                      1
2019   pt2020                  1
2019   público                 1
2019   quase                   1
2019   queremos                1
2019   quotas                  1
2019   quotidiano              1
2019   rácio                   1
2019   reais                   1
2019   real                    1
2019   recentes                1
2019   reclassificados         1
2019   record                  1
2019   recuar                  1
2019   recuperou               1
2019   reforçar                1
2019   reforma                 1
2019   regadio                 1
2019   regionais               1
2019   registou                1
2019   reinstalámos            1
2019   renovada                1
2019   reparar                 1
2019   repor                   1
2019   república               1
2019   reserva                 1
2019   resignamos              1
2019   responsabilidade        1
2019   resposta                1
2019   retificativos           1
2019   retomado                1
2019   retomando               1
2019   revertemos              1
2019   ricos                   1
2019   risco                   1
2019   rosa                    1
2019   roteiro                 1
2019   s                       1
2019   sabemos                 1
2019   sair                    1
2019   saíram                  1
2019   salário                 1
2019   saldos                  1
2019   saudar                  1
2019   sazonais                1
2019   século                  1
2019   secundário              1
2019   seguramente             1
2019   seguro                  1
2019   semana                  1
2019   semanas                 1
2019   serem                   1
2019   sermos                  1
2019   servem                  1
2019   serviço                 1
2019   serviram                1
2019   severa                  1
2019   sexual                  1
2019   simplex                 1
2019   simplificámos           1
2019   simultaneamente         1
2019   simultâneo              1
2019   sinalização             1
2019   síntese                 1
2019   sistemas                1
2019   sns                     1
2019   soberania               1
2019   sobressalto             1
2019   sobretudo               1
2019   soflusa                 1
2019   sólida                  1
2019   sólidas                 1
2019   subida                  1
2019   subiu                   1
2019   subsistem               1
2019   suécia                  1
2019   superiores              1
2019   sustenta                1
2019   sustentado              1
2019   tantos                  1
2019   taxa                    1
2019   tempos                  1
2019   tendo                   1
2019   terem                   1
2019   termo                   1
2019   trabalhadores           1
2019   tranquilidade           1
2019   tribunal                1
2019   tripla                  1
2019   unicef                  1
2019   único                   1
2019   universalizar           1
2019   valores                 1
2019   valorização             1
2019   viana                   1
2019   virar                   1
2019   virtuoso                1
2019   visão                   1
2019   visto                   1
2019   vitalidade              1
2019   vive                    1
2019   vivem                   1
2019   vivemos                 1
2019   viver                   1
2019   vontade                 1

</div>

```r
word_log_odds <- top_palavras %>%
  bind_log_odds(ano, word, n) 

word_log_odds %>%
  arrange(-log_odds)
```

<div class="kable-table">

ano    word                    n     log_odds
-----  --------------------  ---  -----------
2019   quatro                 21    3.6174541
2016   cumprimos              13    3.0739757
2016   agir                    6    2.1869664
2018   continuidade            8    2.1802960
2018   orçamento               9    2.1052583
2019   anos                   23    2.0103254
2016   euros                   5    1.9959342
2016   projetos                4    1.7847853
2017   estrutural              4    1.7793543
2016   milhões                 6    1.7792334
2016   problemas               9    1.6753042
2019   composições             4    1.6296677
2019   ue                      4    1.6296677
2019   legislatura            12    1.5905240
2016   face                    3    1.5452951
2016   financeiro              3    1.5452951
2016   modernização            3    1.5452951
2017   económico               3    1.5405930
2018   voltar                  4    1.5402441
2016   medidas                 7    1.5320930
2016   orçamental              4    1.5277660
2016   prometemos              4    1.5277660
2017   floresta                4    1.5214542
2018   investimento           24    1.4451052
2019   agradecer               3    1.4109974
2019   cartão                  3    1.4109974
2019   certas                  3    1.4109974
2019   possibilidade           3    1.4109974
2019   renovação               3    1.4109974
2019   solução                 3    1.4109974
2019   sustentável             3    1.4109974
2018   três                    7    1.4077914
2019   confiança              14    1.4020400
2019   melhor                 12    1.3900831
2017   convergência            5    1.3757119
2019   2018                    4    1.3474948
2019   contas                  5    1.3437944
2019   democracia              5    1.3437944
2018   alcançados              3    1.3335753
2018   aprovaremos             3    1.3335753
2018   baixou                  3    1.3335753
2018   cresceu                 3    1.3335753
2018   crise                   3    1.3335753
2018   despesa                 3    1.3335753
2018   melhoria                3    1.3335753
2018   recuos                  3    1.3335753
2018   recuperar               3    1.3335753
2016   incentivo               4    1.3130069
2016   reabilitação            4    1.3130069
2017   podemos                 7    1.2857531
2016   adicionais              2    1.2614226
2016   administrações          2    1.2614226
2016   candidaturas            2    1.2614226
2016   comunitários            2    1.2614226
2016   concursos               2    1.2614226
2016   dias                    2    1.2614226
2016   discordar               2    1.2614226
2016   domínio                 2    1.2614226
2016   dotação                 2    1.2614226
2016   elevado                 2    1.2614226
2016   funcionários            2    1.2614226
2016   fundo                   2    1.2614226
2016   fundos                  2    1.2614226
2016   ignoramos               2    1.2614226
2016   iniciativa              2    1.2614226
2016   janeiro                 2    1.2614226
2016   palavra                 2    1.2614226
2016   poderem                 2    1.2614226
2016   pusemos                 2    1.2614226
2016   resolver                2    1.2614226
2016   sido                    2    1.2614226
2017   abrindo                 2    1.2575845
2017   afinal                  2    1.2575845
2017   assenta                 2    1.2575845
2017   atingiu                 2    1.2575845
2017   avaliação               2    1.2575845
2017   carga                   2    1.2575845
2017   colaboração             2    1.2575845
2017   eixos                   2    1.2575845
2017   enfrentar               2    1.2575845
2017   externa                 2    1.2575845
2017   indicadores             2    1.2575845
2017   interna                 2    1.2575845
2017   matriz                  2    1.2575845
2017   oportunidade            2    1.2575845
2017   passageiros             2    1.2575845
2017   pedrógão                2    1.2575845
2017   prioritária             2    1.2575845
2017   territórios             2    1.2575845
2017   viagens                 2    1.2575845
2016   100                     3    1.2568666
2016   aceleração              3    1.2568666
2016   criar                   3    1.2568666
2016   eliminámos              3    1.2568666
2016   global                  3    1.2568666
2016   planos                  3    1.2568666
2016   urbana                  3    1.2568666
2017   capacidade              3    1.2511870
2017   exige                   3    1.2511870
2017   gerações                3    1.2511870
2017   previsto                3    1.2511870
2018   amanhã                  4    1.2435704
2018   áreas                   4    1.2435704
2018   jovens                  4    1.2435704
2016   compromissos            5    1.2304026
2019   continuar               9    1.2299204
2018   público                 6    1.2221590
2018   serviços                6    1.2221590
2018   dívida                  5    1.2195190
2019   portugal               12    1.2075340
2016   empresas                6    1.1997859
2016   execução                7    1.1924453
2019   2014                    2    1.1518001
2019   30                      2    1.1518001
2019   ala                     2    1.1518001
2019   alternativas            2    1.1518001
2019   capaz                   2    1.1518001
2019   circulação              2    1.1518001
2019   diferente               2    1.1518001
2019   digital                 2    1.1518001
2019   edifício                2    1.1518001
2019   escolher                2    1.1518001
2019   fatalismo               2    1.1518001
2019   forças                  2    1.1518001
2019   fortemente              2    1.1518001
2019   garantindo              2    1.1518001
2019   género                  2    1.1518001
2019   impostos                2    1.1518001
2019   inevitabilidade         2    1.1518001
2019   negamos                 2    1.1518001
2019   orçamentos              2    1.1518001
2019   pediátrica              2    1.1518001
2019   renovar                 2    1.1518001
2019   triplo                  2    1.1518001
2016   país                   14    1.1424221
2016   1                       4    1.1327006
2016   2016                    4    1.1327006
2019   alternativa             4    1.1126603
2019   igualdade               7    1.1121412
2019   constitucional          3    1.0946500
2019   justiça                 3    1.0946500
2019   permitiu                3    1.0946500
2019   procuramos              3    1.0946500
2019   respeito                3    1.0946500
2018   2030                    2    1.0886024
2018   acelerámos              2    1.0886024
2018   adiado                  2    1.0886024
2018   aprovado                2    1.0886024
2018   base                    2    1.0886024
2018   beira                   2    1.0886024
2018   caso                    2    1.0886024
2018   circulante              2    1.0886024
2018   concentrar              2    1.0886024
2018   conseguiríamos          2    1.0886024
2018   críticas                2    1.0886024
2018   dando                   2    1.0886024
2018   descarbonização         2    1.0886024
2018   descida                 2    1.0886024
2018   empobrecimento          2    1.0886024
2018   ferrovia                2    1.0886024
2018   ligação                 2    1.0886024
2018   metade                  2    1.0886024
2018   ministros               2    1.0886024
2018   modo                    2    1.0886024
2018   mudámos                 2    1.0886024
2018   percurso                2    1.0886024
2018   possam                  2    1.0886024
2018   potencial               2    1.0886024
2018   produtivo               2    1.0886024
2018   programas               2    1.0886024
2018   prosseguimos            2    1.0886024
2018   prosseguiremos          2    1.0886024
2018   proteção                2    1.0886024
2018   quadros                 2    1.0886024
2018   reduzindo               2    1.0886024
2018   reprogramação           2    1.0886024
2018   resolução               2    1.0886024
2018   visa                    2    1.0886024
2017   cidadãos                5    1.0855883
2017   públicas                5    1.0855883
2016   ambição                 3    1.0297319
2016   lançámos                3    1.0297319
2016   maio                    3    1.0297319
2016   marca                   3    1.0297319
2017   abandono                3    1.0233047
2018   2017                    5    1.0179514
2018   2                       3    1.0011328
2018   bloqueios               3    1.0011328
2018   competências            3    1.0011328
2018   conseguimos             3    1.0011328
2018   cultura                 3    1.0011328
2018   estabilidade            3    1.0011328
2018   florestal               3    1.0011328
2018   garantir                3    1.0011328
2018   mobilidade              3    1.0011328
2018   promoção                3    1.0011328
2018   administração           4    0.9971625
2018   criação                 4    0.9971625
2018   passo                   4    0.9971625
2017   continuar               7    0.9938006
2016   assente                 4    0.9786552
2016   visão                   4    0.9786552
2017   valorização             4    0.9705012
2017   debate                  5    0.9638033
2016   agimos                  2    0.9277482
2016   baixos                  2    0.9277482
2016   capitalizar             2    0.9277482
2016   clima                   2    0.9277482
2016   concretizar             2    0.9277482
2017   carreiras               2    0.9227921
2017   carris                  2    0.9227921
2017   contexto                2    0.9227921
2017   continuaremos           2    0.9227921
2017   dever                   2    0.9227921
2017   difícil                 2    0.9227921
2017   finalmente              2    0.9227921
2017   fiscal                  2    0.9227921
2017   funções                 2    0.9227921
2017   garanta                 2    0.9227921
2017   intervenção             2    0.9227921
2017   iremos                  2    0.9227921
2017   missão                  2    0.9227921
2017   precisámos              2    0.9227921
2017   promovendo              2    0.9227921
2017   saída                   2    0.9227921
2017   sermos                  2    0.9227921
2017   stcp                    2    0.9227921
2017   união                   2    0.9227921
2017   unidade                 2    0.9227921
2019   aumentámos              4    0.9151510
2019   balanço                 4    0.9151510
2019   segurança               4    0.9151510
2016   194                     1    0.8917446
2016   2008                    1    0.8917446
2016   3,2                     1    0.8917446
2016   400                     1    0.8917446
2016   450                     1    0.8917446
2016   454                     1    0.8917446
2016   5,5                     1    0.8917446
2016   600                     1    0.8917446
2016   abertura                1    0.8917446
2016   acabámos                1    0.8917446
2016   acelerador              1    0.8917446
2016   acordo                  1    0.8917446
2016   adotamos                1    0.8917446
2016   agindo                  1    0.8917446
2016   alteração               1    0.8917446
2016   ambiente                1    0.8917446
2016   âmbito                  1    0.8917446
2016   ancora                  1    0.8917446
2016   antecipação             1    0.8917446
2016   antecipem               1    0.8917446
2016   ap                      1    0.8917446
2016   apresenta               1    0.8917446
2016   apresentando            1    0.8917446
2016   aprofunda               1    0.8917446
2016   àqueles                 1    0.8917446
2016   arautos                 1    0.8917446
2016   assegure                1    0.8917446
2016   assegurem               1    0.8917446
2016   associe                 1    0.8917446
2016   assumidos               1    0.8917446
2016   assumimos               1    0.8917446
2016   assumir                 1    0.8917446
2016   atingido                1    0.8917446
2016   atribuição              1    0.8917446
2016   avisos                  1    0.8917446
2016   b                       1    0.8917446
2016   bacias                  1    0.8917446
2016   baixámos                1    0.8917446
2016   baixaram                1    0.8917446
2016   basta                   1    0.8917446
2016   casas                   1    0.8917446
2016   celebrámos              1    0.8917446
2016   chegar                  1    0.8917446
2016   científico              1    0.8917446
2016   circular                1    0.8917446
2016   cláusula                1    0.8917446
2016   coerente                1    0.8917446
2016   comprometido            1    0.8917446
2016   comunidades             1    0.8917446
2016   concretiza              1    0.8917446
2016   confirmados             1    0.8917446
2016   conjuntamente           1    0.8917446
2016   conseguiremos           1    0.8917446
2016   consistente             1    0.8917446
2016   constatarem             1    0.8917446
2016   constituir              1    0.8917446
2016   construindo             1    0.8917446
2016   contrária               1    0.8917446
2016   contribuição            1    0.8917446
2016   cooperam                1    0.8917446
2016   coro                    1    0.8917446
2016   correspondem            1    0.8917446
2016   corresponderam          1    0.8917446
2016   correspondesse          1    0.8917446
2016   credibilizar            1    0.8917446
2016   criando                 1    0.8917446
2016   csi                     1    0.8917446
2016   cubra                   1    0.8917446
2016   declarou                1    0.8917446
2016   democrático             1    0.8917446
2016   desbloquear             1    0.8917446
2016   desenhar                1    0.8917446
2016   desfavorecidas          1    0.8917446
2016   desgraça                1    0.8917446
2016   desmentir               1    0.8917446
2016   diálogo                 1    0.8917446
2016   dinamizar               1    0.8917446
2016   disse                   1    0.8917446
2016   diversidade             1    0.8917446
2016   diversificado           1    0.8917446
2016   economias               1    0.8917446
2016   eficiência              1    0.8917446
2016   elegemos                1    0.8917446
2016   empreendedores          1    0.8917446
2016   encenação               1    0.8917446
2016   energética              1    0.8917446
2016   enfrentados             1    0.8917446
2016   enfrentam               1    0.8917446
2016   enfrentando             1    0.8917446
2016   enormes                 1    0.8917446
2016   erradicação             1    0.8917446
2016   escondem                1    0.8917446
2016   escondemos              1    0.8917446
2016   estabelecemos           1    0.8917446
2016   estabilizar             1    0.8917446
2016   excecional              1    0.8917446
2016   executados              1    0.8917446
2016   existem                 1    0.8917446
2016   existiam                1    0.8917446
2016   faça                    1    0.8917446
2016   favorável               1    0.8917446
2016   feriados                1    0.8917446
2016   financiar               1    0.8917446
2016   fomentar                1    0.8917446
2016   fonte                   1    0.8917446
2016   fundaram                1    0.8917446
2016   grau                    1    0.8917446
2016   greve                   1    0.8917446
2016   herdámos                1    0.8917446
2016   hidrográficas           1    0.8917446
2016   histórico               1    0.8917446
2016   ia                      1    0.8917446
2016   imi                     1    0.8917446
2016   incentivem              1    0.8917446
2016   ine                     1    0.8917446
2016   iniciando               1    0.8917446
2016   interesse               1    0.8917446
2016   interesses              1    0.8917446
2016   investe                 1    0.8917446
2016   investidores            1    0.8917446
2016   iva                     1    0.8917446
2016   judiciário              1    0.8917446
2016   justifica               1    0.8917446
2016   levará                  1    0.8917446
2016   limitações              1    0.8917446
2016   limpa                   1    0.8917446
2016   majoração               1    0.8917446
2016   manifestada             1    0.8917446
2016   mapa                    1    0.8917446
2016   máxima                  1    0.8917446
2016   mecanismo               1    0.8917446
2016   medida                  1    0.8917446
2016   morada                  1    0.8917446
2016   move                    1    0.8917446
2016   municipal               1    0.8917446
2016   municipalização         1    0.8917446
2016   nacionais               1    0.8917446
2016   ninguém                 1    0.8917446
2016   noção                   1    0.8917446
2016   obteve                  1    0.8917446
2016   obtido                  1    0.8917446
2016   obtivemos               1    0.8917446
2016   ocultação               1    0.8917446
2016   oposição                1    0.8917446
2016   órgãos                  1    0.8917446
2016   orientados              1    0.8917446
2016   pagámos                 1    0.8917446
2016   pais                    1    0.8917446
2016   partidos                1    0.8917446
2016   passarem                1    0.8917446
2016   paz                     1    0.8917446
2016   pedu                    1    0.8917446
2016   penhora                 1    0.8917446
2016   pensionistas            1    0.8917446
2016   período                 1    0.8917446
2016   permitir                1    0.8917446
2016   pilares                 1    0.8917446
2016   porta                   1    0.8917446
2016   posteriores             1    0.8917446
2016   precisa                 1    0.8917446
2016   previsões               1    0.8917446
2016   previstos               1    0.8917446
2016   privada                 1    0.8917446
2016   procura                 1    0.8917446
2016   procurámos              1    0.8917446
2016   prometêramos            1    0.8917446
2016   propósitos              1    0.8917446
2016   qren                    1    0.8917446
2016   qualificado             1    0.8917446
2016   reanimar                1    0.8917446
2016   reconhece               1    0.8917446
2016   reconhecemos            1    0.8917446
2016   reconhecido             1    0.8917446
2016   recorde                 1    0.8917446
2016   reforçam                1    0.8917446
2016   reintroduzimos          1    0.8917446
2016   rejeitámos              1    0.8917446
2016   relativos               1    0.8917446
2016   relatório               1    0.8917446
2016   relevantes              1    0.8917446
2016   representam             1    0.8917446
2016   respeitam               1    0.8917446
2016   respeitar               1    0.8917446
2016   restabelecendo          1    0.8917446
2016   restauração             1    0.8917446
2016   revela                  1    0.8917446
2016   revisão                 1    0.8917446
2016   revolvidos              1    0.8917446
2016   rigor                   1    0.8917446
2016   rompe                   1    0.8917446
2016   rsi                     1    0.8917446
2016   salario                 1    0.8917446
2016   saldo                   1    0.8917446
2016   salvaguarda             1    0.8917446
2016   seis                    1    0.8917446
2016   serenidade              1    0.8917446
2016   setores                 1    0.8917446
2016   sobretaxa               1    0.8917446
2016   socias                  1    0.8917446
2016   startup                 1    0.8917446
2016   surpreende              1    0.8917446
2016   suspensos               1    0.8917446
2016   tarifa                  1    0.8917446
2016   temporário              1    0.8917446
2016   tomar                   1    0.8917446
2016   ultrapassaríamos        1    0.8917446
2016   unilateral              1    0.8917446
2016   urbanas                 1    0.8917446
2016   urbano                  1    0.8917446
2016   urbanos                 1    0.8917446
2016   veem                    1    0.8917446
2016   vencimentos             1    0.8917446
2016   verdadeiras             1    0.8917446
2016   viu                     1    0.8917446
2016   vivam                   1    0.8917446
2016   vivia                   1    0.8917446
2017   100.000                 1    0.8890314
2017   15,4                    1    0.8890314
2017   173                     1    0.8890314
2017   175.000                 1    0.8890314
2017   24                      1    0.8890314
2017   470.000                 1    0.8890314
2017   65                      1    0.8890314
2017   9,5                     1    0.8890314
2017   abalados                1    0.8890314
2017   aberto                  1    0.8890314
2017   acorreram               1    0.8890314
2017   adalberto               1    0.8890314
2017   adiar                   1    0.8890314
2017   adjudicaram             1    0.8890314
2017   adversidade             1    0.8890314
2017   afetados                1    0.8890314
2017   agentes                 1    0.8890314
2017   agilizem                1    0.8890314
2017   ajustamento             1    0.8890314
2017   alargar                 1    0.8890314
2017   aliás                   1    0.8890314
2017   alívio                  1    0.8890314
2017   alto                    1    0.8890314
2017   altura                  1    0.8890314
2017   ambicionamos            1    0.8890314
2017   ameaçadas               1    0.8890314
2017   amigos                  1    0.8890314
2017   andar                   1    0.8890314
2017   antevisão               1    0.8890314
2017   antigas                 1    0.8890314
2017   apelo                   1    0.8890314
2017   apreciação              1    0.8890314
2017   apresentarei            1    0.8890314
2017   aprofundaremos          1    0.8890314
2017   apurar                  1    0.8890314
2017   arranque                1    0.8890314
2017   assegurando             1    0.8890314
2017   associativas            1    0.8890314
2017   atuais                  1    0.8890314
2017   atual                   1    0.8890314
2017   autocarros              1    0.8890314
2017   autónomas               1    0.8890314
2017   autonomização           1    0.8890314
2017   auxílio                 1    0.8890314
2017   avançados               1    0.8890314
2017   beneficiando            1    0.8890314
2017   brio                    1    0.8890314
2017   c’s                     1    0.8890314
2017   cabalmente              1    0.8890314
2017   cabos                   1    0.8890314
2017   campos                  1    0.8890314
2017   catástrofe              1    0.8890314
2017   catástrofes             1    0.8890314
2017   centralismo             1    0.8890314
2017   certo                   1    0.8890314
2017   chamas                  1    0.8890314
2017   cidades                 1    0.8890314
2017   científicos             1    0.8890314
2017   cinco                   1    0.8890314
2017   cívica                  1    0.8890314
2017   classes                 1    0.8890314
2017   coesos                  1    0.8890314
2017   colocaremos             1    0.8890314
2017   combata                 1    0.8890314
2017   combateram              1    0.8890314
2017   começarmos              1    0.8890314
2017   comoção                 1    0.8890314
2017   competência             1    0.8890314
2017   competitivos            1    0.8890314
2017   comprometeram           1    0.8890314
2017   comprova                1    0.8890314
2017   comunicações            1    0.8890314
2017   concelhos               1    0.8890314
2017   concentração            1    0.8890314
2017   concluir                1    0.8890314
2017   concretização           1    0.8890314
2017   condenando              1    0.8890314
2017   consensualizar          1    0.8890314
2017   constituída             1    0.8890314
2017   consumidores            1    0.8890314
2017   continuada              1    0.8890314
2017   continuará              1    0.8890314
2017   contratação             1    0.8890314
2017   contribua               1    0.8890314
2017   contributos             1    0.8890314
2017   cortar                  1    0.8890314
2017   criarmos                1    0.8890314
2017   crime                   1    0.8890314
2017   cumpriram               1    0.8890314
2017   cumpriu                 1    0.8890314
2017   currículos              1    0.8890314
2017   dará                    1    0.8890314
2017   décadas                 1    0.8890314
2017   decisiva                1    0.8890314
2017   decorrem                1    0.8890314
2017   défices                 1    0.8890314
2017   demarcação              1    0.8890314
2017   deslocalizar            1    0.8890314
2017   destruído               1    0.8890314
2017   destruir                1    0.8890314
2017   devida                  1    0.8890314
2017   dignificação            1    0.8890314
2017   diretamente             1    0.8890314
2017   dirigida                1    0.8890314
2017   dizermos                1    0.8890314
2017   domínios                1    0.8890314
2017   dor                     1    0.8890314
2017   económicos              1    0.8890314
2017   educativos              1    0.8890314
2017   emergência              1    0.8890314
2017   empenhados              1    0.8890314
2017   empobrecer              1    0.8890314
2017   empreendedor            1    0.8890314
2017   encontra                1    0.8890314
2017   esclarecer              1    0.8890314
2017   especializados          1    0.8890314
2017   esperar                 1    0.8890314
2017   estando                 1    0.8890314
2017   estimule                1    0.8890314
2017   estradas                1    0.8890314
2017   estrategicamente        1    0.8890314
2017   estruturas              1    0.8890314
2017   estudos                 1    0.8890314
2017   eucalipto               1    0.8890314
2017   evidenciando            1    0.8890314
2017   evitar                  1    0.8890314
2017   exercício               1    0.8890314
2017   exigido                 1    0.8890314
2017   expectativas            1    0.8890314
2017   experiência             1    0.8890314
2017   exportação              1    0.8890314
2017   factos                  1    0.8890314
2017   faleceram               1    0.8890314
2017   familiares              1    0.8890314
2017   fase                    1    0.8890314
2017   fernandes               1    0.8890314
2017   fibra                   1    0.8890314
2017   flexível                1    0.8890314
2017   fogo                    1    0.8890314
2017   fortalecermos           1    0.8890314
2017   fruto                   1    0.8890314
2017   fundio                  1    0.8890314
2017   gera                    1    0.8890314
2017   gerais                  1    0.8890314
2017   globais                 1    0.8890314
2017   góis                    1    0.8890314
2017   governativo             1    0.8890314
2017   gratidão                1    0.8890314
2017   habitações              1    0.8890314
2017   horizontais             1    0.8890314
2017   humana                  1    0.8890314
2017   ibérico                 1    0.8890314
2017   imediatamente           1    0.8890314
2017   imediatas               1    0.8890314
2017   impacto                 1    0.8890314
2017   impõe                   1    0.8890314
2017   importante              1    0.8890314
2017   importantes             1    0.8890314
2017   impressionante          1    0.8890314
2017   incêndios               1    0.8890314
2017   independente            1    0.8890314
2017   informação              1    0.8890314
2017   inquérito               1    0.8890314
2017   inquéritos              1    0.8890314
2017   inserção                1    0.8890314
2017   inteligente             1    0.8890314
2017   intensamente            1    0.8890314
2017   interioridade           1    0.8890314
2017   internamente            1    0.8890314
2017   investigam              1    0.8890314
2017   justificar              1    0.8890314
2017   laborais                1    0.8890314
2017   lamentar                1    0.8890314
2017   lançarmos               1    0.8890314
2017   libertarmos             1    0.8890314
2017   marcam                  1    0.8890314
2017   marcha                  1    0.8890314
2017   março                   1    0.8890314
2017   martirizados            1    0.8890314
2017   máximos                 1    0.8890314
2017   mecanismos              1    0.8890314
2017   médias                  1    0.8890314
2017   medo                    1    0.8890314
2017   melhorada               1    0.8890314
2017   memória                 1    0.8890314
2017   mercados                1    0.8890314
2017   micro                   1    0.8890314
2017   mini                    1    0.8890314
2017   ministério              1    0.8890314
2017   mobilização             1    0.8890314
2017   mobilizarem             1    0.8890314
2017   modernizarmos           1    0.8890314
2017   moderno                 1    0.8890314
2017   monocultura             1    0.8890314
2017   mostram                 1    0.8890314
2017   mostrando               1    0.8890314
2017   natureza                1    0.8890314
2017   necessidade             1    0.8890314
2017   nenhum                  1    0.8890314
2017   ocorrido                1    0.8890314
2017   oferta                  1    0.8890314
2017   orçamentais             1    0.8890314
2017   organismos              1    0.8890314
2017   passámos                1    0.8890314
2017   pensar                  1    0.8890314
2017   percorreu               1    0.8890314
2017   perdemos                1    0.8890314
2017   perder                  1    0.8890314
2017   permitirão              1    0.8890314
2017   persistindo             1    0.8890314
2017   plantio                 1    0.8890314
2017   podermos                1    0.8890314
2017   populares               1    0.8890314
2017   pós                     1    0.8890314
2017   pòs                     1    0.8890314
2017   prejuízo                1    0.8890314
2017   prepararmos             1    0.8890314
2017   primado                 1    0.8890314
2017   privadas                1    0.8890314
2017   procurando              1    0.8890314
2017   produtiva               1    0.8890314
2017   produziram              1    0.8890314
2017   projetarmos             1    0.8890314
2017   promoveremos            1    0.8890314
2017   promovermos             1    0.8890314
2017   propriedades            1    0.8890314
2017   proprietários           1    0.8890314
2017   qualificadas            1    0.8890314
2017   receamos                1    0.8890314
2017   reclamar                1    0.8890314
2017   reconstrução            1    0.8890314
2017   reforçarmos             1    0.8890314
2017   reforce                 1    0.8890314
2017   relativo                1    0.8890314
2017   reordenamento           1    0.8890314
2017   reordenar               1    0.8890314
2017   replicar                1    0.8890314
2017   responda                1    0.8890314
2017   respondendo             1    0.8890314
2017   responsabilidades       1    0.8890314
2017   respostas               1    0.8890314
2017   restabelecer            1    0.8890314
2017   restabelecidos          1    0.8890314
2017   revitalizar             1    0.8890314
2017   revolta                 1    0.8890314
2017   revoltas                1    0.8890314
2017   satisfação              1    0.8890314
2017   secretaria              1    0.8890314
2017   seguir                  1    0.8890314
2017   sete                    1    0.8890314
2017   simplificar             1    0.8890314
2017   socorro                 1    0.8890314
2017   solicitou               1    0.8890314
2017   soluções                1    0.8890314
2017   subitamente             1    0.8890314
2017   sucedida                1    0.8890314
2017   surgem                  1    0.8890314
2017   sustentadas             1    0.8890314
2017   sustentáveis            1    0.8890314
2017   tarefas                 1    0.8890314
2017   tarifários              1    0.8890314
2017   técnica                 1    0.8890314
2017   técnicos                1    0.8890314
2017   tensão                  1    0.8890314
2017   terras                  1    0.8890314
2017   terreno                 1    0.8890314
2017   territoriais            1    0.8890314
2017   terror                  1    0.8890314
2017   tido                    1    0.8890314
2017   total                   1    0.8890314
2017   trabalhamos             1    0.8890314
2017   trabalhos               1    0.8890314
2017   transparência           1    0.8890314
2017   travar                  1    0.8890314
2017   últimas                 1    0.8890314
2017   unirem                  1    0.8890314
2017   utentes                 1    0.8890314
2017   valorizarmos            1    0.8890314
2017   vencer                  1    0.8890314
2017   verdadeira              1    0.8890314
2017   volume                  1    0.8890314
2019   futuro                  8    0.8473075
2016   queremos                3    0.8471227
2016   reformas                3    0.8471227
2019   bases                   3    0.8461633
2019   linha                   3    0.8461633
2019   reduzimos               3    0.8461633
2019   reduzir                 3    0.8461633
2016   desenvolvimento         4    0.8447257
2016   investimentos           4    0.8447257
2016   º                       4    0.8447257
2018   austeridade             5    0.8411457
2018   pública                 5    0.8411457
2017   conhecimento            3    0.8400648
2017   interior                3    0.8400648
2019   portugueses            11    0.8149168
2019   0,5                     1    0.8142517
2019   11                      1    0.8142517
2019   119                     1    0.8142517
2019   121,5                   1    0.8142517
2019   180                     1    0.8142517
2019   2.000                   1    0.8142517
2019   2050                    1    0.8142517
2019   28                      1    0.8142517
2019   2º                      1    0.8142517
2019   300.000                 1    0.8142517
2019   305                     1    0.8142517
2019   31                      1    0.8142517
2019   35                      1    0.8142517
2019   350.000                 1    0.8142517
2019   382                     1    0.8142517
2019   3º                      1    0.8142517
2019   64                      1    0.8142517
2019   8,2                     1    0.8142517
2019   89                      1    0.8142517
2019   9                       1    0.8142517
2019   93                      1    0.8142517
2019   abrange                 1    0.8142517
2019   adjudicar               1    0.8142517
2019   admitimos               1    0.8142517
2019   adotadas                1    0.8142517
2019   adotar                  1    0.8142517
2019   afirmação               1    0.8142517
2019   afirmar                 1    0.8142517
2019   agenda                  1    0.8142517
2019   agendamento             1    0.8142517
2019   alargado                1    0.8142517
2019   alcançado               1    0.8142517
2019   alcançando              1    0.8142517
2019   aliviámos               1    0.8142517
2019   anacrónico              1    0.8142517
2019   antídoto                1    0.8142517
2019   anunciar                1    0.8142517
2019   apareceu                1    0.8142517
2019   apoiado                 1    0.8142517
2019   apoiar                  1    0.8142517
2019   aprofundando            1    0.8142517
2019   aprovações              1    0.8142517
2019   armadas                 1    0.8142517
2019   arrastaram              1    0.8142517
2019   assegurámos             1    0.8142517
2019   assumi                  1    0.8142517
2019   atraiu                  1    0.8142517
2019   atrevo                  1    0.8142517
2019   autoestima              1    0.8142517
2019   be                      1    0.8142517
2019   bloqueia                1    0.8142517
2019   calendário              1    0.8142517
2019   caminhos                1    0.8142517
2019   canavezes               1    0.8142517
2019   carbónica               1    0.8142517
2019   cartões                 1    0.8142517
2019   castelopermitirá        1    0.8142517
2019   cede                    1    0.8142517
2019   celeridade              1    0.8142517
2019   centrais                1    0.8142517
2019   chave                   1    0.8142517
2019   circulam                1    0.8142517
2019   círculo                 1    0.8142517
2019   claros                  1    0.8142517
2019   co2                     1    0.8142517
2019   colegas                 1    0.8142517
2019   colocando               1    0.8142517
2019   começámos               1    0.8142517
2019   compara                 1    0.8142517
2019   compete                 1    0.8142517
2019   concessão               1    0.8142517
2019   conciliação             1    0.8142517
2019   concludentes            1    0.8142517
2019   configuram              1    0.8142517
2019   conflito                1    0.8142517
2019   conhecemos              1    0.8142517
2019   conquistou              1    0.8142517
2019   consciência             1    0.8142517
2019   consenso                1    0.8142517
2019   considerou              1    0.8142517
2019   consistência            1    0.8142517
2019   constituímos            1    0.8142517
2019   construíram             1    0.8142517
2019   conta                   1    0.8142517
2019   contactar               1    0.8142517
2019   contínua                1    0.8142517
2019   contributo              1    0.8142517
2019   cooperação              1    0.8142517
2019   cor                     1    0.8142517
2019   cp                      1    0.8142517
2019   cresce                  1    0.8142517
2019   crescemos               1    0.8142517
2019   cuidar                  1    0.8142517
2019   cumprindo               1    0.8142517
2019   curto                   1    0.8142517
2019   custo                   1    0.8142517
2019   décimas                 1    0.8142517
2019   declarada               1    0.8142517
2019   definição               1    0.8142517
2019   democrática             1    0.8142517
2019   demográfico             1    0.8142517
2019   demonstram              1    0.8142517
2019   deram                   1    0.8142517
2019   derrubar                1    0.8142517
2019   desassossego            1    0.8142517
2019   descrença               1    0.8142517
2019   desejavam               1    0.8142517
2019   desenvolvidos           1    0.8142517
2019   designadamente          1    0.8142517
2019   desistimos              1    0.8142517
2019   deslocar                1    0.8142517
2019   dezenas                 1    0.8142517
2019   diabo                   1    0.8142517
2019   diáspora                1    0.8142517
2019   dignidade               1    0.8142517
2019   direto                  1    0.8142517
2019   discriminações          1    0.8142517
2019   disfarçou               1    0.8142517
2019   dispensa                1    0.8142517
2019   dispõem                 1    0.8142517
2019   disponibilizámos        1    0.8142517
2019   distanciamento          1    0.8142517
2019   douro                   1    0.8142517
2019   drasticamente           1    0.8142517
2019   duplicou                1    0.8142517
2019   duradoura               1    0.8142517
2019   efetiva                 1    0.8142517
2019   elaborámos              1    0.8142517
2019   elementos               1    0.8142517
2019   eletrificação           1    0.8142517
2019   elevada                 1    0.8142517
2019   emigração               1    0.8142517
2019   emissões                1    0.8142517
2019   emitidos                1    0.8142517
2019   encara                  1    0.8142517
2019   encerradas              1    0.8142517
2019   encerramento            1    0.8142517
2019   enorme                  1    0.8142517
2019   entendido               1    0.8142517
2019   equipamento             1    0.8142517
2019   escolares               1    0.8142517
2019   escrupuloso             1    0.8142517
2019   espaço                  1    0.8142517
2019   estancou                1    0.8142517
2019   estimadas               1    0.8142517
2019   estónia                 1    0.8142517
2019   estrangeiro             1    0.8142517
2019   estudantes              1    0.8142517
2019   eurobarómetro           1    0.8142517
2019   evidência               1    0.8142517
2019   exemplos                1    0.8142517
2019   exigência               1    0.8142517
2019   fator                   1    0.8142517
2019   fatores                 1    0.8142517
2019   férias                  1    0.8142517
2019   financiado              1    0.8142517
2019   fiscalização            1    0.8142517
2019   formações               1    0.8142517
2019   fugimos                 1    0.8142517
2019   ganho                   1    0.8142517
2019   ganhos                  1    0.8142517
2019   garantia                1    0.8142517
2019   garantimos              1    0.8142517
2019   garantiu                1    0.8142517
2019   gerar                   1    0.8142517
2019   governámos              1    0.8142517
2019   grupos                  1    0.8142517
2019   harmonia                1    0.8142517
2019   história                1    0.8142517
2019   históricos              1    0.8142517
2019   hospital                1    0.8142517
2019   ideia                   1    0.8142517
2019   imobilizadas            1    0.8142517
2019   imobilizado             1    0.8142517
2019   impasse                 1    0.8142517
2019   implementámos           1    0.8142517
2019   impressivo              1    0.8142517
2019   incerteza               1    0.8142517
2019   inclusão                1    0.8142517
2019   incógnita               1    0.8142517
2019   inconstitucional        1    0.8142517
2019   independência           1    0.8142517
2019   induz                   1    0.8142517
2019   infraestruturas         1    0.8142517
2019   inovadoras              1    0.8142517
2019   inspirada               1    0.8142517
2019   instabilidade           1    0.8142517
2019   instituições            1    0.8142517
2019   integralmente           1    0.8142517
2019   internacionais          1    0.8142517
2019   interrompida            1    0.8142517
2019   islândia                1    0.8142517
2019   joão                    1    0.8142517
2019   juro                    1    0.8142517
2019   legítima                1    0.8142517
2019   lei                     1    0.8142517
2019   leis                    1    0.8142517
2019   liberdade               1    0.8142517
2019   libertaram              1    0.8142517
2019   líder                   1    0.8142517
2019   líquido                 1    0.8142517
2019   lixo                    1    0.8142517
2019   maiores                 1    0.8142517
2019   manifestas              1    0.8142517
2019   marco                   1    0.8142517
2019   massiva                 1    0.8142517
2019   maternidades            1    0.8142517
2019   melhorando              1    0.8142517
2019   mensal                  1    0.8142517
2019   migratórios             1    0.8142517
2019   militar                 1    0.8142517
2019   militares               1    0.8142517
2019   mina                    1    0.8142517
2019   moderação               1    0.8142517
2019   morosa                  1    0.8142517
2019   motivação               1    0.8142517
2019   motivou                 1    0.8142517
2019   mundo                   1    0.8142517
2019   muro                    1    0.8142517
2019   nacionalidad            1    0.8142517
2019   necessariamente         1    0.8142517
2019   nelas                   1    0.8142517
2019   neutralidade            1    0.8142517
2019   norma                   1    0.8142517
2019   noruega                 1    0.8142517
2019   notações                1    0.8142517
2019   notável                 1    0.8142517
2019   oásis                   1    0.8142517
2019   ocorrendo               1    0.8142517
2019   oe                      1    0.8142517
2019   oeste                   1    0.8142517
2019   online                  1    0.8142517
2019   orgulhoso               1    0.8142517
2019   orientação              1    0.8142517
2019   otimismo                1    0.8142517
2019   ousado                  1    0.8142517
2019   outrem                  1    0.8142517
2019   pagávamos               1    0.8142517
2019   painel                  1    0.8142517
2019   partilho                1    0.8142517
2019   passada                 1    0.8142517
2019   pcp                     1    0.8142517
2019   pedido                  1    0.8142517
2019   pendências              1    0.8142517
2019   percorrido              1    0.8142517
2019   permite                 1    0.8142517
2019   permitindo              1    0.8142517
2019   pessimismo              1    0.8142517
2019   pev                     1    0.8142517
2019   pme’s                   1    0.8142517
2019   pobres                  1    0.8142517
2019   podíamos                1    0.8142517
2019   político                1    0.8142517
2019   pontuais                1    0.8142517
2019   populismo               1    0.8142517
2019   português               1    0.8142517
2019   positivo                1    0.8142517
2019   positivos               1    0.8142517
2019   precisar                1    0.8142517
2019   precisava               1    0.8142517
2019   prescindem              1    0.8142517
2019   presente                1    0.8142517
2019   prestar                 1    0.8142517
2019   prestígio               1    0.8142517
2019   preventiva              1    0.8142517
2019   preventivamente         1    0.8142517
2019   previram                1    0.8142517
2019   previstas               1    0.8142517
2019   principal               1    0.8142517
2019   principalmente          1    0.8142517
2019   privação                1    0.8142517
2019   processuais             1    0.8142517
2019   produtividade           1    0.8142517
2019   programação             1    0.8142517
2019   programado              1    0.8142517
2019   progressos              1    0.8142517
2019   projeta                 1    0.8142517
2019   promoveu                1    0.8142517
2019   propondo                1    0.8142517
2019   prosseguido             1    0.8142517
2019   provam                  1    0.8142517
2019   provámos                1    0.8142517
2019   provou                  1    0.8142517
2019   ps                      1    0.8142517
2019   quase                   1    0.8142517
2019   quotas                  1    0.8142517
2019   quotidiano              1    0.8142517
2019   rácio                   1    0.8142517
2019   reais                   1    0.8142517
2019   recentes                1    0.8142517
2019   reclassificados         1    0.8142517
2019   record                  1    0.8142517
2019   recuar                  1    0.8142517
2019   recuperou               1    0.8142517
2019   regadio                 1    0.8142517
2019   reinstalámos            1    0.8142517
2019   renovada                1    0.8142517
2019   reparar                 1    0.8142517
2019   repor                   1    0.8142517
2019   reserva                 1    0.8142517
2019   resignamos              1    0.8142517
2019   responsabilidade        1    0.8142517
2019   retificativos           1    0.8142517
2019   retomado                1    0.8142517
2019   ricos                   1    0.8142517
2019   rosa                    1    0.8142517
2019   roteiro                 1    0.8142517
2019   s                       1    0.8142517
2019   saíram                  1    0.8142517
2019   saldos                  1    0.8142517
2019   saudar                  1    0.8142517
2019   sazonais                1    0.8142517
2019   seguramente             1    0.8142517
2019   seguro                  1    0.8142517
2019   semana                  1    0.8142517
2019   semanas                 1    0.8142517
2019   serem                   1    0.8142517
2019   servem                  1    0.8142517
2019   serviram                1    0.8142517
2019   severa                  1    0.8142517
2019   sexual                  1    0.8142517
2019   simultâneo              1    0.8142517
2019   sinalização             1    0.8142517
2019   síntese                 1    0.8142517
2019   sobretudo               1    0.8142517
2019   sólida                  1    0.8142517
2019   sólidas                 1    0.8142517
2019   subida                  1    0.8142517
2019   subiu                   1    0.8142517
2019   subsistem               1    0.8142517
2019   suécia                  1    0.8142517
2019   superiores              1    0.8142517
2019   sustenta                1    0.8142517
2019   tantos                  1    0.8142517
2019   tempos                  1    0.8142517
2019   terem                   1    0.8142517
2019   tranquilidade           1    0.8142517
2019   tribunal                1    0.8142517
2019   tripla                  1    0.8142517
2019   unicef                  1    0.8142517
2019   único                   1    0.8142517
2019   universalizar           1    0.8142517
2019   viana                   1    0.8142517
2019   virtuoso                1    0.8142517
2019   visto                   1    0.8142517
2019   vitalidade              1    0.8142517
2019   vivem                   1    0.8142517
2019   vivemos                 1    0.8142517
2018   essencial               4    0.7897359
2018   pib                     4    0.7897359
2018   pobreza                 4    0.7897359
2018   rendimentos             4    0.7897359
2018   sociedade               4    0.7897359
2018   territorial             4    0.7897359
2019   20                      2    0.7861941
2019   2015                    2    0.7861941
2019   aumentou                2    0.7861941
2019   cidadão                 2    0.7861941
2019   constituição            2    0.7861941
2019   crianças                2    0.7861941
2019   criou                   2    0.7861941
2019   cumprir                 2    0.7861941
2019   desígnio                2    0.7861941
2019   ensino                  2    0.7861941
2019   esperança               2    0.7861941
2019   gerou                   2    0.7861941
2019   institucional           2    0.7861941
2019   média                   2    0.7861941
2019   navios                  2    0.7861941
2019   países                  2    0.7861941
2019   previsibilidade         2    0.7861941
2019   recuperaram             2    0.7861941
2019   reduziu                 2    0.7861941
2019   reforça                 2    0.7861941
2019   responder               2    0.7861941
2019   situação                2    0.7861941
2019   suma                    2    0.7861941
2019   superior                2    0.7861941
2019   transtejo               2    0.7861941
2018   0,9                     1    0.7695763
2018   1,5                     1    0.7695763
2018   11,2                    1    0.7695763
2018   113                     1    0.7695763
2018   12,6                    1    0.7695763
2018   190                     1    0.7695763
2018   1991                    1    0.7695763
2018   2,7                     1    0.7695763
2018   2.500                   1    0.7695763
2018   2002                    1    0.7695763
2018   246                     1    0.7695763
2018   300                     1    0.7695763
2018   302                     1    0.7695763
2018   4,7                     1    0.7695763
2018   40                      1    0.7695763
2018   41                      1    0.7695763
2018   5                       1    0.7695763
2018   50                      1    0.7695763
2018   68                      1    0.7695763
2018   7,2                     1    0.7695763
2018   7.000                   1    0.7695763
2018   7.900                   1    0.7695763
2018   700                     1    0.7695763
2018   80                      1    0.7695763
2018   85                      1    0.7695763
2018   9,1                     1    0.7695763
2018   95                      1    0.7695763
2018   aberta                  1    0.7695763
2018   aceitar                 1    0.7695763
2018   acentuada               1    0.7695763
2018   acreditamos             1    0.7695763
2018   acrescentar             1    0.7695763
2018   acusação                1    0.7695763
2018   adiada                  1    0.7695763
2018   adiámos                 1    0.7695763
2018   aeroporto               1    0.7695763
2018   agricultura             1    0.7695763
2018   ajudando                1    0.7695763
2018   alargamento             1    0.7695763
2018   alcançar                1    0.7695763
2018   alexandre               1    0.7695763
2018   alta                    1    0.7695763
2018   ampla                   1    0.7695763
2018   angular                 1    0.7695763
2018   anual                   1    0.7695763
2018   anualmente              1    0.7695763
2018   apostámos               1    0.7695763
2018   apresentaremos          1    0.7695763
2018   aproveitamento          1    0.7695763
2018   aproximando             1    0.7695763
2018   arrancou                1    0.7695763
2018   articulada              1    0.7695763
2018   assegura                1    0.7695763
2018   assegurar               1    0.7695763
2018   assistentes             1    0.7695763
2018   assumida                1    0.7695763
2018   assumirmos              1    0.7695763
2018   aumenta                 1    0.7695763
2018   aumentadas              1    0.7695763
2018   aumentámo               1    0.7695763
2018   avançámos               1    0.7695763
2018   avanços                 1    0.7695763
2018   baixas                  1    0.7695763
2018   bancário                1    0.7695763
2018   biomassa                1    0.7695763
2018   caia                    1    0.7695763
2018   capacitar               1    0.7695763
2018   captação                1    0.7695763
2018   carência                1    0.7695763
2018   causa                   1    0.7695763
2018   centralidade            1    0.7695763
2018   cirurgias               1    0.7695763
2018   claramente              1    0.7695763
2018   coeficiente             1    0.7695763
2018   colaborativos           1    0.7695763
2018   combater                1    0.7695763
2018   completado              1    0.7695763
2018   completo                1    0.7695763
2018   componente              1    0.7695763
2018   conceção                1    0.7695763
2018   concentrámo             1    0.7695763
2018   conciliar               1    0.7695763
2018   concurso                1    0.7695763
2018   conjuntura              1    0.7695763
2018   conquistámos            1    0.7695763
2018   consecutivo             1    0.7695763
2018   conseguido              1    0.7695763
2018   conservatório           1    0.7695763
2018   consolidando            1    0.7695763
2018   constante               1    0.7695763
2018   constrói                1    0.7695763
2018   construímos             1    0.7695763
2018   consultas               1    0.7695763
2018   contratando             1    0.7695763
2018   contribui               1    0.7695763
2018   contribuições           1    0.7695763
2018   cortámos                1    0.7695763
2018   cresceram               1    0.7695763
2018   d                       1    0.7695763
2018   danos                   1    0.7695763
2018   decididamente           1    0.7695763
2018   decisão                 1    0.7695763
2018   decisivo                1    0.7695763
2018   decorre                 1    0.7695763
2018   decorrentes             1    0.7695763
2018   desbloqueámos           1    0.7695763
2018   desejamos               1    0.7695763
2018   desempregados           1    0.7695763
2018   desencorajados          1    0.7695763
2018   desmente                1    0.7695763
2018   destaca                 1    0.7695763
2018   destaque                1    0.7695763
2018   desvalorizar            1    0.7695763
2018   determinados            1    0.7695763
2018   deu                     1    0.7695763
2018   devolução               1    0.7695763
2018   devolvendo              1    0.7695763
2018   die                     1    0.7695763
2018   digitalização           1    0.7695763
2018   digna                   1    0.7695763
2018   diminuíram              1    0.7695763
2018   dinamismo               1    0.7695763
2018   discutir                1    0.7695763
2018   distante                1    0.7695763
2018   dividido                1    0.7695763
2018   dotar                   1    0.7695763
2018   duplicação              1    0.7695763
2018   duração                 1    0.7695763
2018   duvidavam               1    0.7695763
2018   economicamente          1    0.7695763
2018   eficiente               1    0.7695763
2018   elevados                1    0.7695763
2018   emigrantes              1    0.7695763
2018   emigrar                 1    0.7695763
2018   empregadas              1    0.7695763
2018   encapotada              1    0.7695763
2018   encargos                1    0.7695763
2018   encontraram             1    0.7695763
2018   enfrente                1    0.7695763
2018   entidades               1    0.7695763
2018   entramos                1    0.7695763
2018   entrará                 1    0.7695763
2018   equilibrar              1    0.7695763
2018   escala                  1    0.7695763
2018   escalão                 1    0.7695763
2018   escolhemos              1    0.7695763
2018   esgotando               1    0.7695763
2018   espaços                 1    0.7695763
2018   espanha                 1    0.7695763
2018   especialmente           1    0.7695763
2018   específicos             1    0.7695763
2018   estaremos               1    0.7695763
2018   estímulo                1    0.7695763
2018   estratégicas            1    0.7695763
2018   excelente               1    0.7695763
2018   exploradas              1    0.7695763
2018   extensão                1    0.7695763
2018   extraordinário          1    0.7695763
2018   falam                   1    0.7695763
2018   faremos                 1    0.7695763
2018   ficar                   1    0.7695763
2018   flexibilização          1    0.7695763
2018   florestais              1    0.7695763
2018   forçados                1    0.7695763
2018   formou                  1    0.7695763
2018   fragmentada             1    0.7695763
2018   fronteira               1    0.7695763
2018   frotas                  1    0.7695763
2018   fundamental             1    0.7695763
2018   garante                 1    0.7695763
2018   georreferenciados       1    0.7695763
2018   gini                    1    0.7695763
2018   governado               1    0.7695763
2018   graças                  1    0.7695763
2018   habitacional            1    0.7695763
2018   herculano               1    0.7695763
2018   homóloga                1    0.7695763
2018   hospitalares            1    0.7695763
2018   humanos                 1    0.7695763
2018   i                       1    0.7695763
2018   ignorada                1    0.7695763
2018   igualmente              1    0.7695763
2018   impasses                1    0.7695763
2018   importância             1    0.7695763
2018   impulso                 1    0.7695763
2018   incentivos              1    0.7695763
2018   incluirá                1    0.7695763
2018   indexante               1    0.7695763
2018   infelizmente            1    0.7695763
2018   inferior                1    0.7695763
2018   inflação                1    0.7695763
2018   integrados              1    0.7695763
2018   internacionalização     1    0.7695763
2018   investigação            1    0.7695763
2018   investindo              1    0.7695763
2018   investiremos            1    0.7695763
2018   ip3                     1    0.7695763
2018   laboratórios            1    0.7695763
2018   lançado                 1    0.7695763
2018   lançamento              1    0.7695763
2018   legislativa             1    0.7695763
2018   levando                 1    0.7695763
2018   levou                   1    0.7695763
2018   libertarem              1    0.7695763
2018   ligações                1    0.7695763
2018   limite                  1    0.7695763
2018   lista                   1    0.7695763
2018   lo                      1    0.7695763
2018   localização             1    0.7695763
2018   lojas                   1    0.7695763
2018   longa                   1    0.7695763
2018   macroeconómicos         1    0.7695763
2018   manter                  1    0.7695763
2018   marcar                  1    0.7695763
2018   médico                  1    0.7695763
2018   meios                   1    0.7695763
2018   melhorem                1    0.7695763
2018   mergulhado              1    0.7695763
2018   metropolitanas          1    0.7695763
2018   minimalista             1    0.7695763
2018   modernizar              1    0.7695763
2018   mondego                 1    0.7695763
2018   motivados               1    0.7695763
2018   mudar                   1    0.7695763
2018   muitas                  1    0.7695763
2018   naturalmente            1    0.7695763
2018   norte                   1    0.7695763
2018   objetivo                1    0.7695763
2018   obrigados               1    0.7695763
2018   operacionais            1    0.7695763
2018   ordenamento             1    0.7695763
2018   p.p                     1    0.7695763
2018   parcial                 1    0.7695763
2018   parlamento              1    0.7695763
2018   participada             1    0.7695763
2018   pedra                   1    0.7695763
2018   penalizadores           1    0.7695763
2018   percentuais             1    0.7695763
2018   permitem                1    0.7695763
2018   peso                    1    0.7695763
2018   planeamento             1    0.7695763
2018   plenamente              1    0.7695763
2018   pleno                   1    0.7695763
2018   pnpot                   1    0.7695763
2018   possíveis               1    0.7695763
2018   precários               1    0.7695763
2018   prédios                 1    0.7695763
2018   prepara                 1    0.7695763
2018   preparar                1    0.7695763
2018   preparem                1    0.7695763
2018   principais              1    0.7695763
2018   procederemos            1    0.7695763
2018   procuram                1    0.7695763
2018   professores             1    0.7695763
2018   profunda                1    0.7695763
2018   progredindo             1    0.7695763
2018   progressivo             1    0.7695763
2018   promovam                1    0.7695763
2018   promover                1    0.7695763
2018   proposta                1    0.7695763
2018   prosseguindo            1    0.7695763
2018   quadruplicar            1    0.7695763
2018   quebrado                1    0.7695763
2018   queda                   1    0.7695763
2018   questionavam            1    0.7695763
2018   realizado               1    0.7695763
2018   realizarem              1    0.7695763
2018   receitas                1    0.7695763
2018   recrutamento            1    0.7695763
2018   recuando                1    0.7695763
2018   reduzido                1    0.7695763
2018   reencontrarmos          1    0.7695763
2018   reforçada               1    0.7695763
2018   reforçámos              1    0.7695763
2018   registado               1    0.7695763
2018   regresso                1    0.7695763
2018   rejeitamos              1    0.7695763
2018   rejeitar                1    0.7695763
2018   rejuvenescer            1    0.7695763
2018   relançar                1    0.7695763
2018   relativa                1    0.7695763
2018   renunciado              1    0.7695763
2018   repetido                1    0.7695763
2018   replicando              1    0.7695763
2018   repondo                 1    0.7695763
2018   reposição               1    0.7695763
2018   respeita                1    0.7695763
2018   resultam                1    0.7695763
2018   resultariam             1    0.7695763
2018   resumo                  1    0.7695763
2018   retirar                 1    0.7695763
2018   retrato                 1    0.7695763
2018   retrocesso              1    0.7695763
2018   rigorosa                1    0.7695763
2018   ritmo                   1    0.7695763
2018   rodovia                 1    0.7695763
2018   rodoviária              1    0.7695763
2018   rompendo                1    0.7695763
2018   rural                   1    0.7695763
2018   sabor                   1    0.7695763
2018   sacrificar              1    0.7695763
2018   secundária              1    0.7695763
2018   seguido                 1    0.7695763
2018   sentido                 1    0.7695763
2018   sessão                  1    0.7695763
2018   setembro                1    0.7695763
2018   si                      1    0.7695763
2018   significativo           1    0.7695763
2018   símbolos                1    0.7695763
2018   sine                    1    0.7695763
2018   sines                   1    0.7695763
2018   sinistralidade          1    0.7695763
2018   subsídios               1    0.7695763
2018   suporta                 1    0.7695763
2018   tornado                 1    0.7695763
2018   tradução                1    0.7695763
2018   trajeto                 1    0.7695763
2018   transferência           1    0.7695763
2018   transição               1    0.7695763
2018   transversal             1    0.7695763
2018   trata                   1    0.7695763
2018   tribunais               1    0.7695763
2018   última                  1    0.7695763
2018   ultrapassar             1    0.7695763
2018   valorizar               1    0.7695763
2018   vamos                   1    0.7695763
2018   variação                1    0.7695763
2018   verdadeiros             1    0.7695763
2018   via                     1    0.7695763
2018   viabilizado             1    0.7695763
2018   viabilizou              1    0.7695763
2018   viável                  1    0.7695763
2018   viram                   1    0.7695763
2018   vista                   1    0.7695763
2018   voltámos                1    0.7695763
2018   voltassem               1    0.7695763
2018   zif                     1    0.7695763
2016   um                     18    0.7673197
2016   nacional                7    0.7670411
2018   resultados              6    0.7496722
2018   empresarial             3    0.7403366
2018   forte                   3    0.7403366
2018   m                       3    0.7403366
2018   níveis                  3    0.7403366
2018   porto                   3    0.7403366
2018   pré                     3    0.7403366
2018   pt2020                  3    0.7403366
2018   reforço                 3    0.7403366
2018   sustentabilidade        3    0.7403366
2018   volvidos                3    0.7403366
2017   coesão                  4    0.7173862
2018   nacional                9    0.7097542
2018   19                      2    0.7045877
2018   2019                    2    0.7045877
2018   apoios                  2    0.7045877
2018   assumido                2    0.7045877
2018   aumentar                2    0.7045877
2018   ciclo                   2    0.7045877
2018   ciência                 2    0.7045877
2018   criámos                 2    0.7045877
2018   deficiência             2    0.7045877
2018   empregos                2    0.7045877
2018   escola                  2    0.7045877
2018   estruturais             2    0.7045877
2018   familiar                2    0.7045877
2018   feito                   2    0.7045877
2018   finanças                2    0.7045877
2018   geração                 2    0.7045877
2018   oportunidades           2    0.7045877
2018   percorrer               2    0.7045877
2018   permitirá               2    0.7045877
2018   proximidade             2    0.7045877
2018   recuou                  2    0.7045877
2018   recursos                2    0.7045877
2018   reforçando              2    0.7045877
2018   sociais                 2    0.7045877
2018   zona                    2    0.7045877
2019   vida                    5    0.7002072
2016   europeia                3    0.6956680
2016   anterior                2    0.6913322
2016   autonomia               2    0.6913322
2016   compromisso             2    0.6913322
2016   destruição              2    0.6913322
2016   homólogo                2    0.6913322
2016   meta                    2    0.6913322
2016   municípios              2    0.6913322
2016   papel                   2    0.6913322
2016   parlamentar             2    0.6913322
2016   plano                   2    0.6913322
2016   precisamos              2    0.6913322
2016   qualificação            2    0.6913322
2016   república               2    0.6913322
2016   simplex                 2    0.6913322
2016   trimestre               2    0.6913322
2018   execução                7    0.6889579
2018   políticas               7    0.6889579
2017   2016                    3    0.6880516
2017   habitação               3    0.6880516
2017   política                3    0.6880516
2017   territorial             3    0.6880516
2017   últimos                 3    0.6880516
2017   aprovar                 2    0.6855724
2017   autarquias              2    0.6855724
2017   claro                   2    0.6855724
2017   conjunto                2    0.6855724
2017   desafio                 2    0.6855724
2017   dois                    2    0.6855724
2017   económica               2    0.6855724
2017   estratégica             2    0.6855724
2017   homólogo                2    0.6855724
2017   indústria               2    0.6855724
2017   início                  2    0.6855724
2017   linhas                  2    0.6855724
2017   passado                 2    0.6855724
2017   problema                2    0.6855724
2017   procedimento            2    0.6855724
2017   qualificação            2    0.6855724
2017   trimestre               2    0.6855724
2018   coesão                  5    0.6840218
2018   públicas                5    0.6840218
2018   saúde                   5    0.6840218
2016   programa                8    0.6815746
2019   longo                   3    0.6455394
2019   transportes             3    0.6455394
2017   senhoras                6    0.6318515
2016   governo                 8    0.6210626
2017   presidente              7    0.6153702
2019   aumento                 4    0.5978252
2018   economia                7    0.5760171
2019   recuperação             6    0.5721263
2016   condições               3    0.5666644
2016   direitos                3    0.5666644
2016   família                 3    0.5666644
2016   modelo                  3    0.5666644
2017   deputados               6    0.5591574
2017   senhor                  6    0.5591574
2017   senhores                6    0.5591574
2018   reformas                3    0.5293273
2019   aprovámos               2    0.5268240
2019   cortes                  2    0.5268240
2019   desafio                 2    0.5268240
2019   estratégicos            2    0.5268240
2019   internacional           2    0.5268240
2019   material                2    0.5268240
2019   melhorar                2    0.5268240
2019   normalidade             2    0.5268240
2019   parlamentar             2    0.5268240
2019   passado                 2    0.5268240
2019   plano                   2    0.5268240
2019   precisamos              2    0.5268240
2019   problema                2    0.5268240
2019   qualidade               2    0.5268240
2019   taxas                   2    0.5268240
2017   assembleia              4    0.5151500
2017   défice                  4    0.5151500
2016   comissão                2    0.5130747
2016   construir               2    0.5130747
2016   contratos               2    0.5130747
2016   dificuldades            2    0.5130747
2016   pré                     2    0.5130747
2016   prioridades             2    0.5130747
2016   taxa                    2    0.5130747
2017   2020                    2    0.5066479
2017   níveis                  2    0.5066479
2017   prioridades             2    0.5066479
2017   processo                2    0.5066479
2017   sabemos                 2    0.5066479
2017   sucesso                 2    0.5066479
2018   programa               10    0.5026224
2018   podemos                 6    0.4996693
2018   públicos                6    0.4996693
2016   200                     1    0.4886051
2016   4.0                     1    0.4886051
2016   90                      1    0.4886051
2016   abril                   1    0.4886051
2016   acima                   1    0.4886051
2016   adotámos                1    0.4886051
2016   adultos                 1    0.4886051
2016   alargámos               1    0.4886051
2016   alqueva                 1    0.4886051
2016   apresentámos            1    0.4886051
2016   apresentar              1    0.4886051
2016   assumiu                 1    0.4886051
2016   atingir                 1    0.4886051
2016   atualizámos             1    0.4886051
2016   autonomias              1    0.4886051
2016   azul                    1    0.4886051
2016   capitalização           1    0.4886051
2016   central                 1    0.4886051
2016   compensação             1    0.4886051
2016   concertação             1    0.4886051
2016   confiar                 1    0.4886051
2016   conhecer                1    0.4886051
2016   conjuntas               1    0.4886051
2016   consolidação            1    0.4886051
2016   construção              1    0.4886051
2016   continuados             1    0.4886051
2016   contratar               1    0.4886051
2016   crescer                 1    0.4886051
2016   crucial                 1    0.4886051
2016   cuidados                1    0.4886051
2016   dados                   1    0.4886051
2016   defesa                  1    0.4886051
2016   deixaram                1    0.4886051
2016   dependência             1    0.4886051
2016   desequilíbrios          1    0.4886051
2016   diminuir                1    0.4886051
2016   direito                 1    0.4886051
2016   efeitos                 1    0.4886051
2016   eleições                1    0.4886051
2016   energia                 1    0.4886051
2016   estável                 1    0.4886051
2016   europa                  1    0.4886051
2016   extraordinária          1    0.4886051
2016   frente                  1    0.4886051
2016   honrando                1    0.4886051
2016   honrar                  1    0.4886051
2016   identidade              1    0.4886051
2016   irs                     1    0.4886051
2016   letivo                  1    0.4886051
2016   levar                   1    0.4886051
2016   locais                  1    0.4886051
2016   mar                     1    0.4886051
2016   melhores                1    0.4886051
2016   mínimos                 1    0.4886051
2016   necessárias             1    0.4886051
2016   outubro                 1    0.4886051
2016   pagar                   1    0.4886051
2016   particular              1    0.4886051
2016   património              1    0.4886051
2016   permanente              1    0.4886051
2016   permitiram              1    0.4886051
2016   posições                1    0.4886051
2016   possa                   1    0.4886051
2016   precariedade            1    0.4886051
2016   precisamente            1    0.4886051
2016   produz                  1    0.4886051
2016   qualificações           1    0.4886051
2016   realidade               1    0.4886051
2016   reconstruir             1    0.4886051
2016   recuperámos             1    0.4886051
2016   rede                    1    0.4886051
2016   regadios                1    0.4886051
2016   relevante               1    0.4886051
2016   repusemos               1    0.4886051
2016   requalificação          1    0.4886051
2016   resposta                1    0.4886051
2016   resultado               1    0.4886051
2016   rumo                    1    0.4886051
2016   sair                    1    0.4886051
2016   setor                   1    0.4886051
2016   sistemas                1    0.4886051
2016   soberania               1    0.4886051
2016   solidariedade           1    0.4886051
2016   suficiente              1    0.4886051
2016   tecido                  1    0.4886051
2016   única                   1    0.4886051
2016   unidades                1    0.4886051
2016   universalização         1    0.4886051
2016   vive                    1    0.4886051
2016   viver                   1    0.4886051
2016   vontade                 1    0.4886051
2017   17                      1    0.4845344
2017   18                      1    0.4845344
2017   1º                      1    0.4845344
2017   abril                   1    0.4845344
2017   acaso                   1    0.4845344
2017   acessível               1    0.4845344
2017   aconteceu               1    0.4845344
2017   adotámos                1    0.4845344
2017   ágil                    1    0.4845344
2017   alterações              1    0.4845344
2017   apresentar              1    0.4845344
2017   arrendamento            1    0.4845344
2017   bens                    1    0.4845344
2017   cadastro                1    0.4845344
2017   centro                  1    0.4845344
2017   climáticas              1    0.4845344
2017   concertação             1    0.4845344
2017   confiar                 1    0.4845344
2017   conhecer                1    0.4845344
2017   consolidar              1    0.4845344
2017   coragem                 1    0.4845344
2017   defesa                  1    0.4845344
2017   deixar                  1    0.4845344
2017   descongelamento         1    0.4845344
2017   diminuição              1    0.4845344
2017   diminuir                1    0.4845344
2017   direito                 1    0.4845344
2017   discussão               1    0.4845344
2017   efeito                  1    0.4845344
2017   eficaz                  1    0.4845344
2017   energia                 1    0.4845344
2017   equilíbrio              1    0.4845344
2017   esforço                 1    0.4845344
2017   especial                1    0.4845344
2017   espera                  1    0.4845344
2017   esquecer                1    0.4845344
2017   estarmos                1    0.4845344
2017   executar                1    0.4845344
2017   expansão                1    0.4845344
2017   extraordinária          1    0.4845344
2017   florestas               1    0.4845344
2017   freguesias              1    0.4845344
2017   função                  1    0.4845344
2017   iniciámos               1    0.4845344
2017   instrumentos            1    0.4845344
2017   junho                   1    0.4845344
2017   lançou                  1    0.4845344
2017   letivo                  1    0.4845344
2017   mar                     1    0.4845344
2017   melhorou                1    0.4845344
2017   metas                   1    0.4845344
2017   ministro                1    0.4845344
2017   necessária              1    0.4845344
2017   outubro                 1    0.4845344
2017   parlamentares           1    0.4845344
2017   pedagógica              1    0.4845344
2017   permitiram              1    0.4845344
2017   persistência            1    0.4845344
2017   piloto                  1    0.4845344
2017   população               1    0.4845344
2017   pôr                     1    0.4845344
2017   possa                   1    0.4845344
2017   postos                  1    0.4845344
2017   precisamente            1    0.4845344
2017   preciso                 1    0.4845344
2017   produz                  1    0.4845344
2017   progresso               1    0.4845344
2017   qualificações           1    0.4845344
2017   reabertura              1    0.4845344
2017   reafirmar               1    0.4845344
2017   reconstruir             1    0.4845344
2017   redes                   1    0.4845344
2017   regiões                 1    0.4845344
2017   registando              1    0.4845344
2017   retomando               1    0.4845344
2017   revitalização           1    0.4845344
2017   setor                   1    0.4845344
2017   simultaneamente         1    0.4845344
2017   sns                     1    0.4845344
2017   solidariedade           1    0.4845344
2017   suficiente              1    0.4845344
2017   sustentada              1    0.4845344
2017   turma                   1    0.4845344
2019   lisboa                  3    0.4781310
2019   médio                   3    0.4781310
2018   aumento                 4    0.4554894
2018   desenvolvimento         4    0.4554894
2018   famílias                4    0.4554894
2018   redução                 4    0.4554894
2016   competitividade         3    0.4544034
2016   famílias                3    0.4544034
2019   emprego                10    0.4495069
2017   competitividade         3    0.4458070
2016   social                  5    0.4350453
2018   abono                   2    0.4319845
2018   aquisição               2    0.4319845
2018   centros                 2    0.4319845
2018   claro                   2    0.4319845
2018   compromisso             2    0.4319845
2018   desemprego              2    0.4319845
2018   económica               2    0.4319845
2018   euro                    2    0.4319845
2018   formação                2    0.4319845
2018   gestão                  2    0.4319845
2018   material                2    0.4319845
2018   território              2    0.4319845
2018   assembleia              5    0.4143782
2018   recuperação             6    0.3883878
2019   18                      1    0.3723395
2019   1º                      1    0.3723395
2019   22                      1    0.3723395
2019   250                     1    0.3723395
2019   abrir                   1    0.3723395
2019   adultos                 1    0.3723395
2019   ágil                    1    0.3723395
2019   alargámos               1    0.3723395
2019   alargando               1    0.3723395
2019   alterações              1    0.3723395
2019   apresentámos            1    0.3723395
2019   autonomias              1    0.3723395
2019   azul                    1    0.3723395
2019   básico                  1    0.3723395
2019   capitalização           1    0.3723395
2019   climáticas              1    0.3723395
2019   confirma                1    0.3723395
2019   conjuntas               1    0.3723395
2019   credibilidade           1    0.3723395
2019   crescer                 1    0.3723395
2019   dados                   1    0.3723395
2019   deixaram                1    0.3723395
2019   desinvestimento         1    0.3723395
2019   determinante            1    0.3723395
2019   discussão               1    0.3723395
2019   empreendedorismo        1    0.3723395
2019   espera                  1    0.3723395
2019   esquecer                1    0.3723395
2019   estável                 1    0.3723395
2019   executar                1    0.3723395
2019   exportações             1    0.3723395
2019   facto                   1    0.3723395
2019   ferroviário             1    0.3723395
2019   fizemos                 1    0.3723395
2019   focados                 1    0.3723395
2019   função                  1    0.3723395
2019   honrar                  1    0.3723395
2019   identidade              1    0.3723395
2019   inovador                1    0.3723395
2019   investimos              1    0.3723395
2019   irs                     1    0.3723395
2019   junho                   1    0.3723395
2019   juros                   1    0.3723395
2019   lançou                  1    0.3723395
2019   locais                  1    0.3723395
2019   melhores                1    0.3723395
2019   melhorou                1    0.3723395
2019   metas                   1    0.3723395
2019   metro                   1    0.3723395
2019   metros                  1    0.3723395
2019   minho                   1    0.3723395
2019   ministro                1    0.3723395
2019   motor                   1    0.3723395
2019   necessária              1    0.3723395
2019   necessários             1    0.3723395
2019   obras                   1    0.3723395
2019   pagar                   1    0.3723395
2019   par                     1    0.3723395
2019   parlamentares           1    0.3723395
2019   permanente              1    0.3723395
2019   pessoal                 1    0.3723395
2019   portuguesas             1    0.3723395
2019   posições                1    0.3723395
2019   postos                  1    0.3723395
2019   precariedade            1    0.3723395
2019   precoce                 1    0.3723395
2019   procurado               1    0.3723395
2019   profissionais           1    0.3723395
2019   profissional            1    0.3723395
2019   real                    1    0.3723395
2019   registou                1    0.3723395
2019   resposta                1    0.3723395
2019   retomando               1    0.3723395
2019   revertemos              1    0.3723395
2019   risco                   1    0.3723395
2019   sair                    1    0.3723395
2019   salário                 1    0.3723395
2019   secundário              1    0.3723395
2019   serviço                 1    0.3723395
2019   simplificámos           1    0.3723395
2019   simultaneamente         1    0.3723395
2019   sistemas                1    0.3723395
2019   sns                     1    0.3723395
2019   soberania               1    0.3723395
2019   soflusa                 1    0.3723395
2019   tendo                   1    0.3723395
2019   valores                 1    0.3723395
2019   vive                    1    0.3723395
2019   viver                   1    0.3723395
2019   vontade                 1    0.3723395
2016   fundamentais            2    0.3707478
2016   salários                2    0.3707478
2017   10                      2    0.3637340
2017   administração           2    0.3637340
2017   alternativa             2    0.3637340
2017   década                  2    0.3637340
2019   crescimento             9    0.3633653
2018   4                       3    0.3527215
2018   desigualdades           3    0.3527215
2018   habitação               3    0.3527215
2018   lisboa                  3    0.3527215
2018   reforma                 3    0.3527215
2018   ano                     9    0.3491716
2019   prazo                   3    0.3346007
2019   rendimento              3    0.3346007
2016   investimento           12    0.3341465
2019   construir               2    0.3295166
2019   contrário               2    0.3295166
2019   dificuldades            2    0.3295166
2019   m                       2    0.3295166
2019   reforço                 2    0.3295166
2019   sustentabilidade        2    0.3295166
2019   termos                  2    0.3295166
2019   volvidos                2    0.3295166
2018   um                     22    0.3293274
2017   crescimento             7    0.3202822
2018   3                       4    0.3167342
2018   17                      1    0.3053112
2018   200                     1    0.3053112
2018   22                      1    0.3053112
2018   250                     1    0.3053112
2018   4.0                     1    0.3053112
2018   90                      1    0.3053112
2018   abrir                   1    0.3053112
2018   acaso                   1    0.3053112
2018   acessível               1    0.3053112
2018   acima                   1    0.3053112
2018   aconteceu               1    0.3053112
2018   alargando               1    0.3053112
2018   alqueva                 1    0.3053112
2018   arrendamento            1    0.3053112
2018   assumiu                 1    0.3053112
2018   atingir                 1    0.3053112
2018   atualizámos             1    0.3053112
2018   básico                  1    0.3053112
2018   bens                    1    0.3053112
2018   cadastro                1    0.3053112
2018   central                 1    0.3053112
2018   centro                  1    0.3053112
2018   compensação             1    0.3053112
2018   confirma                1    0.3053112
2018   consolidação            1    0.3053112
2018   consolidar              1    0.3053112
2018   construção              1    0.3053112
2018   continuados             1    0.3053112
2018   contratar               1    0.3053112
2018   coragem                 1    0.3053112
2018   credibilidade           1    0.3053112
2018   crucial                 1    0.3053112
2018   cuidados                1    0.3053112
2018   deixar                  1    0.3053112
2018   dependência             1    0.3053112
2018   descongelamento         1    0.3053112
2018   desequilíbrios          1    0.3053112
2018   desinvestimento         1    0.3053112
2018   determinante            1    0.3053112
2018   diminuição              1    0.3053112
2018   efeito                  1    0.3053112
2018   efeitos                 1    0.3053112
2018   eficaz                  1    0.3053112
2018   eleições                1    0.3053112
2018   empreendedorismo        1    0.3053112
2018   equilíbrio              1    0.3053112
2018   esforço                 1    0.3053112
2018   especial                1    0.3053112
2018   estarmos                1    0.3053112
2018   europa                  1    0.3053112
2018   expansão                1    0.3053112
2018   exportações             1    0.3053112
2018   facto                   1    0.3053112
2018   ferroviário             1    0.3053112
2018   fizemos                 1    0.3053112
2018   florestas               1    0.3053112
2018   focados                 1    0.3053112
2018   freguesias              1    0.3053112
2018   frente                  1    0.3053112
2018   honrando                1    0.3053112
2018   iniciámos               1    0.3053112
2018   inovador                1    0.3053112
2018   instrumentos            1    0.3053112
2018   investimos              1    0.3053112
2018   juros                   1    0.3053112
2018   levar                   1    0.3053112
2018   metro                   1    0.3053112
2018   metros                  1    0.3053112
2018   minho                   1    0.3053112
2018   mínimos                 1    0.3053112
2018   motor                   1    0.3053112
2018   necessárias             1    0.3053112
2018   necessários             1    0.3053112
2018   obras                   1    0.3053112
2018   par                     1    0.3053112
2018   particular              1    0.3053112
2018   património              1    0.3053112
2018   pedagógica              1    0.3053112
2018   persistência            1    0.3053112
2018   pessoal                 1    0.3053112
2018   piloto                  1    0.3053112
2018   população               1    0.3053112
2018   pôr                     1    0.3053112
2018   portuguesas             1    0.3053112
2018   preciso                 1    0.3053112
2018   precoce                 1    0.3053112
2018   procurado               1    0.3053112
2018   profissionais           1    0.3053112
2018   profissional            1    0.3053112
2018   progresso               1    0.3053112
2018   reabertura              1    0.3053112
2018   reafirmar               1    0.3053112
2018   real                    1    0.3053112
2018   realidade               1    0.3053112
2018   recuperámos             1    0.3053112
2018   rede                    1    0.3053112
2018   redes                   1    0.3053112
2018   regadios                1    0.3053112
2018   regiões                 1    0.3053112
2018   registando              1    0.3053112
2018   registou                1    0.3053112
2018   relevante               1    0.3053112
2018   repusemos               1    0.3053112
2018   requalificação          1    0.3053112
2018   resultado               1    0.3053112
2018   revertemos              1    0.3053112
2018   revitalização           1    0.3053112
2018   risco                   1    0.3053112
2018   rumo                    1    0.3053112
2018   salário                 1    0.3053112
2018   secundário              1    0.3053112
2018   serviço                 1    0.3053112
2018   simplificámos           1    0.3053112
2018   soflusa                 1    0.3053112
2018   sustentada              1    0.3053112
2018   tecido                  1    0.3053112
2018   tendo                   1    0.3053112
2018   turma                   1    0.3053112
2018   única                   1    0.3053112
2018   unidades                1    0.3053112
2018   universalização         1    0.3053112
2018   valores                 1    0.3053112
2017   políticas               4    0.2719061
2016   inovação                3    0.2657850
2016   ano                     6    0.2620341
2016   1.000                   1    0.2619669
2016   2015                    1    0.2619669
2016   45                      1    0.2619669
2016   7                       1    0.2619669
2016   abriu                   1    0.2619669
2016   ação                    1    0.2619669
2016   ativa                   1    0.2619669
2016   ciclo                   1    0.2619669
2016   combate                 1    0.2619669
2016   contexto                1    0.2619669
2016   corte                   1    0.2619669
2016   custos                  1    0.2619669
2016   deficiência             1    0.2619669
2016   demos                   1    0.2619669
2016   desafios                1    0.2619669
2016   determinação            1    0.2619669
2016   dimensão                1    0.2619669
2016   endividamento           1    0.2619669
2016   escolas                 1    0.2619669
2016   esperança               1    0.2619669
2016   estratégia              1    0.2619669
2016   estruturais             1    0.2619669
2016   excessivos              1    0.2619669
2016   financeira              1    0.2619669
2016   financiamento           1    0.2619669
2016   funções                 1    0.2619669
2016   gratuitos               1    0.2619669
2016   hectares                1    0.2619669
2016   horizonte               1    0.2619669
2016   institucional           1    0.2619669
2016   intervenção             1    0.2619669
2016   manuais                 1    0.2619669
2016   mínimo                  1    0.2619669
2016   missão                  1    0.2619669
2016   moderadoras             1    0.2619669
2016   necessário              1    0.2619669
2016   necessidades            1    0.2619669
2016   página                  1    0.2619669
2016   populações              1    0.2619669
2016   precisámos              1    0.2619669
2016   prestação               1    0.2619669
2016   proximidade             1    0.2619669
2016   recursos                1    0.2619669
2016   reduziu                 1    0.2619669
2016   regionais               1    0.2619669
2016   saída                   1    0.2619669
2016   sobressalto             1    0.2619669
2016   termo                   1    0.2619669
2016   trabalhadores           1    0.2619669
2016   união                   1    0.2619669
2016   unidade                 1    0.2619669
2016   virar                   1    0.2619669
2017   15                      1    0.2570111
2017   19                      1    0.2570111
2017   7                       1    0.2570111
2017   abriu                   1    0.2570111
2017   acesso                  1    0.2570111
2017   alcançámos              1    0.2570111
2017   alunos                  1    0.2570111
2017   apoios                  1    0.2570111
2017   ativa                   1    0.2570111
2017   aumentar                1    0.2570111
2017   baixa                   1    0.2570111
2017   clima                   1    0.2570111
2017   combate                 1    0.2570111
2017   conclusão               1    0.2570111
2017   criados                 1    0.2570111
2017   crianças                1    0.2570111
2017   criou                   1    0.2570111
2017   custos                  1    0.2570111
2017   endividamento           1    0.2570111
2017   escolas                 1    0.2570111
2017   estratégia              1    0.2570111
2017   excessivos              1    0.2570111
2017   finanças                1    0.2570111
2017   gerou                   1    0.2570111
2017   hectares                1    0.2570111
2017   manutenção              1    0.2570111
2017   média                   1    0.2570111
2017   mercado                 1    0.2570111
2017   nação                   1    0.2570111
2017   necessário              1    0.2570111
2017   necessidades            1    0.2570111
2017   oportunidades           1    0.2570111
2017   participação            1    0.2570111
2017   permitam                1    0.2570111
2017   populações              1    0.2570111
2017   previsibilidade         1    0.2570111
2017   projeto                 1    0.2570111
2017   prosseguir              1    0.2570111
2017   recuou                  1    0.2570111
2017   recuperaram             1    0.2570111
2017   reforçando              1    0.2570111
2017   reforçar                1    0.2570111
2017   século                  1    0.2570111
2017   sociais                 1    0.2570111
2017   sustentado              1    0.2570111
2017   trabalhadores           1    0.2570111
2017   zona                    1    0.2570111
2017   inovação                3    0.2563282
2016   balanço                 2    0.2523014
2016   médio                   2    0.2523014
2016   pobreza                 2    0.2523014
2017   essencial               2    0.2447550
2017   europeia                2    0.2447550
2017   reforma                 2    0.2447550
2019   medidas                 4    0.2413656
2019   prioridade              4    0.2413656
2018   2020                    2    0.2236947
2018   ambição                 2    0.2236947
2018   contrário               2    0.2236947
2018   contratos               2    0.2236947
2018   lançámos                2    0.2236947
2018   processo                2    0.2236947
2018   sucesso                 2    0.2236947
2019   austeridade             3    0.2088773
2018   condições               3    0.2008165
2018   educação                3    0.2008165
2018   escolar                 3    0.2008165
2018   família                 3    0.2008165
2018   pensões                 3    0.2008165
2018   emprego                10    0.1939739
2019   problemas               5    0.1813138
2017   resultados              3    0.1748935
2019   10                      2    0.1704242
2019   década                  2    0.1704242
2019   fundamentais            2    0.1704242
2019   investir                2    0.1704242
2019   mudança                 2    0.1704242
2019   passo                   2    0.1704242
2019   salários                2    0.1704242
2016   pensões                 2    0.1507010
2016   prazo                   2    0.1507010
2016   rendimento              2    0.1507010
2016   valorização             2    0.1507010
2017   direitos                2    0.1426621
2017   educação                2    0.1426621
2017   escolar                 2    0.1426621
2017   modelo                  2    0.1426621
2017   prazo                   2    0.1426621
2017   visão                   2    0.1426621
2017   governo                 6    0.1421634
2019   1.000                   1    0.1204209
2019   15                      1    0.1204209
2019   2019                    1    0.1204209
2019   45                      1    0.1204209
2019   abriu                   1    0.1204209
2019   ação                    1    0.1204209
2019   acesso                  1    0.1204209
2019   agimos                  1    0.1204209
2019   alcançámos              1    0.1204209
2019   alunos                  1    0.1204209
2019   assumido                1    0.1204209
2019   baixa                   1    0.1204209
2019   ciência                 1    0.1204209
2019   conclusão               1    0.1204209
2019   corte                   1    0.1204209
2019   criados                 1    0.1204209
2019   criámos                 1    0.1204209
2019   demos                   1    0.1204209
2019   desafios                1    0.1204209
2019   determinação            1    0.1204209
2019   difícil                 1    0.1204209
2019   dimensão                1    0.1204209
2019   empregos                1    0.1204209
2019   escola                  1    0.1204209
2019   estratégia              1    0.1204209
2019   familiar                1    0.1204209
2019   feito                   1    0.1204209
2019   financeira              1    0.1204209
2019   financiamento           1    0.1204209
2019   geração                 1    0.1204209
2019   gratuitos               1    0.1204209
2019   horizonte               1    0.1204209
2019   manuais                 1    0.1204209
2019   manutenção              1    0.1204209
2019   mercado                 1    0.1204209
2019   mínimo                  1    0.1204209
2019   moderadoras             1    0.1204209
2019   nação                   1    0.1204209
2019   necessário              1    0.1204209
2019   necessidades            1    0.1204209
2019   página                  1    0.1204209
2019   participação            1    0.1204209
2019   percorrer               1    0.1204209
2019   permitam                1    0.1204209
2019   permitirá               1    0.1204209
2019   populações              1    0.1204209
2019   prestação               1    0.1204209
2019   projeto                 1    0.1204209
2019   prosseguir              1    0.1204209
2019   reforçar                1    0.1204209
2019   regionais               1    0.1204209
2019   século                  1    0.1204209
2019   sermos                  1    0.1204209
2019   sobressalto             1    0.1204209
2019   sustentado              1    0.1204209
2019   termo                   1    0.1204209
2019   trabalhadores           1    0.1204209
2019   virar                   1    0.1204209
2018   crescimento             9    0.1171296
2016   portugueses             6    0.1070678
2016   abono                   1    0.1064589
2016   aprovámos               1    0.1064589
2016   aprovar                 1    0.1064589
2016   autarquias              1    0.1064589
2016   bloqueios               1    0.1064589
2016   centros                 1    0.1064589
2016   cortes                  1    0.1064589
2016   descentralização        1    0.1064589
2016   desemprego              1    0.1064589
2016   estratégicos            1    0.1064589
2016   euro                    1    0.1064589
2016   excessivo               1    0.1064589
2016   funcionamento           1    0.1064589
2016   indústria               1    0.1064589
2016   início                  1    0.1064589
2016   melhorar                1    0.1064589
2016   mobilidade              1    0.1064589
2016   normalidade             1    0.1064589
2016   previsto                1    0.1064589
2016   procedimento            1    0.1064589
2016   procuramos              1    0.1064589
2016   qualidade               1    0.1064589
2016   respeito                1    0.1064589
2016   taxas                   1    0.1064589
2016   território              1    0.1064589
2017   2                       1    0.1007801
2017   aceleração              1    0.1007801
2017   aquisição               1    0.1007801
2017   autonomia               1    0.1007801
2017   centros                 1    0.1007801
2017   competências            1    0.1007801
2017   descentralização        1    0.1007801
2017   desemprego              1    0.1007801
2017   euro                    1    0.1007801
2017   excessivo               1    0.1007801
2017   florestal               1    0.1007801
2017   formação                1    0.1007801
2017   funcionamento           1    0.1007801
2017   garantir                1    0.1007801
2017   gestão                  1    0.1007801
2017   global                  1    0.1007801
2017   internacional           1    0.1007801
2017   justiça                 1    0.1007801
2017   meta                    1    0.1007801
2017   municípios              1    0.1007801
2017   papel                   1    0.1007801
2017   promoção                1    0.1007801
2017   qualidade               1    0.1007801
2017   república               1    0.1007801
2017   simplex                 1    0.1007801
2017   território              1    0.1007801
2019   3                       3    0.0968622
2019   cidadãos                3    0.0968622
2018   futuro                  6    0.0954285
2018   défice                  4    0.0778046
2018   empresas                4    0.0778046
2018   prioridade              4    0.0778046
2018   investimentos           3    0.0673321
2016   redução                 2    0.0615479
2018   10                      2    0.0549368
2018   conhecimento            2    0.0549368
2018   década                  2    0.0549368
2018   investir                2    0.0549368
2018   mudança                 2    0.0549368
2018   queremos                2    0.0549368
2018   reabilitação            2    0.0549368
2018   salários                2    0.0549368
2017   pública                 2    0.0530480
2017   público                 2    0.0530480
2018   1.000                   1    0.0388182
2018   15                      1    0.0388182
2018   20                      1    0.0388182
2018   45                      1    0.0388182
2018   7                       1    0.0388182
2018   ação                    1    0.0388182
2018   acesso                  1    0.0388182
2018   alcançámos              1    0.0388182
2018   alunos                  1    0.0388182
2018   ativa                   1    0.0388182
2018   aumentou                1    0.0388182
2018   baixa                   1    0.0388182
2018   baixos                  1    0.0388182
2018   capitalizar             1    0.0388182
2018   carreiras               1    0.0388182
2018   carris                  1    0.0388182
2018   cidadão                 1    0.0388182
2018   combate                 1    0.0388182
2018   conclusão               1    0.0388182
2018   concretizar             1    0.0388182
2018   constituição            1    0.0388182
2018   continuaremos           1    0.0388182
2018   corte                   1    0.0388182
2018   criados                 1    0.0388182
2018   cumprir                 1    0.0388182
2018   custos                  1    0.0388182
2018   demos                   1    0.0388182
2018   desafios                1    0.0388182
2018   desígnio                1    0.0388182
2018   determinação            1    0.0388182
2018   dever                   1    0.0388182
2018   dimensão                1    0.0388182
2018   endividamento           1    0.0388182
2018   ensino                  1    0.0388182
2018   escolas                 1    0.0388182
2018   excessivos              1    0.0388182
2018   finalmente              1    0.0388182
2018   financeira              1    0.0388182
2018   financiamento           1    0.0388182
2018   fiscal                  1    0.0388182
2018   garanta                 1    0.0388182
2018   gratuitos               1    0.0388182
2018   hectares                1    0.0388182
2018   horizonte               1    0.0388182
2018   iremos                  1    0.0388182
2018   manuais                 1    0.0388182
2018   manutenção              1    0.0388182
2018   mercado                 1    0.0388182
2018   mínimo                  1    0.0388182
2018   moderadoras             1    0.0388182
2018   nação                   1    0.0388182
2018   navios                  1    0.0388182
2018   página                  1    0.0388182
2018   países                  1    0.0388182
2018   participação            1    0.0388182
2018   permitam                1    0.0388182
2018   prestação               1    0.0388182
2018   projeto                 1    0.0388182
2018   promovendo              1    0.0388182
2018   prosseguir              1    0.0388182
2018   reforça                 1    0.0388182
2018   reforçar                1    0.0388182
2018   regionais               1    0.0388182
2018   responder               1    0.0388182
2018   século                  1    0.0388182
2018   situação                1    0.0388182
2018   sobressalto             1    0.0388182
2018   stcp                    1    0.0388182
2018   suma                    1    0.0388182
2018   superior                1    0.0388182
2018   sustentado              1    0.0388182
2018   termo                   1    0.0388182
2018   transtejo               1    0.0388182
2018   virar                   1    0.0388182
2019   4                       2    0.0367634
2019   desigualdades           2    0.0367634
2019   política                2    0.0367634
2019   rendimentos             2    0.0367634
2019   sociedade               2    0.0367634
2019   últimos                 2    0.0367634
2017   públicos                3    0.0311632
2017   ano                     5    0.0063700
2019   debate                  3   -0.0043145
2019   presidente              6   -0.0061178
2016   2020                    1   -0.0127599
2016   empresarial             1   -0.0127599
2016   jovens                  1   -0.0127599
2016   linha                   1   -0.0127599
2016   porto                   1   -0.0127599
2016   pt2020                  1   -0.0127599
2016   reduzimos               1   -0.0127599
2016   reduzir                 1   -0.0127599
2016   sabemos                 1   -0.0127599
2016   sucesso                 1   -0.0127599
2016   termos                  1   -0.0127599
2016   saúde                   2   -0.0180669
2016   três                    2   -0.0180669
2017   2018                    1   -0.0190704
2017   amanhã                  1   -0.0190704
2017   áreas                   1   -0.0190704
2017   bases                   1   -0.0190704
2017   comissão                1   -0.0190704
2017   construir               1   -0.0190704
2017   contrário               1   -0.0190704
2017   forte                   1   -0.0190704
2017   maio                    1   -0.0190704
2017   marca                   1   -0.0190704
2017   reduzir                 1   -0.0190704
2017   taxa                    1   -0.0190704
2017   termos                  1   -0.0190704
2017   3                       2   -0.0270020
2016   portugal                5   -0.0286695
2019   políticas               4   -0.0325590
2019   deputados               5   -0.0546251
2019   senhor                  5   -0.0546251
2019   senhores                5   -0.0546251
2019   abono                   1   -0.0557390
2019   anterior                1   -0.0557390
2019   aquisição               1   -0.0557390
2019   autonomia               1   -0.0557390
2019   capacidade              1   -0.0557390
2019   conjunto                1   -0.0557390
2019   conseguimos             1   -0.0557390
2019   cultura                 1   -0.0557390
2019   descentralização        1   -0.0557390
2019   destruição              1   -0.0557390
2019   dois                    1   -0.0557390
2019   eliminámos              1   -0.0557390
2019   estabilidade            1   -0.0557390
2019   estratégica             1   -0.0557390
2019   excessivo               1   -0.0557390
2019   formação                1   -0.0557390
2019   funcionamento           1   -0.0557390
2019   gestão                  1   -0.0557390
2019   início                  1   -0.0557390
2019   linhas                  1   -0.0557390
2019   procedimento            1   -0.0557390
2019   república               1   -0.0557390
2019   simplex                 1   -0.0557390
2019   2017                    2   -0.0789024
2019   assente                 2   -0.0789024
2019   condições               2   -0.0789024
2019   direitos                2   -0.0789024
2019   educação                2   -0.0789024
2019   escolar                 2   -0.0789024
2019   família                 2   -0.0789024
2019   pensões                 2   -0.0789024
2016   economia                3   -0.0819962
2018   1                       2   -0.0874952
2018   aumentámos              2   -0.0874952
2018   contas                  2   -0.0874952
2018   segurança               2   -0.0874952
2018   últimos                 2   -0.0874952
2016   vida                    2   -0.0901559
2017   economia                3   -0.0932944
2018   social                  5   -0.0962011
2019   resultados              3   -0.0967279
2017   vida                    2   -0.0995049
2016   criação                 1   -0.1102667
2016   interior                1   -0.1102667
2016   investir                1   -0.1102667
2016   longo                   1   -0.1102667
2016   mudança                 1   -0.1102667
2016   transportes             1   -0.1102667
2017   criação                 1   -0.1171472
2017   fundamentais            1   -0.1171472
2017   incentivo               1   -0.1171472
2017   investir                1   -0.1171472
2017   longo                   1   -0.1171472
2017   mudança                 1   -0.1171472
2017   transportes             1   -0.1171472
2016   presidente              4   -0.1278364
2018   100                     1   -0.1492479
2018   anterior                1   -0.1492479
2018   aprovámos               1   -0.1492479
2018   aprovar                 1   -0.1492479
2018   autarquias              1   -0.1492479
2018   conjunto                1   -0.1492479
2018   constitucional          1   -0.1492479
2018   cortes                  1   -0.1492479
2018   criar                   1   -0.1492479
2018   descentralização        1   -0.1492479
2018   destruição              1   -0.1492479
2018   dois                    1   -0.1492479
2018   estratégica             1   -0.1492479
2018   estratégicos            1   -0.1492479
2018   excessivo               1   -0.1492479
2018   exige                   1   -0.1492479
2018   funcionamento           1   -0.1492479
2018   gerações                1   -0.1492479
2018   indústria               1   -0.1492479
2018   internacional           1   -0.1492479
2018   linhas                  1   -0.1492479
2018   melhorar                1   -0.1492479
2018   meta                    1   -0.1492479
2018   municípios              1   -0.1492479
2018   normalidade             1   -0.1492479
2018   papel                   1   -0.1492479
2018   permitiu                1   -0.1492479
2018   planos                  1   -0.1492479
2018   taxas                   1   -0.1492479
2018   urbana                  1   -0.1492479
2017   social                  3   -0.1499379
2016   défice                  2   -0.1561642
2016   prioridade              2   -0.1561642
2018   debate                  3   -0.1600264
2018   inovação                3   -0.1600264
2017   anos                    8   -0.1652764
2017   prioridade              2   -0.1659086
2018   senhoras                5   -0.1800170
2019   compromissos            2   -0.1812208
2019   famílias                2   -0.1812208
2019   investimentos           2   -0.1812208
2019   º                       2   -0.1812208
2019   pública                 2   -0.1812208
2019   redução                 2   -0.1812208
2019   serviços                2   -0.1812208
2019   social                  4   -0.1863621
2017   melhor                  4   -0.1891692
2016   senhoras                3   -0.1915359
2019   abandono                1   -0.1930000
2019   comissão                1   -0.1930000
2019   contratos               1   -0.1930000
2019   empresarial             1   -0.1930000
2019   floresta                1   -0.1930000
2019   forte                   1   -0.1930000
2019   maio                    1   -0.1930000
2019   marca                   1   -0.1930000
2019   porto                   1   -0.1930000
2019   processo                1   -0.1930000
2019   prometemos              1   -0.1930000
2019   pt2020                  1   -0.1930000
2019   sabemos                 1   -0.1930000
2019   taxa                    1   -0.1930000
2016   4                       1   -0.1933960
2016   aumentámos              1   -0.1933960
2016   desigualdades           1   -0.1933960
2016   dívida                  1   -0.1933960
2016   lisboa                  1   -0.1933960
2016   pib                     1   -0.1933960
2016   política                1   -0.1933960
2016   reforma                 1   -0.1933960
2017   1                       1   -0.2008009
2017   4                       1   -0.2008009
2017   balanço                 1   -0.2008009
2017   democracia              1   -0.2008009
2017   desigualdades           1   -0.2008009
2017   médio                   1   -0.2008009
2017   pib                     1   -0.2008009
2017   rendimentos             1   -0.2008009
2017   segurança               1   -0.2008009
2017   sociedade               1   -0.2008009
2018   assente                 2   -0.2112699
2018   convergência            2   -0.2112699
2018   modelo                  2   -0.2112699
2018   rendimento              2   -0.2112699
2017   emprego                 5   -0.2222898
2017   igualdade               2   -0.2272852
2018   país                   11   -0.2349804
2016   deputados               3   -0.2420463
2016   senhor                  3   -0.2420463
2016   senhores                3   -0.2420463
2019   senhoras                4   -0.2568368
2019   governo                 6   -0.2580775
2018   deputados               5   -0.2594543
2018   senhor                  5   -0.2594543
2018   senhores                5   -0.2594543
2019   públicos                3   -0.2610681
2016   educação                1   -0.2663195
2016   escolar                 1   -0.2663195
2019   saúde                   2   -0.2732682
2016   públicos                2   -0.2739593
2017   2017                    1   -0.2742131
2017   milhões                 1   -0.2742131
2017   pensões                 1   -0.2742131
2017   rendimento              1   -0.2742131
2018   abandono                1   -0.2969104
2018   bases                   1   -0.2969104
2018   comissão                1   -0.2969104
2018   dificuldades            1   -0.2969104
2018   linha                   1   -0.2969104
2018   orçamental              1   -0.2969104
2018   prioridades             1   -0.2969104
2018   reduzimos               1   -0.2969104
2018   sabemos                 1   -0.2969104
2018   taxa                    1   -0.2969104
2018   termos                  1   -0.2969104
2019   conhecimento            1   -0.3067870
2019   incentivo               1   -0.3067870
2019   interior                1   -0.3067870
2019   queremos                1   -0.3067870
2019   país                    9   -0.3170681
2018   competitividade         2   -0.3211782
2018   compromissos            2   -0.3211782
2018   º                       2   -0.3211782
2016   recuperação             2   -0.3271889
2016   austeridade             1   -0.3316241
2018   portugueses             7   -0.3335940
2017   aumento                 1   -0.3399773
2017   º                       1   -0.3399773
2017   redução                 1   -0.3399773
2017   serviços                1   -0.3399773
2017   legislatura             3   -0.3489587
2018   igualdade               3   -0.3504702
2019   inovação                2   -0.3571765
2016   3                       1   -0.3910200
2016   cidadãos                1   -0.3910200
2018   confiança               7   -0.3971449
2017   saúde                   1   -0.3998086
2017   três                    1   -0.3998086
2017   confiança               4   -0.4036167
2019   economia                3   -0.4046876
2019   dívida                  1   -0.4048928
2019   essencial               1   -0.4048928
2019   europeia                1   -0.4048928
2019   habitação               1   -0.4048928
2019   pib                     1   -0.4048928
2019   pobreza                 1   -0.4048928
2019   reforma                 1   -0.4048928
2018   legislatura             5   -0.4070882
2018   fundamentais            1   -0.4200833
2018   interior                1   -0.4200833
2018   longo                   1   -0.4200833
2018   transportes             1   -0.4200833
2016   melhor                  3   -0.4223619
2016   emprego                 4   -0.4271332
2019   assembleia              2   -0.4344798
2019   défice                  2   -0.4344798
2016   futuro                  2   -0.4699885
2018   presidente              5   -0.4760536
2017   futuro                  2   -0.4818265
2019   um                     14   -0.4857267
2019   convergência            1   -0.4917727
2019   milhões                 1   -0.4917727
2019   modelo                  1   -0.4917727
2019   valorização             1   -0.4917727
2019   visão                   1   -0.4917727
2016   assembleia              1   -0.4965076
2018   governo                 6   -0.4971805
2017   empresas                1   -0.5061080
2018   vida                    2   -0.5111154
2017   portugal                3   -0.5162600
2017   programa                3   -0.5162600
2018   democracia              1   -0.5268231
2018   europeia                1   -0.5268231
2018   médio                   1   -0.5268231
2018   política                1   -0.5268231
2016   igualdade               1   -0.5440973
2017   nacional                2   -0.5667535
2019   competitividade         1   -0.5702052
2019   desenvolvimento         1   -0.5702052
2019   público                 1   -0.5702052
2017   país                    5   -0.5861851
2017   portugueses             3   -0.5913766
2017   um                      9   -0.6105305
2016   confiança               3   -0.6124070
2019   ano                     4   -0.6162010
2018   direitos                1   -0.6217491
2018   prazo                   1   -0.6217491
2018   valorização             1   -0.6217491
2018   visão                   1   -0.6217491
2018   anos                   10   -0.6282252
2019   coesão                  1   -0.6420377
2019   orçamento               1   -0.6420377
2017   recuperação             1   -0.6421659
2018   portugal                5   -0.6670750
2019   programa                4   -0.6681048
2019   empresas                1   -0.7707110
2018   melhor                  4   -0.7853359
2018   cidadãos                1   -0.7867525
2016   crescimento             2   -0.8041902
2016   legislatura             1   -0.8509259
2017   investimento            5   -0.8762792
2018   problemas               2   -0.8809414
2019   cumprimos               1   -0.8845432
2019   podemos                 1   -0.8845432
2019   nacional                2   -0.9101144
2019   investimento            8   -0.9137891
2018   medidas                 1   -0.9287929
2019   execução                1   -0.9371908
2018   continuar               1   -1.2230867
2016   anos                    2   -1.2325316
2018   quatro                  1   -1.4650609

</div>

Vendo isto num gráfico


```r
a <- word_log_odds %>%
    group_by(ano) %>%
    top_n(5, log_odds) %>%
    ungroup %>%
    mutate(ano = as.factor(ano),
           word = fct_reorder(word, log_odds)) %>%
    ggplot(aes(word, log_odds, fill = ano)) +
    geom_col(show.legend = FALSE) +
    facet_wrap(~ano, scales = "free_y") +
    coord_flip() +
    scale_y_continuous(expand = c(0,0)) +
    labs(y = "Razões de probabilidades ponderadas de palavras por discurso",
         x = NULL,
         title = "Quais as palavras mais específicas em cada um dos discursos?",
         subtitle = "Palavras com maior probabilidade de serem encontradas no discurso no discurso de cada ano.") + 
   theme_hc()

a
```

![](analise_files/figure-html/unnamed-chunk-16-1.png)<!-- -->

```r
ggsave("output/grafico_prob_palavras.svg", plot = a,
  scale = 1, width = NA, height = NA, units = c("in", "cm", "mm"),
  dpi = 300, limitsize = TRUE)
```

```
## Saving 7 x 5 in image
```

Palavras que diferenciam (ou marcam) este discurso de outros




## Positividade/negatividade dos discursos

Uma das análises possíveis é fazer "sentiment analysis" dos dados - ou seja, que emoções estão associadas a um texto. A área da análise textual de sentimento está em franca expansão, com diversos recursos e metodologias a ser aplicadas.

Para este trabalho, e por uma questão de limitação de tempo e recursos, recorreu-se a uma das formas de análise mais rudimentares - a análise por associação de sentimento palavra a palavra.

Para tal, recorreu-se ao [trabalho de Saif M. Mohammad e Peter D. Turney](https://saifmohammad.com/WebPages/NRC-Emotion-Lexicon.html) que, através de crowsoursing, criaram uma base de dados com 14.183 palavras que atribui um valor booleano a um ou mais dos oito sentimentos básicos definidos na teoria Robert Plutchik. A estes oito sentimentos, acrescentaram ainda mais dois: positiva ou negativa.

Segundo este autor, as oito emoções básicas são: Raiva (Anger), Medo (Fear), Trizteza (Sadness), Nojo (Disgust), Surpresa (Surprise), Antecipação (Anticipation), Alegria (Joy) e Confiança (Trust).

Assim, em jeito de exemplo, a palavra "abominar" provoca, segundo a base de dados dos autores, os sentimentos de Raiva, Nojo, e Medo, ao passo que a palavra "elogio" provoca sentimentos de Antecipação, Felicidade e Confiança.

A lista de palavras e a respectiva classificação de Saif M. Mohammad e Peter D. Turney, do National Research Council Canada, pode ser consultada [aqui](input/NRC-Emotion-Lexicon.csv)



```r
sentiments <- import("input/NRC-Emotion-Lexicon.csv")
data3 <- data2 %>% left_join(sentiments, by= "word")

data_sentiment <- data3 %>% na.omit()
```


### Costa tem discursos mais positivos ou negativos?

```r
sum(data_sentiment$Positive)/nrow(data_sentiment)
```

```
## [1] 0.2694981
```

```r
sum(data_sentiment$Negative)/nrow(data_sentiment)
```

```
## [1] 0.1247104
```
Quase uma em cada três palavras proferidas por Costa na globalidade dos quatro discursos.

### Qual foi o discurso mais positivo?E o mais negativo?


```r
data_sentiment %>% 
   group_by(ano) %>% 
   summarise(Sum_Posit = sum(Positive),
             Sum_Nega = sum(Negative),
             count = n()) -> sentiments_pos_neg

sentiments_pos_neg$per_Pos <- (sentiments_pos_neg$Sum_Posit/sentiments_pos_neg$count)*100
sentiments_pos_neg$per_Neg <- (sentiments_pos_neg$Sum_Nega/sentiments_pos_neg$count)*100

sentiments_pos_neg
```

<div class="kable-table">

ano     Sum_Posit   Sum_Nega   count    per_Pos    per_Neg
-----  ----------  ---------  ------  ---------  ---------
2016          115         70     495   23.23232   14.14141
2017          159         76     561   28.34225   13.54724
2018          211        104     819   25.76313   12.69841
2019          213         73     715   29.79021   10.20979

</div>


```r
sentimentos_transformed <- import("input/sentimentos_transformed.csv")

#sentimentos_transformed$per <- as.numeric(sentimentos_transformed$per)

b <- sentimentos_transformed %>% ggplot() + geom_bar(aes(y = per, x = ano, fill= Sentimento), stat="identity") + 
  coord_flip() +
    labs(y = NULL,
         x = NULL) + 
  scale_fill_manual(values = c("#ff4c4c","#bfbfbf", "#66b266" )) + 
  scale_y_continuous(labels = function(x) paste0(x, "%")) +
   theme_hc()

b
```

![](analise_files/figure-html/unnamed-chunk-20-1.png)<!-- -->

```r
ggsave("output/grafico_sentimentos.svg", plot = b,
  scale = 1, width = NA, height = NA, units = c("in", "cm", "mm"),
  dpi = 300, limitsize = TRUE)
```

```
## Saving 7 x 5 in image
```




## Que sentimento prevalece em cada um dos anos?




```r
sentimentos_ano <- data_sentiment %>% 
                group_by(ano) %>% 
                summarise_at(c("Anger","Anticipation","Disgust","Fear","Joy","Sadness","Surprise","Trust"), sum, na.rm = TRUE)

sentimentos_ano
```

<div class="kable-table">

ano     Anger   Anticipation   Disgust   Fear   Joy   Sadness   Surprise   Trust
-----  ------  -------------  --------  -----  ----  --------  ---------  ------
2016       31             37        15     36    25        23          6      82
2017       33             47        25     50    20        30         15      97
2018       31             55        24     54    34        38          8     129
2019       25             55        23     57    54        19         20     152

</div>

```r
sentimentos_ano <- as.data.frame(t(sentimentos_ano))
colnames(sentimentos_ano ) <- as.character(unlist(sentimentos_ano [1,]))
sentimentos_ano <- sentimentos_ano[-1, ]

sentimentos_ano <- sentimentos_ano %>%  add_rownames( var = "sentimento" )
```

```
## Warning: Deprecated, use tibble::rownames_to_column() instead.
```

```r
export(sentimentos_ano,"output/sentimentos_ano.csv","csv")
```
# Análise por bigramas


```r
data_bigrams <- df %>%
  unnest_tokens(bigram, text, token = "ngrams", n = 2)


bigrams_separated <- data_bigrams %>%
  separate(bigram, c("word1", "word2"), sep = " ")

bigrams_filtered <- bigrams_separated %>%
  filter(!word1 %in% stop_words$word) %>%
  filter(!word2 %in% stop_words$word)

# new bigram counts:
bigram_counts <- bigrams_filtered %>% 
  count(word1, word2, sort = TRUE)

bigrams_united <- bigrams_filtered %>%
  unite(bigram, word1, word2, sep = " ")

bigrams_united
```

<div class="kable-table">

ano    bigram                         
-----  -------------------------------
2016   senhor presidente              
2016   presidente senhoras            
2016   senhores deputados             
2016   governo assumiu                
2016   assumiu funções                
2016   três propósitos                
2016   propósitos fundamentais        
2016   mudança manifestada            
2016   austeridade iniciando          
2016   famílias criando               
2016   criando condições              
2016   união europeia                 
2016   um modelo                      
2016   competitividade assente        
2016   baixos salários                
2016   desenhar um                    
2016   desenvolvimento assente        
2016   parlamentar consistente        
2016   identidade coerente            
2016   posições conjuntas             
2016   legislatura cumprimos          
2016   permanente sobressalto         
2016   país vivia                     
2016   vivia construindo              
2016   construindo um                 
2016   um clima                       
2016   paz social                     
2016   normalidade institucional      
2016   institucional portugal         
2016   um país                        
2016   soberania cooperam             
2016   interesse nacional             
2016   nacional um                    
2016   um país                        
2016   construir compromissos         
2016   autonomias regionais           
2016   democrático portugal           
2016   portugal vive                  
2016   um clima                       
2016   concertação social             
2016   diálogo social                 
2016   social um                      
2016   um país                        
2016   pré avisos                     
2016   greve baixaram                 
2016   2016 prometemos                
2016   cumprimos cumprimos            
2016   famílias eliminámos            
2016   irs baixámos                   
2016   taxa máxima                    
2016   salvaguarda pusemos            
2016   família repusemos              
2016   mínimos socias                 
2016   família reduzimos              
2016   taxas moderadoras              
2016   tarifa social                  
2016   ano letivo                     
2016   1 º                            
2016   º ano                          
2016   1 º                            
2016   º ciclo                        
2016   famílias cumprimos             
2016   pensionistas rejeitámos        
2016   anterior governo               
2016   comprometido acabámos          
2016   contribuição extraordinária    
2016   reforma cumprimos              
2016   salario mínimo                 
2016   mínimo nacional                
2016   nacional eliminámos            
2016   funcionários públicos          
2016   públicos pusemos               
2016   alteração unilateral           
2016   funcionários públicos          
2016   feriados nacionais             
2016   sido suspensos                 
2016   mapa judiciário                
2016   interior prometemos            
2016   medida contrária               
2016   portugueses prometemos         
2016   ação política                  
2016   futuro senhor                  
2016   senhor presidente              
2016   presidente senhoras            
2016   senhores deputados             
2016   um governo                     
2016   enormes desafios               
2016   sido necessário                
2016   empresas face                  
2016   problemas enfrentam            
2016   saída limpa                    
2016   problemas existem              
2016   agir agir                      
2016   agir precisamos                
2016   melhor emprego                 
2016   criar um                       
2016   um ambiente                    
2016   ambiente favorável             
2016   eliminámos limitações          
2016   autarquias poderem             
2016   poderem investir               
2016   procurámos estabilizar         
2016   autonomia financeira           
2016   programa capitalizar           
2016   adotamos medidas               
2016   fundo nacional                 
2016   reabilitação urbana            
2016   reabilitação urbana            
2016   fundo azul                     
2016   programa nacional              
2016   cubra 90                       
2016   possa levar                    
2016   bacias hidrográficas           
2016   país obteve                    
2016   programa startup               
2016   startup portugal               
2016   país precisa                   
2016   um tecido                      
2016   tecido empresarial             
2016   empreendedores agir            
2016   fundos comunitários            
2016   comunitários representam       
2016   administrações elegemos        
2016   fundos comunitários            
2016   empresas lançámos              
2016   plano 100                      
2016   pagar 100                      
2016   100 milhões                    
2016   100 dias                       
2016   governo cumprimos              
2016   200 milhões                    
2016   euros estabelecemos            
2016   queremos atingir               
2016   450 milhões                    
2016   45 acima                       
2016   ano homólogo                   
2016   homólogo obtivemos             
2016   investidores corresponderam    
2016   procura recorde                
2016   recorde apresentando           
2016   apresentando candidaturas      
2016   euros demos                    
2016   investimento municipal         
2016   municipal justifica            
2016   economias locais               
2016   domínio lançámos               
2016   dotação global                 
2016   400 milhões                    
2016   projetos relativos             
2016   escolas unidades               
2016   património histórico           
2016   histórico celebrámos           
2016   pedu planos                    
2016   planos estratégicos            
2016   desenvolvimento urbano         
2016   reabilitação urbana            
2016   urbana orientados              
2016   centros urbanos                
2016   dimensão permitiram            
2016   dotação global                 
2016   1.000 milhões                  
2016   municípios apresentar          
2016   mobilidade urbanas             
2016   intervenção social             
2016   comunidades desfavorecidas     
2016   basta contratar                
2016   portugal 2020                  
2016   2020 um                        
2016   um mecanismo                   
2016   mecanismo excecional           
2016   recursos adicionais            
2016   adicionais àqueles             
2016   projetos previstos             
2016   anos posteriores               
2016   assumir agir                   
2016   criar condições                
2016   faça previsões                 
2016   medidas necessárias            
2016   agimos agir                    
2016   execução orçamental            
2016   comissão europeia              
2016   europeia reconhece             
2016   execução orçamental            
2016   1 º                            
2016   º trimestre                    
2016   2016 reduziu                   
2016   período homólogo               
2016   défice orçamental              
2016   1 º                            
2016   º trimestre                    
2016   execução orçamental            
2016   défice excessivo               
2016   planos b                       
2016   medidas adicionais             
2016   determinação rigor             
2016   rigor senhor                   
2016   senhor presidente              
2016   presidente senhoras            
2016   senhores deputados             
2016   deputados cumprimos            
2016   move concretizar               
2016   precisamente construir         
2016   construir um                   
2016   um país                        
2016   vivam melhor                   
2016   melhor um                      
2016   um país                        
2016   assegure melhores              
2016   melhores condições             
2016   interesses correspondem        
2016   verdadeiras prioridades        
2016   governo um                     
2016   um país                        
2016   jovens veem                    
2016   veem reconhecido               
2016   longo prazo                    
2016   programa nacional              
2016   reformas um                    
2016   um programa                    
2016   seis pilares                   
2016   pilares fundamentais           
2016   pré escolar                    
2016   três anos                      
2016   emprego científico             
2016   iniciativa indústria           
2016   indústria 4.0                  
2016   economia circular              
2016   eficiência energética          
2016   programa capitalizar           
2016   prestação única                
2016   cuidados continuados           
2016   comissão europeia              
2016   portugal declarou              
2016   programa nacional              
2016   reformas revela                
2016   revela um                      
2016   um grau                        
2016   ambição suficiente             
2016   desequilíbrios excessivos      
2016   excessivos apresenta           
2016   apresenta medidas              
2016   medidas relevantes             
2016   dívida privada                 
2016   programa nacional              
2016   médio prazo                    
2016   competitividade assente        
2016   baixos salários                
2016   desenvolvimento assente        
2016   bloqueios estruturais          
2016   crescimento melhor             
2016   melhor emprego                 
2017   senhor presidente              
2017   presidente senhoras            
2017   senhores deputados             
2017   défice excessivo               
2017   subitamente abalados           
2017   últimas décadas                
2017   revolta evidenciando           
2017   vida humana                    
2017   populações ameaçadas           
2017   gratidão devida                
2017   auxílio internacional          
2017   sociedade mostrando            
2017   extraordinária capacidade      
2017   tarefas imediatas              
2017   enfrentar um                   
2017   um desafio                     
2017   desafio estrutural             
2017   capacidade produtiva           
2017   territórios afetados           
2017   estradas linhas                
2017   média tensão                   
2017   esclarecer cabalmente          
2017   factos apurar                  
2017   apurar responsabilidades       
2017   total transparência            
2017   organismos públicos            
2017   públicos produziram            
2017   estudos científicos            
2017   governo dará                   
2017   inquérito crime                
2017   crime aberto                   
2017   ministério público             
2017   comissão técnica               
2017   técnica independente           
2017   independente constituída       
2017   senhor presidente              
2017   presidente senhoras            
2017   senhores deputados             
2017   podermos evitar                
2017   país espera                    
2017   governo criou                  
2017   ano passado                    
2017   governo lançou                 
2017   podemos continuar              
2017   podemos continuar              
2017   micro mini                     
2017   mini fundio                    
2017   gera rendimento                
2017   rendimento suficiente          
2017   estruturas associativas        
2017   associativas privadas          
2017   necessária valorização         
2017   valorização económica          
2017   podemos continuar              
2017   aprovar mecanismos             
2017   agilizem finalmente            
2017   podemos continuar              
2017   podemos continuar              
2017   impacto económico              
2017   revoltas populares             
2017   difícil exige                  
2017   prazo produz                   
2017   produz resultados              
2017   deixar andar                   
2017   começarmos apelo               
2017   esforço conjunto               
2017   reforma estrutural             
2017   sete concelhos                 
2017   concelhos martirizados         
2017   góis um                        
2017   um projeto                     
2017   projeto piloto                 
2017   reordenamento florestal        
2017   efeito iremos                  
2017   iremos deslocalizar            
2017   possa executar                 
2017   executar senhor                
2017   senhor presidente              
2017   presidente senhoras            
2017   senhores deputados             
2017   cinco indicadores              
2017   política económica             
2017   desemprego recuou              
2017   população ativa                
2017   decisiva criação               
2017   agentes económicos             
2017   consumidores encontra          
2017   clima económico                
2017   económico atingiu              
2017   atingiu máximos                
2017   últimos 15                     
2017   15 anos                        
2017   1 º                            
2017   º trimestre                    
2017   crescimento homólogo           
2017   últimos 18                     
2017   18 anos                        
2017   indicadores avançados          
2017   avançados mostram              
2017   atuais níveis                  
2017   economia sustentado            
2017   pib atingiu                    
2017   1º trimestre                   
2017   século retomando               
2017   retomando finalmente           
2017   zona euro                      
2017   últimos 10                     
2017   10 anos                        
2017   país cumpriu                   
2017   metas orçamentais              
2017   orçamentais registando         
2017   défices excessivos             
2017   um acaso                       
2017   resultados surgem              
2017   adotámos afinal                
2017   direitos laborais              
2017   crescimento homólogo           
2017   15,4 afinal                    
2017   carga fiscal                   
2017   apoios sociais                 
2017   finanças públicas              
2017   carga fiscal                   
2017   gerou confiança                
2017   crescimento melhor             
2017   melhor emprego                 
2017   igualdade investir             
2017   senhor presidente              
2017   presidente senhoras            
2017   senhores deputados             
2017   exige persistência             
2017   exige políticas                
2017   políticas sustentadas          
2017   sustentáveis um                
2017   defesa nacional                
2017   segurança interna              
2017   moderno ágil                   
2017   ágil eficaz                    
2017   eficaz inteligente             
2017   estrategicamente empreendedor  
2017   empreendedor um                
2017   serviços públicos              
2017   participação cívica            
2017   coesão social                  
2017   direitos fundamentais          
2017   ministro adalberto             
2017   adalberto campos               
2017   campos fernandes               
2017   garantir um                    
2017   um sns                         
2017   educação aprofundaremos        
2017   défice estrutural              
2017   país promovendo                
2017   sucesso escolar                
2017   antigas gerações               
2017   ano letivo                     
2017   importantes contributos        
2017   sucesso escolar                
2017   autonomia pedagógica           
2017   gestão flexível                
2017   territórios educativos         
2017   intervenção prioritária        
2017   políticas públicas             
2017   públicas dirigida              
2017   classes médias                 
2017   cidades promovendo             
2017   arrendamento acessível         
2017   ajustamento governativo        
2017   amanhã apresentarei            
2017   previsto precisamente          
2017   intervenção estratégica        
2017   administração pública          
2017   setor público                  
2017   funções públicas               
2017   públicas continuaremos         
2017   programa simplex               
2017   simplex procurando             
2017   procurando simplificar         
2017   competências qualificadas      
2017   domínios especializados        
2017   especializados permitirão      
2017   permitirão diminuir            
2017   contratação externa            
2017   administração pública          
2017   funções públicas               
2017   públicas continuará            
2017   prioridade estando             
2017   estando previsto               
2017   verdadeira coesão              
2017   coesão social                  
2017   essencial pôr                  
2017   transportes públicos           
2017   perder passageiros             
2017   ano 7                          
2017   carris permitiram              
2017   470.000 viagens                
2017   65 anos                        
2017   100.000 viagens                
2017   10 anos                        
2017   17 anos                        
2017   carris abriu                   
2017   carreiras abrindo              
2017   stcp adjudicaram               
2017   autocarros aliás               
2017   indústria nacional             
2017   podemos replicar               
2017   experiência comprova           
2017   comprova podemos               
2017   podemos confiar                
2017   melhor senhor                  
2017   senhor presidente              
2017   presidente senhoras            
2017   senhores deputados             
2017   preciso pensar                 
2017   atual legislatura              
2017   médio prazo                    
2017   reforce simultaneamente        
2017   competitividade externa        
2017   coesão interna                 
2017   convergência continuada        
2017   união europeia                 
2017   portugal pós                   
2017   pós 2020                       
2017   2020 impõe                     
2017   debate iniciámos               
2017   concertação territorial        
2017   regiões autónomas              
2017   linhas gerais                  
2017   programa pòs                   
2017   pòs 2020                       
2017   2020 debate                    
2017   iremos alargar                 
2017   trabalhos parlamentares        
2017   trabalhamos assenta            
2017   dois eixos                     
2017   eixos horizontais              
2017   horizontais inovação           
2017   conhecimento qualificação      
2017   qualificação formação          
2017   4 eixos                        
2017   eixos territoriais             
2017   territoriais energia           
2017   alterações climáticas          
2017   climáticas economia            
2017   mar inserção                   
2017   mercados globais               
2017   globais interioridade          
2017   mercado ibérico                
2017   modelo claro                   
2017   claro sermos                   
2017   coesos internamente            
2017   internamente sermos            
2017   economia global                
2017   união europeia                 
2017   europeia três                  
2017   três c’s                       
2017   c’s competitividade            
2017   convergência senhor            
2017   senhor presidente              
2017   presidente senhoras            
2017   senhores deputados             
2017   emergência persistindo         
2017   visão estratégica              
2018   senhor presidente              
2018   presidente senhoras            
2018   senhores deputados             
2018   deputados 1                    
2018   quatro compromissos            
2018   governo assumiu                
2018   austeridade devolvendo         
2018   normalidade constitucional     
2018   um programa                    
2018   programa assente               
2018   proteção social                
2018   social promovendo              
2018   modo sustentado                
2018   finanças públicas              
2018   públicas reduzindo             
2018   dívida pública                 
2018   pública 2                      
2018   três anos                      
2018   conseguiríamos conciliar       
2018   zona euro                      
2018   euro três                      
2018   três anos                      
2018   anos volvidos                  
2018   volvidos podemos               
2018   conseguiríamos melhorar        
2018   competitividade rompendo       
2018   desenvolvimento assente        
2018   direitos três                  
2018   três anos                      
2018   anos volvidos                  
2018   volvidos confirma              
2018   três anos                      
2018   anos volvidos                  
2018   volvidos podemos               
2018   acrescentar conseguimos        
2018   rendimentos levou              
2018   um aumento                     
2018   deu um                         
2018   um impulso                     
2018   impulso decisivo               
2018   pib registado                  
2018   investimento cresceu           
2018   cresceu 9,1                    
2018   variação homóloga              
2018   últimos 19                     
2018   19 anos                        
2018   serviços cresceram             
2018   cresceram 11,2                 
2018   resultados falam               
2018   desemprego recuou              
2018   2002 7,2                       
2018   7,2 registando                 
2018   zona euro                      
2018   longa duração                  
2018   melhor emprego                 
2018   emprego 85                     
2018   parcial encontraram            
2018   encontraram um                 
2018   um emprego                     
2018   permitiu recuperar             
2018   recuperar 45                   
2018   população ativa                
2018   ativa quebrado                 
2018   retrocesso social              
2018   desigualdades diminuíram       
2018   pobreza recuou                 
2018   níveis pré                     
2018   pré crise                      
2018   crescimento melhor             
2018   melhor emprego                 
2018   execução orçamental            
2018   contas públicas                
2018   pib levando                    
2018   comissão europeia              
2018   retirar portugal               
2018   desequilíbrios macroeconómicos 
2018   macroeconómicos excessivos     
2018   dívida pública                 
2018   pública registou               
2018   1991 recuando                  
2018   2017 4                         
2018   dívida pública                 
2018   compromisso assumido           
2018   segurança social               
2018   dívida resultado               
2018   gestão rigorosa                
2018   rigorosa assegura              
2018   salários aumentámos            
2018   salário mínimo                 
2018   mínimo nacional                
2018   rendimento real                
2018   famílias cresceu               
2018   cresceu 4,7                    
2018   mínimos sociais                
2018   sociais atualizámos            
2018   apoios sociais                 
2018   sociais aumentámos             
2018   pobreza baixou                 
2018   níveis pré                     
2018   pré crise                      
2018   dimensão relevante             
2018   governo prosseguiremos         
2018   legislatura senhor             
2018   senhor presidente              
2018   presidente senhoras            
2018   senhores deputados             
2018   deputados 3                    
2018   portugueses sabemos            
2018   preciso continuar              
2018   queremos prosseguir            
2018   finanças públicas              
2018   podemos pôr                    
2018   constante sobressalto          
2018   jovens obrigados               
2018   salários baixos                
2018   empregos precários             
2018   sociedade fragmentada          
2018   um território                  
2018   território dividido            
2018   podemos voltar                 
2018   aceitar um                     
2018   distante senhor                
2018   senhor presidente              
2018   presidente senhoras            
2018   senhores deputados             
2018   deputados 4                    
2018   três críticas                  
2018   críticas fundamentais          
2018   procuram desvalorizar          
2018   resultados alcançados          
2018   investimento público           
2018   público esgotando              
2018   serviços públicos              
2018   públicos resultariam           
2018   austeridade encapotada         
2018   adiado sine                    
2018   sine die                       
2018   reformas necessárias           
2018   realidade desmente             
2018   três críticas                  
2018   investimento público           
2018   público cresceu                
2018   cresceu 22                     
2018   um crescimento                 
2018   particular destaque            
2018   administração central          
2018   investimento público           
2018   potencial produtivo            
2018   coesão territorial             
2018   investimento público           
2018   proximidade destaca            
2018   20 tribunais                   
2018   246 espaços                    
2018   113 centros                    
2018   200 escolas                    
2018   tornado verdadeiros            
2018   verdadeiros símbolos           
2018   escola secundária              
2018   secundária alexandre           
2018   alexandre herculano            
2018   conservatório nacional         
2018   programa nacional              
2018   um excelente                   
2018   um investimento                
2018   investimento fundamental       
2018   potencial produtivo            
2018   visa criar                     
2018   metade replicando              
2018   regiões centro                 
2018   beira alta                     
2018   beira baixa                    
2018   investimento ferroviário       
2018   últimos 100                    
2018   100 anos                       
2018   rodovia ignorada               
2018   pt2020 concentrámo             
2018   localização empresarial        
2018   ip3 via                        
2018   via essencial                  
2018   sinistralidade rodoviária      
2018   economia exige                 
2018   exige forte                    
2018   forte investimento             
2018   mobilidade urbana              
2018   urbana demos                   
2018   demos prioridade               
2018   material circulante            
2018   porto lançámos                 
2018   lançámos decididamente         
2018   portugal 2020                  
2018   adiámos investimento           
2018   investimento público           
2018   público aumentámo              
2018   aumentámo lo                   
2018   serviços públicos              
2018   públicos voltámos              
2018   serviço nacional               
2018   um reforço                     
2018   reforço anual                  
2018   700 m                          
2018   m contratando                  
2018   7.900 profissionais            
2018   profissionais alargando        
2018   saúde familiar                 
2018   cuidados continuados           
2018   continuados integrados         
2018   taxas moderadoras              
2018   saúde baixou                   
2018   consultas hospitalares         
2018   família apostámos              
2018   apostámos igualmente           
2018   escola pública                 
2018   pública tendo                  
2018   7.000 professores              
2018   2.500 assistentes              
2018   assistentes operacionais       
2018   turma manuais                  
2018   manuais gratuitos              
2018   1 º                            
2018   º ciclo                        
2018   ação social                    
2018   social reforçada               
2018   sucesso escolar                
2018   flexibilização pedagógica      
2018   sucesso escolar                
2018   escolar aumentou               
2018   aumentou 2                     
2018   2 p.p                          
2018   ensino básico                  
2018   abandono precoce               
2018   precoce baixou                 
2018   12,6 aproximando               
2018   austeridade investimos         
2018   serviços públicos              
2018   país enfrente                  
2018   bloqueios estruturais          
2018   pedra angular                  
2018   freguesias municípios          
2018   áreas metropolitanas           
2018   metropolitanas podemos         
2018   finalmente assumida            
2018   visa assegurar                 
2018   assegurar habitação            
2018   habitação acessível            
2018   carência habitacional          
2018   adiada simplificámos           
2018   gestão florestal               
2018   áreas florestais               
2018   florestais possam              
2018   escala economicamente          
2018   economicamente viável          
2018   lançámos um                    
2018   um programa                    
2018   cadastro rural                 
2018   projeto piloto                 
2018   prédios georreferenciados      
2018   georreferenciados amanhã       
2018   ministros extraordinário       
2018   extraordinário aprovaremos     
2018   planos regionais               
2018   ordenamento florestal          
2018   florestal criámos              
2018   programa capitalizar           
2018   excessivo endividamento        
2018   financiamento bancário         
2018   empresas portuguesas           
2018   portuguesas um                 
2018   principais bloqueios           
2018   bloqueios estruturais          
2018   empresas voltassem             
2018   contratar ajudando             
2018   investimento empresarial       
2018   termos assumido                
2018   política transversal           
2018   pré escolar                    
2018   3 anos                         
2018   laboratórios colaborativos     
2018   colaborativos investindo       
2018   educação formação              
2018   formação investigação          
2018   desenvolvimento transferência  
2018   tecido empresarial             
2018   indústria 4.0                  
2018   visão estratégica              
2018   sociedade procurado            
2018   procurado dotar                
2018   necessários instrumentos       
2018   instrumentos estratégicos      
2018   médio prazo                    
2018   feito anualmente               
2018   programa nacional              
2018   portugal 2030                  
2018   aprovaremos amanhã             
2018   debate público                 
2018   programa nacional              
2018   queremos discutir              
2018   2 3                            
2018   recuos penalizadores           
2018   infelizmente aconteceu         
2018   aeroporto internacional        
2018   elevados custos                
2018   economia nacional              
2018   nacional suporta               
2018   suporta senhor                 
2018   senhor presidente              
2018   presidente senhoras            
2018   senhores deputados             
2018   setembro entramos              
2018   última sessão                  
2018   sessão legislativa             
2018   um ano                         
2018   vamos sacrificar               
2018   rumo garantir                  
2018   trajeto seguido                
2018   resultados alcançados          
2018   serviços públicos              
2018   coesão territorial             
2018   um orçamento                   
2018   continuidade continuidade      
2018   ano prosseguiremos             
2018   ano consecutivo                
2018   aumentadas 68                  
2018   68 acima                       
2018   inflação continuidade          
2018   proteção social                
2018   aumento progressivo            
2018   três anos                      
2018   4 º                            
2018   º escalão                      
2018   pleno funcionamento            
2018   prestação única                
2018   deficiência relativa           
2018   encargos específicos           
2018   específicos decorrentes        
2018   deficiência continuidade       
2018   serviços públicos              
2018   públicos educação              
2018   saúde naturalmente             
2018   transportes públicos           
2018   ano iremos                     
2018   iremos quadruplicar            
2018   investimento realizado         
2018   legislatura anterior           
2018   recursos humanos               
2018   material circulante            
2018   metro investiremos             
2018   investiremos 50                
2018   50 m                           
2018   soflusa continuidade           
2018   coesão territorial             
2018   territorial dando              
2018   dando tradução                 
2018   tradução financeira            
2018   programa nacional              
2018   coesão territorial             
2018   aprovaremos amanhã             
2018   recuperação económica          
2018   inovador aprovámos             
2018   permitirá aumentar             
2018   contas públicas                
2018   um orçamento                   
2018   podemos graças                 
2018   fizemos concentrar             
2018   desígnio nacional              
2018   jovens possam                  
2018   realizarem plenamente          
2018   vista pessoal                  
2018   profissional garantir          
2018   orçamento incluirá             
2018   incluirá um                    
2018   um programa                    
2018   forte estímulo                 
2018   estímulo fiscal                
2018   mobilidade familiar            
2018   emigrantes especialmente       
2018   viram forçados                 
2018   forte crise                    
2018   crise económica                
2018   permitam alcançar              
2018   i d                            
2018   2019 reforçando                
2018   cultura garantir               
2018   cultura reforçando             
2018   administração pública          
2018   um papel                       
2018   papel essencial                
2018   valorizar rejuvenescer         
2018   administração procederemos     
2018   1.000 jovens                   
2018   formação superior              
2018   áreas estratégicas             
2018   políticas públicas             
2018   administração senhor           
2018   senhor presidente              
2018   presidente senhoras            
2018   senhores deputados             
2018   permitirá cumprir              
2018   crescimento melhor             
2018   melhor emprego                 
2018   dois anos                      
2018   continuaremos motivados        
2018   marcar passo                   
2018   passo progredindo              
2018   progredindo passo              
2019   senhor presidente              
2019   presidente senhoras            
2019   senhores deputados             
2019   democracia vive                
2019   vitalidade democrática         
2019   tantos países                  
2019   mudança necessária             
2019   impasse bloqueia               
2019   descrença mina                 
2019   democracia cede                
2019   grupos parlamentares           
2019   ps be                          
2019   be pcp                         
2019   terem ousado                   
2019   ousado derrubar                
2019   derrubar um                    
2019   um muro                        
2019   muro anacrónico                
2019   governo garantindo             
2019   cidadãos desejavam             
2019   portugal precisava             
2019   constituímos provou            
2019   cumprir integralmente          
2019   posições conjuntas             
2019   deram consistência             
2019   compromissos internacionais    
2019   últimos quatro                 
2019   quatro anos                    
2019   anos um                        
2019   fatores centrais               
2019   portugueses deixaram           
2019   sobressalto quotidiano         
2019   orçamentos retificativos       
2019   permanente conflito            
2019   conflito constitucional        
2019   quatro anos                    
2019   cumprir passo                  
2019   quatro anos                    
2019   quatro orçamentos              
2019   medidas previstas              
2019   quatro anos                    
2019   sólida cooperação              
2019   cooperação institucional       
2019   república escrupuloso          
2019   escrupuloso respeito           
2019   autonomias regionais           
2019   quatro anos                    
2019   um consenso                    
2019   consenso parlamentar           
2019   parlamentar alargado           
2019   investimentos estratégicos     
2019   recentes aprovações            
2019   programa nacional              
2019   investimentos 20               
2019   20 30                          
2019   programação militar            
2019   infraestruturas militares      
2019   militares fundamentais         
2019   notável contributo             
2019   forças armadas                 
2019   prestígio internacional        
2019   quatro anos                    
2019   orientação sexual              
2019   quatro anos                    
2019   normalidade constitucional     
2019   um único                       
2019   único pedido                   
2019   fiscalização preventiva        
2019   declarada inconstitucional     
2019   tribunal constitucional        
2019   suma governámos                
2019   estabilidade política          
2019   normalidade institucional      
2019   elementos fundamentais         
2019   2018 senhor                    
2019   senhor presidente              
2019   presidente senhoras            
2019   senhores deputados             
2019   governo assumi                 
2019   assumi um                      
2019   um triplo                      
2019   triplo desígnio                
2019   crescimento melhor             
2019   melhor emprego                 
2019   igualdade quatro               
2019   quatro anos                    
2019   anos volvidos                  
2019   balanço podemos                
2019   triplo desígnio                
2019   crescimento portugal           
2019   portugal cresce                
2019   cresce 9                       
2019   termos reais                   
2019   quatro anos                    
2019   anos tendo                     
2019   tendo retomado                 
2019   2019 um                        
2019   um crescimento                 
2019   crescimento superior           
2019   ue retomando                   
2019   convergência interrompida      
2019   um crescimento                 
2019   crescimento fortemente         
2019   fortemente sustentado          
2019   investimento empresarial       
2019   empresarial apoiado            
2019   elevada execução               
2019   investimento induz             
2019   maiores quotas                 
2019   mercado demonstram             
2019   melhor emprego                 
2019   quatro anos                    
2019   criados 350.000                
2019   um aumento                     
2019   rendimento médio               
2019   médio mensal                   
2019   mensal líquido                 
2019   salário mínimo                 
2019   quase 20                       
2019   melhor evidência               
2019   outrem serem                   
2019   serem contratos                
2019   melhor antídoto                
2019   privação material              
2019   material severa                
2019   pobres reduziu                 
2019   crescimento melhor             
2019   melhor emprego                 
2019   igualdade provam               
2019   gerou crescimento              
2019   criou emprego                  
2019   enorme aumento                 
2019   contas certas                  
2019   gerou confiança                
2019   criou emprego                  
2019   círculo virtuoso               
2019   termos contas                  
2019   contas certas                  
2019   dívida pública                 
2019   diabo apareceu                 
2019   investimento público           
2019   público financiado             
2019   oe aumentou                    
2019   aumentou 45                    
2019   prestação social               
2019   3 milhões                      
2019   1.000 m                        
2019   irs ano                        
2019   taxas moderadoras              
2019   disponibilizámos manuais       
2019   manuais escolares              
2019   escolares gratuitos            
2019   ensino básico                  
2019   300.000 crianças               
2019   reduzimos drasticamente        
2019   transportes públicos           
2019   melhor emprego                 
2019   contas certas                  
2019   país conquistou                
2019   fator essencial                
2019   permitiu sair                  
2019   défice excessivo               
2019   contínua redução               
2019   0,5 permitindo                 
2019   2.000 m                        
2019   atraiu valores                 
2019   valores record                 
2019   investimento direto            
2019   direto estrangeiro             
2019   emigração massiva              
2019   massiva garantindo             
2019   últimos dois                   
2019   dois anos                      
2019   anos saldos                    
2019   saldos migratórios             
2019   migratórios positivos          
2019   quatro anos                    
2019   anos portugal                  
2019   portugal melhorou              
2019   melhorou melhorando            
2019   portugueses senhor             
2019   senhor presidente              
2019   presidente senhoras            
2019   senhores deputados             
2019   deputados cumprimos            
2019   futuro inspirada               
2019   longo prazo                    
2019   diáspora alargando             
2019   vida nacional                  
2019   descentralização configuram    
2019   efetiva igualdade              
2019   vida pessoal                   
2019   pessoal familiar               
2019   economia azul                  
2019   economia digital               
2019   autonomia financeira           
2019   renovada prioridade            
2019   cultura ciência                
2019   um roteiro                     
2019   neutralidade carbónica         
2019   futuro assente                 
2019   crescimento sustentável        
2019   quatro anos                    
2019   demos prioridade               
2019   desafios estratégicos          
2019   desafio demográfico            
2019   alterações climáticas          
2019   sociedade digital              
2019   segurança social               
2019   dimensão estratégica           
2019   unicef considerou              
2019   melhores políticas             
2019   31 países                      
2019   países desenvolvidos           
2019   inovação registou              
2019   portugal colocando             
2019   pme’s inovadoras               
2019   sermos reclassificados         
2019   um país                        
2019   país fortemente                
2019   fortemente inovador            
2019   comissão europeia              
2019   europeia portugal              
2019   ano passado                    
2019   segurança social               
2019   22 anos                        
2019   anos senhor                    
2019   senhor presidente              
2019   presidente senhoras            
2019   senhores deputados             
2019   país cor                       
2019   balanço positivo               
2019   quatro anos                    
2019   permite esquecer               
2019   medidas adotadas               
2019   portugal subiu                 
2019   18 º                           
2019   3 º                            
2019   º país                         
2019   nelas servem                   
2019   quatro anos                    
2019   anos reduzimos                 
2019   pendências processuais         
2019   baixa taxa                     
2019   abandono escolar               
2019   escolar precoce                
2019   escola pública                 
2019   ensino superior                
2019   superior aumentou              
2019   aumentou 4                     
2019   ação social                    
2019   social escolar                 
2019   formações superiores           
2019   superiores admitimos           
2019   saúde crescemos                
2019   melhor emprego                 
2019   reduzir conhecemos             
2019   legítima exigência             
2019   problema procuramos            
2019   melhor solução                 
2019   solução permitam               
2019   3 exemplos                     
2019   exemplos 1º                    
2019   cidadão simplificámos          
2019   renovação aumentámos           
2019   contactar preventivamente      
2019   renovar propondo               
2019   propondo agendamento           
2019   agendamento programado         
2019   renovação online               
2019   junho aumentámos               
2019   cartões emitidos               
2019   transportes públicos           
2019   longo prazo                    
2019   um forte                       
2019   forte investimento             
2019   cp metros                      
2019   legislatura investimos         
2019   investimos quatro              
2019   legislatura anterior           
2019   transportes públicos           
2019   simultaneamente procurado      
2019   procurado responder            
2019   médio prazo                    
2019   equipamento imobilizado        
2019   30 composições                 
2019   material ferroviário           
2019   aprovámos permitirá            
2019   circulação dezenas             
2019   castelopermitirá deslocar      
2019   linhas designadamente          
2019   oeste 3º                       
2019   saúde procuramos               
2019   procuramos responder           
2019   problemas históricos           
2019   arrastaram anos                
2019   problemas pontuais             
2019   semana passada                 
2019   lisboa seguramente             
2019   ala pediátrica                 
2019   s joão                         
2019   ala pediátrica                 
2019   pediátrica elaborámos          
2019   edifício garantimos            
2019   financiamento necessário       
2019   edifício principal             
2019   senhor presidente              
2019   presidente senhores            
2019   senhores deputados             
2019   prestar contas                 
2019   quatro anos                    
2019   distanciamento necessários     
2019   alternativa podíamos           
2019   pessimismo provámos            
2019   assembleia construíram         
2019   sobretudo agradecer            
2019   portugueses quatro             
2019   quatro anos                    
2019   anos volvidos                  
2019   país recuperou                 
2019   respeito internacional         
2019   otimismo portugal              
2019   quatro anos                    
2019   portugueses vivem              
2019   vivem melhor                   
2019   quatro anos                    
2019   anos portugal                  
2019   portugueses recuperaram        
2019   confiança recuperaram          
2019   esperança assente              
2019   bases sólidas                  
2019   quatro anos                    
2019   ministro partilho              
2019   principalmente um              
2019   um cidadão                     
2019   cidadão português              

</div>

## Top bigrams


```r
top_bigrams <- bigrams_united %>%
                  count(bigram, sort = TRUE) %>% 
                  mutate(bigram = reorder(bigram, n))
top_bigrams 
```

<div class="kable-table">

bigram                             n
-------------------------------  ---
quatro anos                       19
senhor presidente                 19
senhores deputados                19
presidente senhoras               18
melhor emprego                    11
programa nacional                  9
um país                            7
1 º                                6
crescimento melhor                 6
investimento público               6
serviços públicos                  6
três anos                          6
anos volvidos                      5
podemos continuar                  5
transportes públicos               5
coesão territorial                 4
comissão europeia                  4
dívida pública                     4
execução orçamental                4
médio prazo                        4
sucesso escolar                    4
um programa                        4
administração pública              3
bloqueios estruturais              3
contas certas                      3
défice excessivo                   3
desenvolvimento assente            3
finanças públicas                  3
longo prazo                        3
º trimestre                        3
programa capitalizar               3
reabilitação urbana                3
segurança social                   3
taxas moderadoras                  3
um crescimento                     3
união europeia                     3
zona euro                          3
10 anos                            2
ação social                        2
ala pediátrica                     2
alterações climáticas              2
ano letivo                         2
ano passado                        2
anos portugal                      2
apoios sociais                     2
aprovaremos amanhã                 2
autonomia financeira               2
autonomias regionais               2
baixos salários                    2
carga fiscal                       2
coesão social                      2
competitividade assente            2
contas públicas                    2
crescimento homólogo               2
criou emprego                      2
cuidados continuados               2
demos prioridade                   2
deputados cumprimos                2
desemprego recuou                  2
dois anos                          2
dotação global                     2
ensino básico                      2
escola pública                     2
forte investimento                 2
funcionários públicos              2
funções públicas                   2
fundos comunitários                2
gerou confiança                    2
governo assumiu                    2
indústria 4.0                      2
investimento empresarial           2
legislatura anterior               2
material circulante                2
mínimo nacional                    2
níveis pré                         2
normalidade constitucional         2
normalidade institucional          2
º ciclo                            2
políticas públicas                 2
população ativa                    2
portugal 2020                      2
posições conjuntas                 2
potencial produtivo                2
pré crise                          2
pré escolar                        2
prestação única                    2
projeto piloto                     2
proteção social                    2
resultados alcançados              2
salário mínimo                     2
tecido empresarial                 2
três críticas                      2
triplo desígnio                    2
um aumento                         2
um clima                           2
um orçamento                       2
visão estratégica                  2
volvidos podemos                   2
0,5 permitindo                     1
1.000 jovens                       1
1.000 m                            1
1.000 milhões                      1
100 anos                           1
100 dias                           1
100 milhões                        1
100.000 viagens                    1
113 centros                        1
12,6 aproximando                   1
15 anos                            1
15,4 afinal                        1
17 anos                            1
18 anos                            1
18 º                               1
19 anos                            1
1991 recuando                      1
1º trimestre                       1
2 3                                1
2 p.p                              1
2.000 m                            1
2.500 assistentes                  1
20 30                              1
20 tribunais                       1
200 escolas                        1
200 milhões                        1
2002 7,2                           1
2016 prometemos                    1
2016 reduziu                       1
2017 4                             1
2018 senhor                        1
2019 reforçando                    1
2019 um                            1
2020 debate                        1
2020 impõe                         1
2020 um                            1
22 anos                            1
246 espaços                        1
3 anos                             1
3 exemplos                         1
3 milhões                          1
3 º                                1
30 composições                     1
300.000 crianças                   1
31 países                          1
4 eixos                            1
4 º                                1
400 milhões                        1
45 acima                           1
450 milhões                        1
470.000 viagens                    1
50 m                               1
65 anos                            1
68 acima                           1
7,2 registando                     1
7.000 professores                  1
7.900 profissionais                1
700 m                              1
abandono escolar                   1
abandono precoce                   1
ação política                      1
aceitar um                         1
acrescentar conseguimos            1
adalberto campos                   1
adiada simplificámos               1
adiado sine                        1
adiámos investimento               1
adicionais àqueles                 1
administração central              1
administração procederemos         1
administração senhor               1
administrações elegemos            1
adotámos afinal                    1
adotamos medidas                   1
aeroporto internacional            1
agendamento programado             1
agentes económicos                 1
ágil eficaz                        1
agilizem finalmente                1
agimos agir                        1
agir agir                          1
agir precisamos                    1
ajustamento governativo            1
alexandre herculano                1
alteração unilateral               1
alternativa podíamos               1
amanhã apresentarei                1
ambição suficiente                 1
ambiente favorável                 1
ano 7                              1
ano consecutivo                    1
ano homólogo                       1
ano iremos                         1
ano prosseguiremos                 1
anos posteriores                   1
anos reduzimos                     1
anos saldos                        1
anos senhor                        1
anos tendo                         1
anos um                            1
anterior governo                   1
antigas gerações                   1
apostámos igualmente               1
apresenta medidas                  1
apresentando candidaturas          1
aprovámos permitirá                1
aprovar mecanismos                 1
apurar responsabilidades           1
áreas estratégicas                 1
áreas florestais                   1
áreas metropolitanas               1
arrastaram anos                    1
arrendamento acessível             1
assegurar habitação                1
assegure melhores                  1
assembleia construíram             1
assistentes operacionais           1
associativas privadas              1
assumi um                          1
assumir agir                       1
assumiu funções                    1
atingiu máximos                    1
ativa quebrado                     1
atraiu valores                     1
atuais níveis                      1
atual legislatura                  1
aumentadas 68                      1
aumentámo lo                       1
aumento progressivo                1
aumentou 2                         1
aumentou 4                         1
aumentou 45                        1
austeridade devolvendo             1
austeridade encapotada             1
austeridade iniciando              1
austeridade investimos             1
autarquias poderem                 1
autocarros aliás                   1
autonomia pedagógica               1
auxílio internacional              1
avançados mostram                  1
bacias hidrográficas               1
baixa taxa                         1
balanço podemos                    1
balanço positivo                   1
bases sólidas                      1
basta contratar                    1
be pcp                             1
beira alta                         1
beira baixa                        1
c’s competitividade                1
cadastro rural                     1
campos fernandes                   1
capacidade produtiva               1
carência habitacional              1
carreiras abrindo                  1
carris abriu                       1
carris permitiram                  1
cartões emitidos                   1
castelopermitirá deslocar          1
centros urbanos                    1
cidadão português                  1
cidadão simplificámos              1
cidadãos desejavam                 1
cidades promovendo                 1
cinco indicadores                  1
circulação dezenas                 1
círculo virtuoso                   1
claro sermos                       1
classes médias                     1
clima económico                    1
climáticas economia                1
coesão interna                     1
coesos internamente                1
colaborativos investindo           1
começarmos apelo                   1
comissão técnica                   1
competências qualificadas          1
competitividade externa            1
competitividade rompendo           1
comprometido acabámos              1
compromisso assumido               1
compromissos internacionais        1
comprova podemos                   1
comunidades desfavorecidas         1
comunitários representam           1
concelhos martirizados             1
concertação social                 1
concertação territorial            1
confiança recuperaram              1
conflito constitucional            1
conhecimento qualificação          1
conseguiríamos conciliar           1
conseguiríamos melhorar            1
consenso parlamentar               1
conservatório nacional             1
constante sobressalto              1
constituímos provou                1
construindo um                     1
construir compromissos             1
construir um                       1
consultas hospitalares             1
consumidores encontra              1
contactar preventivamente          1
contínua redução                   1
continuados integrados             1
continuaremos motivados            1
continuidade continuidade          1
contratação externa                1
contratar ajudando                 1
contribuição extraordinária        1
convergência continuada            1
convergência interrompida          1
convergência senhor                1
cooperação institucional           1
cp metros                          1
cresce 9                           1
cresceram 11,2                     1
cresceu 22                         1
cresceu 4,7                        1
cresceu 9,1                        1
crescimento fortemente             1
crescimento portugal               1
crescimento superior               1
crescimento sustentável            1
criados 350.000                    1
criando condições                  1
criar condições                    1
criar um                           1
crime aberto                       1
crise económica                    1
críticas fundamentais              1
cubra 90                           1
cultura ciência                    1
cultura garantir                   1
cultura reforçando                 1
cumprimos cumprimos                1
cumprir integralmente              1
cumprir passo                      1
dando tradução                     1
debate iniciámos                   1
debate público                     1
decisiva criação                   1
declarada inconstitucional         1
defesa nacional                    1
défice estrutural                  1
défice orçamental                  1
défices excessivos                 1
deficiência continuidade           1
deficiência relativa               1
deixar andar                       1
democracia cede                    1
democracia vive                    1
democrático portugal               1
deputados 1                        1
deputados 3                        1
deputados 4                        1
deram consistência                 1
derrubar um                        1
desafio demográfico                1
desafio estrutural                 1
desafios estratégicos              1
descentralização configuram        1
descrença mina                     1
desenhar um                        1
desenvolvimento transferência      1
desenvolvimento urbano             1
desequilíbrios excessivos          1
desequilíbrios macroeconómicos     1
desígnio nacional                  1
desigualdades diminuíram           1
determinação rigor                 1
deu um                             1
diabo apareceu                     1
diálogo social                     1
diáspora alargando                 1
difícil exige                      1
dimensão estratégica               1
dimensão permitiram                1
dimensão relevante                 1
direitos fundamentais              1
direitos laborais                  1
direitos três                      1
direto estrangeiro                 1
disponibilizámos manuais           1
distanciamento necessários         1
distante senhor                    1
dívida privada                     1
dívida resultado                   1
dois eixos                         1
domínio lançámos                   1
domínios especializados            1
economia azul                      1
economia circular                  1
economia digital                   1
economia exige                     1
economia global                    1
economia nacional                  1
economia sustentado                1
economias locais                   1
economicamente viável              1
económico atingiu                  1
edifício garantimos                1
edifício principal                 1
educação aprofundaremos            1
educação formação                  1
efeito iremos                      1
efetiva igualdade                  1
eficaz inteligente                 1
eficiência energética              1
eixos horizontais                  1
eixos territoriais                 1
elementos fundamentais             1
elevada execução                   1
elevados custos                    1
eliminámos limitações              1
emergência persistindo             1
emigração massiva                  1
emigrantes especialmente           1
empreendedor um                    1
empreendedores agir                1
emprego 85                         1
emprego científico                 1
empregos precários                 1
empresarial apoiado                1
empresas face                      1
empresas lançámos                  1
empresas portuguesas               1
empresas voltassem                 1
encargos específicos               1
encontraram um                     1
enfrentar um                       1
enorme aumento                     1
enormes desafios                   1
ensino superior                    1
equipamento imobilizado            1
escala economicamente              1
esclarecer cabalmente              1
escola secundária                  1
escolar aumentou                   1
escolar precoce                    1
escolares gratuitos                1
escolas unidades                   1
escrupuloso respeito               1
esforço conjunto                   1
especializados permitirão          1
específicos decorrentes            1
esperança assente                  1
essencial pôr                      1
estabilidade política              1
estando previsto                   1
estímulo fiscal                    1
estradas linhas                    1
estrategicamente empreendedor      1
estruturas associativas            1
estudos científicos                1
euro três                          1
europeia portugal                  1
europeia reconhece                 1
europeia três                      1
euros demos                        1
euros estabelecemos                1
excessivo endividamento            1
excessivos apresenta               1
executar senhor                    1
exemplos 1º                        1
exige forte                        1
exige persistência                 1
exige políticas                    1
experiência comprova               1
extraordinária capacidade          1
extraordinário aprovaremos         1
faça previsões                     1
factos apurar                      1
família apostámos                  1
família reduzimos                  1
família repusemos                  1
famílias cresceu                   1
famílias criando                   1
famílias cumprimos                 1
famílias eliminámos                1
fator essencial                    1
fatores centrais                   1
feito anualmente                   1
feriados nacionais                 1
finalmente assumida                1
financiamento bancário             1
financiamento necessário           1
fiscalização preventiva            1
fizemos concentrar                 1
flexibilização pedagógica          1
florestais possam                  1
florestal criámos                  1
forças armadas                     1
formação investigação              1
formação superior                  1
formações superiores               1
forte crise                        1
forte estímulo                     1
fortemente inovador                1
fortemente sustentado              1
freguesias municípios              1
fundo azul                         1
fundo nacional                     1
futuro assente                     1
futuro inspirada                   1
futuro senhor                      1
garantir um                        1
georreferenciados amanhã           1
gera rendimento                    1
gerou crescimento                  1
gestão flexível                    1
gestão florestal                   1
gestão rigorosa                    1
globais interioridade              1
góis um                            1
governo assumi                     1
governo criou                      1
governo cumprimos                  1
governo dará                       1
governo garantindo                 1
governo lançou                     1
governo prosseguiremos             1
governo um                         1
gratidão devida                    1
greve baixaram                     1
grupos parlamentares               1
habitação acessível                1
histórico celebrámos               1
homólogo obtivemos                 1
horizontais inovação               1
i d                                1
identidade coerente                1
igualdade investir                 1
igualdade provam                   1
igualdade quatro                   1
impacto económico                  1
impasse bloqueia                   1
importantes contributos            1
impulso decisivo                   1
incluirá um                        1
independente constituída           1
indicadores avançados              1
indústria nacional                 1
infelizmente aconteceu             1
inflação continuidade              1
infraestruturas militares          1
iniciativa indústria               1
inovação registou                  1
inovador aprovámos                 1
inquérito crime                    1
institucional portugal             1
instrumentos estratégicos          1
interesse nacional                 1
interesses correspondem            1
interior prometemos                1
internamente sermos                1
intervenção estratégica            1
intervenção prioritária            1
intervenção social                 1
investidores corresponderam        1
investimento cresceu               1
investimento direto                1
investimento ferroviário           1
investimento fundamental           1
investimento induz                 1
investimento municipal             1
investimento realizado             1
investimentos 20                   1
investimentos estratégicos         1
investimos quatro                  1
investiremos 50                    1
ip3 via                            1
iremos alargar                     1
iremos deslocalizar                1
iremos quadruplicar                1
irs ano                            1
irs baixámos                       1
jovens obrigados                   1
jovens possam                      1
jovens veem                        1
junho aumentámos                   1
laboratórios colaborativos         1
lançámos decididamente             1
lançámos um                        1
legislatura cumprimos              1
legislatura investimos             1
legislatura senhor                 1
legítima exigência                 1
linhas designadamente              1
linhas gerais                      1
lisboa seguramente                 1
localização empresarial            1
longa duração                      1
m contratando                      1
macroeconómicos excessivos         1
maiores quotas                     1
manuais escolares                  1
manuais gratuitos                  1
mapa judiciário                    1
mar inserção                       1
marcar passo                       1
massiva garantindo                 1
material ferroviário               1
material severa                    1
mecanismo excecional               1
média tensão                       1
medida contrária                   1
medidas adicionais                 1
medidas adotadas                   1
medidas necessárias                1
medidas previstas                  1
medidas relevantes                 1
médio mensal                       1
melhor antídoto                    1
melhor evidência                   1
melhor senhor                      1
melhor solução                     1
melhor um                          1
melhores condições                 1
melhores políticas                 1
melhorou melhorando                1
mensal líquido                     1
mercado demonstram                 1
mercado ibérico                    1
mercados globais                   1
metade replicando                  1
metas orçamentais                  1
metro investiremos                 1
metropolitanas podemos             1
micro mini                         1
migratórios positivos              1
militares fundamentais             1
mini fundio                        1
mínimos sociais                    1
mínimos socias                     1
ministério público                 1
ministro adalberto                 1
ministro partilho                  1
ministros extraordinário           1
mobilidade familiar                1
mobilidade urbana                  1
mobilidade urbanas                 1
modelo claro                       1
moderno ágil                       1
modo sustentado                    1
move concretizar                   1
mudança manifestada                1
mudança necessária                 1
municipal justifica                1
municípios apresentar              1
muro anacrónico                    1
nacional eliminámos                1
nacional suporta                   1
nacional um                        1
necessária valorização             1
necessários instrumentos           1
nelas servem                       1
neutralidade carbónica             1
notável contributo                 1
º ano                              1
º escalão                          1
º país                             1
oe aumentou                        1
oeste 3º                           1
orçamentais registando             1
orçamento incluirá                 1
orçamentos retificativos           1
ordenamento florestal              1
organismos públicos                1
orientação sexual                  1
otimismo portugal                  1
ousado derrubar                    1
outrem serem                       1
pagar 100                          1
país conquistou                    1
país cor                           1
país cumpriu                       1
país enfrente                      1
país espera                        1
país fortemente                    1
país obteve                        1
país precisa                       1
país promovendo                    1
país recuperou                     1
país vivia                         1
países desenvolvidos               1
papel essencial                    1
parcial encontraram                1
parlamentar alargado               1
parlamentar consistente            1
participação cívica                1
particular destaque                1
passo progredindo                  1
património histórico               1
paz social                         1
pediátrica elaborámos              1
pedra angular                      1
pedu planos                        1
pendências processuais             1
pensionistas rejeitámos            1
perder passageiros                 1
período homólogo                   1
permanente conflito                1
permanente sobressalto             1
permitam alcançar                  1
permite esquecer                   1
permitirá aumentar                 1
permitirá cumprir                  1
permitirão diminuir                1
permitiu recuperar                 1
permitiu sair                      1
pessimismo provámos                1
pessoal familiar                   1
pib atingiu                        1
pib levando                        1
pib registado                      1
pilares fundamentais               1
plano 100                          1
planos b                           1
planos estratégicos                1
planos regionais                   1
pleno funcionamento                1
pme’s inovadoras                   1
pobres reduziu                     1
pobreza baixou                     1
pobreza recuou                     1
podemos confiar                    1
podemos graças                     1
podemos pôr                        1
podemos replicar                   1
podemos voltar                     1
poderem investir                   1
podermos evitar                    1
política económica                 1
política transversal               1
políticas sustentadas              1
populações ameaçadas               1
porto lançámos                     1
portugal 2030                      1
portugal colocando                 1
portugal cresce                    1
portugal declarou                  1
portugal melhorou                  1
portugal pós                       1
portugal precisava                 1
portugal subiu                     1
portugal vive                      1
portuguesas um                     1
portugueses deixaram               1
portugueses prometemos             1
portugueses quatro                 1
portugueses recuperaram            1
portugueses sabemos                1
portugueses senhor                 1
portugueses vivem                  1
pós 2020                           1
pòs 2020                           1
possa executar                     1
possa levar                        1
prazo produz                       1
pré avisos                         1
precisamente construir             1
preciso continuar                  1
preciso pensar                     1
precoce baixou                     1
prédios georreferenciados          1
presidente senhores                1
prestação social                   1
prestar contas                     1
prestígio internacional            1
previsto precisamente              1
principais bloqueios               1
principalmente um                  1
prioridade estando                 1
privação material                  1
problema procuramos                1
problemas enfrentam                1
problemas existem                  1
problemas históricos               1
problemas pontuais                 1
procura recorde                    1
procurado dotar                    1
procurado responder                1
procuram desvalorizar              1
procurámos estabilizar             1
procuramos responder               1
procurando simplificar             1
produz resultados                  1
profissionais alargando            1
profissional garantir              1
programa assente                   1
programa pòs                       1
programa simplex                   1
programa startup                   1
programação militar                1
progredindo passo                  1
projetos previstos                 1
projetos relativos                 1
propondo agendamento               1
propósitos fundamentais            1
proximidade destaca                1
ps be                              1
pt2020 concentrámo                 1
pública 2                          1
pública registou                   1
pública tendo                      1
públicas continuará                1
públicas continuaremos             1
públicas dirigida                  1
públicas reduzindo                 1
público aumentámo                  1
público cresceu                    1
público esgotando                  1
público financiado                 1
públicos educação                  1
públicos produziram                1
públicos pusemos                   1
públicos resultariam               1
públicos voltámos                  1
qualificação formação              1
quase 20                           1
quatro compromissos                1
quatro orçamentos                  1
queremos atingir                   1
queremos discutir                  1
queremos prosseguir                1
realidade desmente                 1
realizarem plenamente              1
recentes aprovações                1
recorde apresentando               1
recuos penalizadores               1
recuperação económica              1
recuperar 45                       1
recursos adicionais                1
recursos humanos                   1
reduzimos drasticamente            1
reduzir conhecemos                 1
reforce simultaneamente            1
reforço anual                      1
reforma cumprimos                  1
reforma estrutural                 1
reformas necessárias               1
reformas revela                    1
reformas um                        1
regiões autónomas                  1
regiões centro                     1
rendimento médio                   1
rendimento real                    1
rendimento suficiente              1
rendimentos levou                  1
renovação aumentámos               1
renovação online                   1
renovada prioridade                1
renovar propondo                   1
reordenamento florestal            1
república escrupuloso              1
respeito internacional             1
resultados falam                   1
resultados surgem                  1
retirar portugal                   1
retomando finalmente               1
retrocesso social                  1
revela um                          1
revolta evidenciando               1
revoltas populares                 1
rigor senhor                       1
rigorosa assegura                  1
rodovia ignorada                   1
rumo garantir                      1
s joão                             1
saída limpa                        1
salario mínimo                     1
salários aumentámos                1
salários baixos                    1
saldos migratórios                 1
salvaguarda pusemos                1
saúde baixou                       1
saúde crescemos                    1
saúde familiar                     1
saúde naturalmente                 1
saúde procuramos                   1
século retomando                   1
secundária alexandre               1
segurança interna                  1
seis pilares                       1
semana passada                     1
serem contratos                    1
sermos reclassificados             1
serviço nacional                   1
serviços cresceram                 1
sessão legislativa                 1
sete concelhos                     1
setembro entramos                  1
setor público                      1
sido necessário                    1
sido suspensos                     1
simplex procurando                 1
simultaneamente procurado          1
sine die                           1
sinistralidade rodoviária          1
soberania cooperam                 1
sobressalto quotidiano             1
sobretudo agradecer                1
sociais atualizámos                1
sociais aumentámos                 1
social escolar                     1
social promovendo                  1
social reforçada                   1
social um                          1
sociedade digital                  1
sociedade fragmentada              1
sociedade mostrando                1
sociedade procurado                1
soflusa continuidade               1
sólida cooperação                  1
solução permitam                   1
startup portugal                   1
stcp adjudicaram                   1
subitamente abalados               1
suma governámos                    1
superior aumentou                  1
superiores admitimos               1
suporta senhor                     1
sustentáveis um                    1
tantos países                      1
tarefas imediatas                  1
tarifa social                      1
taxa máxima                        1
técnica independente               1
tendo retomado                     1
terem ousado                       1
termos assumido                    1
termos contas                      1
termos reais                       1
territoriais energia               1
territorial dando                  1
território dividido                1
territórios afetados               1
territórios educativos             1
tornado verdadeiros                1
total transparência                1
trabalhamos assenta                1
trabalhos parlamentares            1
tradução financeira                1
trajeto seguido                    1
três c’s                           1
três propósitos                    1
tribunal constitucional            1
turma manuais                      1
ue retomando                       1
última sessão                      1
últimas décadas                    1
últimos 10                         1
últimos 100                        1
últimos 15                         1
últimos 18                         1
últimos 19                         1
últimos dois                       1
últimos quatro                     1
um acaso                           1
um ambiente                        1
um ano                             1
um cidadão                         1
um consenso                        1
um desafio                         1
um emprego                         1
um excelente                       1
um forte                           1
um governo                         1
um grau                            1
um impulso                         1
um investimento                    1
um mecanismo                       1
um modelo                          1
um muro                            1
um papel                           1
um projeto                         1
um reforço                         1
um roteiro                         1
um sns                             1
um tecido                          1
um território                      1
um triplo                          1
um único                           1
unicef considerou                  1
único pedido                       1
urbana demos                       1
urbana orientados                  1
valores record                     1
valorização económica              1
valorizar rejuvenescer             1
vamos sacrificar                   1
variação homóloga                  1
veem reconhecido                   1
verdadeira coesão                  1
verdadeiras prioridades            1
verdadeiros símbolos               1
via essencial                      1
vida humana                        1
vida nacional                      1
vida pessoal                       1
viram forçados                     1
visa assegurar                     1
visa criar                         1
vista pessoal                      1
vitalidade democrática             1
vivam melhor                       1
vivem melhor                       1
vivia construindo                  1
volvidos confirma                  1

</div>

"melhor emprego" foi a expressão mais vezes usada pelo PM nos quatro discursos (11 vezes)	


## Top bigrams 2019


```r
top_bigrams_2019 <- bigrams_united %>%
                  filter(ano == 2019) %>% 
                  count(bigram, sort = TRUE) %>% 
                  mutate(bigram = reorder(bigram, n))
top_bigrams_2019 
```

<div class="kable-table">

bigram                          n
----------------------------  ---
quatro anos                    19
melhor emprego                  5
senhor presidente               5
senhores deputados              5
presidente senhoras             4
contas certas                   3
transportes públicos            3
ala pediátrica                  2
anos portugal                   2
anos volvidos                   2
crescimento melhor              2
criou emprego                   2
longo prazo                     2
segurança social                2
triplo desígnio                 2
um crescimento                  2
0,5 permitindo                  1
1.000 m                         1
18 º                            1
2.000 m                         1
20 30                           1
2018 senhor                     1
2019 um                         1
22 anos                         1
3 exemplos                      1
3 milhões                       1
3 º                             1
30 composições                  1
300.000 crianças                1
31 países                       1
abandono escolar                1
ação social                     1
agendamento programado          1
alterações climáticas           1
alternativa podíamos            1
ano passado                     1
anos reduzimos                  1
anos saldos                     1
anos senhor                     1
anos tendo                      1
anos um                         1
aprovámos permitirá             1
arrastaram anos                 1
assembleia construíram          1
assumi um                       1
atraiu valores                  1
aumentou 4                      1
aumentou 45                     1
autonomia financeira            1
autonomias regionais            1
baixa taxa                      1
balanço podemos                 1
balanço positivo                1
bases sólidas                   1
be pcp                          1
cartões emitidos                1
castelopermitirá deslocar       1
cidadão português               1
cidadão simplificámos           1
cidadãos desejavam              1
circulação dezenas              1
círculo virtuoso                1
comissão europeia               1
compromissos internacionais     1
confiança recuperaram           1
conflito constitucional         1
consenso parlamentar            1
constituímos provou             1
contactar preventivamente       1
contínua redução                1
convergência interrompida       1
cooperação institucional        1
cp metros                       1
cresce 9                        1
crescimento fortemente          1
crescimento portugal            1
crescimento superior            1
crescimento sustentável         1
criados 350.000                 1
cultura ciência                 1
cumprir integralmente           1
cumprir passo                   1
declarada inconstitucional      1
défice excessivo                1
democracia cede                 1
democracia vive                 1
demos prioridade                1
deputados cumprimos             1
deram consistência              1
derrubar um                     1
desafio demográfico             1
desafios estratégicos           1
descentralização configuram     1
descrença mina                  1
diabo apareceu                  1
diáspora alargando              1
dimensão estratégica            1
direto estrangeiro              1
disponibilizámos manuais        1
distanciamento necessários      1
dívida pública                  1
dois anos                       1
economia azul                   1
economia digital                1
edifício garantimos             1
edifício principal              1
efetiva igualdade               1
elementos fundamentais          1
elevada execução                1
emigração massiva               1
empresarial apoiado             1
enorme aumento                  1
ensino básico                   1
ensino superior                 1
equipamento imobilizado         1
escola pública                  1
escolar precoce                 1
escolares gratuitos             1
escrupuloso respeito            1
esperança assente               1
estabilidade política           1
europeia portugal               1
exemplos 1º                     1
fator essencial                 1
fatores centrais                1
financiamento necessário        1
fiscalização preventiva         1
forças armadas                  1
formações superiores            1
forte investimento              1
fortemente inovador             1
fortemente sustentado           1
futuro assente                  1
futuro inspirada                1
gerou confiança                 1
gerou crescimento               1
governo assumi                  1
governo garantindo              1
grupos parlamentares            1
igualdade provam                1
igualdade quatro                1
impasse bloqueia                1
infraestruturas militares       1
inovação registou               1
investimento direto             1
investimento empresarial        1
investimento induz              1
investimento público            1
investimentos 20                1
investimentos estratégicos      1
investimos quatro               1
irs ano                         1
junho aumentámos                1
legislatura anterior            1
legislatura investimos          1
legítima exigência              1
linhas designadamente           1
lisboa seguramente              1
maiores quotas                  1
manuais escolares               1
massiva garantindo              1
material ferroviário            1
material severa                 1
medidas adotadas                1
medidas previstas               1
médio mensal                    1
médio prazo                     1
melhor antídoto                 1
melhor evidência                1
melhor solução                  1
melhores políticas              1
melhorou melhorando             1
mensal líquido                  1
mercado demonstram              1
migratórios positivos           1
militares fundamentais          1
ministro partilho               1
mudança necessária              1
muro anacrónico                 1
nelas servem                    1
neutralidade carbónica          1
normalidade constitucional      1
normalidade institucional       1
notável contributo              1
º país                          1
oe aumentou                     1
oeste 3º                        1
orçamentos retificativos        1
orientação sexual               1
otimismo portugal               1
ousado derrubar                 1
outrem serem                    1
país conquistou                 1
país cor                        1
país fortemente                 1
país recuperou                  1
países desenvolvidos            1
parlamentar alargado            1
pediátrica elaborámos           1
pendências processuais          1
permanente conflito             1
permite esquecer                1
permitiu sair                   1
pessimismo provámos             1
pessoal familiar                1
pme’s inovadoras                1
pobres reduziu                  1
portugal colocando              1
portugal cresce                 1
portugal melhorou               1
portugal precisava              1
portugal subiu                  1
portugueses deixaram            1
portugueses quatro              1
portugueses recuperaram         1
portugueses senhor              1
portugueses vivem               1
posições conjuntas              1
presidente senhores             1
prestação social                1
prestar contas                  1
prestígio internacional         1
principalmente um               1
privação material               1
problema procuramos             1
problemas históricos            1
problemas pontuais              1
procurado responder             1
procuramos responder            1
programa nacional               1
programação militar             1
propondo agendamento            1
ps be                           1
público financiado              1
quase 20                        1
quatro orçamentos               1
recentes aprovações             1
reduzimos drasticamente         1
reduzir conhecemos              1
rendimento médio                1
renovação aumentámos            1
renovação online                1
renovada prioridade             1
renovar propondo                1
república escrupuloso           1
respeito internacional          1
s joão                          1
salário mínimo                  1
saldos migratórios              1
saúde crescemos                 1
saúde procuramos                1
semana passada                  1
serem contratos                 1
sermos reclassificados          1
simultaneamente procurado       1
sobressalto quotidiano          1
sobretudo agradecer             1
social escolar                  1
sociedade digital               1
sólida cooperação               1
solução permitam                1
suma governámos                 1
superior aumentou               1
superiores admitimos            1
tantos países                   1
taxas moderadoras               1
tendo retomado                  1
terem ousado                    1
termos contas                   1
termos reais                    1
tribunal constitucional         1
ue retomando                    1
últimos dois                    1
últimos quatro                  1
um aumento                      1
um cidadão                      1
um consenso                     1
um forte                        1
um muro                         1
um país                         1
um roteiro                      1
um triplo                       1
um único                        1
unicef considerou               1
único pedido                    1
valores record                  1
vida nacional                   1
vida pessoal                    1
vitalidade democrática          1
vivem melhor                    1

</div>





