---
title: "Médicos Tarefeiros nos Hospitais Portugueses"
author: "Rui Barros (rui.barros@rr.pt) e Inês Rocha (ines.rocha@rr.pt)"
date: "2019-06-19"
output:
   html_document:
     keep_md: TRUE
     code_folding: show
     echo: TRUE
     warning: FALSE
     message: FALSE
     theme: lumen
     df_print: kable
     toc: yes
     toc_depth: 4
     toc_float: 
       collapsed: false
       smooth_scroll: false
subtitle: Análise aos dados da ACSS e do Portal BASE  
---


# Introdução

Neste trabalho, procuramos avaliar a dimensão do negócio da prestação de serviços médicos - vulgos tarefeiros - nas contas dos hospitais portugueses, bem como perceber se os hospitais portigueses cumprem as normas de transparência.

Para tal recorremos ao [portal BASE](http://www.base.gov.pt/Base/pt/Homepage) e, através deste, procurar avaliar o volume deste negócio, comparando-o com os dados disponibilizados pela ACSS nos últimos anos à imprensa.

Elencamos, numa primeira fase, uma base de dados com todos os NIF's associados a Hospitais Públicos. Procedeu-se então à recolha de todos os contratos celebrados entre 2011 e 2019 entre esses Hospitais e qualquer tipo de instituição.

Feita essa primeira recolha, procedeu-se a uma limpeza automatizada que eliminou um conjunto de contratos que não estariam diretamente relacionados com a contratação de médicos tarefeiros (ver [Recolha de Dados](#recolha_e_importação_de_dados)).

Como não era certo que essa primeira recolha não tivesse eliminado um volume significativo de contratos válidos para a nossa análise, gerou-se uma lista de Entidades Adjucicatárias - isto é, uma base de dados com o NIF de todas as empresas que celebraram pelo menos uma vez um contrato com hospitais públicos dessa base de dados mais filtrada - para proceder a uma segunda recolha.

Conseguiram-se assim obter todos os contratos celebrados com essas Entidades.

Precedeu-se então a uma classificação manual dessa segunda base de dados, classificando todos os contratos que constavam com "N" para os casos em que claramente não eram contratos de serviços médicos, "S" para os contratos que indiciavam um contrato de prestação de serviços médicos e "NS" para os casos que não era claro se os contratos em questão eram referentes a contratação de médicos.

Esta última limpeza foi feita de forma manual, tendo por base a leitura da descrição do contrato e o contrato disponibilizado em pdf (quando disponível).

Como os critérios da ACSS sobre o que considera um contrato de prestação de serviços não são nunca clarificados pela instituição - nomeadamente no que toca a contratação de MCDT's - os autores desta análise admitem que alguns contratos possam ter sido indevidamente classificados.


## Packages necessários

Importação dos pacotes utilizados nesta análise:


```r
#Caso não tenha o package "needs", correr a linha abaixo, eliminando o "#"
#install.packages("needs")
library(needs)
needs(tidyr,readr,dplyr,ggplot2,rio,stringr,data.table,knitr,bizdays,fuzzyjoin)
```


# Recolha e importação de dados

## Lista de Hospitais a recolher

De forma a podermos fazer a recolha dos dados, foi necessário compilar uma lista de hospitais do país. Como ponto de partida, recorremos ao portal da transparência, que já elencava uma lista de hospitais que tinham contratado médicos tarefeiros em 2016.

Essa lista foi, no entanto, completada com os restantes hospitais que constam no [Portal do SNS](https://www.sns.gov.pt/institucional/entidades-de-saude/?fbclid=IwAR3xoZrN2IQ78bNogK7h2bIN0z6duH25VZxuSvCdxo55Fa6Ja0XbEadqyfs) do SNS.

Fechada a lista de Hospitais, recorreu-se a múltiplas fontes como o portal [NIF.pt](https://www.nif.pt/) e o próprio [portal BASE](http://www.base.gov.pt)
para obter a lista de NIF's destas instituições.


```r
bd_hospitais <-import("input/prestadores-de-servicos-medicos_2016.csv")
bd_hospitais <- data.frame(bd_hospitais$Instituição,bd_hospitais$`Localização Geográfica`) 

bd_hospitais <- distinct(bd_hospitais)
bd_hospitais$nif <- ""
export(bd_hospitais,"output/hospitais_sem_nif.csv","csv")

bd_hospitais <- import("input/hospitais_com_nif.csv")
```


## Query ao Portal Base

Tendo os NIF's dos hospitais que se pretende recolher, recorreu-se à API do Portal Base para recolher todos os contratos de todos os hospitais entre 2011 e 2019.

*nota: este processo é longo e pode demorar até vários dias, dependendo da resposta do Portal Base. Como a recolha já foi feita, recomenda-se que se recorra aos ficheiros já disponibilizados neste repositório se quiser reproduzir a anlise feita*


```r
# for (i in 1:nrow(bd_hospitais)) { 
#     for (a in 2011:2019) {
#         
#         for (x in 01:12) {
#             
# 
#         for (d in seq(1, 31, by=10)) {
#             
#         print(paste("getting ", i , bd_hospitais[i,1],"___", a,"-",x,"-",d, "...",a,"-",x,"-",d+10))               
#               
#   
#         if (x < 10 | d < 10) {
#             
#             d_1 <- d+10
# 
#             
#             f<- paste0("http://www.base.gov.pt/base2/rest/contratos.csv?texto=&tipo=0&tipocontrato=0&cpv=&numeroanuncio=&aqinfo=&adjudicante=",bd_hospitais[i,3],"&adjudicataria=&desdeprecocontrato_false=&desdeprecocontrato=&ateprecocontrato_false=&ateprecocontrato=&desdedatacontrato=&atedatacontrato=&desdedatapublicacao=", a,"-0",x,"-0",d,"&atedatapublicacao=",a,"-0",x,"-0",d_1,"&desdeprazoexecucao=&ateprazoexecucao=&desdedatafecho=&atedatafecho=&desdeprecoefectivo_false=&desdeprecoefectivo=&ateprecoefectivo_false=&ateprecoefectivo=&pais=0&distrito=0&concelho=0&sort(-publicationDate)")
#           
#         df <- import(f)
#         
#         Sys.sleep(3)
#         
#           export(df,paste0("output/contratos_hospitais_all/bd_contratos_",bd_hospitais[i,3],"__", a,"-",x,"-",d,".csv"),"csv")
#             
#            
#         }
#             
#         if(x >= 10 | d >= 10) {
#             
#          d_1 <- d+10
# 
#             
#         f<- paste0("http://www.base.gov.pt/base2/rest/contratos.csv?texto=&tipo=0&tipocontrato=0&cpv=&numeroanuncio=&aqinfo=&adjudicante=",bd_hospitais[i,3],"&adjudicataria=&desdeprecocontrato_false=&desdeprecocontrato=&ateprecocontrato_false=&ateprecocontrato=&desdedatacontrato=&atedatacontrato=&desdedatapublicacao=", a,"-",x,"-",d,"&atedatapublicacao=",a,"-",x,"-",d_1,"&desdeprazoexecucao=&ateprazoexecucao=&desdedatafecho=&atedatafecho=&desdeprecoefectivo_false=&desdeprecoefectivo=&ateprecoefectivo_false=&ateprecoefectivo=&pais=0&distrito=0&concelho=0&sort(-publicationDate)")
#           
#         df <- import(f)
#         Sys.sleep(3)
#             
#         export(df,paste0("output/contratos_hospitais_all/bd_contratos_",bd_hospitais[i,3],"__", a,"-",x,"-",d,".csv"),"csv")
#             
#         }
#             
#             
#             
#             
#         }
#             
# 
#             
#         }
#     }
#     
# 
# 
# }
# rm(df)
```

## Junção de todos os contratos num DataFrame

De forma a conduzir a análise e limpeza de dados destas bases de dados, construimos um dataframe geral com todos os contratos chamado *db_contratos*.



```r
files <- list.files(path="output/contratos_hospitais_all/",pattern="*.csv")
db_contratos <- lapply(paste0("output/contratos_hospitais_all/",files), import)

#x <- bind_rows(db_contratos)
x <- rbindlist(db_contratos)

export(x, "output/contratos_hospitais.csv", "csv")
```


## Filtrar apenas os casos contratos de tarefeiros

A base de dados em causa necessita de ser filtrada de forma a trabalhar com uma base de dados com os casos pretendidos.


```r
x$`Objeto do Contrato` <- str_to_lower(x$`Objeto do Contrato`)

db_smedicos <- x %>% filter(str_detect(`Objeto do Contrato`,"serviços médicos|serviços medicos|servicos medicos|serviços de médico|serviços de medico|servicos de medicos|prest serv med|serv med|serviço med.|trabalho médico|trabalho medico|cuidados médicos|cuidados medicos|triagem médica|triagem medica|serviços de medicina|prestação de médicos|prestacão de médicos|fornecimento de pessoal médico|assistência médica|assistencia medica|anatomia patológica|anatomia patologica|anestesiologia|angiologia e cirurgia vascular|cardiologia|cardiologia pediátrica|cardiologia pediatrica|cirurgia cardiotorácica|cirurgia cardiotoracica|cirurgia geral|cirurgia Maxilo-Facial|cirurgia pediátrica|cirurgia pediatrica |cirurgia plástica |cirurgia plastica|cirurgia torácica|cirurgia toracica|dermato-venereologia|doenças infecciosas|doenças infeciosas|endocrinologia e nutrição|estomatologia|gastrenterologia|genética médica|genetica medica|ginecologia|ginecologia/obstetrícia|ginecologia/obstetricia|obstetricia|obstetrícia|imunoalergologia|imunohemoterapia|farmacologia|hematologia|medicina desportiva|medicina física e de reabilitação|medicina geral e familiar|medicina intensiva|medicina interna|medicina legal|medicina nuclear|medicina tropical|nefrologia|neurocirurgia|neurologia|neurorradiologia|oftalmologia|oncologia|ortopedia|otorrinolaringologia|patologia clínica|patologia clinica|pediatria|pneumologia|psiquiatria|psiquiatria da infância e da adolescência|radiologia|radioncologia|reumatologia|saúde pública|urologia|serv méd|anestesia|anestesista|cirurgião|uci|urgência|médico|médica|medico|medica|obstetra|mgf|neurologista|psiquiatra|codificação clínica|codificação clinica"))
```

Depois só consideramos a aquisição de serviços no campo "Tipo(s) de Contrato", uma vez que a contratação de médicos caberiasó nesta categoria.



```r
db_smedicos <- db_smedicos %>% filter(`Tipo(s) de Contrato` == "Aquisição de serviços")
```

Como os resultados ainda continham um conjunto muito alargado de dados que não correspondiam a dados de contratos de tarefeiros, foram eliminados multiplos CPV's que não serviriam.



```r
df <- db_smedicos %>% filter(!str_detect(`CPVs`,"Serviços de reparação e manutenção|Manutenção e reparação de equipamento|Serviços de calibragem|Serviços de instalação|Serviços de alojamento|Serviços de restauração|Serviços de provimento de refeições|Serviços de confecção de refeições|Serviços de comércio a retalho|Serviços de transporte|Serviços de hotelaria|Serviços de táxis|Serviços de agências de viagens|Serviços de viagens|Serviços de portagem em auto-estradas|Serviços de reabastecimento de veículos|Serviços de telecomunicações|Serviços públicos|Serviços anexos e auxiliares dos transportes|Serviços de teleconferência|Serviços de distribuição de gás|Serviços de consultoria financeira|Serviços de arquitectura|Estudo de viabilidade, serviço consultivo, análise|Serviços de consultoria|Serviços técnicos|Serviços de assistência técnica|Serviços de apoio técnico|Serviços relacionados com a construção|Serviços de gestão de projectos de construção|Serviços de controlo e monitorização|Serviços laboratoriais|Serviços de TI|Serviços de desenvolvimento de software|Serviços de análise e avaliação em matéria de garantias da qualidade dos sistemas|Serviços de apoio a sistemas|Serviços relacionados com software|Serviços de assistência em matéria de software|Serviços de introdução de dados|Serviços de gestão de redes de dados|Serviços de fornecimento|Serviços relacionados com a informática|Serviços de gestão relacionados com a informática|Serviços de informática prestados por profissionais|Serviços de gestão de instalações para manutenção de sistemas informáticos|Serviços de rede informática|Serviços de investigação e desenvolvimento e serviços de consultoria conexos|Serviços de desenvolvimento experimental e de investigação|Serviços de plantação e manutenção de áreas verdes|Serviços jurídicos|Serviços de assessoria jurídica|Serviços de certificação|Serviços de contabilidade|Serviços de auditoria|Serviços relacionados com a gestão|Serviços de gestão de projectos|Serviços de concepção de projectos|Serviços auxiliares de escritório|Serviços de formação de pessoal|Serviços de segurança|Serviços de vigilância|Serviços empresariais diversos e afins|Serviços de concepção de modelos especializados|Serviços de ensino e formação|Serviços de formação|Serviços de medicina do trabalho|Serviços de análises clínicas|Serviços de recolha de roupa por lavandarias"))
```



```r
df <- df %>% filter(!str_detect(`Objeto do Contrato`,"requalificação|reparação|medicina no trabalho|medicina do trabalho|media clipping|manutencao|manutenção|remodelação|protecções radilogicas|serviços de vigil|ância|actualização da base de dados|mcdt|configuração de equipamento|tdt|medidores de pressão|equipamento|equipamentos|consumíveis|tomadas|publicação num jornal diário"))
```

## Tornar blanks em NA's

Como a base de dados tinha alguns casos em que as células vazias não correspondiam an NA para o R, fizemos uma função rápida para tornar esses valores em NA's


```r
empty_as_na <- function(x){
    if("factor" %in% class(x)) x <- as.character(x) ## since ifelse wont work with factors
    ifelse(as.character(x)!="", x, NA)
}

## transform all columns
df <- df %>% mutate_each(funs(empty_as_na)) 
```

```
## Warning: funs() is soft deprecated as of dplyr 0.8.0
## please use list() instead
## 
##   # Before:
##   funs(name = f(.))
## 
##   # After: 
##   list(name = ~ f(.))
## This warning is displayed once per session.
```

## Limpar repetidos

Devido ao processo de recolha, um conjunto alargado de casos era duplicado, pelo que tivemos de proceder à limpeza de dados repetidos.


```r
duplicados <- df[duplicated(df),]
df <- distinct(df)
```


## Limpar colunas

As colunas correspondentes às Entidade(s) Adjudicante(s) e às Entidade(s) Adjudicatárias não estavam uniformizadas, impossibilitanto uma agregação eficiente. Felizmente, essas colunas incluiam também o NIF entre "()". Procedeu-se à individualização desse valor numa nova coluna para os dois casos

### Limpar Coluna Entidade(s) Adjudicante(s)



```r
df <- separate(df,`Entidade(s) Adjudicante(s)`,c("Entidade(s) Adjudicante(s)","NIF_Adjudicante"),sep = "[(]")
```

```
## Warning: Expected 2 pieces. Additional pieces discarded in 120 rows [395,
## 397, 398, 432, 570, 571, 572, 573, 578, 579, 580, 672, 693, 703, 704, 705,
## 706, 707, 708, 709, ...].
```

```r
df$NIF_Adjudicante <- gsub(")", "",df$NIF_Adjudicante)
```


```r
df$NIF_Adjudicante <- gsub("ARSA","503148768",df$NIF_Adjudicante)
df$NIF_Adjudicante <- gsub("ARSC","503122165",df$NIF_Adjudicante)
df$NIF_Adjudicante <- gsub("ARSLVT","503148776",df$NIF_Adjudicante)
df$NIF_Adjudicante <- gsub("ARSN","503135593",df$NIF_Adjudicante)
df$NIF_Adjudicante <- gsub("CHBV, E. P. E.","510123210",df$NIF_Adjudicante)
df$NIF_Adjudicante <- gsub("CHLC","508080142",df$NIF_Adjudicante)
df$NIF_Adjudicante <- gsub("CHLO","507618319",df$NIF_Adjudicante)
df$NIF_Adjudicante <- gsub("CHMT","506361608",df$NIF_Adjudicante)
df$NIF_Adjudicante <- gsub("CHP","508331471",df$NIF_Adjudicante)
df$NIF_Adjudicante <- gsub("IOGP","600000052",df$NIF_Adjudicante)
df$NIF_Adjudicante <- gsub("ULSAM","508786193",df$NIF_Adjudicante)
df$NIF_Adjudicante <- gsub("ULSNA","508094461",df$NIF_Adjudicante)
```



```r
#verficar quantos dados são maiores do que um NIF
plus <- df %>% filter(nchar(df$NIF_Adjudicante) > 10)
```

Dos casos recolhidos, há 2 em que o adjudicante são duas entidades:
[1] Hospital do Espírito Santo de Évora, E.P.E. (508085888), Administração Regional de Saúde do Alentejo, IP
[2] Centro Hospitalar do Oeste (510412009, Centro Hospitalar do Oeste Norte -CHON)

No caso 1 [o anuncio desta contratação](http://dre.pt/util/getpdf.asp?s=udrcp&serie=2&data=2010-07-16&iddr=137&iddip=403492594) prevê que estes serviços médicos sejam para Hospital do Espirito Santo de Évora, EPE, ACES AC I - SUB Estremoz, ACES, AC II - Montemor-o-novo, ACES AL - SUB Alcácer do Sal, ACES AL - SUB Odemira (todos pertencentes à Administração Regional de Saúde do Alentejo).


```r
df$NIF_Adjudicante <- gsub("508085888, Administração Regional de Saúde do Alentejo, IP","508085888,503148768",df$NIF_Adjudicante)
df$`Entidade(s) Adjudicante(s)`<- gsub("Hospital do Espírito Santo de Évora, E.P.E.","Hospital do Espírito Santo de Évora, E.P.E. , Administração Regional de Saúde do Alentejo",df$`Entidade(s) Adjudicante(s)`)
```

Já no caso 2, optamos por agregar esse valor para o  Centro Hospitalar do Oeste (510412009) uma vez que "O CHO resultou da junção, a 21 de novembro de 2012, do antigo Centro Hospitalar do Oeste Norte (CHON) e do antigo Centro Hospitalar de Torres Vedras (CHTV)".


```r
df$NIF_Adjudicante <- gsub("510412009, Centro Hospitalar do Oeste Norte -CHON","510412009",df$NIF_Adjudicante)
df$NIF_Adjudicante <- gsub("509186998, Centro Hospitalar de Setúbal, E. P. E.","509186998,507606787",df$NIF_Adjudicante)
df$NIF_Adjudicante <- gsub("507606787, Centro Hospitalar Barreiro Montijo, E. P. E.
","507606787,509186998",df$NIF_Adjudicante)
```

### Limpar Coluna Adjudicatário


```r
df <- separate(df,`Entidade(s) Adjudicatária(s)`,c("Entidade(s) Adjudicatária(s)","NIF_Adjudicatária"),sep = "[(]",extra = "merge")
df$NIF_Adjudicatária <- gsub(")", "",df$NIF_Adjudicatária)
```


```r
#casos em que os () das abreviaturas não permitiram atribuir nif
df$NIF_Adjudicatária <- gsub("CHLO \\(507618319", "507618319",df$NIF_Adjudicatária)
df$NIF_Adjudicatária <- gsub("CHNVG \\(508142156", "508142156",df$NIF_Adjudicatária)
```



```r
#verficar quantos dados são maiores do que um NIF
plus <- df %>% filter(nchar(df$NIF_Adjudicatária) > 10)
```

## Novo Scrape

Apesar do dataframe **df** incluir uma grande quantidade de contratos, optamos realizar uma nova recolha de forma a assegurar que a limpeza de dados não eliminou nenhum caso que deveria ter sido mantido.

Para tal, fizemos uma nova recolha, desta vez utilizando o NIF das entidades adjudcatrárias, assumindo que, no scrape inicial, recolheu-se pelo menos **1** NIF de uma entidade adjudicatária.

Foi por issso necessário gerar uma nova base de dados, só com os NIF's das entidades adjudicatárias.



```r
df_new_import <- df %>% select(`Entidade(s) Adjudicatária(s)`,NIF_Adjudicatária)
a <- df_new_import  %>% filter(nchar(df_new_import$NIF_Adjudicatária) > 10)
df_new_import <- df_new_import  %>% filter(nchar(df_new_import$NIF_Adjudicatária) < 10)
a <- separate(a,NIF_Adjudicatária,c("NIF_Adjudicante", "NIF_Adjudicante2","NIF_Adjudicante3","NIF_Adjudicante4","NIF_Adjudicante5","NIF_Adjudicante6"),sep = "[(]",extra="merge")
```

```
## Warning: Expected 6 pieces. Missing pieces filled with `NA` in 23 rows [1,
## 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 21, ...].
```

```r
a$NIF_Adjudicante <- gsub(",.*", "", a$NIF_Adjudicante)
a$NIF_Adjudicante <- gsub(" .*", "", a$NIF_Adjudicante)
a$NIF_Adjudicante <- gsub("ACSS", NA, a$NIF_Adjudicante)
a$NIF_Adjudicante2 <- gsub(",.*", "", a$NIF_Adjudicante2)
a$NIF_Adjudicante2<- gsub(" .*", "", a$NIF_Adjudicante2)
a$NIF_Adjudicante3<- gsub(",.*", "", a$NIF_Adjudicante3)
a$NIF_Adjudicante3<- gsub(" .*", "", a$NIF_Adjudicante3)
a$NIF_Adjudicante4<- gsub(",.*", "", a$NIF_Adjudicante4)
a$NIF_Adjudicante4<- gsub(" .*", "", a$NIF_Adjudicante4)
a$NIF_Adjudicante5<- gsub(",.*", "", a$NIF_Adjudicante5)
a$NIF_Adjudicante5<- gsub(" .*", "", a$NIF_Adjudicante5)
a$NIF_Adjudicante6<- gsub(",.*", "", a$NIF_Adjudicante6)
a$NIF_Adjudicante6<- gsub(" .*", "", a$NIF_Adjudicante6)
#
a <- gather(a)
a <- rename(a,`Entidade(s) Adjudicatária(s)` = key)
a <- rename(a,`NIF_Adjudicatária` = value)
a <- filter(a,`Entidade(s) Adjudicatária(s)` != "Entidade(s) Adjudicatária(s)")
a <- a %>% drop_na()

df_new_import <- rbind(df_new_import,a)

df_new_import <- df_new_import[ !duplicated(df_new_import$NIF_Adjudicatária), ]
```


Gerada a nova base de dados com NIF's que queriamos recolher, procedeu-se a uma segunda recolha de dados.



```r
# for (i in 1:nrow(df_new_import)) {
#     
#     print(paste("getting ", i ,"---", df_new_import[i,1]))               
#     
#     f<- paste0("http://www.base.gov.pt/base2/rest/contratos.csv?texto=&tipo=0&tipocontrato=0&cpv=&numeroanuncio=&aqinfo=&adjudicante=&adjudicataria=",df_new_import[i,2],"&desdeprecocontrato_false=&desdeprecocontrato=&ateprecocontrato_false=&ateprecocontrato=&desdedatacontrato=&atedatacontrato=&desdedatapublicacao=&atedatapublicacao=&desdeprazoexecucao=&ateprazoexecucao=&desdedatafecho=&atedatafecho=&desdeprecoefectivo_false=&desdeprecoefectivo=&ateprecoefectivo_false=&ateprecoefectivo=&pais=0&distrito=0&concelho=0&sort(-publicationDate)")
#      
#     t <- import(f)
#     
#     Sys.sleep(3)
#     
#     export(t,paste0("output/contratos_tarefeiros_all/bd_contratos_",df_new_import[i,2],"__",".csv"),"csv")
# 
# }
```


Os novos contratos foram agregados numa nova base de dados, a precisar de limpeza:


```r
files <- list.files(path="output/contratos_tarefeiros_all/",pattern="*.csv")
db_contratos_new <- lapply(paste0("output/contratos_tarefeiros_all/",files), import)

#x <- bind_rows(db_contratos)
x_new <- rbindlist(db_contratos_new)

export(x_new, "output/contratos_tarefeiros.csv", "csv")
```


Como queriamos ter a certeza de que esta segunda limpeza, numa base de dados mais refinada, não eliminiria casos de forma desncessária, procedemos à sua limpeza manual, num documento do Google Sheets.

Numa nova coluna, **KEEP?**, foram identificados como "N" os contratos que não correspondiam a Médicos Tarefeiros, com "S" os que sim e "NS" aqueles que não era posível, nem através da leitura do contrato (quando diponível), verificar se isso existia. 

Feita esta última classificação...


```r
df_main <- import("input/contratos_tarefeiros_limpa.csv")

x_final <- df_main
```

## Limpar colunas desncessárias



```r
x_final$`Data de Fecho do Contrato.y` <- NULL
x_final$`Preço Total Efetivo.y` <- NULL
x_final$`Causas das Alterações ao Prazo.y` <- NULL
x_final$`Causas das Alterações ao Preço.y` <- NULL
x_final$Estado.y <- NULL
x_final$`N.º registo do Acordo Quadro.y` <- NULL
x_final$`Descrição do Acordo Quadro.y` <- NULL
x_final$`Procedimento Centralizado.y` <- NULL
x_final$duvida <- NULL
x_final$`Preço Contratual.y`<- NULL
x_final$`Data de Publicação.y` <- NULL
x_final$`Local de Execução.y` <- NULL
x_final$Fundamentação.y <- NULL
x_final$V22 <- NULL
```

## Dataframe final

Temos assim um dataframe com os dados filtrados e praticamente prontos para ser analisadas. Foi necessário, no entanto, proceder mais uma vez à limpeza e uniformização de um conjunto de dados sujos - como já tinha acontecido anteriormente.

Como só nos interessam os casos em classificados como válidos, e para tornar a análise mais fácil, começamos por filtrar os casos classificados como válidos.


```r
x_final <- filter(x_final, `KEEP?` == "S")
```

## Limpeza de NIFS- Entidades Adjudicantes



```r
x_final <- separate(x_final,`Entidade(s) Adjudicante(s)`,c("Entidade(s) Adjudicante(s)","NIF_Adjudicante"), sep = "[(]",extra = "merge")
x_final$NIF_Adjudicante <-gsub(")", "",x_final$NIF_Adjudicante)
```



```r
x_final$NIF_Adjudicante <- gsub("ARSA \\(503148768","503148768",x_final$NIF_Adjudicante)
x_final$NIF_Adjudicante <- gsub("ARSC \\(503122165","503122165",x_final$NIF_Adjudicante)
x_final$NIF_Adjudicante <- gsub("ARSLVT \\(503148776","503148776",x_final$NIF_Adjudicante)
x_final$NIF_Adjudicante <- gsub("ARSN \\(503135593","503135593",x_final$NIF_Adjudicante)
x_final$NIF_Adjudicante <- gsub("CHBV, E. P. E. \\(510123210","510123210",x_final$NIF_Adjudicante)
x_final$NIF_Adjudicante <- gsub("CHLC \\(508080142","508080142",x_final$NIF_Adjudicante)
x_final$NIF_Adjudicante <- gsub("CHLO \\(507618319","507618319",x_final$NIF_Adjudicante)
x_final$NIF_Adjudicante <- gsub("CHMT \\(506361608","506361608",x_final$NIF_Adjudicante)
x_final$NIF_Adjudicante <- gsub("CHP \\(508331471","508331471",x_final$NIF_Adjudicante)
x_final$NIF_Adjudicante <- gsub("CHTV \\(505950413","505950413",x_final$NIF_Adjudicante)
x_final$NIF_Adjudicante <- gsub("IOGP \\(600000052","600000052",x_final$NIF_Adjudicante)
x_final$NIF_Adjudicante <- gsub("ULSAM \\(508786193","508786193",x_final$NIF_Adjudicante)
x_final$NIF_Adjudicante <- gsub("ULSNA \\(508094461","508094461",x_final$NIF_Adjudicante)
x_final$NIF_Adjudicante <- gsub("HRS \\(501536272","501536272",x_final$NIF_Adjudicante)
x_final$NIF_Adjudicante <- gsub("FMH \\(501621288","501621288",x_final$NIF_Adjudicante)
```

## Verificar quantos casos têm mais do que um Adjudicante


```r
#verficar quantos dados são maiores do que um NIF
plus <- x_final %>% filter(nchar(x_final$NIF_Adjudicant) > 10)
```


Como já indicado, optamos por agregar esse valor para o  Centro Hospitalar do Oeste (510412009) uma vez que "O CHO resultou da junção, a 21 de novembro de 2012, do antigo Centro Hospitalar do Oeste Norte (CHON) e do antigo Centro Hospitalar de Torres Vedras (CHTV)".


```r
x_final$NIF_Adjudicante <- gsub("508877547, CENTRO HOSPITALAR DO OESTE \\(510412009","510412009",x_final$NIF_Adjudicante)

x_final$NIF_Adjudicante <- gsub("510412009, Centro Hospitalar do Oeste Norte -CHON \\(508877547","510412009",x_final$NIF_Adjudicante)
```


No outro caso, trata-se mesmo de um caso em que foi adjudicado a vários hospitais, pelo que...


```r
x_final[811,6] <- "Centro Hospitalar Barreiro Montijo,Centro Hospitalar de Setúbal,Hospital Garcia de Orta"

x_final$NIF_Adjudicante <- gsub("509186998, Centro Hospitalar de Setúbal, E. P. E. \\(507606787, Hospital Garcia de Orta, EPE \\(506361470","509186998, 507606787, 507606787",x_final$NIF_Adjudicante)


#Administração Regional de Saúde do Alentejo, IP 	

x_final$NIF_Adjudicante <- gsub("503148768, Hospital do Espírito Santo de Évora, E.P.E. \\(508085888","503148768, 508085888",x_final$NIF_Adjudicante)

x_final[12,6] <- "Administração Regional de Saúde do Alentejo, IP,Hospital do Espírito Santo de Évora, E.P.E."
```




```r
#verficar quantos dados são maiores do que um NIF
plus <- x_final %>% filter(nchar(x_final$NIF_Adjudicante) > 10)
```


## Limpeza de NIFS  Entidades Adjudicatárias



```r
x_final <- separate(x_final,`Entidade(s) Adjudicatária(s)`,c("Entidade(s) Adjudicatária(s)","NIF_Adjudicatária"), sep = "[(]",extra = "merge")
x_final$NIF_Adjudicatária <-gsub(")", "",x_final$NIF_Adjudicatária)
```


```r
x_final$NIF_Adjudicatária <- gsub("EX-SELECT \\(505121247","505121247",x_final$NIF_Adjudicatária)
x_final$NIF_Adjudicatária <- gsub("CHNVG \\(508142156","508142156",x_final$NIF_Adjudicatária)
x_final$NIF_Adjudicatária <- gsub("CHLO \\(507618319","507618319",x_final$NIF_Adjudicatária)
```



## Limpar euros

Os valores correspondentes ao montante contratual continham o símbolo "€" e um ponto a separar os milhões. Foi necessário eliminar também esses casos e converter o valor para numeral.


```r
x_final$`Preço Contratualx` <- gsub("€","",x_final$`Preço Contratualx`)
x_final$`Preço Contratualx` <- gsub(",",".",x_final$`Preço Contratualx`)
x_final$`Preço Contratualx` <- gsub("[[:space:]]","",x_final$`Preço Contratualx`)
x_final$`Preço Contratualx` <- as.numeric(x_final$`Preço Contratualx`)

x_final$`Preço Total Efetivo.x` <- gsub(" €","",x_final$`Preço Total Efetivo.x`)
x_final$`Preço Total Efetivo.x` <- gsub("€","",x_final$`Preço Total Efetivo.x`)
x_final$`Preço Total Efetivo.x` <- as.numeric(gsub(",", ".", gsub("\\.", "", x_final$`Preço Total Efetivo.x`)))
```

##Limpar prazo de execução

O campo "Prazo de Execução" também tinha a unidade de medida - isto é "X dias". Eliminamos esse dado para tornar o valor em numérico.


```r
x_final$`Prazo de Execução`<- as.numeric(gsub(" dias", "", x_final$`Prazo de Execução`))
```

## Transformar em datas

Torna-se mais fácil fazer esta transformação com as colunas do dataframe como datas. Procedeu-se então também a essa limpeza.


```r
x_final$`Data de Publicação.x`<- as.Date(x_final$`Data de Publicação.x`, "%d-%m-%Y")
x_final$`Data de Celebração do Contrato`<- as.Date(x_final$`Data de Celebração do Contrato`, "%d-%m-%Y")
x_final$`Data de Fecho do Contrato.x`<- as.Date(x_final$`Data de Fecho do Contrato.x`, "%d-%m-%Y")
```


# NIFS adjudicatários com mais do que um


```r
#verficar quantos dados são maiores do que um NIF
plus <- x_final %>% filter(nchar(x_final$NIF_Adjudicatária) > 10)
export(plus, "contratos_contratos_maisdeum.csv", "csv")
x_final <- x_final %>% filter(nchar(x_final$NIF_Adjudicatária) < 10)
```

Estes casos foram consequentemete limpos de forma a avaliar cadaum dos casos. Foram eliminados os casos repetidos. Esta limpeza foi feita através de uma ferramenta externa: Google Sheets.


# Importar desdobramento de contratos


```r
x_desdobrado <- import("input/base_contratos_atribuidos_a_mais_do_que_um.csv")
x_final$NIF_Adjudicante <- as.character(x_final$NIF_Adjudicante)
x_desdobrado$NIF_Adjudicante <- as.character(x_desdobrado$NIF_Adjudicante)


x_desdobrado$`Data de Publicação.x`<- as.Date(x_desdobrado$`Data de Publicação.x`, "%Y-%m-%d")
x_desdobrado$`Data de Celebração do Contrato`<- as.Date(x_desdobrado$`Data de Celebração do Contrato`, "%Y-%m-%d")
x_desdobrado$`Data de Fecho do Contrato.x`<- as.Date(x_desdobrado$`Data de Fecho do Contrato.x`, "%Y-%m-%d")



x_final <- bind_rows(x_final,x_desdobrado)
```



## Eliminar possiveis duplicados


```r
x_final <- distinct(x_final)
```



## Eliminar possiveis duplicados


```r
x_final <- distinct(x_final)
```




## Criar coluna de Preço

A base de dados recolida tinha também um problema em relação aos preços. É que tem dois campos: o Preço Contratual e Preço Total Efectivo - isto é, o preço inicialmente previsto e o preço final efectivo. Em teoria, o preço final efectivo só deveria estar ausente dos contratos mais recentes, mas isso não é uma realidade.

Isto complica as contas. Por isso, Criou-se uma terceira

Assim, criamos uma nova coluna para esses valores que entende o preço verdadeiro como o preço totak efectivo mas, quando não há esse valor, considera o Preço Contratual.


```r
x_final$preço_final <- x_final$`Preço Total Efetivo.x`

x_final$preço_final[is.na(x_final$preço_final)] <- x_final$`Preço Contratualx`[is.na(x_final$preço_final)]
```


Com isto, a base de dados fica limpa e pronta a ser analisada. Filtramos apenas os dados que foram classificadas como "S".


```r
df <- filter(x_final, `KEEP?` == "S")

export(df,"output/contratos_considerados.csv","csv")
#adicionar dataframes que queremos deixar cair depois disto
rm(plus, x_desdobrado)
```

# Análise 

Recolhidos todos os dados, eis as perguntas que tentamos responder com a análise que se segue:


## [1] - Quantos contratos temos?


```r
nrow(df_main)
```

```
## [1] 39275
```

```r
df_cNS <-df_main %>% filter(`KEEP?` == "NS")
nrow(df_cNS)
```

```
## [1] 37
```

```r
nrow(df)
```

```
## [1] 4987
```

```r
min(df$`Data de Publicação.x`)
```

```
## [1] "2008-08-21"
```

```r
max(df$`Data de Publicação.x`)
```

```
## [1] "2019-04-09"
```

De um total de 39.275 casos recolhidos do Portal Base recorrendo à metodologia acima referida, só 4.987 foram considerados válidos como sendo contratação de médicos tarefeiros. 

Na base de dados, filtrada através de uma análise manual aos contratos, há 37 casos que foram classificados como "Não sabemos" uma vez que a descrição ou o contrato (ou falta dele) não serem o suficientementemente específicos para poderem ser conclusivos.

Os contratos recolhidos vão desde "2008-08-21" a 2019-04-09", sendo que a distribuição por ano na Data de publicação não é uniforme.



```r
n_contratos_anos <- df %>%
      mutate(year = format(`Data de Publicação.x`, "%Y")) %>%
      group_by(year) %>%
      tally()


ggplot(data=n_contratos_anos , aes(x=year, y=n)) +
    labs(title="Número de contratos publicados em cada ano",
        x ="ano", y = "Nº de Contratos") +
    geom_bar(stat='identity')
```

![](main_files/figure-html/unnamed-chunk-43-1.png)<!-- -->

O ano de 2017 foi assim o ano em que se publicaram mais contratos. Importa lembrar que o ano de 2019 não pode ser comparado com os restantes porque o ano ainda não terminou e que os dados só podem ser vistos como fidedignos a partir de 2011 (ver metodologia)


```r
nrow(df %>% filter(is.na(`Data de Celebração do Contrato`)))
```

```
## [1] 64
```

Contabilizamos os dados pela Data de Publicação e não Data de Celebração uma vez que há um total de 64 contratos que têm data de publicação na plataforma mas não Data de Celebração.


## [2] - Quantos contratos ficaram por fechar?

Do total de 4.987 contratos que foram classificados como referentes à contratação de médicos tarefeiros, fica claro que o número não só é inferior ao total dos dados que foram publicados pela imprensa (ver mais nos pontos posteriores), como muitos destes contratos não foram sequer fechados.



```r
nao_fechados <- df %>% filter(is.na(`Data de Fecho do Contrato.x`))
nrow(nao_fechados)
```

```
## [1] 2727
```

```r
(nrow(nao_fechados)/nrow(df)) * 100
```

```
## [1] 54.68217
```

2.727 dos 4.987 contratos analisados (55%) não têm uma data de de Fecho de Contrato indicado no Portal Base, significando isso que estes contratos não foram fechados.


## [3] - Quantos contratos "não fechados" há para cada um dos anos?


```r
contratos_delebrados_ano <- df %>%
      mutate(year = format(`Data de Celebração do Contrato`, "%Y")) %>%
      group_by(year) %>%
      tally()

nao_fechados_ano <- nao_fechados %>%
      mutate(year = format(`Data de Celebração do Contrato`, "%Y")) %>%
      group_by(year) %>%
      tally()

nao_fechados_ano <- left_join(contratos_delebrados_ano,nao_fechados_ano, by= c("year"))

nao_fechados_ano <- nao_fechados_ano %>% rename("total_contratos_celebrados" = "n.x") %>% rename("total_contratos_não_fechados" = "n.y")

nao_fechados_ano$per_por_fechar_daquele_ano <- (nao_fechados_ano$total_contratos_não_fechados/nao_fechados_ano$total_contratos_celebrados) * 100

knitr::kable(nao_fechados_ano)
```



year    total_contratos_celebrados   total_contratos_não_fechados   per_por_fechar_daquele_ano
-----  ---------------------------  -----------------------------  ---------------------------
NA                              64                             57                     89.06250
2008                             1                              1                    100.00000
2009                            27                             26                     96.29630
2010                            41                             31                     75.60976
2011                            47                             18                     38.29787
2012                           162                             67                     41.35802
2013                           509                            169                     33.20236
2014                           590                            225                     38.13559
2015                           627                            282                     44.97608
2016                          1018                            625                     61.39489
2017                          1100                            670                     60.90909
2018                           757                            512                     67.63540
2019                            44                             44                    100.00000

2017 é o ano cujos os contratos celebrados nesse ano menos vezes foram fechados. 



## [4] - Quantos contratos já deveriam ter fechado?

A análise anterior, apesar de útil, pode ser falaciosa uma vez que inclui os contratos que já deveriam ter sido publicados, mas também os contratos que ainda estão a decorrer (e que, portanto, ainda não têm a "obrigação" de ter uma Data de Fecho do Contrato ou um preço final.)

Felizmente, o Portal Base também indica um prazo de execução para aquele trabalho. Se assumirmos que o prazo de execução inicia na data de Celebração do Contrato, é possível calcular uma data de quando, em teoria, o contrato deveria estar terminado.

De notar que, segundo a lei, os contratos devem ser publicados 20 dias úteis depois da serem fechados. Recorremos ao pacote "bizdays" para fazer esse cálculo. Por ser mais fácil para este cálculo, ignoramos os feriados.




```r
cal <- create.calendar("Actual", weekdays=c("saturday", "sunday"))

df$data_conclusão_teorica <- df$`Data de Celebração do Contrato` + df$`Prazo de Execução`

df$data_conclusão_teorica <- add.bizdays(df$data_conclusão_teorica, 20, cal)
```


```r
df$dif_teoricaVsReal <- df$`Data de Fecho do Contrato.x` - df$data_conclusão_teorica

dif_teoricaVsReal <- df %>% group_by(dif_teoricaVsReal ) %>%
  tally()
```

Se olharmos para os casos em que se verifica uma Data de Fecho do Contrato.x, vemos que o caso mais comum é o contrato terminar 29 dias antes do que era expectável, seguido por 58 dia antes e 57	dias antes.

Isto levanta algumas questões em relação à validade deste novo indicador, mas é o melhor dado que conseguimos ter com a base de dados obtida.



```r
nao_fechados <- df %>% filter(is.na(`Data de Fecho do Contrato.x`))

não_fechados_real <- nao_fechados %>% filter(data_conclusão_teorica < as.Date("2019-04-20"))

nrow(não_fechados_real)
```

```
## [1] 2477
```

```r
nrow(não_fechados_real)/nrow(df)
```

```
## [1] 0.4966914
```

Ainda assim, se tomarmos como real o valor atribuido com base no prazo de execução, bem como a data em que os dados foram recolhidos, há 2.477 (50%) contratos de prestação de serviços médicos que já deveriam ter alguma informação sobre a sua conclusão no Base.




```r
não_fechados_real_mais90 <- nao_fechados %>% filter(data_conclusão_teorica+90 < as.Date("2019-04-20"))

nrow(não_fechados_real_mais90)
```

```
## [1] 2214
```

```r
nrow(não_fechados_real_mais90)/nrow(df)
```

```
## [1] 0.4439543
```

Se formos "benevolentes" e dermos uma janela temporal de 90 dias a este prazo - isto é, se admitirmos que, para além do prazo de execução previsto no contrato, a entidade pudesse demorar mais 90 dias quer a publicar o contrato, quer a coloca-lo em execução - este número não é muito diferente.

Há 2.214 casos que, ainda com este "prazo mais alargado", não possuem informação nenhuma sobre a sua data de conclusão ou o seu preço.


## [5] - Quantos contratos já deveriam ter fechado (em cada um dos anos)

Se virmos isto por anos...


```r
não_fechados_ano_real <- não_fechados_real %>%
      mutate(year = format(`Data de Celebração do Contrato`, "%Y")) %>%
      group_by(year) %>%
      tally()


ggplot(data=não_fechados_ano_real , aes(x=year, y=n)) +
    labs(title="Número de contratos não fechados para além do prazo de execução (+20 dias úteis)",
        x ="ano", y = "Nº de Contratos") +
    geom_bar(stat='identity')
```

![](main_files/figure-html/unnamed-chunk-51-1.png)<!-- -->



## [6] - Quantos contratos tem por fechar cada um dos hospitais?

Se considerarmos novamente somente os contratos que, a 20-04-2019, já deveriam ter os seus contratos fechados com base no Prazo de Execução, é possível elencar uma lista das instituições menos "cumpridoras".



```r
lista_menos_cumpridores <- não_fechados_real %>%
      group_by(NIF_Adjudicante) %>%
      tally()

#Lista NIF's de Hospitais
nifs_hospitais <- df %>% distinct(NIF_Adjudicante, .keep_all = TRUE) %>% select(`Entidade(s) Adjudicante(s)`,NIF_Adjudicante)

export(nifs_hospitais,"nifs_hospitais_analisados.csv","csv")

lista_menos_cumpridores <- left_join(lista_menos_cumpridores,nifs_hospitais)
```

```
## Joining, by = "NIF_Adjudicante"
```

```r
lista_menos_cumpridores <- lista_menos_cumpridores %>% arrange(-n)

kable(head(lista_menos_cumpridores))
```



NIF_Adjudicante      n  Entidade(s) Adjudicante(s)                                     
----------------  ----  ---------------------------------------------------------------
506361608          322  Centro Hospitalar do Médio Tejo, E.P.E.                        
508085888          273  Hospital do Espírito Santo de Évora, E. P. E.                  
503148776          261  Administração Regional de Saúde de Lisboa e Vale do Tejo, I.P. 
507606787          185  Centro Hospitalar de Setúbal E.P.E.                            
507618319          165  Centro Hospitalar de Lisboa Ocidental, EPE                     
503035416          134  HOSPITAL PROFESSOR DOUTOR FERNANDO FONSECA, EPE                

O Centro Hospitalar do Médio Tejo, E.P.E. tem 322	contratos por fechar, seguido pelo Hospital do Espírito Santo de Évora, E. P. E. 



## [6] - Quanto gastou cada um dos hospitais? [Preço Total Efectivo]

Como a análise anterior permite concluir, há um conjunto alargado de entidades que lançam os contratos no Portal Base, mas depois não os actualizam com a data de conclusão nem com a Preço Total Efectivo que o contrato custou.

Isto leva a que os valores efectivos do custo dos contratos esteja muito longe de ser o real.

Ainda assim, se usarmos somente o Preço Total Efectivo como indicador de quanto gastou cada uma das entidades, este é o panorama.


```r
gasto_real_hospital <- aggregate(`Preço Total Efetivo.x` ~ NIF_Adjudicante, df, sum)

gasto_real_hospital <- left_join(gasto_real_hospital,nifs_hospitais) %>% arrange(-`Preço Total Efetivo.x`)
```

```
## Joining, by = "NIF_Adjudicante"
```



```r
ggplot(data=gasto_real_hospital[1:10,], aes(y=`Preço Total Efetivo.x`, x=reorder(`Entidade(s) Adjudicante(s)`, `Preço Total Efetivo.x`))) +
    coord_flip() +
    labs(title="Hospitais que mais gastaram com Tarefeiros (Preço Total Efetivo)",
        x ="Empresas", y = "Montante") +
    scale_y_continuous(labels = scales::comma) +
    geom_bar(stat='identity')
```

![](main_files/figure-html/unnamed-chunk-54-1.png)<!-- -->


## [7] - Quanto gastou cada um dos hospitais? [Preço Contratual]

Apesar do Preço Total Efectivo ser o único valor que nos garante o valor real gasto pelos hospitais neste género de serviço, a análise anterior permite-nos perceber que este valor está longe de ser real, uma vez que uma grande parte dos hospitais não publica quanto efectivamente gastou com estes contratos.

Podemos usar, no entanto, o Preço Contratual, como um valor indicativo para esse contrato (sabendo que, depois de terminado, o Preço real pode efectivamente mudar).



```r
gasto_esperado_hospital <- aggregate(`Preço Contratualx` ~ NIF_Adjudicante, df, sum)

gasto_esperado_hospital <- left_join(gasto_esperado_hospital,nifs_hospitais) %>% arrange(-`Preço Contratualx`)
```

```
## Joining, by = "NIF_Adjudicante"
```

Visualizando essest dados...



```r
ggplot(data=gasto_esperado_hospital[1:10,], aes(y=`Preço Contratualx`, x=reorder(`Entidade(s) Adjudicante(s)`, `Preço Contratualx`))) +
    coord_flip() +
    labs(title="Hospitais que mais gastaram com Tarefeiros (Preço Contratual)",
        x ="Empresas", y = "Montante") +
    scale_y_continuous(labels = scales::comma) +
    geom_bar(stat='identity')
```

![](main_files/figure-html/unnamed-chunk-56-1.png)<!-- -->


## [8] - Quanto gastou cada um dos hospitais? [Preço Final]

Os dos indicadores podem ser falaciosos. Se, por um lado, o Preço Total Efectivo não consta numa grande maioria dos contratos, que não estão fechados, por outro, sabemos que o Preço Contratual também não corresponde a um valor real, porque resulta de uma estimativa de quanto a instituição publica espera gastar. 

Desenvolvemos por isso o indicador Preço Final, que, quando existe, usa o Preço Total Efectivo e, quando não, usa o Preço Contratual.

Aplicando as mesmas contas dos pontos anteriores...



```r
gasto_hospital_preço_final <- aggregate(preço_final ~ NIF_Adjudicante, df, sum)

gasto_hospital_preço_final  <- left_join(gasto_hospital_preço_final ,nifs_hospitais) %>% arrange(-preço_final)
```

```
## Joining, by = "NIF_Adjudicante"
```

Visualizando essest dados...



```r
ggplot(data=gasto_hospital_preço_final [1:10,], aes(y=preço_final, x=reorder(`Entidade(s) Adjudicante(s)`, preço_final))) +
    coord_flip() +
    labs(title="Hospitais que mais gastaram com Tarefeiros (Preço Final)",
        x ="Empresas", y = "Montante") +
    scale_y_continuous(labels = scales::comma) +
    geom_bar(stat='identity')
```

![](main_files/figure-html/unnamed-chunk-58-1.png)<!-- -->


## [9] - Quanto gastou cada um dos hospitais por ano? [Preço Final]


Se usarmos este último indicador para olhar para o volume gasto por cada um dos hospitais em cada um dos anos...


```r
gasto_hospital_preço_final_ano<- df %>%
      mutate(year = format(`Data de Celebração do Contrato`, "%Y"))

gasto_hospital_preço_final_ano <- aggregate(preço_final ~ NIF_Adjudicante+year, gasto_hospital_preço_final_ano, sum)

gasto_hospital_preço_final_ano <- left_join(gasto_hospital_preço_final_ano,nifs_hospitais) %>% arrange(-preço_final)
```

```
## Joining, by = "NIF_Adjudicante"
```

```r
gasto_hospital_preço_final_ano <- gasto_hospital_preço_final_ano  %>%
    group_by(year) %>%
    mutate(rank_ano = order(order(preço_final, decreasing=TRUE)))
```

![](main_files/figure-html/unnamed-chunk-60-1.png)<!-- -->

De ressalvar, no entanto, que este cálculo usa apenas a data de celebração do contrato, e não efectivamente quanto é que os hospitais gastam em cada um dos anos, uma vez que um contrato pode ser celebrado para vários anos.

## [10] - Quanto se gastou gastou em relação aos dados da ACSS? 

A ACSS é (Administração Central do Sistema de Saúde) é a entidade responsável por fornecer os dados oficiais dos gastos com médicos tarefeiros. A **Renascença** compilou os gastos anuais, através de multiplas fontes para aferir os gastos, segundo esta instituição, para cada um dos anos:

2012 - Balanço social do SNS, 2014
2013 - Balanço social do SNS, 2014
2014 - Balanço social do SNS, 2014
2015 - Dados da ACSS citados na imprensa
2016 - Relatório social do SNS (2016)
2017 - Dados enviados à RR pela ACSS
2018 - Dados enviados à RR pela ACSS

Todos os ficheiros a que a **Renascença** recorreu para criar a base de dados da ACSS estão disponíveis em [input/documentos_acss_pdf](input/documentos_acss_pdf) e foram convertidos em ficheiros .csv.


Decidimos por isso comparar estes valores com os da nossa base de dados.


```r
df_acss <- import("input/dados_acss_totais_ano.csv")

df_acss$`Gastos com tarefeiros` <- as.numeric(df_acss$`Gastos com tarefeiros`)
df_acss$ano <- as.numeric(df_acss$ano)


ano_preço_contrato <- df %>%
      mutate(ano = format(`Data de Celebração do Contrato`, "%Y")) %>%
      group_by(ano) %>%
      summarise(total_preco_contratual = sum(`Preço Contratualx`))

ano_preço_contrato$ano <- as.numeric(ano_preço_contrato$ano)

ano_preço_efectivo <- df %>%
      filter(!is.na(`Preço Total Efetivo.x`)) %>% 
      mutate(ano = format(`Data de Celebração do Contrato`, "%Y")) %>%
      group_by(ano) %>%
      summarise(total_preco_efectivo = sum(`Preço Total Efetivo.x`))


ano_preço_final <- df %>%
      mutate(ano = format(`Data de Celebração do Contrato`, "%Y")) %>%
      group_by(ano) %>%
      summarise(total_preco_final = sum(preço_final))

ano_preço_contrato$ano <- as.numeric(ano_preço_contrato$ano)
      
ano_preço_contrato$ano <- as.numeric(ano_preço_contrato$ano)
ano_preço_efectivo$ano <- as.numeric(ano_preço_efectivo$ano)
ano_preço_final$ano <- as.numeric(ano_preço_final$ano)


df_acss <- left_join(df_acss,ano_preço_contrato)
```

```
## Joining, by = "ano"
```

```r
df_acss <- left_join(df_acss,ano_preço_efectivo)
```

```
## Joining, by = "ano"
```

```r
df_acss <- left_join(df_acss,ano_preço_final)
```

```
## Joining, by = "ano"
```

```r
df_acss$per_preco_contratual <- (df_acss$total_preco_contratual/df_acss$`Gastos com tarefeiros`) * 100

df_acss$per_preco_efectivo <- (df_acss$total_preco_efectivo/df_acss$`Gastos com tarefeiros`) * 100

df_acss$per_preco_final <- (df_acss$total_preco_final/df_acss$`Gastos com tarefeiros`) * 100

kable(df_acss)
```



  ano   Gastos com tarefeiros   total_preco_contratual   total_preco_efectivo   total_preco_final   per_preco_contratual   per_preco_efectivo   per_preco_final
-----  ----------------------  -----------------------  ---------------------  ------------------  ---------------------  -------------------  ----------------
 2012               100234653                  8932144                4259920             9398256               8.911234             4.249947          9.376254
 2013                98579178                 25738526                9184042            24674790              26.109496             9.316412         25.030428
 2014                89729368                 30051460               13394647            28542009              33.491220            14.927829         31.808994
 2015                92000000                 34744613               12547347            32731283              37.765884            13.638421         35.577481
 2016                97808205                 44737033               13240200            43744965              45.739551            13.536901         44.725250
 2017               101297198                 46402716               12350225            43187430              45.808489            12.192069         42.634377
 2018               104512119                 39265886                8284835            39574350              37.570653             7.927152         37.865800

```r
export(df_acss,"output/gastos_rr_vs_acss.csv","csv")
```

Em nenhum dos anos os gastos registados na nossa base de dados aproximam-se dos valores indicados pela ACSS - isto usando os três indicadores de Preço, que como indicado em cima não podem ser vistos como o preço real (como é o caso dos dados da ACSS).



### [11] - Quanto gastou cada um dos hospitais em relação aos dados da ACSS?

Como os cálculos acima apontam, há uma grande discrepância entre os dados da ACSS e aqueles que foram recolhidos pela RR. Mas será que esta discrepância é igual para todos os hospitais?

A ACSS faculta os dados agregados por hospital para os anos de 2016, 2017 e 2018. No entanto esses dados estão bastante sujos e sem uma coluna de uniformização para poder proceder à análise pretendida.

Em 2016, os dados para o Instituto Nacional de Emergência Médica IP e do Instituto Português do Sangue e da Transplantação IP estavam desagregados por região. A Renascença procedeu à sua soma.



```r
total_2016 <- import("input/dados_acss_totais_2016.csv")
nifs_hospitais <- rename(nifs_hospitais, "Entidade" = "Entidade(s) Adjudicante(s)")
```



```r
total_2016$Entidade <- gsub("CH", "Centro Hospitalar",total_2016$Entidade)

total_2016 <- stringdist_join(total_2016, nifs_hospitais,
                by = "Entidade",
                mode = "left",
                ignore_case = FALSE,
                method = "jw",
                max_dist = 10,
                distance_col = "dist")%>%
  group_by(Entidade.x) %>%
  top_n(1, -dist)

#export para limpeza manual
export(total_2016,"output/total_2016_acss.csv","csv")

#limpeza manual dos dados
total_2016 <- import("input/total_2016_acss_limpo.csv")

total_2016 <- total_2016 %>% 
              select("Entidade.y","NIF_Adjudicante","Total Encargos")

total_2016$`Total Encargos` <- gsub("€", "", total_2016$`Total Encargos`)
total_2016$`Total Encargos` <- gsub(",", "", total_2016$`Total Encargos`) 
total_2016$`Total Encargos` <- gsub(" ", "", total_2016$`Total Encargos`) %>% as.numeric(total_2016$`Total Encargos`)

total_2016 <- total_2016 %>% rename("Total Encargos 2016" = "Total Encargos")
```


Fazendo o mesmo para os Dados de 2017 e 2018...



```r
total_2017_18 <- read_csv("input/total_2017_18_acss.csv")
```

```
## Parsed with column specification:
## cols(
##   Entidade = col_character(),
##   `2017` = col_character(),
##   `2018` = col_character()
## )
```

```r
total_2017_18 <- stringdist_join(total_2017_18, nifs_hospitais,
                by = "Entidade",
                mode = "left",
                ignore_case = FALSE,
                method = "jw",
                max_dist = 10,
                distance_col = "dist")%>%
  group_by(Entidade.x) %>%
  top_n(1, -dist)

#export para limpeza manual
export(total_2017_18, "output/dados_acss_2017_18.csv","csv")

#import de dados limpos
total_2017_18 <- import("input/dados_acss_2017_18_limpos.csv")

total_2017_18 <-total_2017_18 %>%  select("Entidade.y","NIF_Adjudicante","2017","2018")

total_2017_18$`2017` <- gsub("€", "", total_2017_18$`2017`)
total_2017_18$`2017` <- gsub(",", "", total_2017_18$`2017`) 
total_2017_18$`2017` <- gsub(" ", "", total_2017_18$`2017`) %>% as.numeric(total_2017_18$`2017`)


total_2017_18$`2018` <- gsub("€", "", total_2017_18$`2018`)
total_2017_18$`2018` <- gsub(",", "", total_2017_18$`2018`) 
total_2017_18$`2018` <- gsub(" ", "", total_2017_18$`2018`) %>% as.numeric(total_2017_18$`2018`)
```


Juntandos os dois dataframes...


```r
total_acss <- full_join(total_2017_18,total_2016, by="NIF_Adjudicante")


total_acss <- total_acss %>% select("NIF_Adjudicante","Entidade.y.x","Total Encargos 2016","2017","2018") %>% rename("2016" = "Total Encargos 2016")
```


Temos assim um dataframe para 2016, 2017, e 2018 com o valor oficial para os três anos. Mas como é que estes valores se comparam com os dados recolhidos pela RR?

Usamos o valor "preço_final" como indicador por entendermos que é o mais válido



```r
total_rr <- df %>% 
      mutate(ano = format(`Data de Celebração do Contrato`, "%Y")) %>%
      group_by(ano)

total_rr <- aggregate(preço_final ~ NIF_Adjudicante + ano, total_rr, sum)

total_rr_2016 <- filter(total_rr, ano == "2016")
total_rr_2017 <- filter(total_rr, ano == "2017")
total_rr_2018 <- filter(total_rr, ano == "2018")

total_acss$NIF_Adjudicante <- as.character(total_acss$NIF_Adjudicante)

total_acss <- left_join(total_acss,total_rr_2016, by="NIF_Adjudicante")
total_acss$ano <- NULL
total_acss <- total_acss %>% rename("preço_final_2016" = "preço_final")
total_acss$dif_2016 <- total_acss$preço_final_2016 - total_acss$`2016`


total_acss <- left_join(total_acss,total_rr_2017, by="NIF_Adjudicante")
total_acss$ano <- NULL
total_acss <- total_acss %>% rename("preço_final_2017" = "preço_final")
total_acss$dif_2017 <- total_acss$preço_final_2017 - total_acss$`2017`

total_acss <- left_join(total_acss,total_rr_2018, by="NIF_Adjudicante")
total_acss$ano <- NULL
total_acss <- total_acss %>% rename("preço_final_2018" = "preço_final")
total_acss$dif_2018 <- total_acss$preço_final_2018 - total_acss$`2018`


total_acss <- total_acss %>% select("NIF_Adjudicante","Entidade.y.x","2016","preço_final_2016","dif_2016","2017","preço_final_2017","dif_2017","2018","preço_final_2018","dif_2018")

print(total_acss)
```

```
##    NIF_Adjudicante
## 1        503148776
## 2        501427511
## 3        503122165
## 4        502828790
## 5        503148768
## 6        503148709
## 7        510103448
## 8        507606787
## 9        508085888
## 10       509186998
## 11       501356126
## 12       509822940
## 13       508093937
## 14       506361462
## 15       509822932
## 16       506361470
## 17       507618319
## 18       510123210
## 19       503135593
## 20       600000052
## 21       508080827
## 22       508741823
## 23       508786193
## 24       509932584
## 25       508338476
## 26       506361608
## 27       506361390
## 28       506361527
## 29       508094461
## 30       508318262
## 31       510412009
## 32       509309844
## 33       501626123
## 34       506362299
## 35       508754275
## 36       506361438
## 37       508142156
## 38       508481287
## 39       506361616
## 40       506361659
## 41       510445152
## 42       510745997
## 43       502423943
## 44       506361381
## 45       508752000
## 46       503767336
## 47       508080142
## 48       508878462
## 49       508331471
## 50       509821197
## 51       501510150
## 52       508100496
## 53       503035416
##                                                               Entidade.y.x
## 1          Administração Regional de Saúde de Lisboa e Vale do Tejo, I.P. 
## 2                     Instituto Nacional de Saúde Dr. Ricardo Jorge, I.P. 
## 3                         Administração Regional de Saúde do Centro, I.P. 
## 4                                     Hospital de Magalhães Lemos, E.P.E. 
## 5                      Administração Regional de Saúde do Alentejo, I. P. 
## 6                       Administração Regional de Saúde do Algarve, I. P. 
## 7                  Centro Hospitalar e Universitário de Coimbra, E. P. E. 
## 8                                      Centro Hospitalar de Setúbal E.P.E.
## 9                           Hospital do Espírito Santo de Évora, E. P. E. 
## 10                           Centro Hospitalar Barreiro Montijo, E. P. E. 
## 11                            Instituto Nacional de Emergência Médica, IP 
## 12                              Centro Hospitalar Tondela-Viseu, E. P. E. 
## 13                               Centro Hospitalar do Médio Ave, E. P. E. 
## 14                               Hospital Distrital de Santarém, E. P. E. 
## 15                                  Centro Hospitalar de Leiria, E. P. E. 
## 16                                      Hospital Garcia de Orta, E. P. E. 
## 17                             Centro Hospitalar de Lisboa Ocidental, EPE 
## 18                              Centro Hospitalar do Baixo Vouga, E. P. E 
## 19                          Administração Regional de Saúde do Norte, I.P.
## 20                                Instituto de Oftalmologia Dr. Gama Pinto
## 21                    Hospital da Senhora da Oliveira Guimarães, E. P. E. 
## 22              Centro Hospitalar Póvoa de Varzim - Vila do Conde, E.P.E. 
## 23                           Unidade Local de Saúde do Alto Minho, E.P.E. 
## 24                           Unidade Local de Saúde do Nordeste, E. P. E. 
## 25                                Centro Hospitalar Psiquiatrico de Lisboa
## 26                                Centro Hospitalar do Médio Tejo, E.P.E. 
## 27                         Unidade Local de Saúde de Matosinhos, E. P. E. 
## 28                             Hospital Distrital Figueira da Foz, E.P.E. 
## 29                   Unidade Local de Saúde do Norte Alentejano, E. P. E. 
## 30                          Centro Hospitalar do Tâmega e Sousa, E. P. E. 
## 31                                              Centro Hospitalar do Oeste
## 32                      Unidade Local de Saúde de Castelo Branco, E. P. E.
## 33                 Hospital do Arcebispo João Crisóstomo \u0097 Cantanhede
## 34   Instituto Português de Oncologia do Porto Francisco Gentil, E. P. E. 
## 35                                  Unidade Local Saude Baixo Alentejo EPE
## 36 Instituto Português de Oncologia de Coimbra Francisco Gentil, E. P. E. 
## 37                Centro Hospitalar Vila Nova de Gaia - Espinho, E. P. E. 
## 38              Centro Hospitalar Universitário de Lisboa Norte, E. P. E. 
## 39  Instituto Português de Oncologia de Lisboa Francisco Gentil, E. P. E. 
## 40                                   Centro Hospitalar Cova da Beira, EPE 
## 41                  Unidade Local de Saúde do Litoral Alentejano, E. P. E.
## 42                    Centro Hospitalar Universitário do Algarve, E. P. E.
## 43                                    Instituto Português do Sangue, I.P. 
## 44                                Hospital de Santa Maria Maior, E. P. E. 
## 45                              Unidade Local de Saúde da Guarda, E. P. E.
## 46      Centro de Medicina de Reabilitação da Região Centro - Rovisco Pais
## 47                          Centro Hospitalar de Lisboa Central, E. P. E. 
## 48                      Centro Hospitalar de Entre o Douro e Vouga, E.P.E.
## 49                      Centro Hospitalar Universitário do Porto, E. P. E.
## 50                                     Centro Hospitalar de S. João, E.P.E
## 51                                           Hospital Dr. Francisco Zagalo
## 52                        CENTRO HOSP. DE TRÁS-OS-MONTES E ALTO DOURO, EPE
## 53                    Hospital Professor Doutor Fernando Fonseca, E. P. E.
##       2016 preço_final_2016    dif_2016    2017 preço_final_2017
## 1  3856932       4316596.40   459664.40 2941464       3696032.33
## 2       NA          8900.00          NA   19098               NA
## 3   689052        409147.16  -279904.84  509748        508963.65
## 4    55671               NA          NA   57287               NA
## 5   778171        647844.88  -130326.12  778746          5064.40
## 6   526413        525354.88    -1058.12   40278        383055.98
## 7   713956         64999.58  -648956.42  894192               NA
## 8  3858147        563407.00 -3294740.00 4436020       2569570.08
## 9  1966247       1874491.80   -91755.20 1980556       4221993.30
## 10 2103307       1556734.29  -546572.71 2381016       1361918.87
## 11 2045651               NA          NA 2040475         13064.00
## 12 1243407        567589.13  -675817.87  487621        138922.33
## 13 1445575               NA          NA 1667024          5616.00
## 14 1981373        876330.46 -1105042.54 2247240        177478.81
## 15 4158437       3127538.93 -1030898.07 4042557        567241.95
## 16 3215598       1121619.29 -2093978.71 2962997        425304.11
## 17 1814424       1049077.68  -765346.32 2049206        578540.00
## 18 1987159        129536.80 -1857622.20 2440549               NA
## 19 2358432       1661825.79  -696606.21 2000862       2866224.33
## 20   83007         28048.22   -54958.78   64718         15663.60
## 21 1182751        504560.00  -678191.00 1878849               NA
## 22 1398442               NA          NA 1405232       1725916.00
## 23 2075936        331729.60 -1744206.40 2060456       1034912.28
## 24 2059050               NA          NA 2198573               NA
## 25   85642         51480.61   -34161.39   41929         28761.11
## 26 6211225       6644972.92   433747.92 7363354       4599201.18
## 27 1692733        317040.73 -1375692.27 1735941        353860.65
## 28  490379         97894.80  -392484.20  576522         59283.33
## 29 4462016       4646140.12   184124.12 4496668       5006480.38
## 30 2100195       1656839.82  -443355.18 3167671        887619.25
## 31 5454288        403239.13 -5051048.87 5283945        170764.99
## 32 1105710               NA          NA 1220133               NA
## 33  442987               NA          NA  429394         16500.00
## 34  307309        134794.40  -172514.60  293544        506959.13
## 35 3863080               NA          NA 4521364               NA
## 36  108075        503100.00   395025.00   38806               NA
## 37  572579        183789.08  -388789.92  627900        851646.10
## 38 1400986               NA          NA 1185331               NA
## 39  134532               NA          NA  237655         53280.00
## 40  856547        225591.20  -630955.80  978592        125065.20
## 41 3574447               NA          NA 3706611               NA
## 42 5902496       4601637.19 -1300858.81 5852827       4935646.19
## 43  574768               NA          NA  570290               NA
## 44  787325               NA          NA  793087           900.00
## 45 2369877               NA          NA 2454310               NA
## 46   71032               NA          NA   42848               NA
## 47  449657               NA          NA  719066        670723.79
## 48 3345694        534086.15 -2811607.85 3402060       2229625.73
## 49 1315113               NA          NA 1528187               NA
## 50  508484        278380.80  -230103.20  557527         78534.00
## 51  618587        559255.35   -59331.65  630636        633207.70
## 52 2320998               NA          NA 2680138        371138.18
## 53 5084308       2676211.54 -2408096.46 4576096        374182.50
##       dif_2017    2018 preço_final_2018    dif_2018
## 1    754568.33 2874819       3947263.24  1072444.24
## 2           NA   20427               NA          NA
## 3      -784.35  576798        592660.55    15862.55
## 4           NA   72737               NA          NA
## 5   -773681.60  722791          2360.00  -720431.00
## 6    342777.98  439398               NA          NA
## 7           NA  913233         76867.00  -836366.00
## 8  -1866449.92 5007978       3087751.50 -1920226.50
## 9   2241437.30 1967568       2402121.17   434553.17
## 10 -1019097.13 2751566       1893212.27  -858353.73
## 11 -2027411.00 2123283               NA          NA
## 12  -348698.67  490056               NA          NA
## 13 -1661408.00 1665747        170712.00 -1495035.00
## 14 -2069761.19 2420347         10798.88 -2409548.12
## 15 -3475315.05 3399491        784317.23 -2615173.77
## 16 -2537692.89 3384509        947251.01 -2437257.99
## 17 -1470666.00 1857366        213734.30 -1643631.70
## 18          NA 2689023               NA          NA
## 19   865362.33 1814395        753405.30 -1060989.70
## 20   -49054.40   43137               NA          NA
## 21          NA 1684764               NA          NA
## 22   320684.00 1104309               NA          NA
## 23 -1025543.72 1852521        542150.87 -1310370.13
## 24          NA 2938501               NA          NA
## 25   -13167.89   22822               NA          NA
## 26 -2764152.82 6807076       3704820.55 -3102255.45
## 27 -1382080.35 1634921               NA          NA
## 28  -517238.67  691637               NA          NA
## 29   509812.38 4633810       4884542.00   250732.00
## 30 -2280051.75 4093473       7174332.11  3080859.11
## 31 -5113180.01 5358367               NA          NA
## 32          NA 1437077               NA          NA
## 33  -412894.00  359477               NA          NA
## 34   213415.13  225324        247266.27    21942.27
## 35          NA 4574695               NA          NA
## 36          NA   20605         34944.00    14339.00
## 37   223746.10  744133        881646.10   137513.10
## 38          NA 1164027               NA          NA
## 39  -184375.00  183627        200000.00    16373.00
## 40  -853526.80  910006        277641.90  -632364.10
## 41          NA 3583910               NA          NA
## 42  -917180.81 8519083       5463517.00 -3055566.00
## 43          NA  556978               NA          NA
## 44  -792187.00  828146         24323.00  -803823.00
## 45          NA 2397330               NA          NA
## 46          NA   29321               NA          NA
## 47   -48342.21  695722         15688.57  -680033.43
## 48 -1172434.27 2771407        502625.20 -2268781.80
## 49          NA 1878874         59500.00 -1819374.00
## 50  -478993.00  556179               NA          NA
## 51     2571.70  442714         60503.62  -382210.38
## 52 -2308999.82 2892113        107780.50 -2784332.50
## 53 -4201913.50 3684503        245435.00 -3439068.00
```

```r
export(total_acss,"calculo_gasto_por_hospital.csv","csv")
```

# Taxa de crescimento (2017 para 2018)


```r
total_acss$taxa_crescimeto_2018 <-  (total_acss$`2018`-total_acss$`2017`)/total_acss$`2017`
```


```r
total_acss_2017_2018 <- total_acss
total_acss_2017_2018$dif <- total_acss_2017_2018$`2018`-total_acss_2017_2018$`2017`
total_acss_2017_2018 <- total_acss_2017_2018 %>% arrange(desc(dif))


#table$size1<-ifelse(table$population<500,1,
total_acss_2017_2018$category <- ifelse(total_acss_2017_2018$dif>0,"Hospitais que aumentaram os gastos com tarefeiros em 2018",
                                        ifelse(total_acss_2017_2018$dif<0,"Hospitais que diminuiram os gastos com tarefeiros em 2018",5))

total_acss_2017_2018_up <- total_acss_2017_2018 %>% select("Entidade.y.x","2017","2018","category") %>% 
  filter(category == "Hospitais que aumentaram os gastos com tarefeiros em 2018")
export(total_acss_2017_2018_up, "total_acss_2017_2018_up.csv","csv")

total_acss_2017_2018_down <- total_acss_2017_2018 %>% arrange(dif) %>% 
  select("Entidade.y.x","2017","2018","category") %>% 
  filter(category == "Hospitais que diminuiram os gastos com tarefeiros em 2018")
export(total_acss_2017_2018_down, "total_acss_2017_2018_down.csv","csv")
```


### Diferença entre o BASE e a ACSS (2016-2018)


```r
total_acss2 <- total_acss
total_acss2$preço_final_2016 <- total_acss2$preço_final_2016 %>%  replace_na(0)
total_acss2$preço_final_2017 <- total_acss2$preço_final_2017 %>%  replace_na(0)
total_acss2$preço_final_2018 <- total_acss2$preço_final_2018 %>%  replace_na(0)

  
total_acss2$all_base <- total_acss2$preço_final_2016 + total_acss2$preço_final_2017 + total_acss2$preço_final_2018  

total_acss2$all_acss <- total_acss2$`2016` + total_acss2$`2017`+ total_acss2$`2017`

total_acss2 <- total_acss2 %>% select(Entidade.y.x,all_acss,all_base)

total_acss2$dif <- total_acss2$all_base - total_acss2$all_acss

total_acss_zero <- total_acss2 %>% filter(all_base == 0) %>% arrange(desc(all_acss))
total_acss_zero$category <- "Hospitais que não publicaram nenhum contrato"
export(total_acss_zero, "hospitais_nenhum_contrato.csv","csv")

total_acss3 <- total_acss2 %>%  filter(all_base != 0)

total_acss_menos <- total_acss3 %>% filter(dif < 0) %>% arrange(dif)
total_acss_menos$category <- "Hospitais que publicam menos do que aquilo que reportam à ACSS"
export(total_acss_menos, "hospitais_menos_contrato.csv","csv")

total_acss_mais <- total_acss3 %>% filter(dif > 0) %>% arrange(desc(dif))
total_acss_mais$category <- "Hospitais que publicam mais do que aquilo que reportam à ACSS"
export(total_acss_mais, "hospitais_mais_contrato.csv","csv")
```



### [11] - Que empresas mais dinheiro faturaram



```r
faturado_empresa <- aggregate(preço_final ~ NIF_Adjudicatária, df, sum)

NIFS_empresas <- df %>% select("Entidade(s) Adjudicatária(s)","NIF_Adjudicatária")
```


