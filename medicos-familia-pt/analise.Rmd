---
title: "Quem é o Médico de Família com mais utentes em Portugal?"
author: "Rui Barros (rui.barros@rr.pt), Tiago Palma (tiago.palma@rr.pt)"
date: "Última atualização: `r format(Sys.time(), '%d/%m/%Y')`"
output: 
  renascencaR::TemaRenascenca:
    toc: true
    code_folding: "show"
    number_sections: "false"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Introdução

Nesta breve análise, vamos perceber quem é o médico de família com mais utentes em Portugal. Vamos basear-nos nos dados disponíveis [num dashboard](https://app.powerbi.com/view?r=eyJrIjoiYjU3OTRkODMtNTY1OS00NTQ4LWE2NzEtMjg4ODYyZDQzZTRlIiwidCI6IjIyYzg0NjA4LWYwMWQtNDZjNS04MDI0LTYzY2M5NjJlNWY1MSIsImMiOjh9&fbclid=IwAR3gCIgFojQn0v7uOmCTcxJMKPSEj9eY9AbjS3aVhanN6IFqksAln13s7Tg) disponível no site [**BI da Reforma** do SNS](https://www.sns.gov.pt/sns/reforma-do-sns/cuidados-de-saude-primarios-2/bi-da-reforma/).

Os dados tiveram que ser recolhidos e verificados manualmente porque o Estado Português entende que estes dados devem ser públicos mas não descarregáveis.

Os dados também não constam do Portal de Dados Abertos do SNS nem do dados.gov.

## Questões

- Qual o médico com mais utentes em Portugal e em que centro de saúde/USF trabalha.
- Quantos médicos tem 100% em relação às Unidades Ponderada
- Os médicos mais velhos tendem a ter mais utentes?
- Qual o centro de saúde com a média de utentes por médico mais alta.
- Unidade Familiar com a média de idades mais alta dos seus médicos
- Quantos médicos estão a um ano de ir para a reforma - ou seja, quantos médicos são precisos contratar para o SNS só para manter os atuais utentes com médico de família?

## Importar packages

```{r packages}
##Package para importar outros packages
library(needs)

needs(tidyverse, DT,rio)

```


## Importar dados

Os dados estavam [num dashboard](https://app.powerbi.com/view?r=eyJrIjoiYjU3OTRkODMtNTY1OS00NTQ4LWE2NzEtMjg4ODYyZDQzZTRlIiwidCI6IjIyYzg0NjA4LWYwMWQtNDZjNS04MDI0LTYzY2M5NjJlNWY1MSIsImMiOjh9&fbclid=IwAR3gCIgFojQn0v7uOmCTcxJMKPSEj9eY9AbjS3aVhanN6IFqksAln13s7Tg) feito no Microsoft Power BI.

De acordo com esse mesmo dashboard, os dados são da Base de Dados NCSP da ACSS, mas nenhuma fonte contactada conseguiu fornecer esta base de dados.

Segundo o mesmo dashboard, os dados datam de *20190831*, que assumimos que corresponde a 31/08/2019.

A **Renascença** fez a recolha de dados manuais, tendo guardado esses dados num ficheiro tsv.

Os dados foram confirmados manualmente se forma a assegurar que todos os médicos constavam da base de dados recolhida.

```{r}
medicos_de_familia <- read_delim("dados/dados_processados/medicos_de_familia.tsv", 
    "\t", escape_double = FALSE, locale = locale(decimal_mark = ",", 
        grouping_mark = "."), trim_ws = TRUE)
```

## Limpeza de dados

Os dados foram importados sem qualquer problema. 🎉🎉🎉

## Análise

### Médico de família com mais utentes

```{r}
top_utentes <- medicos_de_familia %>% arrange(-`INSCRITOS`)
DT::datatable(top_utentes, rownames = FALSE, options = list(pageLength = 10, scrollX = TRUE))
```


**Luís Peixoto**, registado na ordem dos médicos com o número 24374, da UCSP Abrantes, é o médico de família em Portugal com mais utentes atribuidoí.


### Médicos 100% ocupados

```{r}
medicos_100 <- medicos_de_familia %>% filter(INSCRITOS == UP)
DT::datatable(medicos_100, rownames = FALSE, options = list(pageLength = 10, scrollX = TRUE))
```

Há 6, mas têm todos um número de utenete baixo.

### Unidade com média de utentes por médico mais alta

```{r}
avrg_unidade <- medicos_de_familia %>% 
    group_by(UF) %>%
    summarise(média_por_UF = mean(INSCRITOS,na.rm = TRUE))

DT::datatable(head(avrg_unidade), rownames = FALSE, options = list(pageLength = 10, scrollX = TRUE))

    
```

A UF Convenção - Canidelo é a que tem a maior média de utentes - 2211.333 utentes utente.


### Idade dos Médicos Vs. N Utentes

```{r}
x <- medicos_de_familia %>% filter(IDADE == "(Vazio)")
length(x$IDADE)

x <- medicos_de_familia %>% filter(IDADE != "(Vazio)")

x$IDADE <- as.numeric(x$IDADE)

ggplot(x, aes(x=IDADE, y=INSCRITOS)) +
  geom_point(shape=23) +
    geom_smooth()
```

Há uma subida no número de utentes até por volta dos 35 anos (início de carreira) e uma descida no final da carreira (a partir dos 64).

É curioso um ligeiro subir a partir dos 61 e que os casos mais extremos de número de utentes por médico se verifique entre os 60 e os 70.

De notar que 46 médicos foram excluídos desta análise por não terem idade registada na base de dados.

### UF com a média de idades mais alta

```{r}
avrg_idade <- x %>% 
    group_by(UF) %>%
    summarise(Idade = mean(IDADE,na.rm = TRUE))

avrg_idade_velhos <- avrg_idade %>% arrange(-Idade)

DT::datatable(head(avrg_idade_velhos), rownames = FALSE, options = list(pageLength = 10, scrollX = TRUE))

```

Há três UF's com uma média de idades superior à da Idade da Reforma em Portugal...
É o caso da UCSP Carvalhosa, da ACES Porto Ocidental, que tem três médicos com 68, 69 e 67 anos.

No polo oposto...

```{r}
avrg_idade_novos <- avrg_idade %>% arrange(Idade)
DT::datatable(avrg_idade_novos, rownames = FALSE, options = list(pageLength = 10, scrollX = TRUE))

```

a UCSP Moscavide é a UF mais jovem de Portugal, com uma média de idades de 29 anos. Isto porque só tem uma médica, PAULA MAIA SILVA.


### Médicos prestes a ir para a reforma

Se considerarmos que a idade da reforma é aos 66 anos...

```{r}
reformados_66 <- x %>% filter(IDADE > 65)
length(reformados_66$IDADE)
```

```{r}
sum(reformados_66$INSCRITOS)
```

Há 302 médicos de família em Portugal que já atingiram a idade da reforma. Significa isto que, caso todos estes médicos decidam pedir a reforma ao mesmo tempo, há **48.4937** portugueses que ficam sem médico de família.

## Conclusões

- Luís Peixoto, registado na ordem dos médicos com o número 24374, da UCSP Abrantes, é o médico de família em Portugal com mais utentes atribuidoí (2887);
- Há seis médicos com 100% de ocupação, mas todos têm um número de utenete baixo.
- A `UF Convenção - Canidelo` é a que tem o valor médio de utentes mais alto - 2211,333 utentes.
- Em relação à idade dos médicos e o número de utentes atribuidoí, há uma subida no número de utentes até por volta dos 35 anos (início de carreira) e uma descida no final da carreira (a partir dos 64).
- Há um ligeiro aumento do número de utentes atribuidoí a partir dos 61.
- Os casos mais extremos no que toca ao número de utentes por médico verificam-se todos entre os 60 e os 70 anos.
- Há três UFs em Portugal cuja a média de idades dos médicos que lá trabalham é superior à da idade da reforma. É o caso da UCSP Carvalhosa, da ACES Porto Ocidental, que tem três médicos com 68, 69 e 67 anos.
- A UCSP Moscavide é a UF mais jovem de Portugal. Tem apenas uma médica com 29 anos (Ana Paula Maia Silva).
- Há 302 médicos de família em Portugal que já atingiram a idade da reforma. Significa isto que, caso todos estes médicos decidam pedir a reforma ao mesmo tempo, há 48.4937 portugueses que ficam sem médico de família.

## Exportar dados pa
r}`
export(medicos_de_familia,"dados/dados_finais/numero_utentes_medico_de_familia.csv","csv")
```



