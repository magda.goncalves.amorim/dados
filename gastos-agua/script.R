library("tidyverse")

encargos2018 <- read_csv("encargos2018_agua.csv", 
                         col_types = cols(encargo_mensal_aa_ = col_number(), 
                                          encargo_mensal_ar_ = col_number(), 
                                          encargo_mensal_ru_ = col_number()), 
                         locale = locale(decimal_mark = ",", grouping_mark = "."))

encargos2017 <- read_csv("encargos2017_agua.csv", 
                         col_types = cols(encargo_mensal_aa = col_number(), 
                                          encargo_mensal_ar = col_number(), 
                                          encargo_mensal_ru = col_number()), 
                         locale = locale(decimal_mark = ",", grouping_mark = "."))

encargos2016 <- read_csv("encargos2016_agua.csv", 
                         col_types = cols(encargo_mensal_aa = col_number(), 
                                          encargo_mensal_ar = col_number(), 
                                          encargo_mensal_ru = col_number()), 
                         locale = locale(decimal_mark = ",", grouping_mark = "."))



encargos2018$gasto <- encargos2018$encargo_mensal_aa_ + encargos2018$encargo_mensal_ar_ + encargos2018$encargo_mensal_ru_

encargos2017$gasto <- encargos2017$encargo_mensal_aa + encargos2017$encargo_mensal_ar + encargos2017$encargo_mensal_ru

encargos2016$gasto <- encargos2016$encargo_mensal_aa + encargos2016$encargo_mensal_ar + encargos2016$encargo_mensal_ru


encargos2018 <- encargos2018 %>% select(distrito, concelho, gasto)

encargos2017 <- encargos2017 %>% select(distrito, concelho, gasto)

encargos2016 <- encargos2016 %>% select(distrito, concelho, gasto)



gastos_agua <- left_join(encargos2018,encargos2017,by=c("distrito","concelho"), suffix =c("_2018","_2017"))

gastos_agua <- left_join(gastos_agua,encargos2016, by=c("distrito","concelho")) %>%
    rename("gasto_2016" = "gasto") %>% 
    arrange(-gasto_2018)

gastos_agua$rank <- NA
gastos_agua$rank <- 1:nrow(gastos_agua)

write.csv(gastos_agua,file="gastos_agua_2016_2018.csv")








