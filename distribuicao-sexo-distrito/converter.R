library(tidyverse)
library(rio)

dados <- read_delim("sexo-municioios.csv", ";", escape_double = FALSE, trim_ws = TRUE)

dados$Total <- as.numeric( gsub(" ","",dados$Total))
dados$Masculino <- as.numeric( gsub(" ","",dados$Masculino))
dados$Feminino <- as.numeric( gsub(" ","",dados$Feminino))

concelhos <- read_csv("concelhos.csv")

dados <- dados %>% filter(`Âmbito Geográfico` == "Município") %>% rename(Concelhos = Anos)

dados <- left_join(dados,concelhos) %>% select(Distrito,Concelhos,Total,Masculino,Feminino)

export(dados,"dados.csv","csv")

dados <- read_delim("dados_limpos.csv",",")

x <- dados %>% group_by(Distrito) %>%  summarise(Total = sum(Total, na.rm = TRUE),
                                                 Masculino = sum(Masculino, na.rm = TRUE),
                                                 Feminino = sum(Feminino, na.rm = TRUE))

x$Masculino_ <- (x$Masculino/x$Total)*100
x$Feminino_ <- (x$Feminino/x$Total)*100

export(x,"dados_final.csv","csv")



