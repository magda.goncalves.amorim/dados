|   |   |
|---|---|
|Fonte | Santuário de Fátima|
| Metodologia| Os dados foram facultados pelo Santuário de Fátima em ficheiros .pdf, que podem ser acedidos em [Original](Original). Recorreu-se ao [Open Refine](http://openrefine.org) para transformar os dados em formato .csv, que depois foram importados para para um documento.xls (disponível [aqui](Extracted))|
| Artigo(s)| [Sul-coreanos, filipinos e indonésios entre os estrangeiros que mais visitam Fátima](http://rr.sapo.pt/noticia/83383/fatima_e_o_mundo_santuario_recebe_cerca_de_970_mil_peregrinos_estrangeiros_em_sete_anos)|