Manifesto eleitoral do MAS - Europeias 2019
Travar os privilégios das elites. Defender os direitos de quem trabalha!
Os maiores 26 multimilionários do mundo detêm tantos recursos quanto 50% da população mundial. Os acordos e tratados europeus não combatem a desigualdade, a corrupção, a especulação financeira, as dívidas públicas ilegítimas ou a “engenharia” fiscal ao serviço das fortunas de banqueiros e grandes empresários. A banca é salva à custa da destruição dos salários, dos direitos laborais e democráticos, dos serviços públicos e das PMEs.
Há alternativas:
Tecto máximo para os rendimentos das administrações dos bancos e grandes empresas! Salário mínimo europeu de 900€!
Taxar as grandes fortunas em 70% para baixar os impostos sobre o trabalho, o consumo e as PMEs!
Fim das ETT! Horário máximo europeu de 35h semanais para atingir o pleno emprego!
Nem mais 1€ público para a banca privada! Renacionalização de todos os setores estratégicos, começando pela banca, para controlar preços e o investimento nacional!
Fim da violência sobre a Mulher, Negros, Imigrantes e LGBTs!
Queremos construir uma sociedade livre da violência sobre as mulheres, os negros e negras, os imigrantes e as LGBTs. Uma sociedade com trabalho de qualidade, salários dignos e garantia de acesso aos serviços públicos para todos e todas. A ingerência da UE e EUA, de Trump, em vários pontos do globo gera instabilidade e guerra, causando a morte e deslocação de milhões de seres humanos e potenciando a acção de organizações terroristas e de extrema-direita.

Há alternativas:
É preciso transformar a política do défice 0% numa política de violência zero! Penas mais duras e efectivas para agressores machistas, racistas e LGBTfóbicos!
Fim da NATO e da Europa Fortaleza! Fim de todos os “campos de detenção”! Ninguém é ilegal, direito irrestrito de asilo!
O capitalismo destrói o planeta. Transição energética Já!
Precisamos de um sistema económico cuja principal preocupação seja a sustentabilidade ambiental. O capitalismo já demonstrou ser incapaz de o fazer, pois a economia mundial é controlada pelas grandes indústrias automóvel, energética, petrolífera e bancos, cujas fortunas estão dependentes do contínuo consumo de combustíveis fósseis.
Há alternativas:
Nacionalização das grandes indústrias energéticas, petrolíferas e automóvel para investir numa total transição energética até 2035!
Criação de milhões de empregos em sectores ambientalmente sustentáveis!
Investimento em transportes colectivos públicos, acessíveis e de qualidade!
Travar a extrema-direita e o neofascismo!
A solução para a crise, a desigualdade, a violência e a crise ambiental não virá da direita, nem da extrema-direita. Trump, Bolsonaro, Le Pen, Salvini ou Órban são inimigos do povo e têm o objectivo de aumentar a desigualdade, a violência e a opressão sobre quem trabalha. A extrema-direita combate-se com unidade das forças políticas, sociais e sindicais que representam os interesses de quem vive do seu trabalho.
Há alternativas:
Unidade internacional contra a extrema-direita e o neofascismo! Apelamos a toda a esquerda europeia, sobretudo aos seus maiores representantes, como Podemos, Syriza, Corbyn, Melenchon, Bloco de Esquerda e PCP, assim como as principais organizações sindicais e restantes movimentos sociais, a convocarem um dia de luta europeu contra a extrema-direita e o neofascimo!
Por uma Europa sem muros nem austeridade!
Uma alternativa séria a esta UE só é possível através da total independência face às poderosas e corruptas elites europeias que nos têm governado. É por isso que a nossa lista é composta por trabalhadoras e trabalhadores, mulheres e homens, negras, negros e LGBTs. É por isso que a nossa campanha é exclusivamente financiada pelos nossos militantes e apoiantes e é alimentada da confiança que depositamos nas lutas por dignas condições de vida e de trabalho. Nem a UE de Merkel, Macron e Costa, nem a UE da extrema-direita! Faz falta uma esquerda anti-capitalista no Parlamento Europeu. O MAS pretende ser essa alternativa à esquerda.

