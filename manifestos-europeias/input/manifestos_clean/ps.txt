MANIFESTO ELEITORAL

Partido Socialista – Eleições Europeias 2019

Um novo Contrato Social para a Europa

Nestas eleições europeias, de 26 de maio de 2019, os cidadãos europeus e os portugueses têm nas mãos o futuro da Europa. E é importante que participem, porque o seu voto decide – e cada voto conta.

A Europa que somos está hoje sob sério ataque dos movimentos populistas, sobretudo da extrema-direita nacionalista e xenófoba. Um pouco por toda a parte, esses movimentos agitam as bandeiras do nacionalismo, exploram toda a espécie de descontentamentos, promovem notícias falsas nas redes sociais e fomentam a insegurança e o medo. Para cada problema complexo, prometem uma “solução” simplista e, à falta de melhor, tentam vender a ilusão de um regresso redentor às fronteiras nacionais.

Pela nossa parte, não temos dúvidas: os grandes problemas do nosso tempo exigem mais cooperação entre os países europeus, não menos. E testemunhamos, sem qualquer hesitação, que a integração de Portugal na União Europeia fez a diferença na qualidade de vida dos portugueses, transformou profundamente o nosso País e significou um salto enorme em termos de progresso e desenvolvimento, mas também em termos de abertura, liberdade de circulação e criação de oportunidades para todos, sobretudo os mais jovens. A tal ponto que uma coisa é certa: não queremos andar para trás! Porque defender a Europa é defender Portugal!

Por isso, agora que a Europa que ajudámos a construir está sob violento ataque, o Partido Socialista, fiel como sempre ao projeto europeu, sabe bem de que lado deve estar. E sabe que é sua responsabilidade mobilizar os portugueses e as portuguesas para, nestas eleições europeias, fazer o que precisa de ser feito: defender a Europa. É isso, antes de mais nada, o que significa votar no PS: cada voto no PS é um voto em defesa da Europa e do projeto europeu. Cada voto no PS é um voto na defesa dos interesses dos portugueses na Europa.

Contudo, dizemos também, com clareza, que a Europa precisa de mudar e de fazer bastante mais e melhor para corresponder à legítima expetativa dos seus cidadãos – é essa, aliás, a melhor resposta ao populismo eurocético e é esse o nosso combate de todos os dias, como socialistas comprometidos com a justiça social e com o projeto europeu. Só uma Europa mais atenta à pobreza e às desigualdades, só uma Europa capaz de avançar para novas políticas que melhorem a vida concreta dos seus cidadãos, das famílias e das empresas pode estar à altura dos enormes desafios que tem pela frente. Pelo contrário, a atitude conformista, de permanente bloqueio a todas as mudanças, é a pior resposta que o projeto europeu poderia dar aos problemas do presente e do futuro. Acontece que a plataforma política que a Direita representa em Portugal, independentemente dos emblemas partidários, é exatamente a mesma que na União Europeia insiste na austeridade, aposta tudo na “força máxima” das sanções e promove uma constante resistência às mudanças necessárias.

É aqui, portanto, que reside a escolha política fundamental que os portugueses têm diante de si nestas eleições europeias: de um lado, a posição conformista da Direita; do outro, a alternativa progressista do PS.

É com a nossa atitude, de sempre, de fidelidade ao projeto europeu, mas também de defesa intransigente dos interesses de Portugal e dos portugueses, que o Partido Socialista, contra os adversários da União Europeia, mas também contra todas as subserviências e todos os conformismos, apresenta a sua alternativa progressista para o futuro da Europa.

UM NOVO CONTRATO SOCIAL, PARA UMA EUROPA À ALTURA DOS DESAFIOS DO PRESENTE E DO FUTURO

A União Europeia, que constitui o mais promissor projeto de integração política e económica de toda a história contemporânea, tornou possível que, ao longo de seis décadas, a Europa se mantivesse como um espaço de paz, de estabilidade e de prosperidade, proporcionando aos seus cidadãos condições de vida e direitos sociais sem paralelo no mundo desenvolvido.

Todavia, o desenvolvimento económico da União Europeia não chegou de forma equitativa a todos os Estados, regiões e populações, persistindo elevados níveis de desigualdade e bolsas intoleráveis de pobreza. Ainda não se dissiparam totalmente os efeitos da crise financeira e da Grande Recessão, a economia europeia enfrenta importantes novos desafios e as alterações climáticas exigem uma renovada determinação.  

Por tudo isto, propomos um Novo Contrato Social para a Europa. Um Contrato Social que se traduza numa Nova Agenda Social, numa Nova Agenda de Crescimento e Emprego e numa Nova Agenda de Inovação e Sustentabilidade.

UMA NOVA AGENDA SOCIAL

O primeiro eixo de um Novo Contrato Social para a Europa deve ser uma Nova Agenda Social de combate à pobreza e às desigualdades, que promova a coesão e a convergência, que valorize a educação e a formação profissional como instrumentos decisivos de mobilidade social, que defenda os direitos dos trabalhadores e a conciliação entre a vida pessoal, profissional e familiar, que combata todas as formas de discriminação, que defenda o modelo social europeu e que garanta o acesso à saúde, à habitação e aos serviços públicos.

Para isso, defendemos:

– Um Quadro Financeiro Plurianual que valorize as políticas de coesão, a política agrícola comum e de pescas, simplificando e tornando mais acessíveis a cidadãos e empresas estas políticas, e tendo em consideração as condições específicas das regiões ultraperiféricas; uma mais justa distribuição geográfica dos investimentos do Plano Juncker e uma nova capacidade orçamental da zona Euro ao serviço não apenas da competitividade, mas também da convergência;

– O reforço e a plena operacionalização do Pilar Europeu dos Direitos Sociais, de modo a combater as desigualdades e a assegurar o futuro do trabalho e da proteção social;

– O combate ao trabalho precário e a garantia legal de um contrato de trabalho digno para todos, que proporcione, independentemente dos setores económicos e dos vínculos laborais, uma remuneração justa, bem como acesso à proteção social e à formação ao longo da vida;

– A promoção de uma efetiva igualdade de género, tanto salarial como em todos os aspetos da vida em sociedade, e o combate à violência doméstica e a todas as formas de discriminação; o desenvolvimento de um novo quadro para a conciliação entre a vida pessoal, profissional e familiar;

– O enfrentar do desafio demográfico, assegurando a sustentabilidade da sociedade europeia e promovendo a participação no mercado de trabalho e o envelhecimento ativo, bem como a sustentabilidade dos sistemas de proteção social;

– A promoção de uma verdadeira igualdade de oportunidades no acesso à educação e à formação;

– O reforço da proteção dos mais vulneráveis, como os jovens ou os mais idosos, através de mecanismos europeus de promoção da educação, da saúde e de combate à pobreza;

– O reforço do programa europeu Garantia Jovem e a criação de um programa Garantia Criança, para combater a pobreza infantil e garantir o acesso ao pré-escolar e aos serviços sociais e de saúde na primeira infância;

– A criação de um Plano Europeu de Políticas de Habitação, que promova o direito à habitação em condições dignas e a preços acessíveis, que combata a exclusão social nas cidades europeias, recorrendo, nomeadamente, aos fundos estruturais como instrumento para a concretização deste Plano;

– A promoção da justiça fiscal à escala europeia, combatendo a evasão fiscal e a concorrência desleal com as PME, assegurando a tributação dos movimentos de capitais, das transações financeiras e da economia digital, e desenvolvendo incentivos fiscais para a criação de emprego, a inovação e o desenvolvimento sustentável.

UMA NOVA AGENDA PARA O CRESCIMENTO E O EMPREGO

A experiência do Governo do Partido Socialista em Portugal provou que é possível conciliar o “virar da página da austeridade” com uma gestão responsável das contas públicas. Ficou demonstrado que a redução progressiva do défice e da dívida pública pode ser feita com melhoria do nível de rendimento dos trabalhadores, dos pensionistas e das famílias, gerando crescimento económico e promovendo a criação de emprego. Impõe-se que, tal como sucedeu em Portugal, a política económica e orçamental da União Europeia se torne mais amiga do crescimento e do emprego.

Para isso, defendemos:

– Uma política orçamental europeia mais favorável ao crescimento, que utilize plenamente os instrumentos de flexibilidade previstos no Pacto de Estabilidade e Crescimento e que promova uma estratégia mais expansionista por parte dos países europeus com excedentes orçamentais;

– A eliminação do défice de investimento que ainda persiste na economia europeia e o reforço dos instrumentos financeiros europeus de apoio ao investimento, incluindo o Quadro Financeiro Plurianual e o Plano Juncker, para o que propomos um Plano de Investimento para a Europa;

– O completar da União Económica e Monetária e da União Bancária, designadamente através: da criação de uma capacidade orçamental própria para a Zona Euro, que apoie as reformas favoráveis à competitividade e à convergência e assegure a função de estabilização das economias face a eventuais choques; da criação neste contexto, de novos mecanismos de apoio às reformas definidas nos Programas Nacionais de Reformas dos Estados-Membros, designadamente as que se traduzam em compromissos a favor da convergência, da educação, do desenvolvimento das indústrias criativas e do património cultural, da ciência, do desenvolvimento tecnológico e da inovação; da reforma do Mecanismo europeu de Estabilidade; da operacionalização do Fundo Único de Resolução bancária e da criação do Esquema Europeu de Garantia de Depósitos.

– O aprofundamento da União de Mercados de Capitais.

UMA NOVA AGENDA PARA A INOVAÇÃO E A SUSTENTABILIDADE

O Novo Contrato Social de que a Europa precisa deve envolver, também, um renovado compromisso e uma reforçada ambição em termos de inovação e sustentabilidade para ganhar os desafios do futuro.

Por isso, defendemos:

– Uma grande aposta dos investimentos europeus na investigação e desenvolvimento, na modernização tecnológica e na qualificação dos recursos humanos, incluindo por via do reforço dos programas de Ciência, Tecnologia e Inovação e do Erasmus+;

– Colocar a inovação como eixo fundamental da política industrial europeia;

– Desenvolver uma estratégia europeia específica para a revolução digital;

– Desenvolver uma nova estratégia europeia de crescimento e desenvolvimento sustentável, assente numa utilização sustentável dos recursos, desde logo os recursos marinhos, novos padrões de consumo e de produção de energia, novas tecnologias energéticas e ambientais e uma grande aposta na economia circular e na reciclagem dos resíduos;

– Implementar o Fundo Europeu de Transição Ambiental, como instrumento de apoio ao tecido empresarial europeu no sentido da descarbonização e da promoção da sustentabilidade ambiental, com especial incidência nas PME e nos territórios carecidos de novos impulsos para a convergência;

– Desenvolver medidas de política ambiciosas para fazer face às alterações climáticas, honrando os compromissos assumidos no Acordo de Paris, designadamente cumprindo o objetivo de redução de CO2, incentivando a descarbonização da economia, promovendo a transição energética e a mobilidade sustentável, assegurando a proteção da biodiversidade e investindo na educação ambiental, na investigação e desenvolvimento e na inovação a favor da sustentabilidade; é, ainda, essencial acelerar o caminho para a União Energética, acelerando o desenvolvimento das interconexões entre os diferentes Estados-Membros;

– Reforçar a capacidade de resposta europeia às catástrofes naturais e ambientais, incluindo por via de um verdadeiro sistema europeu de proteção civil.

POR UMA EUROPA DE VALORES, DEMOCRÁTICA, TOLERANTE E SOLIDÁRIA

É urgente que o projeto Europeu se reencontre de novo com os seus valores, para que a União Europeia seja o que ficou inscrito no seu desígnio fundador: um espaço de respeito integral da dignidade da pessoa humana, dos direitos fundamentais e da democracia; uma terra de liberdade, de tolerância e de solidariedade, e de defesa intransigente do Estado de Direito.

A União Europeia não pode ser complacente com derivas autoritárias, nem com atentados à liberdade de expressão ou à independência do poder judicial que ameaçam de forma sistémica o funcionamento do Estado de Direito.

Por outro lado, é preciso também qualificar o funcionamento da democracia ao nível europeu, fazendo uma verdadeira pedagogia da cidadania europeia, valorizando as eleições para o Parlamento Europeu, reforçando os poderes políticos e legislativos da única instituição europeia diretamente eleita pelos cidadãos e estimulando a participação cívica no debate e nos processos de tomada de decisão sobre questões europeias. Paralelamente, torna-se necessário salvaguardar a integridade democrática do processo eleitoral europeu combatendo a manipulação do debate democrático através de notícias falsas e outras formas abusivas de condicionamento da opinião pública.

A União Europeia não pode escusar-se ao seu dever humanitário indeclinável de acolhimento dos refugiados, nem à solidariedade devida para com os países da sua fronteira marítima a Sul que enfrentam uma situação verdadeiramente dramática no Mediterrâneo. Defendemos uma resposta solidária da União Europeia à chamada crise dos refugiados, à altura das obrigações humanitárias previstas nas convenções internacionais, que dite a partilha dos esforços com respeito pela capacidade de cada Estado-Membro e que seja enquadrada pela necessária revisão do sistema europeu de asilo.

Preconizamos, por outro lado, uma política integrada para as migrações, que comece por atacar as causas fundamentais dos fenómenos migratórios por via de cooperação para o desenvolvimento e para a segurança nos países de origem, que promova a segurança nas fronteiras externas da União Europeia e o combate ao tráfico de seres humanos e que assegure vias legais para uma gestão controlada das migrações, acompanhada de um investimento sério na integração social dos imigrantes e no combate ao racismo, à xenofobia e a todas as formas de discriminação.

No desenvolvimento desta política integrada, atribuímos uma especial importância estratégica à cooperação com África, não apenas através dos instrumentos europeus, mas também no quadro de uma parceria reforçada com as Nações Unidas, em particular o seu Alto Comissariado para os Refugiados e a Organização Internacional para as Migrações. Defendemos, igualmente, o Pacto Global das Migrações, recentemente adotado pela ONU – que Portugal ajudou a negociar e se empenhará em concretizar. E lutaremos para que a União Europeia se mantenha como uma referência à escala global na Cooperação para o Desenvolvimento, honrando os compromissos com os Objetivos do Desenvolvimento Sustentável.

POR UMA EUROPA FORTE, PARA UM MUNDO DE PAZ E UMA GLOBALIZAÇÃO MAIS JUSTA

Num mundo cada vez mais instável e com uma ordem internacional em profunda mutação, mais do que nunca a UE deve assumir o seu papel de referência global em favor da paz, da estabilidade, dos direitos humanos, da democracia, do respeito pelo direito internacional e da valorização do multilateralismo.

A verdade é que todos os grandes problemas do nosso tempo – da globalização ao terrorismo, das migrações às alterações climáticas, dos paraísos fiscais ao desenvolvimento sustentável – exigem mais e melhor cooperação, não mais isolacionismo. É na concertação internacional que podemos encontrar soluções para uma globalização mais justa e um desenvolvimento global mais sustentável e inclusivo, que cumpra os objetivos da Agenda 2030.

Contra as tentações protecionistas, valorizamos, também, o contributo dos acordos comerciais bilaterais e regionais para uma globalização mais regulada e mais justa. Apoiamos, por isso, a agenda comercial que a União Europeia tem vindo a desenvolver e exigimos que garanta a integral salvaguarda dos nossos valores em matéria de direitos sociais, laborais e ambientais, bem como de segurança alimentar e defesa do consumidor.

Sabemos, também, que novos e importantes desafios se colocam à União Europeia em matéria de segurança, de defesa e de combate ao terrorismo. Defendemos, por isso, antes de mais, uma política externa da União orientada para a estabilização e resolução dos conflitos armados na vizinhança europeia, porque a paz e a estabilidade na nossa vizinhança são condições de paz e estabilidade no próprio território europeu. Preconizamos, também, um reforço da cooperação entre os Estados e as autoridades policiais e judiciais no combate ao terrorismo e à criminalidade organizada, incluindo em matéria de tráfico de seres humanos e de gestão das fronteiras e do Espaço Schengen. E defendemos uma política de segurança e defesa europeia, com investimento acrescido e reforço da cooperação entre Estados-Membros, em consonância e complementaridade com a Aliança Atlântica.

RENOVAR A EUROPA, PARA UMA PROSPERIDADE PARTILHADA

Manter tudo como está não é opção. A alternativa progressista que o Partido Socialista propõe visa renovar a Europa e reunificá-la em torno dos seus fundamentos: os valores europeus. Mas visa também renovar as políticas e os instrumentos de ação para vencer os desafios do presente e do futuro. Por isso, propomos um Novo Contrato Social assente numa Agenda Social, numa Agenda de Crescimento e Emprego e numa Agenda para a Inovação e a Sustentabilidade. E por isso nos batemos, igualmente, por uma Europa forte, para um Mundo de paz e uma globalização mais justa.

A Europa que queremos não se resigna às desigualdades, antes ambiciona uma prosperidade partilhada. A alternativa política que o Partido Socialista provou ser possível em Portugal tem de ser possível também à escala europeia: mais rendimentos das famílias e mais investimento, para mais crescimento e mais emprego; menos pobreza e menos desigualdades, para mais justiça social e mais convergência; e contas certas. É este o caminho de que a Europa precisa, é este o nosso compromisso.

No dia 26 de maio os portugueses são chamados a participar na escolha do futuro da Europa e, ao fazê-lo, escolhem também o seu próprio futuro. Votar no PS nestas eleições europeias significa votar em defesa do projeto europeu, mas significa também votar na mudança de que a União Europeia precisa e na defesa dos interesses de Portugal.

O voto no Partido Socialista é o voto certo – porque Somos Europa.
