---
title: "R Notebook"
output: html_notebook
---


```{r}
needs(tidyverse, rio,jsonlite, data.table, RCurl)
```

# Import freguesias

```{r}
freguesias <- import("input/freguesias.csv")
```


```{r}
for (i in 1:nrow(freguesias)) {
    
    print(freguesias[i,4])
    
    json_file <- paste0("https://www.eleicoes.mai.gov.pt/legislativas2015/static-data/territory-results/TERRITORY-RESULTS-LOCAL-",freguesias[i,5],"-AR.json")

    data <- fromJSON(json_file)
    
    x <- data[["currentResults"]][["resultsParty"]]
    x$data[["territoryFullName"]] <- data[["territoryFullName"]]
    x <- x %>% rename( "freguesia" = "data")

    resultados <- x %>% select("freguesia", "acronym","validVotesPercentage","votes")
    resultados$dicofre <- freguesias[i,5]

    info <- tibble(data[["territoryFullName"]],data[["currentResults"]][["blankVotes"]],data[["currentResults"]][["nullVotes"]],data[["currentResults"]][["numberVoters"]],data[["currentResults"]][["subscribedVoters"]],data[["currentResults"]][["percentageVoters"]])
    info <-  info %>% rename("feguesia" = "data[[\"territoryFullName\"]]", "brancos" = "data[[\"currentResults\"]][[\"blankVotes\"]]", "nulos" = "data[[\"currentResults\"]][[\"nullVotes\"]]", "n_vontantes" = "data[[\"currentResults\"]][[\"numberVoters\"]]", "votos_registados" = "data[[\"currentResults\"]][[\"subscribedVoters\"]]", "per_votos" = "data[[\"currentResults\"]][[\"percentageVoters\"]]")
    info$abstencao <- 100 - info$per_votos
    info$dicofre <- freguesias[i,5]
    
    export(resultados,paste0("output/resultados/",freguesias[i,4],freguesias[i,5],".csv"),"csv")
    export(info,paste0("output/info/",freguesias[i,4],freguesias[i,5],".csv"),"csv")


}


```


```{r}

files <- list.files(path="output/resultados/",pattern="*.csv")

db_resultados<- lapply(paste0("output/resultados/",files), import)

x <- rbindlist(db_resultados)

export(x, "resultados_legislativas_2015_freguesias.csv", "csv")
```


```{r}

files <- list.files(path="output/info/",pattern="*.csv")

db_info<- lapply(paste0("output/info/",files), import)

x <- rbindlist(db_info)

export(x, "info_legislativas_2015_freguesias.csv", "csv")

```

