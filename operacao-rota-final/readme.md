|   |   |
|---|---|
|Fonte | Base.gov|
| Metodologia| Recorrendo aos NIF's das entidades envolvidas, procedeu-se a uma recolha dos contratos entre empresas do universo Trandev e autarquias investigadas.
| Análise | Todo o código da análise pode ser consultado [aqui](Untitled.Rmd)
| Artigo(s)|[Empresas do grupo Transdev faturaram 28 milhões em negócios com as autarquias da "Operação Rota Final"](https://rr.sapo.pt/2019/06/12/pais/empresas-do-grupo-transdev-faturaram-28-milhoes-em-negocios-com-as-autarquias-da-operacao-rota-final/noticia/154547/)|