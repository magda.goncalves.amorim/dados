---
title: "R Notebook"
output: html_notebook
---


```{r}
library(needs)
needs("tidyverse","rio","data.table")
```

#Nifs todas as câmaras

```{r}
lista_camaras <- import("lista_camaras_nifs.csv")
```


Recolha de todas as câmaras


```{r} 
for (i in 1:nrow(lista_camaras)) {

    print(paste("getting ", i ,"---", lista_camaras[i,1]))

    f<- paste0("http://www.base.gov.pt/base2/rest/contratos.csv?texto=&tipo=0&tipocontrato=0&cpv=&numeroanuncio=&aqinfo=&adjudicante=",lista_camaras[i,2],"&adjudicataria=&desdeprecocontrato_false=&desdeprecocontrato=&ateprecocontrato_false=&ateprecocontrato=&desdedatacontrato=&atedatacontrato=&desdedatapublicacao=&atedatapublicacao=&desdeprazoexecucao=&ateprazoexecucao=&desdedatafecho=&atedatafecho=&desdeprecoefectivo_false=&desdeprecoefectivo=&ateprecoefectivo_false=&ateprecoefectivo=&pais=0&distrito=0&concelho=0&sort(-publicationDate)")

    t <- import(f)


    export(t,paste0("output/bd_contratos_",lista_camaras[i,2],"__",".csv"),"csv")

}

```

# Export all

```{r}   
files <- list.files(path="output/",pattern="*.csv")
db_contratos_new <- lapply(paste0("output/",files), import)

#x <- bind_rows(db_contratos)
x_new <- rbindlist(db_contratos_new)

export(x_new, "output/contratos_camaras.csv", "csv")
```

# Import all

```{r}

df_main <- import("output/contratos_camaras.csv")

```


# Grupo Transdev

De acordo com um documento da Autoridade da Concorrência, o grupo Transdev é propretário das seguintes empresas:



```{r}
x <- filter(df_main, grepl('500038473|500292531|500158029|500087164|502526483|502550414|502594381|500252173|500148775|506631583|500036365|500038465|500343403|500148775|500367523|500252173', `Entidade(s) Adjudicatária(s)`))
```

```{r}
export(x,"Total_Transdev.csv","csv")
```













