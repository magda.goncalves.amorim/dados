---
title: "Portugal 2020"
author: "Rui Barros (rui.barros@rr.pt)"
date: "`r format(Sys.Date())`"
output:
   html_document:
     keep_md: TRUE
     code_folding: show
     echo: TRUE
     warning: FALSE
     message: FALSE
     theme: lumen
     df_print: kable
     toc: yes
     toc_depth: 4
     toc_float: 
       collapsed: false
       smooth_scroll: false
subtitle: Análise aos dados por geolocalização
---

# Introdução

Análise aos dados do Portugal 2020 para verificar que Concelhos e/ou Freguesias nunca receberam fundos comunitários no quadro do Portugal 2020.

```{r}
#install.packages("needs")
library(needs)
needs(readxl,tidyr,dplyr,ggplot2,rio,readr,stringr,rio)
```


# Questões orientadoras

1 - Quantos Concelhos nunca receberam dados do Portugal 2020.
2 - Quanto recebeu cada um dos Concelhos (?)
3 - Quantas Freguesias nunca receberam dados do Portuagl 2020.


# Importação de dados do Portugal 2020

```{r}
df_port2020 <- import("input/PT2020-Lista_Operacoes-31122018.xlsx")

```

# Limpeza de dados do Portugal 2020 - Municípios

```{r}
df_clean <- df_port2020 %>% filter(`Concelho | Municipality` != "-")

x <- separate_rows(df_clean,`Concelho | Municipality`, sep =";")
x <- x %>% filter(`Concelho | Municipality` != "")

x$`Concelho | Municipality` <- str_trim(x$`Concelho | Municipality`, side = c("left"))

x <- rename(x, "concelho" = "Concelho | Municipality")

```


# Importação de Lista de Concelhos

```{r}
df_concelhos <- import("input/concelhos-metadata.xlsx")
df_concelhos<- df_concelhos %>% select("codigo","designacao")
df_concelhos <- rename(df_concelhos, "concelho" = "designacao")
```

# Join de bds

```{r}
sem_fundos <- anti_join(df_concelhos, x, by="concelho")
```

## Não há nenhum concelho que nunca tenha recebido fundos do Portugal 2020


# Limpeza de dados do Portugal 2020 - Freguesias

```{r}
df_clean <- df_port2020 %>% filter(`Freguesia | Parrish` != "-")

x <- separate_rows(df_clean,`Freguesia | Parrish`, sep =";")
x <- x %>% filter(`Freguesia | Parrish` != "")
x$`Freguesia | Parrish` <- str_trim(x$`Freguesia | Parrish`, side = c("left"))

x <- rename(x, "freguesia" = "Freguesia | Parrish")

```

# Df de limpeza

```{r}
df_limpeza <- x %>% filter(grepl("; ",`Concelho | Municipality`))

x <- anti_join(x,df_limpeza)

export(df_limpeza,"output/df_limpeza.csv","csv")

```

#Import e join de dados limpos

```{r}
t <- import("input/df_limpo.csv")
x <- bind_rows(x,t)

```

#Limpeza de dados

```{r}
x$`Concelho | Municipality` <- gsub(";","",x$`Concelho | Municipality`)
x <- x %>% rename("concelho" = "Concelho | Municipality")

```

# Import de dados das freguesias

```{r}
freguesias <- import("input/freguesias-metadata.xlsx")
```


#Join por freguesia

```{r}
freguesias_nao <- anti_join(freguesias,x, by=c("concelho","freguesia"))
```



# Qual o concelho com mais freguesias que não receberam

```{r}
t <- freguesias_nao  %>%   
      group_by(concelho) %>% 
      tally()
```

## Racio freguesias

```{r}
count_freg <- freguesias  %>%   
      group_by(concelho) %>% 
      tally()
```

## join e %

```{r}
per_freg <- left_join()

```



# Qual o Concelho com mais e menos projectos

```{r}
t <- x  %>%   
      group_by(concelho) %>% 
      tally()
```


#só Funchal
```{r}
corvo <- x %>% 
      filter(concelho == "Corvo")


```


# soma

```{r}
colnames(x)[12] <- "total_financiado"

total <- x  %>%   
      group_by(concelho) %>% 
      summarize(total = sum(total_financiado, na.rm = TRUE))


```

#porto

```{r}
porto <- x %>% 
   filter(concelho == "Porto")
         
```


# Por população

```{r}
total_pop <- import("input/pordata_populacao_concelho.csv")
total_pop$pop <- gsub(" ","",total_pop$pop)
total_pop$pop <- as.numeric(as.character(total_pop$pop))
total_pop <- total_pop  %>% filter(!is.na(pop))

pop_dinheiro <- left_join(total,total_pop,by="concelho")

pop_dinheiro$div <- pop_dinheiro$total / pop_dinheiro$pop


```



# Export Corvo

```{r}
export(corvo,"corvo.csv","csv")

flores <- x %>% 
      filter(concelho == "Lajes das Flores" | concelho == "Santa Cruz das Flores")
export(flores,"flores.csv","csv")

```




```{r}
export(total,"total_dinheiro.csv","csv")
export(t,"n_projectos_aprovados.csv","csv")
export(pop_dinheiro,"pop_dinheiro.csv","csv")


```






# Limpeza de dados

Esta secção é opcional e deve ser usada somente se for necessário fazer limpeza de dados dentro do próprio RStudio.

As alterações de limpeza devem ser feitas na seguinte estrutura:


[Problema]
Identifica-se onde está o problema com estes dados

[Solução pensada]
Descrição da solução pensada e execução do código em R.


O output da limpeza de dados deve ser guardado em [dados/limpos](dados/limpos).


# Análise

A análise é provavelmente a parte que vai tirar mais tempo em todo o trabalho e é onde se chega as principais conclusões.

Para tal, a lógica deve sempre passar pelo uso das **Questões orientadoras** definidas em cima para orientar a análise de dados.

Convencionou-se que se usam subtítulos (###) para colocar as questões.  Assim, o trabalho deve ser orientado na seguinte estrutura:


### Pergunta
```{r}
#código em R para tentar responder à pergunta
```

Resposta.



As respostas deverão, tendencialmente, conduzir a conclusões, que devem ser plasmadas no final do documento.

As análises devem conduzir a novas perguntas, pelo que, caso surjam, deve ser acrescentadas no tópico **Questões orientadoras**.



# Conclusões

Deve plasmar, de forma sucinta, as conclusões obtidas com a análise de dados. Preferencialmente optamos pelo uso de lista não ordenada:

- conclusão 1
- conclusão 2
- conclusão 3

Estas conclusões não devem ser muito longas. Caso seja necessário explicar algo, isso deve ser feito no período anterior.


# Exportação final

Muitas vezes, depois de terminada uma análise de dados, passamos para o processo de desenvolvimento das visualizações de dados com recurso a  d3js. 

Como às vezes estas infografias interativas exigem que os dados sejam formatados de determinada forma, por isso deve-se fazer uma exportação final dos dados sem colunas desnecessárias para [dados/originais](dados/originais) e, caso se justifique, uma exportação para servir essas infografias.








