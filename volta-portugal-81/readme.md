|   |   |
|---|---|
|Fonte |Site da Volta a Portugal|
| Metodologia| Os dados recolhidos em [formato .gpx do site da Volta a Portugal](etapas_gpx) e posteriormente convertidos em [formato json](etapas_json), tendo informação adicional que constava dos ficheiros originais sido eliminada.
| Análise | Os ficheiros geojson foram colocados num mapa.
| Artigo(s)|[Veja se a Volta passa à sua porta. As etapas da prova](https://rr.sapo.pt/2019/07/29/modalidades/veja-se-a-volta-passa-a-sua-porta-as-etapas-da-prova/multimedia/159055/)|