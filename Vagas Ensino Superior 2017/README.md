|   |   |
|---|---|
|Fonte | Direção Geral do Ensino Superior|
| Metodologia| Recorrendo aos dados da Direção Gerla do Ensino Superior, que contemplava já os cursos, instituição, área, número de vagas e nota do último colocado em 2016, a [**Renascença**](rr.sapo.pt) procedeu à limpeza de dados e recorreu-se ao [Open Refine](http://openrefine.org) para transformar os dados e servi-los na newsapp.|
| Artigo(s)| [Interactivo. Consulte as vagas e médias de acesso ao ensino superior](http://rr.sapo.pt/extralarge/89064/interactivo_consulte_as_vagas_e_medias_de_acesso_ao_ensino_superior) <br> [Há mais 150 vagas nas universidades e politécnicos portugueses](http://rr.sapo.pt/el/89060/ha_mais_150_vagas_nas_universidades_e_politecnicos_portugueses)|